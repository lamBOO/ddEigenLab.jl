<img src="media/logo.png" width="300px" />

# ddEigenLab.jl: Domain-Decomposition Eigenvalue Problem Lab

This package provides a lab for Eigenvalue Problems to test:
- Finite Difference Discretizations
- Finite Element Discretizations using `Gridap.jl`
- Optimally preconditioned eigenvalue solvers (IP, SD, LOPCG, ...)
- Linear Periodic Schrödinger Equation
- Domain-decomposition (not yet implemented)

## Installation

Clone repo
```bash
git clone git@git.rwth-aachen.de:lamBOO/ddEigenLab.jl.git
cd ddEigenLab.jl
```
Start julia
```bash
julia
```
Or use Docker
```bash
# BEFORE: Install Docker and Docker-Compose and start daemon
docker-compose run ddeigenlab bash
julia  # Start Julia
```

## Run Examples
Start `Julia` session enable package
```
] dev .
```
Then run some examples with
```
; cd "examples/fd"
include("<example_name>.jl")
```
