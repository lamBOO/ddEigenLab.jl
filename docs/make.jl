push!(LOAD_PATH,"../src/")
push!(LOAD_PATH, "..")

using
  Test,
  Documenter,
  Literate,
  ddEigenLab

const EXAMPLES_DIR = joinpath(@__DIR__, "..", "examples")
const OUTPUT_DIR   = joinpath(@__DIR__, "src/generated")

examples = [
  # "fd/laplace-potential.jl",
]

for example in examples
  # TODO Binder does only work for public repos!
  example_filepath = joinpath(EXAMPLES_DIR, example)
  Literate.markdown(example_filepath, OUTPUT_DIR, documenter=true)
  Literate.notebook(example_filepath, OUTPUT_DIR, documenter=true)
  Literate.script(example_filepath, OUTPUT_DIR, documenter=true)
end

DocMeta.setdocmeta!(
  ddEigenLab,
  :DocTestSetup,
  :(using ddEigenLab);
  recursive=true
)
doctest(ddEigenLab) # doctest doesnt work, do it manually
makedocs(
  # format = Documenter.LaTeX(platform = "docker", version="v0.3"),
  # format = Documenter.LaTeX(platform = "none"),  # only gen. source
  sitename="ddEigenLab.jl",
  authors="Lambert Theisen",
  doctest = false,
  repo = Documenter.Remotes.GitLab("git.rwth-aachen.de", "lamBOO", "ddEigenLab.jl"),
  pages = Any[  # sidebar links
    "Home"    => "index.md",
    "Examples" => Any[
      # "generated/laplace-potential.md",
    ],
    "API" => "api.md",
    "Contents" => "contents.md",
  ]
)
