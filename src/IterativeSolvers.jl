"""
Module to store all iterative eigensolvers.
"""

using Printf
using LinearAlgebra
using SparseArrays
using IterativeSolvers
using AlgebraicMultigrid

import LinearAlgebra: ldiv!, \

export raycoeff, raycoeffg, commutator, arecommuting, RQI, GF_BE, richardson, IP, SI, SD, SD_split, SD_split2, LOPCG, RR, IOI, IPS, orthogonalization, RQI_new, RQI_old

"""
No preconditioner
"""
struct IdentityPreconditioner end
\(::IdentityPreconditioner, x) = copy(x)
ldiv!(::IdentityPreconditioner, x) = x
ldiv!(y, ::IdentityPreconditioner, x) = copyto!(y, x)

@doc raw"""
Compute the commutator $[A,B]=AB-BA$ of two matrices.

```jldoctest
julia> using ddEigenLab, LinearAlgebra;

julia> A = [1 2;3 4];

julia> commutator(A,2*A)
2×2 Matrix{Number}:
 0  0
 0  0
```
"""
function commutator(
  A::AbstractArray{T} where T<:Number,
  B::AbstractArray{T} where T<:Number
) :: AbstractArray{Number}
  return A * B - B * A
end

@doc raw"""
Return whether two matrices commute, i.e. $[A,B]=0$.

```jldoctest
julia> using ddEigenLab, LinearAlgebra;

julia> A = [1 2;3 4];

julia> arecommuting(A,2*A)
true
```
"""
function arecommuting(
  A::AbstractArray{T} where T<:Number,
  B::AbstractArray{T} where T<:Number
) :: Bool
  N = size(A)[1]
  return isapprox(
    commutator(A, B), zeros(Float64, N, N), atol=sqrt(eps(Float64))
  )
end

"""
Stores eigensolver information: `it`, `isconverged`
"""
mutable struct EigensolverInfo
  it :: Int64
  isconverged :: Bool
end

"""
Indicate convergence of method.
"""
@inline converged(res, tol) = norm(res, 2) <= tol

@doc raw"""
    λ(
      x::AbstractVector{T} where T<:Number,
      A::AbstractMatrix{T} where T<:Number
    ) :: Float64

Return Rayleigh coefficient ``\lambda_{A}(x)`` for given vector x with respect
to a matrix ``A``.

## Arguments

The definition reads

``\lambda_{A}(x) = \frac{x^T A x}{x^T x}``

!!! note

    ``\lambda_1(A) \le \lambda_{A}(x) \le \lambda_n(A)``

## Example Usage

```jldoctest
julia> using ddEigenLab, LinearAlgebra;

julia> raycoeff(ones(10), I(10))
1.0
julia> raycoeff(I(3)[:,3], diagm(0 => [1, ℯ, π]))
3.141592653589793
julia> raycoeff(I(3)[:,2], diagm(0 => [1, ℯ, π]))
2.718281828459045
```
"""
function raycoeff(
  x, # ::AbstractVector{T} where T<:Number,
  A # ::AbstractMatrix{T} where T<:Number
) :: Float64
  ((x' * A * x)/(x' * x))
end


"""
TODO
"""
function raycoeffg(
  x::AbstractVector{T} where T<:Number,
  A::AbstractMatrix{T} where T<:Number,
  B::AbstractMatrix{T} where T<:Number
) :: Float64
  dot(x, A, x) / dot(x, B, x)
end


"""
Gram-Schmidt Procedure to orthogonalize a span of vectors
"""
function orthogonalization(ss)
  # Gram-Schmidt orthogonalization
  function proj(u, v)
    # project v to u
    return dot(u, v) / dot(u, u) * u
  end
  for i=1:size(ss)[1]
    ss[i] = ss[i]
    for j=1:i-1
      ss[i] = ss[i] - proj(ss[j], ss[i])
    end
    normalize!(ss[i])
  end
  return ss
end

@doc raw"""
Perform the Generalized Rayleigh-Ritz method

!!! note

    Function changed to return the optimal vector instead of stepwidths. Fix this

## Derivation

For a given set of directions ``\{v_1,\cdots,v_n\}, v_i \in \mathbb{R}^M``, it solves the generalized eigenvalue problem

```math
\begin{bmatrix}
  v_1^T A v_1 & v_1^T A v_2 & \cdots & v_1^T A v_n \\
  v_2^T A v_1 & v_2^T A v_2 & \cdots & v_2^T A v_n \\
  \vdots & \vdots & \vdots & \vdots \\
  v_n^T A v_1 & v_n^T A v_2 & \cdots & v_n^T A v_n
\end{bmatrix}
\begin{bmatrix}
  \alpha_{i,1} \\
  \alpha_{i,2} \\
  \vdots \\
  \alpha_{i,n} \\
\end{bmatrix}
=
\lambda_i
\begin{bmatrix}
  v_1^T B v_1 & v_1^T B v_2 & \cdots & v_1^T B v_n \\
  v_2^T B v_1 & v_2^T B v_2 & \cdots & v_2^T B v_n \\
  \vdots & \vdots & \vdots & \vdots \\
  v_n^T B v_1 & v_n^T B v_2 & \cdots & v_n^T B v_n
\end{bmatrix}
\begin{bmatrix}
  \alpha_{i,1} \\
  \alpha_{i,2} \\
  \vdots \\
  \alpha_{i,n} \\
\end{bmatrix}
```

which is implemented as
```math
\begin{bmatrix}
  v_1^T \\
  v_2^T \\
  \vdots \\
  v_n^T
\end{bmatrix}
A
\begin{bmatrix}
  v_1 & v_2 & \dots & v_n
\end{bmatrix}
\begin{bmatrix}
  \alpha_{i,1} \\
  \alpha_{i,2} \\
  \vdots \\
  \alpha_{i,n} \\
\end{bmatrix}
=
\lambda_i
\begin{bmatrix}
  v_1^T \\
  v_2^T \\
  \vdots \\
  v_n^T
\end{bmatrix}
\begin{bmatrix}
  v_1 & v_2 & \dots & v_n
\end{bmatrix}
\begin{bmatrix}
  \alpha_{i,1} \\
  \alpha_{i,2} \\
  \vdots \\
  \alpha_{i,n} \\
\end{bmatrix}
```

The stepwidth results from the eigenvector components of the ground-state (``i=0``) and are ``\beta_2 = \alpha_{0,2}/\alpha_{0,1}, \cdots, \beta_n = \alpha_{0,n}/\alpha_{0,1}`` such that the linear combination ``v_1 + \beta_2 v_2 + \cdots \beta_n v_n`` is the desired minimizer.

## Example

For the Steepest Descent method, it usually solves

```math
\begin{bmatrix}
  x^T A x & x^T A p \\
  p^T A x & p^T A p
\end{bmatrix}
\begin{bmatrix}
  \alpha_{i,1} \\
  \alpha_{i,2}
\end{bmatrix}
=
\lambda_i
\begin{bmatrix}
  x^T B x & x^T B p \\
  p^T B x & p^T B p
\end{bmatrix}
\begin{bmatrix}
  \alpha_{i,1} \\
  \alpha_{i,2}
\end{bmatrix}
```

and returns ``\beta_2 = \alpha_{0,2}/\alpha_{0,2}`` which is the optimal stepwidth in the algorithm ``x + \beta_2 p``.

"""
function RRg(
  A :: AbstractArray{T} where T<:Number,
  B :: AbstractArray{T} where T<:Number,
  subspace
)
  subspace = orthogonalization(subspace)
  X1 = Array(vcat([v' for v in subspace]...))
  X2 = X1'
  L = Hermitian(X1 * A * X2)
  R = Hermitian(X1 * B * X2)
  _, vecs = eigen(L, R)
  lowvec = vecs[:,1]
  minvec = sum([lowvec[i] * subspace[i] for i=1:size(subspace)[1]])
  return minvec
end

"Calculates the erorrs"
function calc_errors(eval_approx, eval_real, evec_approx, evec_real,
  A :: AbstractArray{T} where T<:Number
)
  calc_errors(eval_approx, eval_real, evec_approx, evec_real, A, I(size(A)[1]))
end
function calc_errors(eval_approx, eval_real, evec_approx, evec_real,
  A :: AbstractArray{T} where T<:Number,
  B :: AbstractArray{T} where T<:Number
)
  Linf_val = max( abs(eval_approx - eval_real) ) / abs(eval_real)

  # Use sum to charaterize sign of eigenvectors (sgn(1) fails for very low)
  evec_approx_sum = sum(evec_approx)
  evec_real_sum = sum(evec_real)

  error_vec = (sign(evec_approx_sum) .* evec_approx) .- (sign(evec_real_sum) .* evec_real[:,1])  # FIXME: Unnecessary allocations

  L1_vec = norm(error_vec, 1)
  L2_vec = norm(error_vec)
  Linf_vec = norm(error_vec, Inf)
  error_res = (A .- eval_approx .* B) * evec_approx  # FIXME: Unnecessary allocations
  L1_res = norm(error_res, 1)
  L2_res = norm(error_res)
  Linf_res = norm(error_res, Inf)
  return Dict([
    ("|eλ|₁", Linf_val),
    ("|e|₁", L1_vec),
    ("|e|₂", L2_vec),
    ("|e|∞", Linf_vec),
    ("|r|₁", L1_res),
    ("|r|₂", L2_res),
    ("|r|∞", Linf_res)
  ])
end


"Prints the erorrs"
function print_errors(errors)
  for (etype, eval) in errors
    @printf "%s=%.5E " etype eval
  end
  @printf "\n"
end


"""
Rayleigh-Quotient Iteration (RQI)
"""
function RQI(
  A :: AbstractArray{T} where T<:Number,
  εₜₒₗ=1E-4,
  iₘₐₓ=100;
  realVal=0,
  realVec=zeros(size(A)[1]),
  verbose=true,
  direct_solver=true,
  xᵢ=ones(size(A)[1])
)
  RQI(A, I(size(A)[1]), εₜₒₗ, iₘₐₓ, realVal=realVal, realVec=realVec, verbose=verbose, direct_solver=direct_solver, xᵢ=xᵢ)
end
function RQI(
  A :: AbstractArray{T} where T<:Number,
  B :: AbstractArray{T} where T<:Number,
  εₜₒₗ=1E-4,
  iₘₐₓ=100;
  realVal=0,
  realVec=zeros(size(A)[1]),
  verbose=true,
  direct_solver=true,
  xᵢ=ones(size(A)[1])
)

  info = EigensolverInfo(0, false)

  N = size(A)[1]
  xᵢ = xᵢ / sqrt(dot(xᵢ, B, xᵢ))
  λᵢ = raycoeffg(xᵢ, A, B)
  res = Inf
  errors = []

  while (info.it < iₘₐₓ && !converged(res, εₜₒₗ))
    info.it += 1

    if direct_solver
      xᵢ = (A .- λᵢ .* B) \ (B * xᵢ)
    else
      b = B * xᵢ
      xᵢ, h = cg(A .- λᵢ .* B, b, log=true, reltol=1E-14, maxiter=10000); @info h
    end
    xᵢ = xᵢ ./ sqrt(dot(xᵢ, B, xᵢ))
    λᵢ = raycoeffg(xᵢ, A, B)
    res = (A .- λᵢ .* B) * xᵢ

    error = calc_errors(λᵢ, realVal, xᵢ, realVec, A, B)
    if verbose
      @printf("[%03d] ", info.it)
      @printf "λᵢ=%.4f " λᵢ
      print_errors(error)
    end
    push!(errors, error)
    if converged(res, εₜₒₗ) info.isconverged = true end
  end

  return [xᵢ, λᵢ, errors, info]
end


# function RQI(A, εₜₒₗ=1E-4, iₘₐₓ=100, realVal=0, realVec=zeros(size(A)[1]), verbose=true;
#   xᵢ=normalize(ones(size(A)[1]), 2))

#   info = EigensolverInfo(0, false)

#   N = size(A)[1]
#   # xᵢ = LinearAlgebra.normalize(ones(N), 2)
#   λᵢ = raycoeff(xᵢ,A)
#   res = Inf
#   errors = []

#   while (info.it < iₘₐₓ && !converged(res, εₜₒₗ))
#     info.it += 1

#     # Templates inplementation with xᵢ=v:
#     y = inv(Array(A - λᵢ * I(N))) * xᵢ
#     θ = norm(y)
#     λᵢ = λᵢ + (y'*xᵢ)/θ^2
#     xᵢ = y/θ
#     res = A*xᵢ - λᵢ*xᵢ

#     error = calc_errors(λᵢ, realVal, xᵢ, realVec, A)
#     if verbose
#       @printf("[%03d] ", info.it)
#       @printf "λᵢ=%.4f " λᵢ
#       print_errors(error)
#     end
#     push!(errors, error)
#     if converged(res, εₜₒₗ) info.isconverged = true end
#   end

#   return [xᵢ, λᵢ, errors, info]
# end

function GF_BE(
  A, P, τ=1, α=1, εₜₒₗ=1E-4, iₘₐₓ=100, realVal=0, realVec=zeros(size(A)[1]), verbose=true
)

  info = EigensolverInfo(0, false)

  N = size(A)[1]
  xᵢ = LinearAlgebra.normalize(ones(N), 2)
  λᵢ = raycoeff(xᵢ,A)
  res = Inf
  errors = []

  #* => Backward Euler AntoineLevitt2018
  # Δt = -1/λᵢ # RQI
  Δt = 1E-2
  alpha_V = 1/Δt
  alpha_Δ = 0.5

  while (info.it < iₘₐₓ && !converged(res, εₜₒₗ))
    info.it += 1

    xᵢ = LinearAlgebra.normalize(P * inv(Array(I(N) + Δt*A)) * xᵢ, 2) # xᵢ = A⁻¹ xᵢ₋₁
    λᵢ = raycoeff(xᵢ,A)

    error = calc_errors(λᵢ, realVal, xᵢ, realVec, A)
    if verbose
      @printf("[%03d] ", info.it)
      @printf "λᵢ=%.4f " λᵢ
      print_errors(error)
    end
    push!(errors, error)
    if converged(res, εₜₒₗ) info.isconverged = true end
  end

  return [xᵢ, λᵢ, errors, info]
end

function richardson(
  A, P, τ=1, α=1, εₜₒₗ=1E-4, iₘₐₓ=100, realVal=0, realVec=zeros(size(A)[1]), verbose=true
)

  info = EigensolverInfo(0, false)

  N = size(A)[1]
  xᵢ = LinearAlgebra.normalize(ones(N), 2)
  λᵢ = raycoeff(xᵢ,A)
  res = Inf
  errors = []

  while (info.it < iₘₐₓ && !converged(res, εₜₒₗ))
    info.it += 1

    #* => Residual based inverse power: (actually Richardson [Argentati2014])
    # P = pinv(A - λᵢ * I(N)) # Naive idea: Use eval guess (not groundstate)
    pᵢ = (A*xᵢ - raycoeff(xᵢ,A)*xᵢ)
    xᵢ = LinearAlgebra.normalize(τ * xᵢ - α * P * pᵢ, 2)
    # # Mix-in inverse power to set direction to groundstate
    # xᵢ = LinearAlgebra.normalize(A\xᵢ, 2) # set direction to groundstate
    λᵢ = raycoeff(xᵢ,A)

    #* => Scaled inverse power: Neymeyr Habil p27
    # xᵢ = LinearAlgebra.normalize((λᵢ * A)\xᵢ,2) # xᵢ = A⁻¹ xᵢ₋₁
    # λᵢ = λ(xᵢ,A)

    #* => Richardson (Knyazev): Has inverted coeffs
    # w_i = inv(A) * (A * xᵢ - λ(xᵢ, A) * xᵢ)
    # xᵢ = LinearAlgebra.normalize(w_i - (1/λ(xᵢ, A)) * xᵢ, 2)

    res = A*xᵢ - λᵢ*xᵢ

    # TODO: Implement gradient type method from Knyazev !!!

    error = calc_errors(λᵢ, realVal, xᵢ, realVec, A)
    if verbose
      @printf("[%03d] ", info.it)
      @printf "λᵢ=%.4f " λᵢ
      print_errors(error)
    end
    push!(errors, error)
    if converged(res, εₜₒₗ) info.isconverged = true end
  end

  return [xᵢ, λᵢ, errors, info]
end

"Inverse Orthogonal Iteration"
function IOI(
  A :: AbstractArray{T} where T<:Number,
  B :: AbstractArray{T} where T<:Number,
  shift, howmany=1, τ=1, α=1, εₜₒₗ=1E-4, iₘₐₓ=100, realVal=0, realVec=zeros(size(A)[1]), verbose=true, X0=rand(size(A)[1], howmany)
)

  info = EigensolverInfo(0, false)

  X, _ = qr(X0)
  X = Array(X)

  D = zeros(howmany)
  res = Inf
  errors = []

  Afac = factorize(A - shift * B)

  while (info.it < iₘₐₓ && !converged(res, εₜₒₗ))
    info.it += 1

    Y = Afac \ (B * X)
    D = (X' * B * X) \ (X' * A * X)
    D = diagm(eigen(D).values)
    res = A * X - B * X * D
    X, _ = qr(Y)
    X = Array(X)

    # normalize
    for i=1:howmany
      X[:,i] =  X[:,i] * 1/sqrt((X[:,i]' * B * X[:,i]))
    end

    error = calc_errors(D[howmany,howmany], realVal, X[:,howmany], realVec, A, B)
    if verbose
      @printf("[%03d] ", info.it)
      @printf "λN=%.4f " D[howmany,howmany]
      print_errors(error)
    end
    push!(errors, error)
    if converged(res, εₜₒₗ) info.isconverged = true end

  end

  return [X, diag(D), errors, info]
end

"""
Inverse power method

=> TODO
"""
function IP(
  A :: AbstractArray{T} where T<:Number,
  shift, τ=1, α=1, εₜₒₗ=1E-4, iₘₐₓ=100, realVal=0, realVec=zeros(size(A)[1]), verbose=true, direct_solver=true
)
  IP(A, I(size(A)[1]), shift, τ, α, εₜₒₗ, iₘₐₓ, realVal, realVec, verbose, direct_solver)
end
function IP(
  A :: AbstractArray{T} where T<:Number,
  B :: AbstractArray{T} where T<:Number,
  shift, τ=1, α=1, εₜₒₗ=1E-4, iₘₐₓ=100, realVal=0, realVec=zeros(size(A)[1]), verbose=true, direct_solver=true;
  xᵢ=ones(size(A)[1])
)

  info = EigensolverInfo(0, false)

  N = size(A)[1]
  xᵢ = xᵢ / sqrt(dot(xᵢ, B, xᵢ))
  λᵢ = raycoeffg(xᵢ, A, B)
  res = Inf
  errors = []

  Afac = factorize(A .- shift .* B)

  while (info.it < iₘₐₓ && !converged(res, εₜₒₗ))
    info.it += 1

    if direct_solver
      xᵢ = Afac \ (B * xᵢ)
    else
      # p = (B*xᵢ - A*xᵢ)
      # Q = A + (p*p') / (p' * xᵢ)
      # Q = A
      ml = ruge_stuben(A .- shift .* B)
      p = aspreconditioner(ml)
      b = B * xᵢ
      xᵢ, h = cg!(xᵢ, A .- shift .* B, b, Pl=p, log=true, verbose=false, reltol=1E-14, maxiter=10000)
      @info h
      # xᵢ, msg = minres(inv(Array(Q)) * (A - shift * B), inv(Array(Q)) * B * xᵢ, reltol=1E-8, maxiter=100, verbose=false, log=true)
      # xᵢ, msg = minres((A - shift * B), (1/raycoeffg(xᵢ,A,B)) * A * xᵢ, reltol=1E-8, maxiter=1000, verbose=false, log=true)  # freitag2007
      # print(msg)
    end
    xᵢ = xᵢ ./ sqrt(dot(xᵢ, B, xᵢ))
    λᵢ = raycoeffg(xᵢ, A, B)
    res = (A .- λᵢ .* B) * xᵢ

    error = calc_errors(λᵢ, realVal, xᵢ, realVec, A, B)
    if verbose
      @printf("[%03d] ", info.it)
      @printf "λᵢ=%.4f " λᵢ
      print_errors(error)
    end
    push!(errors, error)
    if converged(res, εₜₒₗ) info.isconverged = true end
  end

  return [xᵢ, λᵢ, errors, info]
end

@doc raw"""
Steepest Descent (SD) method.

```@repl
1+3
```

```jldoctest
a = 1
b = 2
a + b

# output

3
```

For example, we can compute the eigenpair of the matrix
``
A = \operatorname{diag}(\{1,2,3,4,5\}),
``
which should be ``(1, e_1)``.

```jldoctest
using LinearAlgebra, ddEigenLab;
vec, val, errors = ddEigenLab.SD(
  diagm(0 => [1, 2, 3, 4, 5]),
  inv(diagm(0 => [1, 2, 3, 4, 5])), 1, 1, 1E-16, 3, 1, I(5)[:,1]
);
val ≈ 1.4483419109989184

# output

[001] λᵢ=2.0277 |eλ|₁=1.02767E+00 |r|₂=9.57104E-01 |e|∞=5.92776E-01 |r|₁=1.88494E+00 |e|₂=9.02467E-01 |r|∞=6.09178E-01 |e|₁=1.82591E+00
[002] λᵢ=1.6269 |eλ|₁=6.26941E-01 |r|₂=7.73634E-01 |e|∞=6.25768E-01 |r|₁=1.57979E+00 |e|₂=7.58870E-01 |r|∞=4.46419E-01 |e|₁=1.36258E+00
[003] λᵢ=1.4483 |eλ|₁=4.48342E-01 |r|₂=6.30880E-01 |e|∞=5.96596E-01 |r|₁=1.26253E+00 |e|₂=6.66463E-01 |r|∞=3.48771E-01 |e|₁=1.08407E+00
true
```
"""
function SD(
  A :: AbstractArray{T} where T<:Number,
  P ::AbstractArray{T} where T<:Number,
  τ::Real=1, α=1, εₜₒₗ=1E-4, iₘₐₓ=100, realVal=0,
  realVec=zeros(size(A)[1]), verbose=true
)
  SD(A, I(size(A)[1]), P, τ, α, εₜₒₗ, iₘₐₓ, realVal, realVec, verbose)
end

function SD(
  A :: AbstractArray{T} where T<:Number,
  B :: AbstractArray{T} where T<:Number,
  P ::AbstractArray{T} where T<:Number,
  τ::Real=1, α=1, εₜₒₗ=1E-4, iₘₐₓ=100, realVal=0,
  realVec=zeros(size(A)[1]), verbose=true
)

  info = EigensolverInfo(0, false)

  N = size(A)[1]
  xᵢ = LinearAlgebra.normalize(ones(N), 2)
  λᵢ = raycoeffg(xᵢ,A,B)
  res = Inf
  errors = []

  while (info.it < iₘₐₓ && !converged(res, εₜₒₗ))
    info.it += 1

    res = A * xᵢ - raycoeffg(xᵢ, A, B) * B * xᵢ
    pᵢ = P \ res

    xᵢ = RRg(A, B, [xᵢ, pᵢ])
    xᵢ = xᵢ / sqrt((xᵢ' * B * xᵢ))
    λᵢ = raycoeffg(xᵢ, A, B)

    res = A * xᵢ - raycoeffg(xᵢ, A, B) * B * xᵢ

    error = calc_errors(λᵢ, realVal, xᵢ, realVec, A, B)
    if verbose
      @printf("[%03d] ", info.it)
      @printf "λᵢ=%.4f " λᵢ
      print_errors(error)
    end
    push!(errors, error)
    if converged(res, εₜₒₗ) info.isconverged = true end
  end

  return [xᵢ, λᵢ, errors, info]
end


"""
    LOPCG
"""
function LOPCG(
  A :: AbstractArray{T} where T<:Number,
  P ::AbstractArray{T} where T<:Number,
  τ, α=1, εₜₒₗ=1E-4, iₘₐₓ=100, realVal=0, realVec=zeros(size(A)[1]), verbose=true
)
  LOPCG(A, I(size(A)[1]), P, τ, α, εₜₒₗ, iₘₐₓ, realVal, realVec, verbose)
end


@doc raw"""
    LOPCG(
      A :: AbstractArray{T} where T<:Number,
      B :: AbstractArray{T} where T<:Number;
      tol = 1E-8,
      maxiter = 1000,
      realVal = 0,
      realVec = zeros(size(A)[1]),
      verbose = true,
      Pl = IdentityPreconditioner(),
      x = ones(size(A)[1])
    )
"""
function LOPCG(
  A :: AbstractArray{T} where T<:Number,
  B :: AbstractArray{T} where T<:Number;
  tol = 1E-8,
  maxiter = 1000,
  realVal = 0,
  realVec = zeros(size(A)[1]),
  verbose = true,
  Pl = IdentityPreconditioner(),
  x = ones(size(A)[1])
)

  info = EigensolverInfo(0, false)

  N = size(A)[1]
  x = x / sqrt((x' * B * x))
  λᵢ = raycoeffg(x, A, B)
  xp = Array(I(N)[:,1])  # different from x but unique
  res = Inf
  errors = []

  # Pfac = factorize(P)

  while (info.it < maxiter && !converged(res, tol))
    info.it += 1

    res = A * x - raycoeffg(x, A, B) * B * x

    w = Pl \ res

    # w, h = cg!(res, P, res, log=true, reltol=1E-10, maxiter=10000); @info h

    tmp = x
    x = RRg(A, B, [x, w, xp])
    x = x / sqrt((x' * B * x))
    xp = tmp
    λᵢ = raycoeffg(x, A, B)

    res = A * x - raycoeffg(x,A,B) * B * x

    error = calc_errors(λᵢ, realVal, x, realVec, A, B)
    if verbose
      @printf("[%03d] ", info.it)
      @printf "λᵢ=%.4f " λᵢ
      print_errors(error)
    end
    push!(errors, error)
    if converged(res, tol) info.isconverged = true end
  end

  return [x, λᵢ, errors, info]
end



"""
TODO
"""
function RQI_new(
  A :: AbstractArray{T} where T<:Number,
  P ::AbstractArray{T} where T<:Number,
  τ, α=1, εₜₒₗ=1E-4, iₘₐₓ=100, realVal=0, realVec=zeros(size(A)[1]), verbose=true
)
  RQI_new(A, I(size(A)[1]), P, τ, α, εₜₒₗ, iₘₐₓ, realVal, realVec, verbose)
end


"""
Combine RQI with 2-step recurrence

=> Seems to be robust wrt to gap. Needs no shift.
"""
function RQI_new(
  A :: AbstractArray{T} where T<:Number,
  B :: AbstractArray{T} where T<:Number;
  tol = 1E-8,
  maxiter = 1000,
  realVal = 0,
  realVec = zeros(size(A)[1]),
  verbose = true,
  Pl = IdentityPreconditioner(),
  x = ones(size(A)[1])
)

  info = EigensolverInfo(0, false)

  N = size(A)[1]
  x = x / sqrt((x' * B * x))
  λᵢ = raycoeffg(x, A, B)
  xp = Array(I(N)[:,1])  # different from x but unique
  res = Inf
  errors = []

  # Pfac = factorize(P)

  while (info.it < maxiter && !converged(res, tol))
    info.it += 1

    w = copy(x)
    try
      w = (A - raycoeffg(x, A, B) * B) \ x
    catch SingularException
      # singular, we might have found eigenpair
      if norm(A * x - raycoeffg(x,A,B) * B * x) ≈ 0.0
        # found eigenpair
        break
      else
        # got unlucky Raycoeff is equal to eval but evec is not correct
        # TODO: One step IPM?? Could work for posdef A
        # w = A \ x
        # TODO: One step of Ax??
        w = A * x # guaranteed not in same direction x since its not evec
                  # should also work for non posdef A??
        # break
      end
    end

    # x = w
    x = RRg(A, B, [x, w])

    x = x / sqrt((x' * B * x))
    # xp = tmp
    λᵢ = raycoeffg(x, A, B)

    res = A * x - raycoeffg(x,A,B) * B * x

    error = calc_errors(λᵢ, realVal, x, realVec, A, B)
    if verbose
      @printf("[%03d] ", info.it)
      @printf "λᵢ=%.4f " λᵢ
      print_errors(error)
    end
    push!(errors, error)
    if converged(res, tol) info.isconverged = true end
  end

  return [x, λᵢ, errors, info]
end



"""
TODO
"""
function RQI_old(
  A :: AbstractArray{T} where T<:Number,
  P ::AbstractArray{T} where T<:Number,
  τ, α=1, εₜₒₗ=1E-4, iₘₐₓ=100, realVal=0, realVec=zeros(size(A)[1]), verbose=true
)
  RQI_old(A, I(size(A)[1]), P, τ, α, εₜₒₗ, iₘₐₓ, realVal, realVec, verbose)
end


"""
TODO
"""
function RQI_old(
  A :: AbstractArray{T} where T<:Number,
  B :: AbstractArray{T} where T<:Number;
  tol = 1E-8,
  maxiter = 1000,
  realVal = 0,
  realVec = zeros(size(A)[1]),
  verbose = true,
  Pl = IdentityPreconditioner(),
  x = ones(size(A)[1])
)

  info = EigensolverInfo(0, false)

  N = size(A)[1]
  x = x / sqrt((x' * B * x))
  λᵢ = raycoeffg(x, A, B)
  xp = Array(I(N)[:,1])  # different from x but unique
  res = Inf
  errors = []

  # Pfac = factorize(P)

  while (info.it < maxiter && !converged(res, tol))
    info.it += 1

    w = copy(x)
    try
      w = (A - raycoeffg(x, A, B) * B) \ x
    catch SingularException
      # singular, we found eigenpair
      break
    end


    x = w
    # x = RRg(A, B, [x, w])

    x = x / sqrt((x' * B * x))
    # xp = tmp
    λᵢ = raycoeffg(x, A, B)

    res = A * x - raycoeffg(x,A,B) * B * x

    error = calc_errors(λᵢ, realVal, x, realVec, A, B)
    if verbose
      @printf("[%03d] ", info.it)
      @printf "λᵢ=%.4f " λᵢ
      print_errors(error)
    end
    push!(errors, error)
    if converged(res, tol) info.isconverged = true end
  end

  return [x, λᵢ, errors, info]
end
