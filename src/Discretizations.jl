"""
Discretizations Module
"""

using LinearAlgebra

using ddEigenLab

export FD2D, FD2DAnisotrop


"""
Abstract type for 2D discretizations.
"""
abstract type Discretization2D end


@doc raw"""
Discretize a 2D operator using finite differences.
The boundary conditions can be Dirichlet-Zero or Periodic.
- Laplacian Matrix
`https://en.wikipedia.org/wiki/Kronecker_sum_of_discrete_Laplacians`
"""
struct FD2D5 <: Discretization2D
  A :: AbstractMatrix{Float64}
  Ax :: AbstractMatrix{Float64}
  Axl :: AbstractMatrix{Float64}
  Ay :: AbstractMatrix{Float64}
  Ayl :: AbstractMatrix{Float64}
  AVxy :: AbstractMatrix{Float64}
  function FD2D5(
    L::Operator2D,
    Ω::Rectangle2D,
    bcs::Dict{Symbol, Symbol}
  )
    ⊗ = kron

    xDofs = 0
    xPer = false
    yDofs = 0
    yPer = false
    if bcs[:x] == :d
      xDofs = Ω.nx-2
    elseif  bcs[:x] == :p
      xDofs = Ω.nx-1
      xPer = true
    else
      error("x BCs wrong.")
    end
    if bcs[:y] == :d
      yDofs = Ω.ny-2
    elseif  bcs[:y] == :p
      yDofs = Ω.ny-1
      yPer = true
    else
      error("y BCs wrong.")
    end

    # Negative local Laplacians
    Axl = sparse(Tridiagonal(fill(-1, xDofs-1), fill(2, xDofs), fill(-1, xDofs-1)))
    Ayl = sparse(Tridiagonal(fill(-1, yDofs-1), fill(2, yDofs), fill(-1, yDofs-1)))

    # (Include periodic terms)
    if xPer
      Axl -= sparse([1; Ω.nx-1],[Ω.nx-1; 1],[1; 1])
    end
    if yPer
      Ayl -= sparse([1; Ω.ny-1],[Ω.ny-1; 1],[1; 1])
    end

    Axl *= 1 / Ω.dx^2
    Ayl *= 1 / Ω.dy^2

    # Span global Laplacians
    Ax = I(yDofs) ⊗ Axl
    Ay = Ayl ⊗ I(xDofs)

    # Potential matrix
    xStart = 2
    yStart = 2
    if xPer
      xStart = 1
    end
    if yPer
      yStart = 1
    end
    AVxy = spdiagm(0 => vec(L.Vxy.(Ω.xVals[xStart:end-1], Ω.yVals[yStart:end-1]')))

    A = Ax + Ay + AVxy
    new(A, Ax, Axl, Ay, Ayl, AVxy)
  end
end
FD2D = FD2D5

struct FD2DAnisotrop6 <: Discretization2D
  A :: AbstractMatrix{Float64}
  Ax :: AbstractMatrix{Float64}
  Ay :: AbstractMatrix{Float64}
  function FD2DAnisotrop6(
    L::Operator2D,
    D::AbstractMatrix{Float64},
    Ω::Rectangle2D,
    bcs::Dict{Symbol, Symbol}
  )
    ⊗ = kron

    xDofs = 0
    xPer = false
    yDofs = 0
    yPer = false
    if bcs[:x] == :d
      xDofs = Ω.nx-2
    else
      error("x BCs wrong.")
    end
    if bcs[:y] == :d
      yDofs = Ω.ny-2
    else
      error("y BCs wrong.")
    end

    # Check if diffusion matrix is correct
    @assert size(D) == (Ω.ny, Ω.nx)

    # Negative local Laplacians

    # TODO Remove kronecker and use different j==1

    Axls = Array[]
    for j = 2:yDofs+1
      Axl = 1 / Ω.dx^2 * sparse(Tridiagonal(
        [-(D[j,i-1] + D[j,i])/2 for i=3:xDofs+1],
        [(D[j,i-1] + 2*D[j,i] + D[j,i+1])/2 for i=2:xDofs+1],
        [-(D[j,i+1] + D[j,i])/2 for i=2:xDofs]
      ))
      push!(Axls, Axl)
    end
    Ayls = Array[]
    for i = 2:xDofs+1
      Ayl = 1 / Ω.dy^2 * sparse(Tridiagonal(
        [-(D[j-1,i] + D[j,i])/2 for j=2:yDofs],
        [(D[j-1,i] + 2*D[j,i] + D[j+1,i])/2 for j=2:yDofs+1],
        [-(D[j+1,i] + D[j,i])/2 for j=3:yDofs+1]
      ))
      push!(Ayls, Ayl)
    end

    # Span global Laplacians
    Ax = blockdiag(sparse.(Axls)...)
    # Ay = blockdiag(sparse.(Ayls)...) # FIXME: Ordering wrong
    Ay = vcat([kron(Ayls[i],I(xDofs))[(i-1)*yDofs+1:i*yDofs,:] for i=1:xDofs]...)

    # display(Ay)

    A = Ax + Ay
    new(A, Ax, Ay)
  end
end
FD2DAnisotrop = FD2DAnisotrop6
