"""
The main module of the package ddEigenLab.
"""
module ddEigenLab

  include("IterativeSolvers.jl")
  include("Geometries.jl")
  include("Postprocessing.jl")
  include("Operators.jl")
  include("Discretizations.jl")
  include("DomainDecomposition.jl")

end # module
