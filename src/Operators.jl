export NegativeLaplacePlusPotential, Operator2D

abstract type Operator2D end

@doc raw"""
`NegativeLaplacePlusPotential(V0=0, Vx=x->0, Vy=y->0) Vxy=(x, y) -> 0)`

$-\Delta + V(x,y)$
"""
struct NegativeLaplacePlusPotential3 <: Operator2D
    V0::Float64
    Vx::Function
    Vy::Function
    Vxy::Function
    function NegativeLaplacePlusPotential3(
        V0=0, Vx=x -> 0, Vy=y -> 0, Vxy=(x, y) -> 0
    )
        new(V0, Vx, Vy, Vxy)
    end
end
NegativeLaplacePlusPotential = NegativeLaplacePlusPotential3
