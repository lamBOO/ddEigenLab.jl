using WriteVTK
using LaTeXStrings
using Plots
gr()
using DataFrames
using CSV

using ddEigenLab

export plotErrors, writeErrorsToCsv, makeSpatialMatrix, displaySpectrumStats, plotSolution, plotPotential, writeVtk, Molecule, getParaviewSource

"Plot errors in log-log plot"
function plotErrors(errors, ptitle, figname=nothing)
  gr()
  # Add one machine precision to avoid error in semilog plot
  Linf_val = [errors[i]["|eλ|₁"] for i=1:length(errors)] .+ eps(Float64)
  L1_vec = [errors[i]["|e|₁"] for i=1:length(errors)] .+ eps(Float64)
  L2_vec = [errors[i]["|e|₂"] for i=1:length(errors)] .+ eps(Float64)
  Linf_vec = [errors[i]["|e|∞"] for i=1:length(errors)] .+ eps(Float64)
  L1_res = [errors[i]["|r|₁"] for i=1:length(errors)] .+ eps(Float64)
  L2_res = [errors[i]["|r|₂"] for i=1:length(errors)] .+ eps(Float64)
  Linf_res = [errors[i]["|r|∞"] for i=1:length(errors)] .+ eps(Float64)

  p = plot(
    [i for i=1:length(errors)],
    [Linf_val, L1_vec, L2_vec, Linf_vec, L1_res, L2_res, Linf_res],
    yaxis=:log,
    # xaxis=:log,
    label=hcat(
      L"\|e_{\lambda_1}\|_{L^\infty}",
      L"\|e_{u_1}\|_{L^1}",
      L"\|e_{u_1}\|_{L^2}",
      L"\|e_{u_1}\|_{L^\infty}",
      L"\|r\|_{L^1}",
      L"\|r\|_{L^2}",
      L"\|r\|_{L^\infty}"
    ),
    title=ptitle, marker = (:circle, 2),
    xaxis="iteration",
    legendfontsize=12
  )
  if figname !== nothing
    # savefig(figname)
  end
  return p
end

@doc """
Write errors to CSV file.

Uses `DataFrames`.
"""
function writeErrorsToCsv(errors, filename)
  df = DataFrame(it = 1:size(errors)[1])
  for (key, value) in errors[1]
    df[!, key] = [errors[i][key] for i=1:length(errors)] .+ eps(Float64)
  end
  CSV.write(filename * ".csv", df)
end

function displaySpectrumStats(eigval, eigvec)
  display("Spectrum statistics:")
  display(
    "max gap $(findmax([eigval[i+1]-eigval[i] for i=1:length(eigval)-1]))"
  )
  display(
    "min gap $(findmin([eigval[i+1]-eigval[i] for i=1:length(eigval)-1]))"
  )
  display("first gap $(eigval[2]-eigval[1])")
  display("first rat $(eigval[1]/eigval[2])")
end

function makeSpatialMatrix(phi, Ω::Rectangle2D, bcs::Dict{Symbol, Symbol})
  nx = Ω.nx
  ny = Ω.ny
  valmat = zeros(ny, nx)
  if bcs[:x] == :d && bcs[:y] == :d
    valmat[2:end-1, 2:end-1] = hcat([phi[(i-1)*(nx-2)+1:i*(nx-2)] for i=1:(ny-2)]...)'
  elseif bcs[:x] == :d && bcs[:y] == :p
    valmat[1:end-1, 2:end-1] = hcat([phi[(i-1)*(nx-2)+1:i*(nx-2)] for i=1:(ny-1)]...)'
    valmat[end,:] = valmat[1,:]
  elseif bcs[:x] == :p && bcs[:y] == :d
    valmat[2:end-1, 1:end-1] = hcat([phi[(i-1)*(nx-1)+1:i*(nx-1)] for i=1:(ny-2)]...)'
    valmat[:,end] = valmat[:,1]
  elseif bcs[:x] == :p && bcs[:y] == :p
    valmat[1:end-1, 1:end-1] = hcat([phi[(i-1)*(nx-1)+1:i*(nx-1)] for i=1:(ny-1)]...)'
    valmat[:,end] = valmat[:,1]
    valmat[end,:] = valmat[1,:]
  else
    error("Bcs wrong")
  end
  return valmat
end

# TODO: Make subplots
function plotSolution(evec, Ω::Rectangle2D, bcs::Dict{Symbol, Symbol}, title, density=false)
  gr()
  nx = Ω.nx
  ny = Ω.ny
  xVals = Ω.xVals
  yVals = Ω.yVals

  # Fill spatial matrix
  valmat = makeSpatialMatrix(evec, Ω, bcs)

  # Wave function
  display(heatmap(
    xVals[1:end], yVals[1:end],
    valmat,
    title=title,aspect_ratio=:equal
  ))
  # savefig("waveheat.png")
  display(surface(
    xVals[1:end], yVals[1:end],
    valmat,camera=(30,70),
    title=title,aspect_ratio=:equal
  ))
  # savefig("wavesurf.png")

  # Density
  if density
    display(surface(
      xVals[1:end], yVals[1:end],
      valmat.^2,
      camera=(30,70), title=title,aspect_ratio=:equal
    ))
    # savefig("denssurf.png")
    display(heatmap(
      xVals[1:end], yVals[1:end],
      valmat.^2,
      camera=(30,70), title=title,aspect_ratio=:equal
    ))
    # savefig("densheat.png")
    display(plot(
      xVals[1:end], yVals[1:end],
      valmat.^2,
      camera=(30,70), st=:contourf, title=title,aspect_ratio=:equal
    ))
    # savefig("denscontour.png")
  end
end

"Plot the potential"
function plotPotential(V, xVals, yVals, ptitle="Potential")
  gr()
  display("Start plot potential")
  potential = [V(xx,yy) for xx in xVals[:], yy in yVals[:]]
  display(heatmap(xVals[:], yVals[:], potential',camera=(30,70), title=ptitle,aspect_ratio=:equal))
  # savefig("potheat.png")
  display(surface(
    xVals[:], yVals[:], potential',
    camera=(30,70),
    title=ptitle,
    aspect_ratio=:equal,
    # xlims=(findmin(xVals)[1],findmax(xVals)[1]),
    # ylims=(findmin(yVals)[1],findmax(yVals)[1])
  ))
  # savefig("potsurf.png")
  display("End plot potential")
end

function writeVtk(vec::Array{Float64,1}, Ω::Rectangle2D, bcs::Dict{Symbol, Symbol}, fname="data")
  x = 0:Ω.dx:Ω.Lx
  y = 0:Ω.dy:Ω.Ly
  vtk_grid(fname, x, y) do vtk
    vtk["phi"] = makeSpatialMatrix(vec, Ω, bcs)'
  end
end

@doc raw"""
Represent a molecule with atoms and bonds

## Example

```julia
nw = Molecule(
  [6, 6, 6, 6],
  [
    (1.0, 0., -1.5),
    (2.8, 0., -1.5),
    (4.6, 0., -1.5),
    (6.4, 0., -1.5),
  ],
  [
    (0, 1, 2)
    (1, 2, 2)
    (2, 3, 2)
  ]
)  # nanowire with 4 carbon double-bonded
```
"""
struct Molecule9
  atoms::Array{Int}
  x::Vector{Tuple{Float64, Float64, Float64}}
  bonds::Vector{Tuple{Int, Int, Int}}
end
Molecule = Molecule9
function getParaviewSource(mol)
  atomlines = map(i -> "output.AppendAtom($(mol.atoms[i]),$(mol.x[i][1]),$(mol.x[i][2]),$(mol.x[i][3]))", 1:length(mol.atoms))
  bondlines = map(i -> "output.AppendBond($(mol.bonds[i][1]),$(mol.bonds[i][2]),$(mol.bonds[i][3]))", 1:length(mol.bonds))
  return join(vcat([atomlines, bondlines]...), ";")
end
