"""
Domain Decomposition Module

Contains the modules for the domain decomposition methods.
"""

using LinearAlgebra
using SparseArrays
using SuiteSparse
using ThreadsX
using IterativeSolvers
using Gridap
using GridapDistributed
using Gridap.CellData  # for KDTreeSearch eg
using Gridap.Arrays  # for return_cache eg
import Base.\

using ddEigenLab

export PSM, solve, pmatrix, CustomSpan, GeneralizedNicolaides,
       PartitionOfUnity, Partition, NoCoarseCorrection, apply,
       StationaryMethod, Efendiev, ShiftAndInvertCoarseCorrection, gmsh_partition, AbstractCoarseCorrection, AbstractPartition, DirichletGenEO, create_overlapping_elements_partition!, create_elements_partition, map_to_torus, periodic_extension, PartitionOfUnityGeneric, CGWrapper, GMRESWrapper, PSMWrapper,
       create_elements_partition_gmsh, create_dofs_partition,
       create_pu_matrices

"""
Abstract type for the stationary method.

Used to define the stationary method for the domain decomposition methods in
order to implement backslash operator.
"""
abstract type AbstractStationaryMethod end

abstract type AbstractTwoLevelOverlappingSchwarz end



@doc raw"""
    map_to_torus(x; unitcell = hcat(similar(x).=0, similar(x).=1))

Map the given coordinates `x` to the torus defined by the unit cell `unitcell`.

# Arguments
- `x`: The coordinates to be mapped.
- `unitcell`: The unit cell defining the torus. Default is a unit square.

# Definition
The mapping is defined as

```math
\begin{align}
  \mathbf{x} \mapsto \mathbf{x} - \mathbf{a} \mod (\mathbf{b} - \mathbf{a}) + \mathbf{a}
\end{align}
```

where $\mathbf{a}$ and $\mathbf{b}$ are the lower and upper bounds of the unit cell.

# Returns
The mapped coordinates on the torus.

"""
function map_to_torus(x; unitcell = hcat(similar(x).=0, similar(x).=1))
  return mod.(x .- unitcell[:,1], unitcell[:,2] - unitcell[:,1]) + unitcell[:,1]
end

@doc raw"""
    periodic_extension(f, space; unitcell = hcat(similar(x).=0, similar(x).=1), scaling = I)

Extends a function f periodically using a unit cell and a scaling matrix.

## Definition (including scaling)

    fₑₓₜ(x) = f(S(x - a mod (b - a) + a))

or

```math
\begin{align}
  f_{\text{ext}}(\mathbf{x}) = f(\mathbf{S} (\mathbf{x} - \mathbf{a} \mod (\mathbf{b} - \mathbf{a}) + \mathbf{a})
\end{align}
```

where $\mathbf{a}$ and $\mathbf{b}$ are the lower and upper bounds of the unit cell and $\mathbf{S}$ is the scaling matrix.

## Example

```jldoctest
using ddEigenLab, Gridap
mod_0 = CartesianDiscreteModel((0,1,0,1), (10,10))
V_0 = TestFESpace(mod_0, ReferenceFE(lagrangian, Float64, 1))
f_0 = Gridap.FESpaces.interpolate(x->sin(2*pi*x[1]) * sin(2*pi*x[2]), V_0)

mod_ext = CartesianDiscreteModel((0,3,0,1), (30,10))
V_ext = TestFESpace(mod_ext, ReferenceFE(lagrangian, Float64, 1))
f_ext = periodic_extension(f_0, V_ext, unitcell=[0 1; 0 1]);

println(sin(2*pi*0.2) * sin(2*pi*0.8))
println(f_0(Point(0.2,0.8)))
println(f_ext(Point(0.2,0.8)))
println(f_ext(Point(1.2,0.8)))
println(f_ext(Point(2.2,0.8)))

@assert sin(2*pi*0.2) * sin(2*pi*0.8) ≈
f_0(Point(0.2,0.8)) ≈
f_ext(Point(0.2,0.8)) ≈
f_ext(Point(1.2,0.8)) ≈
f_ext(Point(2.2,0.8))

# output
-0.9045084971874737
-0.9045084971874737
-0.9045084971874737
-0.9045084971874736
-0.9045084971874736
```
"""
function periodic_extension(f, space;
  unitcell = hcat(
    similar(collect(get_triangulation(f).grid.node_coords[1].data)) .= 0,
    similar(collect(get_triangulation(f).grid.node_coords[1].data)) .= 1
  ),
  scaling = I
)
  search_method = KDTreeSearch(num_nearest_vertices=5)
  f_new = Interpolable(f; searchmethod=search_method)
  cache = return_cache(f_new, Point(zeros(size(get_triangulation(f).model.grid_topology.n_m_to_nface_to_mfaces,2) - 1)))
  function helper(x)
    return evaluate!(
      cache, f_new,
      Point(scaling * map_to_torus(collect(x.data), unitcell=unitcell))
    )
  end

  return Gridap.FESpaces.interpolate(helper, space)
end


@doc raw"""
    gmsh_partition(m::Gridap.Geometry.UnstructuredDiscreteModel, tag_names::Vector{String})

Partition the elements of an unstructured discrete model based on their tags.

# Arguments
- `m::Gridap.Geometry.UnstructuredDiscreteModel`: The unstructured discrete model.
- `tag_names::Vector{String}`: The names of the tags used for partitioning.

# Returns
- `partition::Vector{Int32}`: The partition indices for each element.

# Example

TODO: Write example
"""
function gmsh_partition(m::Gridap.Geometry.UnstructuredDiscreteModel, tag_names::Vector{String})
  npars = length(tag_names)
  nelems = length(m.face_labeling.d_to_dface_to_entity[3])
  pinds = [findfirst(name -> name == tname, m.face_labeling.tag_to_name) for tname in tag_names]
  tagsp = [getindex(m.face_labeling.tag_to_entities, i) for i in pinds]

  partition = zeros(Int32, nelems)
  for iel = 1:nelems
    found = false
    for ipar in 1:npars
      if m.face_labeling.d_to_dface_to_entity[3][iel] in tagsp[ipar]
        if !found
          partition[iel] = ipar
        else
          error("element is in multiple partitions")
        end
        found = true
      end
    end
  end

  return partition
end

@doc raw"""
    create_elements_partition(partition::Vector{Int32}, npars::Integer)

Create a partition of elements based on a given partition vector.

# Arguments
- `partition::Vector{Int32}`: A vector representing the partition of elements.
- `npars::Integer`: The number of partitions.

# Returns
- `elemsp::Vector{Vector{Int32}}`: A vector of vectors representing the partitioned elements.

# Definition

```math
\begin{pmatrix}
  i_1 & i_2 & \cdots & i_n
\end{pmatrix}
\mapsto
\begin{pmatrix}
  \{i \mid i \in \{1, \ldots, n\}, p_i = 1\} \\
  \{i \mid i \in \{1, \ldots, n\}, p_i = 2\} \\
  \vdots \\
  \{i \mid i \in \{1, \ldots, n\}, p_i = n\}
\end{pmatrix}
```
where $i_j \in \{1, \ldots, n\}$ with the number of partitions $n$ assuming uniform counting starting from one. E.g.

```math
\begin{pmatrix}
  1 & 1 & 2 & 2 & 3 & 3
\end{pmatrix}
\mapsto
\begin{pmatrix}
  \{1, 2\} \\
  \{3, 4\} \\
  \{5, 6\}
\end{pmatrix}
```

# Examples

```jldoctest
using ddEigenLab
partition = Int32.([1, 1, 2, 2, 3, 3]);
npars = 3;
elemsp = create_elements_partition(partition, npars);
@assert elemsp == [[1, 2], [3, 4], [5, 6]];
# output
```
"""
function create_elements_partition(partition::Vector{Int32}, npars::Integer)
  nelems = length(partition)
  @debug nelems, length(partition)
  @assert nelems == length(partition)
  elemsp = [Vector{Int32}() for _ in 1:npars]
  for iel = 1:nelems
    push!(elemsp[partition[iel]], iel)
  end
  @debug nelems, sum(length.(elemsp))
  @assert nelems == sum(length.(elemsp))
  return elemsp
end

function create_elements_partition_gmsh(m::Gridap.Geometry.UnstructuredDiscreteModel, tag_names::Vector{String})
  npars = length(tag_names)
  elemsp = [Vector{Int32}() for _ in 1:npars]

  nelems = length(m.face_labeling.d_to_dface_to_entity[3])
  pinds = [findfirst(name -> name == tname, m.face_labeling.tag_to_name) for tname in tag_names]
  tagsp = [getindex(m.face_labeling.tag_to_entities, i) for i in pinds]
  for iel = 1:nelems
    for ipar in 1:npars
      if m.face_labeling.d_to_dface_to_entity[3][iel] in tagsp[ipar]
        push!(elemsp[ipar], iel)
      end
    end
  end

  return elemsp
end


@doc raw"""
    create_overlapping_elements_partition!(elemsp, g, npars::Integer, ol::Integer)

Create an δ-overlapping partition of elements based on a given partition vector.
Each partition will be extended by the neighbours of the elements up to order δ.

# Arguments
- `elemsp::Vector{Vector{Int32}}`: A vector of vectors representing the partitioned elements.
- `g::SparseMatrixCSC`: The cell graph of the model.
- `npars::Integer`: The number of partitions.
- `ol::Integer`: The overlap.

# Returns
- `elemsp::Vector{Vector{Int32}}`: A vector of vectors representing the partitioned elements.

# Cell graph
The cell graph of a linear domain with 3 elements is
```jldoctest
using SparseArrays
# sketch
# |---|---|---|
# |1  |2  |3  |
# |---|---|---|
# corresponding cell graph
g = Int8.(sparse([1,2,2,3],[2,1,3,2],[1,1,1,1]))
# output
3×3 SparseMatrixCSC{Int8, Int64} with 4 stored entries:
 ⋅  1  ⋅
 1  ⋅  1
 ⋅  1  ⋅
```

# Definition

```math
\begin{pmatrix}
  \mathcal{I}_1 \\ \mathcal{I}_2 \\ \vdots \\ \mathcal{I}_n
\end{pmatrix}
\mapsto
\begin{pmatrix}
  \mathcal{I}_1 \cup \mathcal{N}_{\delta}(\mathcal{I}_1) \\
  \mathcal{I}_2 \cup \mathcal{N}_{\delta}(\mathcal{I}_2) \\
  \vdots \\
  \mathcal{I}_n \cup \mathcal{N}_{\delta}(\mathcal{I}_n)
\end{pmatrix}
```

where $\mathcal{N}_{\delta}(\mathcal{I}_i)$ is the set of neighbours of $\mathcal{I}_i$ up to order $\delta$ ($\delta=1$ means direct neighbours). E.g.

```math
\begin{pmatrix}
  \{1, 2\} \\
  \{3, 4\} \\
  \{5, 6\}
\end{pmatrix}
\mapsto
\begin{pmatrix}
  \{1, 2, 3, 4\} \\
  \{1, 2, 3, 4, 5, 6\} \\
  \{3, 4, 5, 6\}
\end{pmatrix}
```

# Visual example
```
The chain domain with 6 elements
|---|---|---|---|---|---|
|1  |2  |3  |4  |5  |6  |
|---|---|---|---|---|---|
with the partition into 3 sets
|---|---|---|---|---|---|
|1  |1  |2  |2  |3  |3  |
|---|---|---|---|---|---|
will be partitioned (overlap δ=2) into
|---|---|---|---|---|---|
|12 |12 |123|123|23 |23 |
|---|---|---|---|---|---|
s.t. the second overlapped set now contains the full domain.
```

where the cell graph of the models reads

```math
g = \begin{pmatrix}
  0 & 1 & 0 & 0 & 0 & 0 \\
  1 & 0 & 1 & 0 & 0 & 0 \\
  0 & 1 & 0 & 1 & 0 & 0 \\
  0 & 0 & 1 & 0 & 1 & 0 \\
  0 & 0 & 0 & 1 & 0 & 1 \\
  0 & 0 & 0 & 0 & 1 & 0
\end{pmatrix}
```


# Example

```jldoctest
using ddEigenLab, Gridap, GridapDistributed, Metis
# domain picture:
# |--|--|--|--|
# |--|--|--|--|
# |--|--|--|--|
mod = CartesianDiscreteModel((0,2,0,1), (4,2))
N = 2
g = GridapDistributed.compute_cell_graph(mod)
par = Metis.partition(g, N)
elemspar = create_elements_partition(par, N)
δ = 1  # overlap
elemspar_ol = copy(elemspar)
create_overlapping_elements_partition!(elemspar_ol, g, 2, δ)
println(elemspar)
println(elemspar_ol)
# output
Vector{Int32}[[3, 4, 7, 8], [1, 2, 5, 6]]
Vector{Int32}[[2, 3, 4, 6, 7, 8], [1, 2, 3, 5, 6, 7]]
```
"""
function create_overlapping_elements_partition!(elemsp, g, npars::Integer, ol)
  for iol = 1:ol
    @debug "overlap" iol
    Threads.@threads for ipar = 1:npars
      tmp = copy(elemsp)
      elemsp[ipar] = sort(unique(vcat([g[:, i].nzind for i in tmp[ipar]]...)))
    end
  end
end

function create_dofs_partition(
  elemsp::Vector{Vector{Int32}}, sp::Gridap.FESpaces.UnconstrainedFESpace
)
  m = sp.fe_basis.trian.model
  dim = size(m.grid_topology.n_m_to_nface_to_mfaces,2) - 1
  npars = length(elemsp)
  @debug "create nodesp"
  nodesp = [Vector{Int32}() for _ in 1:npars]
  Threads.@threads for ipar = 1:npars
    nodesp[ipar] = sort(unique(vcat([m.grid_topology.n_m_to_nface_to_mfaces[dim+1][el] for el in elemsp[ipar]]...)))
  end

  @debug "create freenodesp"
  freenodesp = copy(nodesp)
  Threads.@threads for ipar = 1:npars
    filter!(e -> e in sp.metadata.free_dof_to_node, nodesp[ipar])
  end

  @debug "create dofsp"
  reverse_map = zeros(Int32, maximum(sp.metadata.free_dof_to_node))
  for (node, freenode) in enumerate(sp.metadata.free_dof_to_node)
    reverse_map[freenode] = node
  end
  dofsp = [reverse_map[freenodesp[ipar]] for ipar = 1:npars]
  return dofsp
end


function create_pu_matrices(
  dofsp::Vector{Vector{Int32}},
  sp::Gridap.FESpaces.UnconstrainedFESpace,
  check = true
)
  @debug "create Ri and Di"
  npars = length(dofsp)

  Ri = Vector{SparseMatrixCSC}(undef, npars)
  Di = Vector{SparseMatrixCSC}(undef, npars)
  Threads.@threads for ipar = 1:npars
    Ri[ipar] = spzeros(length(dofsp[ipar]), sp.nfree)
    Di[ipar] = spzeros(length(dofsp[ipar]), length(dofsp[ipar]))
    for idof = 1:length(dofsp[ipar])
      Ri[ipar][idof, dofsp[ipar][idof]] = 1
      Di[ipar][idof, idof] = 1 / sum(map(p -> dofsp[ipar][idof] in p, dofsp))
    end
  end

  if check
    @debug "consistency check"
    @assert sum(
      [Ri[i]' * Di[i] * Ri[i] for i in 1:length(Di)]
    ) ≈ I(size(Ri[1])[2])  # check consistency of PU
  end

  return Ri, Di
end



abstract type AbstractPartition end
function Ri(p :: AbstractPartition) return p.Ri end
function Di(p :: AbstractPartition) return p.Di end

struct PartitionOfUnity2 <: AbstractPartition
  Ri::Vector{SparseMatrixCSC}
  Di::Vector{SparseMatrixCSC}
  function PartitionOfUnity2(Ω::Rectangle2D, ol=1; check = true)
    Ri = Vector{SparseMatrixCSC}(undef, Ω.Lx)
    Di = Vector{SparseMatrixCSC}(undef, Ω.Lx)
    for sd = 1:Ω.Lx
      nysd = Ω.rhox - 1
      nxglob = Ω.Lx * Ω.rhox - 1
      nxsd = Ω.rhox + (1 < sd < Ω.Lx) * 1 + (ol-1) * ( (1 < sd < Ω.Lx) * 1 + 1)
      Rsd = spzeros(nxsd*nysd, Ω.N)
      Dsd = spzeros(nxsd*nysd, nxsd*nysd)
      for ny in 1:nysd
        for nx in 1:nxsd
          Rsd[(ny-1)*nxsd + nx, max(((sd-1)*Ω.rhox-ol), 0) + (ny-1)*nxglob + nx] = 1
          Dsd[(ny-1)*nxsd + nx, (ny-1)*nxsd + nx] = (
            1
            - [
              ((nx==1+i && sd!=1) || (nx==nxsd-i && sd!=Ω.Lx))
              for i=0:2*(ol-1)
            ]' * reverse([(i+1)/(2*(ol)) for i=0:2*(ol-1)])
          )
        end
      end
      Ri[sd] = Rsd
      Di[sd] = Dsd  # RAS
    end
    if check
      @info "cons check"
      @assert sum(
        [Ri[i]' * Di[i] * Ri[i] for i in 1:length(Di)]
      ) ≈ I(size(Ri[1])[2])  # check consistency of PU
    end
    return new(Ri, Di)
  end
  function PartitionOfUnity2(Ri, Di)
    return new(Ri, Di)
  end
  function PartitionOfUnity2(
    partition::Vector{Int32}, ol::Int, sp::Gridap.FESpaces.UnconstrainedFESpace;
    check = true
  )
    m = sp.fe_basis.trian.model
    npars = findmax(partition)[1]

    @debug "compute cell graph"
    g = GridapDistributed.compute_cell_graph(m)

    @debug "create elemsp"
    elemsp = create_elements_partition(partition, npars)

    @debug "create overlapping elemsp"
    create_overlapping_elements_partition!(elemsp, g, npars, ol)

    dofsp = create_dofs_partition(elemsp, sp)

    Ri, Di = create_pu_matrices(dofsp, sp, check)

    return new(Ri, Di)
  end
end
PartitionOfUnity = PartitionOfUnity2


struct PartitionOfUnityGeneric2 <: AbstractPartition
  Ri::Vector{SparseMatrixCSC}
  Di::Vector{SparseMatrixCSC}
  function PartitionOfUnityGeneric2(
    partition::Vector{Int32}, ol::Int,
    sp::Gridap.FESpaces.UnconstrainedFESpace,
    pu_matrix::Matrix{Float64};
    check = true
    )

    pu = PartitionOfUnity(partition, ol, sp)

    Di = [spdiagm(pu.Ri[i] * pu_matrix[:,i]) for i in 1:length(pu.Di)]

    if check
      @assert sum(
        [pu.Ri[i]' * Di[i] * pu.Ri[i] for i in 1:length(pu.Di)]
      ) ≈ I(size(pu.Ri[1])[2])  # check consistency of PU
    end

    return new(Ri(pu), Di)
  end
end
PartitionOfUnityGeneric = PartitionOfUnityGeneric2


struct Partition2 <: AbstractPartition
  Ri::Vector{SparseMatrixCSC}
  Di::Vector{SparseMatrixCSC}
  function Partition2(Ω::Rectangle2D, ol=1)
    pu = PartitionOfUnity(Ω, ol)
    # identities
    Di = [sparse(I,d,d) for d in size.(pu.Di, 1)]
    return new(Ri(pu), Di)
  end
  function Partition2(partition::Vector{Int32}, ol::Int, sp::Gridap.FESpaces.UnconstrainedFESpace; check = true)
    pu = PartitionOfUnity(partition, ol, sp, check=check)
    # identities
    Di = [sparse(I,d,d) for d in size.(pu.Di, 1)]
    return new(Ri(pu), Di)
  end
  function Partition2(Ri::Vector{SparseMatrixCSC})
    Di = [sparse(I,d,d) for d in size.(Ri, 1)]
    return new(Ri, Di)
  end
end
Partition = Partition2


abstract type AbstractCoarseCorrection end

function apply(cs :: AbstractCoarseCorrection, x)
  # @info "cc"
  Zx = getZ(cs)' * x
  A0invZx = getA0(cs) \ Zx
  return getZ(cs) * A0invZx
end


struct NoCoarseCorrection3 <: AbstractCoarseCorrection
end
NoCoarseCorrection = NoCoarseCorrection3
function apply(ncc :: NoCoarseCorrection, x)
  return 0
end

struct Nicolaides3 <: AbstractCoarseCorrection
  Z::SparseMatrixCSC{Float64, Int64}
  A0::SparseMatrixCSC{Float64, Int64}
  # TODO: Implement
end
Nicolaides = Nicolaides3


struct Efendiev1 <: AbstractCoarseCorrection
  Z::SparseMatrixCSC{Float64, Int64}
  A0::SparseMatrixCSC{Float64, Int64}
  # TODO: Factorize A0 maybe?

  function Efendiev1(
    A::SparseMatrixCSC{Float64, Int64},
    p :: AbstractPartition,
    f :: AbstractMatrix{Float64}
  )
    # TODO: Doesnt work, fix this
    dim = length(p.Di)
    Zi = Vector{SparseMatrixCSC}(undef, dim)
    for sd = 1:dim
      AA = Ri(p)[sd] * A * Ri(p)[sd]'
      BB = (Di(p)[sd]) * AA * (Di(p)[sd])'
      evals, evecs = eigen(Array(AA), Array(BB))
      # @show evals
      Zsd = sparse(Ri(p)[sd]' * Di(p)[sd] * evecs[:,1])
      # @info Zsd
      Zi[sd] = Zsd
    end
    Z = hcat(Zi...)
    A0 = Z' * A * Z
    new(Z, A0)
  end
end
Efendiev = Efendiev1
function getZ(cs :: Efendiev)
  # @info 1
  return cs.Z
end
getA0(cs :: Efendiev) = return cs.A0


struct GeneralizedNicolaides5 <: AbstractCoarseCorrection
  Z::SparseMatrixCSC{Float64, Int64}
  A0::SparseMatrixCSC{Float64, Int64}
  # TODO: Factorize A0 maybe?

  function GeneralizedNicolaides5(
    A::SparseMatrixCSC{Float64, Int64},
    p :: AbstractPartition,
    f :: AbstractMatrix{Float64}
  )
    dim = length(p.Di)
    Zi = Vector{SparseMatrixCSC}(undef, dim)
    for sd = 1:dim
      Zsd = Ri(p)[sd]' * Di(p)[sd] * Ri(p)[sd] * f
      Zi[sd] = Zsd
    end
    Z = hcat(Zi...)
    # Z = Array(qr(Array(hcat(Zi...))).Q)
    A0 = Z' * A * Z
    new(Z, A0)
  end
end
GeneralizedNicolaides = GeneralizedNicolaides5
getZ(cs :: GeneralizedNicolaides) = return cs.Z
getA0(cs :: GeneralizedNicolaides) = return cs.A0


struct DirichletGenEO2 <: AbstractCoarseCorrection
  Z::SparseMatrixCSC{Float64, Int64}
  A0::SparseMatrixCSC{Float64, Int64}
  # TODO: Factorize A0 maybe?
  # TODO: Is that correctly implemented?

  function DirichletGenEO2(
    A::SparseMatrixCSC{Float64, Int64},
    p :: AbstractPartition,
    tau :: Float64  # TODO: Implement
  )
    dim = length(p.Di)
    Zi = Vector{SparseMatrixCSC}(undef, dim)
    for sd = 1:dim

      range= 1:1

      AA = Ri(p)[sd] * A * Ri(p)[sd]'
      evals, evecs = eigen(Array(AA))
      # @show evals[range]
      Zsd = sparse(Ri(p)[sd]' * evecs[:,range])
      Zi[sd] = Zsd
    end
    Z = hcat(Zi...)
    # Z = Array(qr(Array(hcat(Zi...))).Q)
    A0 = Z' * A * Z
    new(Z, A0)
  end
end
DirichletGenEO = DirichletGenEO2
getZ(cs :: DirichletGenEO2) = return cs.Z
getA0(cs :: DirichletGenEO2) = return cs.A0


struct ShiftAndInvertCoarseCorrection5 <: AbstractCoarseCorrection
  Z::SparseMatrixCSC{Float64, Int64}
  A0::SparseMatrixCSC{Float64, Int64}
  # TODO: Factorize A0 maybe?

  function ShiftAndInvertCoarseCorrection5(
    A::SparseMatrixCSC{Float64, Int64},
    p :: AbstractPartition,
    f :: AbstractMatrix{Float64}
  )
    dim = length(p.Di)
    Zi = Vector{SparseMatrixCSC}(undef, dim)
    for sd = 1:dim
      Zsd = Ri(p)[sd]' * Di(p)[sd] * Ri(p)[sd] * f
      Zi[sd] = Zsd
    end
    Z = hcat(Zi...)
    # Z0 = hcat(Array.(orthogonalization([Z[:,i] for i=1:dim]))...)
    # evals, evec = eigen(Array(Z' * A * Z))
    evals, evec = eigen(Array(Z' * A * Z), Array(Z' * Z))
    @show evals[1]
    A0 = Z' * (A - evals[1] * Array(Z * Z')) * Z
    new(Z, A0)
  end
end
ShiftAndInvertCoarseCorrection = ShiftAndInvertCoarseCorrection5
getZ(cs :: ShiftAndInvertCoarseCorrection) = return cs.Z
getA0(cs :: ShiftAndInvertCoarseCorrection) = return cs.A0


"""
Custom matrix Z, which constructs CS span from column, p108 DJN book
"""
struct CustomSpan4 <: AbstractCoarseCorrection
  # TODO
end
CustomSpanO = CustomSpan4


struct GenEO2 <: AbstractCoarseCorrection
  # TODO
end
GenEOO = GenEO2


struct PSM3 <: AbstractTwoLevelOverlappingSchwarz
  A :: SparseMatrixCSC{Float64, Int64}
  Ai :: Vector{SparseMatrixCSC}
  Aifac :: Vector{SuiteSparse.CHOLMOD.Factor{Float64}}
  par :: AbstractPartition
  cc :: AbstractCoarseCorrection
  function PSM3(
    A :: SparseMatrixCSC{Float64, Int64},
    par :: AbstractPartition,
    cc :: AbstractCoarseCorrection
  )
    Ai = Vector{SparseMatrixCSC}(undef, length(par.Ri))
    Aifac = Vector{SuiteSparse.CHOLMOD.Factor{Float64}}(undef, length(par.Ri))
    for sd = 1:length(par.Ri)
    # Threads.@threads for sd = 1:length(par.Ri)
      Ai[sd] = Ri(par)[sd] * A * Ri(par)[sd]'
      Aifac[sd] = factorize(Symmetric(Ai[sd]))
    end
    return new(A, Ai, Aifac, par, cc)
  end
end
PSM = PSM3

function pmatrix(psm::PSM)
  A = zeros(size(psm.A))
  if !(psm.cc==:none)
    A .+= psm.Z * inv(Array(psm.A0)) *  psm.Z'
  end
  for sd=1:length(psm.par.Ri)
    A .+= psm.Ri[sd]'* psm.Di[sd] * inv(Array(psm.Ai[sd])) * psm.Ri[sd]
  end
  return A
end

function LinearAlgebra.ldiv!(y, psm::PSM, x; cc=true)
  fill!(y, 0)
  y .+= sum(zip(psm.par.Ri, psm.par.Di, psm.Aifac)) do (R, D, A)
    A⁻¹Rr = A \ (R * x)
    R'* (D * A⁻¹Rr)
  end
  if cc y .+= apply(psm.cc, x) end
end
function LinearAlgebra.ldiv!(psm::PSM, x; cc=true)
  y = zeros(size(x))
  ldiv!(y, psm, x, cc=false)
  if cc y .+= apply(psm.cc, x) end
  copyto!(x, y)
end
function (\)(psm::PSM, x; cc=true)
  y = zeros(size(x))
  ldiv!(y, psm, x, cc=false)
  if cc y .+= apply(psm.cc, x) end
  return y
end


struct StationaryMethod9 <: AbstractStationaryMethod
  M
  x0::Vector{Float64}
  maxit::Int
  reltol::Float64
  debug::Bool
  function StationaryMethod9(psm::PSM, x0::Vector{Float64}, maxit::Int, reltol::Float64; debug=true::Bool)
    return new(psm, x0, maxit, reltol, debug)
  end
end
StationaryMethod = StationaryMethod9
function (\)(sm :: AbstractStationaryMethod, x)
  return solve(sm.M, x, x0=sm.x0, maxiter=sm.maxit, reltol=sm.reltol, debug=debug=sm.debug)[1]

  # TODO: Refactor as "CGMethod"
  # TODO Look at IterativeSolver interface
  # x, info = gmres(sm.M.A, x, verbose=false, log=true, Pl=sm.M, reltol=sm.reltol)
  # @info info
  # return x
end


function solve(
  psm::PSM,
  b::Vector{Float64};
  x0::Vector{Float64} = zeros(size(b)),
  maxiter::Int = 1000,
  reltol::Float64 = 1E-8,
  debug::Bool = true,
  history::Bool = false,
  coarse_history::Bool =false
)

  # writeVtk(x_i, Ω, bcs, "psm_0")
  x_i = copy(x0)
  normR0 = norm(b - psm.A*x_i)
  errors = Float64[]
  iters = maxiter
  coarse_updates = Vector{Vector{Float64}}()
  solution_history = Vector{Vector{Float64}}()
  for it = 1:maxiter

  # println("solve")

  # @time begin
    # res = (ddEigenLab.raycoeff(x_i, psm.A)) * x_i .- (psm.A*x_i)
    res = b - psm.A*x_i
    # writeVtk(res, psm.Ω, Dict(:x => :d, :y => :d), "res_$(it)") ##
    # @show norm(res)

    # for sd=1:psm.Ω.Lx
    #   ress = b - psm.A*x_i
    #   A⁻¹Rr = psm.Aifac[sd] \ (psm.Ri[sd] * ress)
    #   x_i .+= psm.Ri[sd]'* (psm.Di[sd] * A⁻¹Rr)
    # end

    # x_il = [zeros(psm.Ω.N) for i=1:psm.Ω.Lx]
    # @time Threads.@threads  for sd=1:psm.Ω.Lx
    #   A⁻¹Rr = psm.Aifac[sd] \ (psm.Ri[sd] * res)
    #   x_i .+= psm.Ri[sd]'* (psm.Di[sd] * A⁻¹Rr)
    # end
    # x_i .+= sum(x_il)


    # @show norm(x_i)
    # writeVtk(x_i, psm.Ω, Dict(:x => :d, :y => :d), "xiprepre_$(it)") ##
    x_i .+= \(psm, res, cc=false)
    # normalize!(x_i)
    # @show norm(x_i)
    # writeVtk(x_i, psm.Ω, Dict(:x => :d, :y => :d), "xipre_$(it)") ##
    # x_i .+= apply(psm.cc, (ddEigenLab.raycoeff(x_i, psm.A)) * x_i .- (psm.A*x_i))
    # ccvec = apply(psm.cc, b - psm.A * x_i)
    # writeVtk(ccvec, psm.Ω, Dict(:x => :d, :y => :d), "cc_$(it)") ##
    ccvec = apply(psm.cc, b - psm.A * x_i)
    x_i .+= ccvec
    if coarse_history && typeof(psm.cc) != NoCoarseCorrection
      push!(coarse_updates, ccvec)
    end
    if history
      push!(solution_history, copy(x_i))  # copy needed, otherwise pointer
    end
    # cc = apply(psm.cc, (ddEigenLab.raycoeff(x_i, psm.A)) * x_i .- (psm.A*x_i))
    # writeVtk(cc, psm.Ω, Dict(:x => :d, :y => :d), "cc_$(it)") ##
    # x_i = ddEigenLab.RRg(psm.A, I(psm.Ω.N), [x_i, cc])
    # x_i .+= cc
    # writeVtk(x_i, psm.Ω, Dict(:x => :d, :y => :d), "xipost_$(it)") ##
    # x_i .+= sum(zip(psm.par.Ri, psm.par.Di, psm.Aifac)) do (R, D, A)
    #   A⁻¹Rr = A \ (R * res)
    #   R'* (D * A⁻¹Rr)
    # end
  # end

  # @time begin
    # if !(psm.cc==:none)
    #   debug && @info "cc"
    #   # Ztr = psm.Z'* (ddEigenLab.raycoeff(x_i, psm.A) * x_i .- (psm.A*x_i))
    #   Ztr = psm.cc.Z'* (b - psm.A*x_i)
    #   # Ztr = psm.cc.Z'* res
    #   A0invZtr = psm.cc.A0 \ Ztr
    #   x_i .+= psm.cc.Z * A0invZtr
    #   # writeVtk(psm.Z * A0invZtr, psm.Ω, Dict(:x => :d, :y => :d), "cc_$(it)")
    # end

  # end

  # @time begin
    # relresnorm = norm(ddEigenLab.raycoeff(x_i, psm.A) * x_i .- (psm.A*x_i))
    resnorm = norm(b - psm.A*x_i)
    relresnorm = resnorm / normR0
    push!(errors, relresnorm)
    debug && @show it, relresnorm, resnorm, normR0
    if relresnorm < reltol
      !debug && @show it
      iters = it
      # writeVtk(x_i, Ω, bcs, "psm_sol")
      break
    end
  # end

  # @info it
  # writeVtk(x_i, psm.Ω, Dict(:x => :d, :y => :d), "psmnew_$(it)") ##

  end
  # @info errors
  return (x_i, (errors=errors, iters=iters, solution_history=solution_history, coarse_updates=coarse_updates))
  # return Ri, Di, Ai
end

function solveAS(psm::PSM, b::Vector{Float64}, x0::Vector{Float64}, maxit::Int, reltol::Float64; debug=true::Bool)

  # TODO: Remvove this and merge with other function

  # writeVtk(x_i, Ω, bcs, "psm_0")
  x_i = copy(x0)
  x_is = [x_i for i=1:length(psm.par.Ri)*maxit]
  normR0 = norm(b - psm.A*x_i)
  errors = Float64[]
  iters = maxit
  for it = 1:maxit

  # println("solve")

  # @time begin
    # res = (ddEigenLab.raycoeff(x_i, psm.A)) * x_i .- (psm.A*x_i)
    # res = b - psm.A*x_i
    # writeVtk(res, psm.Ω, Dict(:x => :d, :y => :d), "res_$(it)") ##
    # @show norm(res)

    # for sd=1:psm.Ω.Lx
    #   ress = b - psm.A*x_i
    #   A⁻¹Rr = psm.Aifac[sd] \ (psm.Ri[sd] * ress)
    #   x_i .+= psm.Ri[sd]'* (psm.Di[sd] * A⁻¹Rr)
    # end

    # x_il = [zeros(psm.Ω.N) for i=1:psm.Ω.Lx]
    # @time Threads.@threads  for sd=1:psm.Ω.Lx
    #   A⁻¹Rr = psm.Aifac[sd] \ (psm.Ri[sd] * res)
    #   x_i .+= psm.Ri[sd]'* (psm.Di[sd] * A⁻¹Rr)
    # end
    # x_i .+= sum(x_il)

    for i = 1:length(psm.par.Ri)
      @info i
      R = psm.par.Ri[i]
      D = psm.par.Di[i]
      A = psm.Aifac[i]
      res = b - psm.A*x_i
      A⁻¹Rr = A \ (R * res)
      x_i .+= R'* (D * A⁻¹Rr)
      x_is[i + (it-1) * length(psm.par.Ri)] = copy(x_i)
    end


    # @show norm(x_i)
    # writeVtk(x_i, psm.Ω, Dict(:x => :d, :y => :d), "xiprepre_$(it)") ##
    # x_i .+= \(psm, res, cc=false)
    # normalize!(x_i)
    # @show norm(x_i)
    # writeVtk(x_i, psm.Ω, Dict(:x => :d, :y => :d), "xipre_$(it)") ##
    # x_i .+= apply(psm.cc, (ddEigenLab.raycoeff(x_i, psm.A)) * x_i .- (psm.A*x_i))
    # ccvec = apply(psm.cc, b - psm.A * x_i)
    # writeVtk(ccvec, psm.Ω, Dict(:x => :d, :y => :d), "cc_$(it)") ##
    # x_i .+= apply(psm.cc, b - psm.A * x_i)
    # cc = apply(psm.cc, (ddEigenLab.raycoeff(x_i, psm.A)) * x_i .- (psm.A*x_i))
    # writeVtk(cc, psm.Ω, Dict(:x => :d, :y => :d), "cc_$(it)") ##
    # x_i = ddEigenLab.RRg(psm.A, I(psm.Ω.N), [x_i, cc])
    # x_i .+= cc
    # writeVtk(x_i, psm.Ω, Dict(:x => :d, :y => :d), "xipost_$(it)") ##
    # x_i .+= sum(zip(psm.par.Ri, psm.par.Di, psm.Aifac)) do (R, D, A)
    #   A⁻¹Rr = A \ (R * res)
    #   R'* (D * A⁻¹Rr)
    # end
  # end

  # @time begin
    # if !(psm.cc==:none)
    #   debug && @info "cc"
    #   # Ztr = psm.Z'* (ddEigenLab.raycoeff(x_i, psm.A) * x_i .- (psm.A*x_i))
    #   Ztr = psm.cc.Z'* (b - psm.A*x_i)
    #   # Ztr = psm.cc.Z'* res
    #   A0invZtr = psm.cc.A0 \ Ztr
    #   x_i .+= psm.cc.Z * A0invZtr
    #   # writeVtk(psm.Z * A0invZtr, psm.Ω, Dict(:x => :d, :y => :d), "cc_$(it)")
    # end

  # end

  # @time begin
    # resnorm = norm(ddEigenLab.raycoeff(x_i, psm.A) * x_i .- (psm.A*x_i))
    resnorm = norm(b - psm.A*x_i) / normR0
    push!(errors, resnorm)
    debug && @show it, resnorm, normR0
    if resnorm < reltol
      !debug && @show it
      iters = it
      # writeVtk(x_i, Ω, bcs, "psm_sol")
      break
    end
  # end

  # @info it
  # writeVtk(x_i, psm.Ω, Dict(:x => :d, :y => :d), "psmnew_$(it)") ##

  end
  # @info errors
  return ([x0, x_is...], (errors=errors, iters=iters))
  # return Ri, Di, Ai
end

@doc raw"""
CG preconditioner wrapper.
"""
struct CGWrapper3
  m::PSM
  opts::NamedTuple
  iterations::Vector{Int64}
  CGWrapper3(m::PSM, opts::NamedTuple) =
    new(m, opts, Int64[])
end
function (\)(cgw::CGWrapper3, b)
  x = zeros(size(b))
  x, info = cg!(x, cgw.m.A, b; Pl=cgw.m, cgw.opts...)
  @info info
  push!(cgw.iterations, info.iters)
  return x
end
CGWrapper = CGWrapper3

@doc raw"""
GMRES preconditioner wrapper.
"""
struct GMRESWrapper3
  m::PSM
  opts::NamedTuple
  iterations::Vector{Int64}
  GMRESWrapper3(m::PSM, opts::NamedTuple) =
    new(m, opts, Int64[])
end
function (\)(gmresw::GMRESWrapper3, b)
  @info "start gmres"
  x = zeros(size(b))
  x, info = gmres!(x, gmresw.m.A, b; Pl=gmresw.m, gmresw.opts...)
  @info "end gmres"
  @info info
  push!(gmresw.iterations, info.iters)
  return x
end
GMRESWrapper = GMRESWrapper3

@doc raw"""
Parallel Schwarz method wrapper to use a preconditioner.
"""
struct PSMWrapper4
  m::PSM
  opts::NamedTuple
  iterations::Vector{Int64}
  PSMWrapper4(m::PSM, opts::NamedTuple) =
    new(m, opts, Int64[])
end
function (\)(psmw::PSMWrapper4, b)
  x, info = ddEigenLab.solve(psmw.m, b; x0=zeros(size(b)), psmw.opts...)
  push!(psmw.iterations, info.iters)
  @info info.iters
  return x
end
PSMWrapper = PSMWrapper4
