using LinearAlgebra

export Rectangle2D

"""
Abstract type for all Geometry objects.
"""
abstract type Geometry2D end

@doc raw"""
`Rectangle2D(Lx, Ly, rhox, rhoy)`

Generate a 2D rectangle with ``Ω = [0,L_x] \times [0,L_y]`` with
discretization uniform density ``ρ = L_x/N_x = L_y/N_y``.
"""
struct Rectangle2D <: Geometry2D
    Lx::Int
    Ly::Int
    rhox::Int
    rhoy::Int
    nx::Int
    ny::Int
    N::Int
    dx::Float64
    dy::Float64
    xVals::Array{T1,2} where T1 <: Float64
    yVals::Array{T2,2} where T2 <: Float64
    function Rectangle2D(Lx::Int, Ly::Int, rhox::Int, rhoy::Int)
        nx = rhox * Lx + 1  # h_x = const
        ny = rhoy * Ly + 1  # h_y = const
        N = (nx - 2) * (ny - 2)
        dx = Lx / (nx - 1)
        dy = Ly / (ny - 1)
        xVals = Array(range(0, stop=Lx, step=dx)')
        yVals = Array(range(0, stop=Ly, step=dy)')
        new(Lx, Ly, rhox, rhoy, nx, ny, N, dx, dy, xVals, yVals)
    end
end
