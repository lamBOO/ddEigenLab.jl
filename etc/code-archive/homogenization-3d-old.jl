using Gridap
using LinearAlgebra
using SparseArrays
using LinearMaps
using SymPy
using DataFrames
using Plots
using IterativeSolvers
using CSV
using FresnelIntegrals
using SpecialFunctions

using ddEigenLab

howmany = 4

"Input string needs x[1],x[2],..."
struct WeightFunction5
  A :: Function
  DA :: Function
  Aeffz :: Function
  DAeffz :: Function
  function WeightFunction5(exp)
    @vars xx yy zz
    xsym = [xx, yy, zz]

    A = eval(Meta.parse("x->"*exp))

    DA = eval(Meta.parse(
      "x->["*
      join([replace(
        string(SymPy.diff(Base.invokelatest(A, xsym),ξ)),
        (string.(xsym).=>["x[$i]" for i=1:3])...) for ξ in xsym
      ], ",")
      *"]"
    ))

    Aeffz = (
      eval(Meta.parse(
        "x->"*replace(
          string(SymPy.integrate(Base.invokelatest(A, xsym),(zz,0,1))),
          (string.(xsym).=>["x[$i]" for i=1:3])...
        )
      ))
    )

    DAeffz = eval(Meta.parse(
      "x->["*
      join(replace.(
        string.([SymPy.integrate(Base.invokelatest(DA, xsym)[i],(zz,0,1)) for i=1:2]),
        (string.(xsym).=>["x[$i]" for i=1:3])...)
      , ",")
      *"]"
    ))

    return new(
      A, DA, Aeffz, DAeffz
    )
  end
end

# w = WeightFunction5("(x[3]^2*(x[3]-1)*( 10*cos(π*x[1])^2 + 10*cos(π*x[2])^2))^2")
# w = WeightFunction5("(x[3]^2*(x[3]-1)*( 100*cos(π*x[1])^2 + 10*cos(π*x[2])^2))^2")
w = WeightFunction5("(sin(π*x[3])^2*( 10*cos(π*x[1])^2 + 10*cos(π*x[2])^2 + 1.1 - sin(π*x[3])^2 ))^2") #! before review


A(x) = (
  # 1)
  # sin(π*x[3])^2*( cos(π*x[1])^2 + cos(π*x[2])^2 + 1.1 - sin(π*x[3])^2 )
  # 2) corrector ops diagonal
  # sin(π*x[3])^2*( cos(π*x[1])^2 + 2*cos(π*x[2])^2 + 1.1 - sin(π*x[3])^2 )
  # 3)
  # sin(π*x[3])^2*( cos(π*x[1])^2 + 2*cos(π*x[2])^2 + 1.1 + exp(cos(π*x[1])^2*cos(π*x[2])^2*cos(π*x[3])^2) - sin(π*x[3])^2 )
  # 4) x,y the same
  # sin(π*x[3])^2*( cos(π*x[1])^2 + cos(π*x[2])^2 + 1.1 + exp(cos(π*x[1])^2*cos(π*x[2])^2*cos(π*x[3])^2) - sin(π*x[3])^2 )
  # 5) for more deviation to sinus
  sin(π*x[3])^2*( 10*cos(π*x[1])^2 + 10*cos(π*x[2])^2 + 1.1 - sin(π*x[3])^2 )
  # 6)
  # x[3]^2*(x[3]-1)*( 10*cos(π*x[1])^2 + 10*cos(π*x[2])^2)# 6)
)^2

Aeffz(x) = (
  # @vars xx yy zz
  # replace(
  #   replace(
  #     replace(
  #       string(
  #         SymPy.integrate(A([xx,yy,zz]),(zz,0,1)).subs(xx,"xxx").subs(yy,"yyy").subs(zz,"zzz")
  #       ),
  #       # "xxx"=>"$x[1]"
  #       "xxx"=>"x[1]"
  #     ),
  #     # "yyy"=>"$x[2]"
  #     "yyy"=>"x[2]"
  #   ),
  #   # "zzz"=>"$x[3]"
  #   "zzz"=>"x[3]"
  # )
  # 5)
  # 37.5*cos(pi*x[1])^4 + 75.0*cos(pi*x[1])^2*cos(pi*x[2])^2 + 2.0*cos(pi*x[1])^2 + 37.5*cos(pi*x[2])^4 + 2.0*cos(pi*x[2])^2 + 0.0396875000000001
  # 6)
  20*cos(pi*x[1])^4/21 + 40*cos(pi*x[1])^2*cos(pi*x[2])^2/21 + 20*cos(pi*x[2])^4/21
)

function DA(x)
  # too slow:
  # @vars xx yy zz
  # return [
  #   [
  #     # eval(Meta.parse(
  #       replace(
  #         replace(
  #           replace(
  #             string(
  #               SymPy.diff(A([xx,yy,zz]),ξξ).subs(xx,"xxx").subs(yy,"yyy").subs(zz,"zzz")
  #             ),
  #             # "xxx"=>"$x[1]"
  #             "xxx"=>"x[1]"
  #           ),
  #           # "yyy"=>"$x[2]"
  #           "yyy"=>"x[2]"
  #         ),
  #         # "zzz"=>"$x[3]"
  #         "zzz"=>"x[3]"
  #       )
  #     # ))
  #   ]
  #   for ξξ in [xx,yy,zz]
  # ]


  return [
    # 1)
    # (12.5663706143592*sin(pi*x[3])^2 - 12.5663706143592*cos(pi*x[1])^2 - 12.5663706143592*cos(pi*x[2])^2 - 13.8230076757951)*sin(pi*x[1])*sin(pi*x[3])^4*cos(pi*x[1]),
    # (12.5663706143592*sin(pi*x[3])^2 - 12.5663706143592*cos(pi*x[1])^2 - 12.5663706143592*cos(pi*x[2])^2 - 13.8230076757951)*sin(pi*x[2])*sin(pi*x[3])^4*cos(pi*x[2]),
    # -(-12.5663706143592*sin(pi*x[3])^2 + 12.5663706143592*cos(pi*x[1])^2 + 12.5663706143592*cos(pi*x[2])^2 + 13.8230076757951)*sin(pi*x[3])^5*cos(pi*x[3]) + 15.2053084433746*(-0.909090909090909*sin(pi*x[3])^2 + 0.909090909090909*cos(pi*x[1])^2 + 0.909090909090909*cos(pi*x[2])^2 + 1)^2*sin(pi*x[3])^3*cos(pi*x[3])
    # 2)
    # (12.5663706143592*sin(pi*x[3])^2 - 12.5663706143592*cos(pi*x[1])^2 - 25.1327412287183*cos(pi*x[2])^2 - 13.8230076757951)*sin(pi*x[1])*sin(pi*x[3])^4*cos(pi*x[1]),
    # (25.1327412287183*sin(pi*x[3])^2 - 25.1327412287183*cos(pi*x[1])^2 - 50.2654824574367*cos(pi*x[2])^2 - 27.6460153515902)*sin(pi*x[2])*sin(pi*x[3])^4*cos(pi*x[2]),
    # -(-12.5663706143592*sin(pi*x[3])^2 + 12.5663706143592*cos(pi*x[1])^2 + 25.1327412287183*cos(pi*x[2])^2 + 13.8230076757951)*sin(pi*x[3])^5*cos(pi*x[3]) + 50.2654824574367*(-sin(pi*x[3])^2/2 + cos(pi*x[1])^2/2 + cos(pi*x[2])^2 + 0.55)^2*sin(pi*x[3])^3*cos(pi*x[3])
    # 3)
    # 4*(-2*pi*exp(cos(pi*x[1])^2*cos(pi*x[2])^2*cos(pi*x[3])^2)*sin(pi*x[1])*cos(pi*x[1])*cos(pi*x[2])^2*cos(pi*x[3])^2 - 2*pi*sin(pi*x[1])*cos(pi*x[1]))*(exp(cos(pi*x[1])^2*cos(pi*x[2])^2*cos(pi*x[3])^2)/2 - sin(pi*x[3])^2/2 + cos(pi*x[1])^2/2 + cos(pi*x[2])^2 + 0.55)*sin(pi*x[3])^4,
    # 4*(-2*pi*exp(cos(pi*x[1])^2*cos(pi*x[2])^2*cos(pi*x[3])^2)*sin(pi*x[2])*cos(pi*x[1])^2*cos(pi*x[2])*cos(pi*x[3])^2 - 4*pi*sin(pi*x[2])*cos(pi*x[2]))*(exp(cos(pi*x[1])^2*cos(pi*x[2])^2*cos(pi*x[3])^2)/2 - sin(pi*x[3])^2/2 + cos(pi*x[1])^2/2 + cos(pi*x[2])^2 + 0.55)*sin(pi*x[3])^4,
    # 4*(-2*pi*exp(cos(pi*x[1])^2*cos(pi*x[2])^2*cos(pi*x[3])^2)*sin(pi*x[3])*cos(pi*x[1])^2*cos(pi*x[2])^2*cos(pi*x[3]) - 2*pi*sin(pi*x[3])*cos(pi*x[3]))*(exp(cos(pi*x[1])^2*cos(pi*x[2])^2*cos(pi*x[3])^2)/2 - sin(pi*x[3])^2/2 + cos(pi*x[1])^2/2 + cos(pi*x[2])^2 + 0.55)*sin(pi*x[3])^4 + 16*pi*(exp(cos(pi*x[1])^2*cos(pi*x[2])^2*cos(pi*x[3])^2)/2 - sin(pi*x[3])^2/2 + cos(pi*x[1])^2/2 + cos(pi*x[2])^2 + 0.55)^2*sin(pi*x[3])^3*cos(pi*x[3])
    # 4)
    # 1.21*(-3.63636363636364*pi*exp(cos(pi*x[1])^2*cos(pi*x[2])^2*cos(pi*x[3])^2)*sin(pi*x[1])*cos(pi*x[1])*cos(pi*x[2])^2*cos(pi*x[3])^2 - 3.63636363636364*pi*sin(pi*x[1])*cos(pi*x[1]))*(0.909090909090909*exp(cos(pi*x[1])^2*cos(pi*x[2])^2*cos(pi*x[3])^2) - 0.909090909090909*sin(pi*x[3])^2 + 0.909090909090909*cos(pi*x[1])^2 + 0.909090909090909*cos(pi*x[2])^2 + 1)*sin(pi*x[3])^4,
    # 1.21*(-3.63636363636364*pi*exp(cos(pi*x[1])^2*cos(pi*x[2])^2*cos(pi*x[3])^2)*sin(pi*x[2])*cos(pi*x[1])^2*cos(pi*x[2])*cos(pi*x[3])^2 - 3.63636363636364*pi*sin(pi*x[2])*cos(pi*x[2]))*(0.909090909090909*exp(cos(pi*x[1])^2*cos(pi*x[2])^2*cos(pi*x[3])^2) - 0.909090909090909*sin(pi*x[3])^2 + 0.909090909090909*cos(pi*x[1])^2 + 0.909090909090909*cos(pi*x[2])^2 + 1)*sin(pi*x[3])^4,
    # 1.21*(-3.63636363636364*pi*exp(cos(pi*x[1])^2*cos(pi*x[2])^2*cos(pi*x[3])^2)*sin(pi*x[3])*cos(pi*x[1])^2*cos(pi*x[2])^2*cos(pi*x[3]) - 3.63636363636364*pi*sin(pi*x[3])*cos(pi*x[3]))*(0.909090909090909*exp(cos(pi*x[1])^2*cos(pi*x[2])^2*cos(pi*x[3])^2) - 0.909090909090909*sin(pi*x[3])^2 + 0.909090909090909*cos(pi*x[1])^2 + 0.909090909090909*cos(pi*x[2])^2 + 1)*sin(pi*x[3])^4 + 4.84*pi*(0.909090909090909*exp(cos(pi*x[1])^2*cos(pi*x[2])^2*cos(pi*x[3])^2) - 0.909090909090909*sin(pi*x[3])^2 + 0.909090909090909*cos(pi*x[1])^2 + 0.909090909090909*cos(pi*x[2])^2 + 1)^2*sin(pi*x[3])^3*cos(pi*x[3])
    # 5
    # -400*pi*(-sin(pi*x[3])^2/10 + cos(pi*x[1])^2 + cos(pi*x[2])^2 + 0.11)*sin(pi*x[1])*sin(pi*x[3])^4*cos(pi*x[1]),
    # -400*pi*(-sin(pi*x[3])^2/10 + cos(pi*x[1])^2 + cos(pi*x[2])^2 + 0.11)*sin(pi*x[2])*sin(pi*x[3])^4*cos(pi*x[2]),
    # 400*pi*(-sin(pi*x[3])^2/10 + cos(pi*x[1])^2 + cos(pi*x[2])^2 + 0.11)^2*sin(pi*x[3])^3*cos(pi*x[3]) - 40*pi*(-sin(pi*x[3])^2/10 + cos(pi*x[1])^2 + cos(pi*x[2])^2 + 0.11)*sin(pi*x[3])^5*cos(pi*x[3]),
    # 6
    -40*pi*x[3]^4*(x[3] - 1)^2*(10*cos(pi*x[1])^2 + 10*cos(pi*x[2])^2)*sin(pi*x[1])*cos(pi*x[1]),
    -40*pi*x[3]^4*(x[3] - 1)^2*(10*cos(pi*x[1])^2 + 10*cos(pi*x[2])^2)*sin(pi*x[2])*cos(pi*x[2]),
    x[3]^4*(2*x[3] - 2)*(10*cos(pi*x[1])^2 + 10*cos(pi*x[2])^2)^2 + 4*x[3]^3*(x[3] - 1)^2*(10*cos(pi*x[1])^2 + 10*cos(pi*x[2])^2)^2
  ]
end

function DAeffz(x)
  # return
  # [
  #   replace(
  #     replace(
  #       replace(
  #         string(
  #           SymPy.integrate(DA([xx,yy,zz])[i],(zz,0,1)).subs(xx,"xxx").subs(yy,"yyy").subs(zz,"zzz")
  #         ),
  #         # "xxx"=>"$x[1]"
  #         "xxx"=>"x[1]"
  #       ),
  #       # "yyy"=>"$x[2]"
  #       "yyy"=>"x[2]"
  #     ),
  #     # "zzz"=>"$x[3]"
  #     "zzz"=>"x[3]"
  #   )
  #   for i=1:2
  # ]


  return [
    # 5
    # (-471.238898038469*cos(pi*x[1])^2 - 471.238898038469*cos(pi*x[2])^2 - 12.5663706143592)*sin(pi*x[1])*cos(pi*x[1]),
    # (-471.238898038469*cos(pi*x[1])^2 - 471.238898038469*cos(pi*x[2])^2 - 12.5663706143592)*sin(pi*x[2])*cos(pi*x[2])
    # 6
    -11.9679720136754*sin(pi*x[1])*cos(pi*x[1])^3 - 11.9679720136754*sin(pi*x[1])*cos(pi*x[1])*cos(pi*x[2])^2,
    -11.9679720136754*sin(pi*x[2])*cos(pi*x[1])^2*cos(pi*x[2]) - 11.9679720136754*sin(pi*x[2])*cos(pi*x[2])^3
  ]
end

function solve_corrector_problem_2d()
  Lx = 1
  Ly = 1
  ρ = 1000
  tol = 1E-8

  domain = (0, Lx, 0, Lx)
  partition = (Lx * ρ, Lx * ρ)
  model = CartesianDiscreteModel(domain, partition; isperiodic=(true, true))  # quads
  # Tets don't work with periodic
  # https://github.com/gridap/Gridap.jl/issues/586
  # model = simplexify(CartesianDiscreteModel(domain, partition))  # tets

  labels = get_face_labeling(model)
  # add_tag_from_tags!(labels, "diri", collect(1:3^d-1))
  add_tag_from_tags!(labels, "diri", [1])

  order = 1  # TODO: Rechange it to 2 or check, order=1 was only for Paraview Integrate filter !!!!
  reffe = ReferenceFE(lagrangian, Float64, order)
  # reffe = Gridap.ReferenceFEs.LagrangianRefFE(Float64,HEX,2)  # same
  V0 = TestFESpace(model, reffe; conformity=:H1)  # ! CHANGED, no diri
  # V0 = TestFESpace(model, reffe; conformity=:H1, dirichlet_tags = ["diri"])  # ! CHANGED, no diri

  Ug = TrialFESpace(V0, 1)

  degree = 7
  Ω = Triangulation(model)
  dΩ = Measure(Ω,degree)

  ls = LUSolver()
  solver = LinearFESolver(ls)

  e1 = VectorValue(1.0,0.0)
  # d = TensorValue(1,0,0,Lx^2)
  d = TensorValue(1,0,0,1)

  CbarA = zeros(2, 2)
  Dbar = 0
  for i=1:2
    b(v) = ∫( (x->w.DAeffz(x)[i]) * v ) * dΩ

    a1(u,v) = ∫( ∇(u) ⋅ ∇(v) * w.Aeffz ) * dΩ
    op1 = AffineFEOperator(a1, b, Ug, V0)

    @info "corrector solve: start"
    @time θ = Gridap.solve(solver, op1)
    @info "corrector solve: end"

    writevtk(Ω, "results_theta_$i", cellfields = ["θ" => θ])
    writevtk(Ω, "results_gradtheta_$i", cellfields = ["∇(θ)" => ∇(θ)])
    writevtk(Ω, "results_atimesgradtheta_$i", cellfields = ["A*∇(θ)" =>
      w.Aeffz * ∇(θ)
    ])
    writevtk(Ω, "results_atimesgradtheta+ei_$i", cellfields = ["A*∇(θ)" =>
    w.Aeffz * ( ∇(θ) + VectorValue(Int.(I(2)[:,i])) )
    ])

    for j=1:2
      Cbar = sum(∫( w.Aeffz * ( Int(i==j) + ∇(θ) ⋅ VectorValue(Int.(I(2)[:,j])) )) * dΩ)
      CbarA[i,j] = Cbar
    end

    Dbar = sum(∫( w.Aeffz ) * dΩ)
  end

  # make elliptic
  # CbarA[3,3] = 10000

  display(CbarA)

  Cbar = TensorValue(CbarA)

  # ahom(u,v) = ∫( Cbar ⋅ ∇(u) ⋅ ∇(v) ) * dΩ
  # bhom(v) = ∫( Dbar * v ) * dΩ
  # ophom = AffineFEOperator(ahom, bhom, Ug, V0)
  # u0 = Gridap.solve(solver, ophom)
  # writevtk(Ω, "results_u0", cellfields = ["u0" => u0])

  return [Cbar, Dbar]
end

function solve_corrector_problem()
  Lx = 1
  Ly = 1
  ρ = 1
  tol = 1E-8

  domain = (0, Lx, 0, Lx, 0, Ly)
  partition = (Lx * ρ*200/10, Lx * ρ*200/10, Ly * ρ*10)
  model = CartesianDiscreteModel(domain, partition; isperiodic=(true, true, false))  # quads
  # Tets don't work with periodic
  # https://github.com/gridap/Gridap.jl/issues/586
  # model = simplexify(CartesianDiscreteModel(domain, partition))  # tets

  labels = get_face_labeling(model)
  # add_tag_from_tags!(labels, "diri", collect(1:3^d-1))
  add_tag_from_tags!(labels, "diri", [1])

  order = 1  # TODO: Rechange it to 2 or check, order=1 was only for Paraview Integrate filter !!!!
  reffe = ReferenceFE(lagrangian, Float64, order)
  # reffe = Gridap.ReferenceFEs.LagrangianRefFE(Float64,HEX,2)  # same
  V0 = TestFESpace(model, reffe; conformity=:H1)  # ! CHANGED, no diri
  # V0 = TestFESpace(model, reffe; conformity=:H1, dirichlet_tags = ["diri"])  # ! CHANGED, no diri

  Ug = TrialFESpace(V0, 1)

  degree = 7
  Ω = Triangulation(model)
  dΩ = Measure(Ω,degree)

  ls = LUSolver()
  solver = LinearFESolver(ls)

  e1 = VectorValue(1.0,0.0)
  # d = TensorValue(1,0,0,Lx^2)
  d = TensorValue(1,0,0,1)

  CbarA = zeros(3, 3)
  Dbar = 0
  for i=1:3
    b(v) = ∫( (x->DA(x)[i]) * v ) * dΩ

    a1(u,v) = ∫( ∇(u) ⋅ ∇(v) * A ) * dΩ
    op1 = AffineFEOperator(a1, b, Ug, V0)

    @info "corrector solve: start"
    @time θ = Gridap.solve(solver, op1)
    @info "corrector solve: end"

    writevtk(Ω, "results_theta_$i", cellfields = ["θ" => θ])
    writevtk(Ω, "results_gradtheta_$i", cellfields = ["∇(θ)" => ∇(θ)])
    writevtk(Ω, "results_atimesgradtheta_$i", cellfields = ["A*∇(θ)" =>
      A*∇(θ)
    ])
    writevtk(Ω, "results_atimesgradtheta+ei_$i", cellfields = ["A*∇(θ)" =>
      A * ( ∇(θ) + VectorValue(Int.(I(3)[:,i])) )
    ])

    for j=1:3
      Cbar = sum(∫( A * ( Int(i==j) + ∇(θ) ⋅ VectorValue(Int.(I(3)[:,j])) )) * dΩ)
      CbarA[i,j] = Cbar
    end

    Dbar = sum(∫( A ) * dΩ)
  end

  # make elliptic
  CbarA[3,3] = 10000

  display(CbarA)

  Cbar = TensorValue(CbarA)

  ahom(u,v) = ∫( Cbar ⋅ ∇(u) ⋅ ∇(v) ) * dΩ
  bhom(v) = ∫( Dbar * v ) * dΩ
  ophom = AffineFEOperator(ahom, bhom, Ug, V0)
  u0 = Gridap.solve(solver, ophom)
  writevtk(Ω, "results_u0", cellfields = ["u0" => u0])

  return [Cbar, Dbar]
end


function execute(Lx, Cbar, Dbar)
  Ly = 1
  ρ = 10
  tol = 1E-8
  maxit = 100  # TODO Check if converged
  σ = 0

  # domain = (0, Lx, 0, Lx, 0, Ly)  # 1)
  domain = (0, 1, 0, 1, 0, Ly)  # 2)
  partition = (round(Int, Lx * ρ), round(Int, Lx * ρ), round(Int, Ly * ρ))
  model = CartesianDiscreteModel(domain, partition; isperiodic=(false, false, false))  # quads
  # model = simplexify(CartesianDiscreteModel(domain, partition))  # tets
  writevtk(model, "model_$(replace(string(Lx),"."=>""))")

  labels = get_face_labeling(model)
  # add_tag_from_tags!(labels, "diri", collect(1:3^d-1))
  # add_tag_from_tags!(labels, "diri", collect(1:3^3-1))
  add_tag_from_tags!(labels, "diri", filter!(e->!(e∈[21,22]), collect(1:3^3-1)))

  order = 2
  reffe = ReferenceFE(lagrangian, Float64, order)
  V0 = TestFESpace(model, reffe; conformity=:H1, dirichlet_tags = ["diri"])

  Ug = TrialFESpace(V0, 0)

  degree = 3
  Ω = Triangulation(model)
  dΩ = Measure(Ω,degree)

  b(v) = ∫( 1 * v ) * dΩ  # dummy

  # A) numerical homogenized

  # A) calculate
  # lhs_hom(u,v) = ∫( Cbar ⋅ ∇(u) ⋅ ∇(v)  ) * dΩ
  # lhs_op_hom = AffineFEOperator(lhs_hom, b, Ug, V0)
  # stiffm_hom = assemble_matrix(lhs_hom, Ug, V0)

  # rhs_hom(u,v) = ∫( Dbar * u * v ) * dΩ
  # rhs_op_hom = AffineFEOperator(rhs_hom, b, Ug, V0)
  # massmat_hom = assemble_matrix_and_vector(rhs_hom, b, Ug, V0)[1]

  # @info "start eigensolve hom"
  # # @time eigvec_hom, eigval_hom, errors_hom, info_hom = IP(stiffm_hom, massmat_hom, σ, 1, 1, tol, maxit, 1)
  # @time eigvec_hom, eigval_hom, errors_hom, info_hom = IOI(stiffm_hom, massmat_hom, σ, howmany, 1, 1, tol, maxit, 1)
  # uh_hom = [
  #   FEFunction(lhs_op_hom.trial, eigvec_hom[:,i])
  #   for i=1:howmany
  # ]
  # @info eigval_hom
  # @info "end eigensolve hom"
  # writevtk(Ω, "results_hom_$(replace(string(Lx),"."=>""))", cellfields = [
  #   "uh$i" => uh_hom[i] for i=1:howmany
  # ])

  # B) analytical solution homogenized
  eigval_mat = [pi^2 * ((Cbar[1,1] * i^2 + Cbar[2,2] * j^2) / Dbar) for i=1:10, j=1:10]
  eigval_hom = sort(vec(eigval_mat))[1:howmany]
  eigval_indices = [findall(x->x==eigval_hom[i], eigval_mat)[1] for i=1:howmany]
  eigfuncs = [
    x->-2/sqrt(Dbar) * sin(1*pi*x[1])*sin(1*pi*x[2]),
    # x->-2/sqrt(Dbar) * sin(2*pi*x[1])*sin(1*pi*x[2]),
    # x->-2/sqrt(Dbar) * sin(1*pi*x[1])*sin(2*pi*x[2]),
    # x->-2/sqrt(Dbar) * sin(2*pi*x[1])*sin(2*pi*x[2]),
  ]  # TODO: Works only for howmany=4
  # TODO: Make parametric with anisotropic Cbar

  # @time eigvec_hom, eigval_hom, errors_hom, info_hom = IOI(stiffm_hom, massmat_hom, σ, howmany, 1, 1, tol, maxit, 1)
  global uh_hom = [
    Gridap.FESpaces.interpolate(eigfuncs[i], V0)
    for i=1:howmany
  ]
  @info eigval_hom
  @info "end eigensolve hom"
  writevtk(Ω, "results_hom_$(replace(string(Lx),"."=>""))", cellfields = [
    "uh$i" => uh_hom[i] for i=1:howmany
  ])


  # non homogenized
  # d = TensorValue(1, 0, 0, 0, 1, 0, 0, 0, 1)  # 1)
  d = TensorValue(1, 0, 0, 0, 1, 0, 0, 0, Lx^2)  # 2)
  # Anew(x) = A(x)  # 1)
  Anew(x) = w.A([Lx*x[1],Lx*x[2],x[3]])  # 2)
  lhs(u,v) = ∫( d ⋅ ∇(u) ⋅ ∇(v) * (x->Anew(x)) ) * dΩ
  lhs_op_nonhom = AffineFEOperator(lhs, b, Ug, V0)
  stiffm_nonhom = assemble_matrix(lhs, Ug, V0)

  rhs_nonhom(u,v) = ∫( u * v * (x->Anew(x)) ) * dΩ
  rhs_op_nonhom = AffineFEOperator(rhs_nonhom, b, Ug, V0)
  massmat_nonhom = assemble_matrix_and_vector(rhs_nonhom, b, Ug, V0)[1]

  @info "start eigensolve nonhom"
  # @time eigvec_nonhom, eigval_nonhom, errors_nonhom, info_nonhom = IP(stiffm_nonhom, massmat_nonhom, σ, 1, 1, tol, maxit, 1)
  @time eigvec_nonhom, eigval_nonhom, errors_nonhom, info_nonhom = IOI(stiffm_nonhom, massmat_nonhom, σ, howmany, 1, 1, tol, maxit, 1)
  eigvec_nonhom = eigvec_nonhom
  @info eigval_nonhom

  # rotate 2 and 3 eigvec
  eigvec_nonhom_tmp = nothing
  rotate = false
  if rotate
    eigvec_nonhom_tmp = [
      eigvec_nonhom[:,1],
      # (eigvec_nonhom[:,2]' * uh_hom[2].free_values * eigvec_nonhom[:,2] + eigvec_nonhom[:,3]' * uh_hom[2].free_values * eigvec_nonhom[:,3]),
      # (eigvec_nonhom[:,2]' * uh_hom[3].free_values * eigvec_nonhom[:,2] + eigvec_nonhom[:,3]' * uh_hom[3].free_values * eigvec_nonhom[:,3]),
      # eigvec_nonhom[:,4]
    ]
    eigvec_nonhom_tmp = [
      eigvec_nonhom_tmp[1] / sqrt(dot(eigvec_nonhom_tmp[1], massmat_nonhom, eigvec_nonhom_tmp[1])),
      # eigvec_nonhom_tmp[2] / sqrt(dot(eigvec_nonhom_tmp[2], massmat_nonhom, eigvec_nonhom_tmp[2])),
      # eigvec_nonhom_tmp[3] / sqrt(dot(eigvec_nonhom_tmp[3], massmat_nonhom, eigvec_nonhom_tmp[3])),
      # eigvec_nonhom_tmp[4] / sqrt(dot(eigvec_nonhom_tmp[4], massmat_nonhom, eigvec_nonhom_tmp[4]))
    ]
  else
    eigvec_nonhom_tmp = [
      eigvec_nonhom[:,1],
      # eigvec_nonhom[:,2],
      # eigvec_nonhom[:,3],
      # eigvec_nonhom[:,4]
    ]
    eigvec_nonhom_tmp = [
      eigvec_nonhom_tmp[1] / sqrt(dot(eigvec_nonhom_tmp[1], massmat_nonhom, eigvec_nonhom_tmp[1])),
      # eigvec_nonhom_tmp[2] / sqrt(dot(eigvec_nonhom_tmp[2], massmat_nonhom, eigvec_nonhom_tmp[2])),
      # eigvec_nonhom_tmp[3] / sqrt(dot(eigvec_nonhom_tmp[3], massmat_nonhom, eigvec_nonhom_tmp[3])),
      # eigvec_nonhom_tmp[4] / sqrt(dot(eigvec_nonhom_tmp[4], massmat_nonhom, eigvec_nonhom_tmp[4]))
    ]
  end
  uh_nonhom = [
    FEFunction(lhs_op_nonhom.trial, eigvec_nonhom_tmp[1]),
    # FEFunction(lhs_op_nonhom.trial, eigvec_nonhom_tmp[2]),
    # FEFunction(lhs_op_nonhom.trial, eigvec_nonhom_tmp[3]),
    # FEFunction(lhs_op_nonhom.trial, eigvec_nonhom_tmp[4])
  ]
  @info "end eigensolve nonhom"
  writevtk(Ω, "results_nonhom_$(replace(string(Lx),"."=>""))", cellfields = [
    "uh$i" => uh_nonhom[i] for i=1:howmany
  ])
  writevtk(Ω, "results_gradnonhom_$(replace(string(Lx),"."=>""))", cellfields = [
    "grad(uh)$i" => ∇(uh_nonhom[i]) for i=1:howmany
  ])


  # Eigenfunction Error
  e = uh_nonhom .- uh_hom
  el2 = [collect(
    [sqrt(sum( ∫( e[i]*e[i] )*dΩ ))./sqrt(sum( ∫( uh_hom[i]*uh_hom[i] )*dΩ )) for i=1:howmany])...
  ]
  eh1 = [collect(
    [sqrt(sum( ∫( e[i]*e[i] + ∇(e[i])⋅∇(e[i]) )*dΩ ))./sqrt(sum( ∫( uh_hom[i]*uh_hom[i] + ∇(uh_hom[i])⋅∇(uh_hom[i]) )*dΩ )) for i=1:howmany])...
  ]

  # Eigenvalues Error
  eevals = [collect((eigval_nonhom .- eigval_hom)/eigval_hom)...]

  # Eigenvalues into array
  eigval_nonhom = [collect(eigval_nonhom)...]

  eigval_hom = [collect(eigval_hom)...]

  return (el2, eh1, eevals, eigval_nonhom, eigval_hom)

end

df = DataFrame(
  Lx = Float64[],
  errorsl2 = Vector{Float64}[],
  errorsh1 = Vector{Float64}[],
  errorsevals = Vector{Float64}[],
  eigval_nonhom = Vector{Float64}[],
  eigval_hom = Vector{Float64}[],
)

# for Lx = [i for i=5:0.1:8]
# for Lx = [4]
# Cbar, Dbar = solve_corrector_problem()
for Lx = vcat([2^i for i=0:1], [2^1+0.1*i for i=1:20])
# for Lx = [2^i for i=0:4]
  @info Lx
  el2, eh1, errorsevals, eigval_nonhom, eigval_hom = execute(Lx, Cbar, Dbar)
  push!(df, (Lx, abs.(el2), abs.(eh1), abs.(errorsevals), eigval_nonhom, eigval_hom))
end

CSV.write("errorsh1.csv",
DataFrame(
  Dict("Lx" => df.Lx,
  ["errorsh1-$j" => [df.errorsh1[i][j] for i=1:size(df)[1]] for j=1:howmany]...)
  )
)
CSV.write("errorsl2.csv",
DataFrame(
  Dict("Lx" => df.Lx,
  ["errorsl2-$j" => [df.errorsl2[i][j] for i=1:size(df)[1]] for j=1:howmany]...)
  )
)
CSV.write("errorsevals.csv",
DataFrame(
  Dict("Lx" => df.Lx,
  ["errorsevals-$j" => [df.errorsevals[i][j] for i=1:size(df)[1]] for j=1:howmany]...)
  )
)

ratios = map(vals -> [vals[j] / vals[j+1] for j=1:howmany-1], df.eigval_nonhom)
CSV.write("ratioevals.csv",
  DataFrame(
    Dict(
      "Lx" => df.Lx,
      ["rat$j" => [ratios[i][j] for i=1:size(ratios)[1]] for j=1:howmany-1]...
    )
  )
)

CSV.write("eigval_hom.csv",
DataFrame(
  Dict("Lx" => df.Lx,
  ["eigval_hom-$j" => [df.eigval_hom[i][j] for i=1:size(df)[1]] for j=1:howmany]...)
  )
)
CSV.write("eigval_nonhom.csv",
DataFrame(
  Dict("Lx" => df.Lx,
  ["eigval_nonhom-$j" => [df.eigval_nonhom[i][j] for i=1:size(df)[1]] for j=1:howmany]...)
  )
)


plotpairs = collect(zip(
  [
    [map(e -> e[i], df[1:1:151,:].errorsl2,) for i=1:howmany],
    [map(e -> e[i], df[1:1:151,:].errorsh1) for i=1:howmany],
    [map(e -> e[i], df[1:1:151,:].errorsevals) for i=1:howmany]
  ],
  [
    ["L2 error$i" for i=1:howmany],
    ["H1 error$i" for i=1:howmany],
    ["lambda error$i" for i=1:howmany]
  ]
))

for pp in plotpairs
  display(plot(
    df[1:1:151,:].Lx,
    [
      [df[1:1:151,:].Lx.^-1],
      [0.2*df[1:1:151,:].Lx.^-2],
      pp[1]...
    ],
    axis=:log,
    # marker=:circle,
    labels=reshape([
      "order 1"
      "order 2"
      pp[2]...
    ], 1, :)
  ))
end

plot(
  df.Lx[2:end],
  [[map(e -> e[i], df.eigval_nonhom) for i=1:howmany][j][2:end] for j=1:howmany],
  marker=:circle,
  labels=reshape([
    ["lambda$i" for i=1:howmany]...
  ], 1, :)
)
