# # 2D Laplacian plus Potential
# Solves the Laplacian eigenvalue problem with zero Dirichlet boudnary conditons
# on a rectangular domain (x,y) ∈ Ω = [0,Lx]×[0,Ly]

using Printf
using LinearAlgebra
using SparseArrays
using DataFrames
using CSV

using ddEigenLab
using ddEigenLab.Geometries





verbose = false
imax = 10000
tol = 1E-10
Ly = 2^3
outputfolder = "."

if !isdir(outputfolder) mkdir(outputfolder) end


Lxs_exps = 3:7
Lxs = 2 .^Lxs_exps
rhos = [1,2,3]
# rhos = [1]
potentials = Dict(
  "constant" => Dict(),
  "harmonic" => Dict(),
  "normalized-harmonic" => Dict(),
  "triangular-well" => Dict(),
  "flipped-gaussian" => Dict(),
)
methods = [
  "IP-noshift",
  "IP-optimalshift",
  "IP-changedmatrix",
  "SD-optimalshift",
  "SD-changedmatrix",
  "LOPCG",
  "SD-split1",
  "SD-split2",
]

dfs = Dict(
  [methodname => Dict(
    [potname => DataFrame(
      "Lx" => Lxs,
      [string(rho) => ones(size(Lxs)[1]) for rho in rhos]...
    ) for (potname,_) in potentials]
  ) for methodname in methods]
)

for (iLx, Lx) in enumerate(Lxs)

  for rho in rhos

    Ω = Rectangle2D(Lx, Ly, rho, rho, Dict(:y => :d, :x => :d))
    potentials["constant"] = Dict(
      "V0" => 1,
      "Vx" => x -> 0,
      "Vy" => y -> 0,
      "Vxy" => (x,y) -> 1,
    )
    potentials["harmonic"] = Dict(
      "V0" => 1,
      "Vx" => x -> (x-Ω.Lx/2)^2,
      "Vy" => y -> (y-Ω.Ly/2)^2,
      "Vxy" => (x,y) -> 1 + (x-Ω.Lx/2)^2 + (y-Ω.Ly/2)^2,
    )
    potentials["normalized-harmonic"] = Dict(
      "V0" => 1,
      "Vx" => x -> ((x-Ω.Lx/2)^2) / ((Ω.Lx/2)^2),
      "Vy" => y -> ((y-Ω.Ly/2)^2) / ((Ω.Ly/2)^2),
      "Vxy" => (x,y) -> 1 + ((x-Ω.Lx/2)^2) / ((Ω.Lx/2)^2) + ((y-Ω.Ly/2)^2) / ((Ω.Ly/2)^2),
    )
    potentials["triangular-well"] = Dict(
      "V0" => 1,
      "Vx" => x -> abs(x-Ω.Lx/2),
      "Vy" => y -> abs(y-Ω.Ly/2),
      "Vxy" => (x,y) -> 1 + abs(x-Ω.Lx/2) + abs(y-Ω.Ly/2),
    )
    potentials["flipped-gaussian"] = Dict(
      "V0" => 1,
      "Vx" => x -> 1 - 1 * exp(-( (x-2*Ω.Lx/4)^2/(2*Ω.Lx))),
      "Vy" => y -> 1 - 1 * exp(-( (y-Ω.Ly/2)^2/(2*Ω.Ly))),
      "Vxy" => (x,y) -> 1 + 1 - 1 * exp(-( (x-2*Ω.Lx/4)^2/(2*Ω.Lx))) + 1 - 1 * exp(-( (y-Ω.Ly/2)^2/(2*Ω.Ly))),
    )

    for (potname, pot) in potentials
      L = NegativeLaplacePlusPotential(pot["V0"], pot["Vx"], pot["Vy"], pot["Vxy"])
      Lh = FD2D(L, Ω)

      λ∞ = eigen(Array(Lh.Ayl+Lh.AVyl+Lh.AV0yl)).values[1]
      P_λ∞ = inv(Array(Lh.A-λ∞*I(Ω.N)))
      Px = inv(Array(Lh.Ax+Lh.AVx))
      Py = inv(Array(Lh.Ay+Lh.AVy))
      P0 = inv(Array(Lh.AV0))

      methodname = "IP-noshift"
      x_i, lambda_i, errors, info = ddEigenLab.IP(Lh.A, 0, 1, 1, tol, imax, 0, 0*ones(size(Lh.A)[1]), verbose)
      println(string(Lx) * " " * string(rho) * " " * methodname * " " * potname * " " * string(info.it))
      dfs[methodname][potname][!, string(rho)][iLx] = info.it
      if !info.isconverged throw(ErrorException("not converged")) end

      methodname = "IP-optimalshift"
      x_i, lambda_i, errors, info = ddEigenLab.IP(Lh.A, λ∞, 1, 1, tol, imax, 0, 0*ones(size(Lh.A)[1]), verbose)
      println(string(Lx) * " " * string(rho) * " " * methodname * " " * potname * " " * string(info.it))
      dfs[methodname][potname][!, string(rho)][iLx] = info.it
      if !info.isconverged throw(ErrorException("not converged")) end

      methodname = "IP-changedmatrix"
      x_i, lambda_i, errors, info = ddEigenLab.IP((Lh.Ax+Lh.AVx)*(Lh.Ay+Lh.AVy)*Lh.A, 0, 1, 1, tol, imax, 0, 0*ones(size(Lh.A)[1]), verbose)
      println(string(Lx) * " " * string(rho) * " " * methodname * " " * potname * " " * string(info.it))
      dfs[methodname][potname][!, string(rho)][iLx] = info.it
      if !info.isconverged throw(ErrorException("not converged")) end

      methodname = "SD-optimalshift"
      x_i, lambda_i, errors, info = ddEigenLab.SD(Lh.A, P_λ∞, 1, 1, tol, imax, 0, 0*ones(size(Lh.A)[1]), verbose)
      println(string(Lx) * " " * string(rho) * " " * methodname * " " * potname * " " * string(info.it))
      dfs[methodname][potname][!, string(rho)][iLx] = info.it
      if !info.isconverged throw(ErrorException("not converged")) end

      methodname = "SD-changedmatrix"
      x_i, lambda_i, errors, info = ddEigenLab.SD(inv(P_λ∞), P_λ∞, 1, 1, tol, imax, 0, 0*ones(size(Lh.A)[1]), verbose)
      println(string(Lx) * " " * string(rho) * " " * methodname * " " * potname * " " * string(info.it))
      dfs[methodname][potname][!, string(rho)][iLx] = info.it
      if !info.isconverged throw(ErrorException("not converged")) end

      methodname = "SD-split1"
      x_i, lambda_i, errors, info = ddEigenLab.SD_split([Lh.Ax+Lh.AVx, Lh.Ay+Lh.AVy, Lh.AV0], [Px, Py, P0], 1, 1, tol, imax, 0, 0*ones(size(Lh.A)[1]), verbose)
      println(string(Lx) * " " * string(rho) * " " * methodname * " " * potname * " " * string(info.it))
      dfs[methodname][potname][!, string(rho)][iLx] = info.it
      if !info.isconverged throw(ErrorException("not converged")) end

      methodname = "LOPCG"
      x_i, lambda_i, errors, info = ddEigenLab.LOPCG(Lh.A, P_λ∞, 1, 1, tol, imax, 0, 0*ones(size(Lh.A)[1]), verbose)
      println(string(Lx) * " " * string(rho) * " " * methodname * " " * potname * " " * string(info.it))
      dfs[methodname][potname][!, string(rho)][iLx] = info.it
      if !info.isconverged throw(ErrorException("not converged")) end

      methodname = "SD-split2"
      x_i, lambda_i, errors, info = ddEigenLab.SD_split2([Lh.Ax+Lh.AVx, Lh.Ay+Lh.AVy, Lh.AV0], [Px, Py, P0], 1, 1, tol, imax, 0, 0*ones(size(Lh.A)[1]), verbose)
      println(string(Lx) * " " * string(rho) * " " * methodname * " " * potname * " " * string(info.it))
      dfs[methodname][potname][!, string(rho)][iLx] = info.it
      if !info.isconverged throw(ErrorException("not converged")) end

    end

  end

end


for (method, data) in dfs
  for (potname, _) in potentials
    println(method * "_" * potname)
    display(dfs[method][potname])
    CSV.write(outputfolder * "/" * method * "_" * potname * ".csv", dfs[method][potname])
  end
end


