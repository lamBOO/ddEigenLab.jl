using Gridap
using LinearAlgebra
using SparseArrays
using LinearMaps

using ddEigenLab

Lx = 50
Ly = 1
ρ = 50

d = 2

Z = 2
alpha = 5


#! Positive sum also works, although system becomes indefinite!
V(x) = 1 + Z^2 * (
  -sum([
    # exp(sum(-([x.data...]-[i-0.5,0.5]).^2))
    1/(norm(-([x.data...]-[i-0.5,0.5]),2) + 0.1) * exp(- alpha * norm(-([x.data...]-[i-0.5,0.5]),2))
   for i=1:Lx])
)


Lxp = Lyp = 1
domain = (5, 6, 0, Lyp)
partition = (Lxp * ρ, Lyp * ρ)
model2 = CartesianDiscreteModel(domain, partition; isperiodic=(true, false))
labels2 = get_face_labeling(model2)
add_tag_from_tags!(labels2, "diri", collect(1:3^d-1))
order = 1
reffe2 = ReferenceFE(lagrangian, Float64, order)
V02 = TestFESpace(model2, reffe2; conformity=:H1, dirichlet_tags = ["diri"])
Ug2 = TrialFESpace(V02, 0)
degree = 2
Ω2 = Triangulation(model2)
dΩ2 = Measure(Ω2, degree)

b2(v) = ∫( 1 * v ) * dΩ2  # dummy
a11(u,v) = ∫( ∇(v) ⋅ ∇(u) + V * u * v ) * dΩ2
op11 = AffineFEOperator(a11, b2, Ug2, V02)
stiffnessmat2 = assemble_matrix(a11, Ug2, V02)
a22(u,v) = ∫( v * u ) * dΩ2
op22 = AffineFEOperator(a22, b2, Ug2, V02)
massmat2 = assemble_matrix_and_vector(a22, b2, Ug2, V02)[1]

@info "start eigensolve periodic cell"

eigvec2, eigval2, errors, info = IP(stiffnessmat2, massmat2, 0, 1, 1, 1E-10, 1000)

@info "end eigensolve periodic cell"

domain = nothing
partition = nothing
if d==2
  domain = (0, Lx, 0, Ly)
  partition = (Lx * ρ, Ly * ρ)
elseif d==3
  domain = (0, Lx, 0, Ly, 0, Ly)
  partition = (Lx * ρ, Ly * ρ, Ly * ρ)
else
  error("we need d=2,3")
end
model = CartesianDiscreteModel(domain, partition; isperiodic=(false, false))  # quads
# model = simplexify(CartesianDiscreteModel(domain, partition))  # tets

labels = get_face_labeling(model)
add_tag_from_tags!(labels, "diri", collect(1:3^d-1))

order = 1
reffe = ReferenceFE(lagrangian, Float64, order)
V0 = TestFESpace(model, reffe; conformity=:H1, dirichlet_tags = ["diri"])

Ug = TrialFESpace(V0, 0)

degree = 2
Ω = Triangulation(model)
dΩ = Measure(Ω,degree)

b(v) = ∫( 1 * v ) * dΩ  # dummy

a1(u,v) = ∫( ∇(v) ⋅ ∇(u) + V * u * v ) * dΩ
op1 = AffineFEOperator(a1, b, Ug, V0)
stiffnessmat = assemble_matrix(a1, Ug, V0)

a2(u,v) = ∫( v * u ) * dΩ
op2 = AffineFEOperator(a2, b, Ug, V0)
massmat = assemble_matrix_and_vector(a2, b, Ug, V0)[1]

@info "start eigensolve"

maxit=50

u0 = Gridap.FESpaces.interpolate(
  # x->sin(1*pi*x[1]/Lx)*sin(1*pi*x[2]/Ly)
  x->1
, V0)
writevtk(Ω, "results_u0", cellfields = ["u0" => u0])
x0 = u0.free_values
σ = eigval2
# σ = 0
@time eigvec, eigval, errors, info = IP(stiffnessmat, massmat, σ, 1, 1, 1E-10, maxit, 1, xᵢ=x0)
# @time eigvec, eigval, errors, info = LOPCG(stiffnessmat, massmat, (stiffnessmat - σ * massmat), 1, 1, 1E-10, maxit, 1, xᵢ=x0)
# @time eigvec, eigval, errors, info = SD(stiffnessmat, massmat, (stiffnessmat - σ * massmat), 1, 1, 1E-10, maxit, 1)

@info "end eigensolve"

writevtk(model, "model")
uh = FEFunction(op1.trial, eigvec);
writevtk(Ω, "results", cellfields = ["uh" => uh])

Vgrid = Gridap.FESpaces.interpolate(V, V0)
writevtk(Ω, "pot", cellfields = ["pot" => Vgrid])

# plotErrors(errors, "Errors")
