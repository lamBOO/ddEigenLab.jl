using Gridap
using LinearAlgebra
using SparseArrays
using LinearMaps
using Distributed
using IterativeSolvers

using ddEigenLab

import Base.\

Lx = 30
Ly = 1
ρ = 10

d = 2

Z = 10
# V(x) = 0
V(x) = Z^2 * (sin(x[1]*π)^2 * 10*sin(x[2]*π)^2)  # works
# V(x) = Z^2 * (sin(x[1]*π)^2 * sin(x[2]*π)^2 + 1*(x[1] % 1))  # doesnt work
# ZZ = 2
# alpha = 5
# # V(x) = Z^2 * sum([exp(-(x-(i-0.5))^2) for i=1:Lx])
# V(x) = 1 + ZZ^2 * (
#   -sum([
#     # exp(sum(-([x.data...]-[i-0.5,0.5]).^2))
#     1/(norm(-([x.data...]-[i-0.5,0.5]),2) + 0.1) * exp(- alpha * norm(-([x.data...]-[i-0.5,0.5]),2))
#    for i=1:Lx])
# )

Lxp = Lyp = 1
domain = (0, 1, 0, Lyp)
partition = (Lxp * ρ, Lyp * ρ)
model2 = CartesianDiscreteModel(domain, partition; isperiodic=(true, false))
labels2 = get_face_labeling(model2)
add_tag_from_tags!(labels2, "diri", collect(1:3^d-1))
order = 1
reffe2 = ReferenceFE(lagrangian, Float64, order)
V02 = TestFESpace(model2, reffe2; conformity=:H1, dirichlet_tags = ["diri"])
Ug2 = TrialFESpace(V02, 0)
degree = 2
Ω2 = Triangulation(model2)
dΩ2 = Measure(Ω2, degree)

b2(v) = ∫( 1 * v ) * dΩ2  # dummy
a11(u,v) = ∫( ∇(v) ⋅ ∇(u) + V * u * v ) * dΩ2
op11 = AffineFEOperator(a11, b2, Ug2, V02)
stiffnessmat2 = assemble_matrix(a11, Ug2, V02)
a22(u,v) = ∫( v * u ) * dΩ2
op22 = AffineFEOperator(a22, b2, Ug2, V02)
massmat2 = assemble_matrix_and_vector(a22, b2, Ug2, V02)[1]

@info "start eigensolve periodic cell"

eigvec2, eigval2, errors, info = IP(stiffnessmat2, massmat2, 0, 1, 1, 1E-10, 1000)

uper = FEFunction(op22.trial, eigvec2);
writevtk(Ω2, "uper", cellfields = ["uper" => uper])

@info "end eigensolve periodic cell"

domain = nothing
partition = nothing
if d==2
  domain = (0, Lx, 0, Ly)
  partition = (Lx * ρ, Ly * ρ)
elseif d==3
  domain = (0, Lx, 0, Ly, 0, Ly)
  partition = (Lx * ρ, Ly * ρ, Ly * ρ)
else
  error("we need d=2,3")
end
model = CartesianDiscreteModel(domain, partition; isperiodic=(false, false))  # quads
# model = simplexify(CartesianDiscreteModel(domain, partition))  # tets

labels = get_face_labeling(model)
add_tag_from_tags!(labels, "diri", collect(1:3^d-1))

order = 1
reffe = ReferenceFE(lagrangian, Float64, order)
V0 = TestFESpace(model, reffe; conformity=:H1, dirichlet_tags = ["diri"])

Ug = TrialFESpace(V0, 0)

degree = 2
Ω = Triangulation(model)
dΩ = Measure(Ω,degree)

b(v) = ∫( 1 * v ) * dΩ  # dummy

a1(u,v) = ∫( ∇(v) ⋅ ∇(u) + V * u * v ) * dΩ
op1 = AffineFEOperator(a1, b, Ug, V0)
stiffnessmat = assemble_matrix(a1, Ug, V0)

a2(u,v) = ∫( v * u ) * dΩ
op2 = AffineFEOperator(a2, b, Ug, V0)
massmat = assemble_matrix_and_vector(a2, b, Ug, V0)[1]

@info "start eigensolve"

maxit=100

# Setup preconditioner
@info "start preconditioner"
M = Lx ÷ 2 - 1
uinf = Array{Any}(undef, M)
xinf = Array{Vector{Float64}}(undef, M)

@time Threads.@threads for i = 1:M
  @info Threads.threadid()
  u0 = Gridap.FESpaces.interpolate(
  # x->sin(1*pi*x[1]/Lx)*sin(1*pi*x[2]/Ly)
  # x->sin(1*pi*x[2]/Ly)
  x->sin(i*pi*x[1]/Lx) * uper(VectorValue(x.data .% 1 .+ [0,0]))
  # x->uper(VectorValue(x.data .% 1))
  # x->1
, V0)
  x0 = u0.free_values
  x0 = x0 ./ sqrt(dot(x0, massmat, x0))
  # x0 = x0 ./ norm(x0)
  u0 = FEFunction(op1.trial, x0);
  uinf[i] = u0
  xinf[i] = x0
  writevtk(Ω, "results_u0_$i", cellfields = ["u0" => u0])
end

P = sum([
  x*(x)' / (x'*(x))
  for x in xinf
])
# UU = svd(P).U[:,1:rank(P)]
UU = hcat(xinf...)
# UU = UU[:,[2,1,3]]  # Get order right, lowest first
PP = I(size(massmat)[1]) - (UU * UU')
# UU = hcat(x0,x1,x2,x3,x4)
# UU = hcat(xinf...)
VV = svd(PP).U[:,M+1:rank(PP)]  # TODO Attention, unsafe
# VV = hcat(map(v -> v./ sqrt(dot(v, massmat, v)), [VV[:,i] for i=1:size(VV)[2]])...)   # VV are euclidian nnormalized
Q = hcat(UU, VV)

Atilde = Array(stiffnessmat - eigval2 * massmat)


# best would be to use 1/L^2 * nu^{(m)} I woudl say
# Negative Aai eigenvalue lead to no conv for LOPCG
Aai = vcat(
  hcat(diagm(diag(UU' * (Atilde) * UU)), 0 * UU' * (Atilde) * VV),  # doesnt work
  # hcat(diagm([x' * (Atilde) * x for x in xinf]), 0 * UU' * (Atilde) * VV),
  # hcat(I(M), 0 * UU' * (Atilde) * VV),  # LOPCG struggles, IP works
  # hcat(diagm(test), 0 * UU' * (Atilde) * VV),  # doesnt work
  # hcat(diagm([x' * (Atilde) * x for x in [x0,x1]]), UU' * (Atilde) * VV),
  # hcat(UU' * (Atilde) * UU, 1 * UU' * (Atilde) * VV),
  hcat(0 * VV' * (Atilde) * UU, VV' * (Atilde) * VV),
)
Aa = Q * Aai * inv(Q)

# @show eigen(Atilde).values[1]
# @show eigen((UU' * (Atilde) * UU)).values[1]
# @show eigen((UU' * (Atilde) * UU)).values[end]
# @show eigen((VV' * (Atilde) * VV)).values[1]
# @show norm(inv(Aa) - inv(Atilde))

struct ApproximateEigenSchurPrecond7
  A
  B
  UU
  VV
  P
  Pperp
  function ApproximateEigenSchurPrecond7(A,B,UU)
    M = size(UU)[2]
    P = UU * UU'
    Pperp = I(size(A)[1]) - UU * inv(UU' * UU) * UU'
    VV = svd(Pperp).U[:, 1:end-M]  #!!!! Attention
    new(A, B, UU, VV, P, Pperp)
  end
end
function \(prec::ApproximateEigenSchurPrecond7, b)
  # P = prec.UU*inv(prec.UU'*(prec.B*prec.UU))*prec.UU'
  # P = prec.UU*prec.UU'
  # # Pperp = prec.B - P
  # Pperp = I(size(massmat)[1]) - P
  # Pperpf = factorize(Pperp)

  # VV = svd(Pperp).U[:,M+1:end]

  # return (Pperp * prec.A) \ (Pperp * b)
  # x, h = cg(prec.A, b, log=true, verbose=false, reltol=1E-14, maxiter=10000)

  #* 1) WORKS with fixed iterations
  # x1 = prec.VV'*ones(size(prec.A)[1])
  # x1, h1 = cg!(x1, prec.VV'*prec.A*prec.VV, prec.VV'*b, log=true, verbose=false,
  # reltol=1E-14,
  # maxiter=10000)
  # x2 = prec.UU'*ones(size(prec.A)[1])
  # x2, h2 = cg!(x2, prec.UU'*prec.A*prec.UU, prec.UU'*b, log=true, verbose=false,
  # reltol=1E-14,
  # maxiter=10000)

  #* 2) projector way WORKS when I - U G-1 UT, but LA way fails then...
  x1 = prec.Pperp'*ones(size(prec.A)[1])
  x1, h1 = cg!(x1, prec.Pperp'*prec.A*prec.Pperp, prec.Pperp'*b, log=true, verbose=false, reltol=1E-14, maxiter=10000)
  x2 = prec.P'*ones(size(prec.A)[1])
  x2, h2 = cg!(x2, prec.P'*prec.A*prec.P, prec.P'*b, log=true, verbose=false, reltol=1E-14, maxiter=10000)

  # x = Pperp*prec.UU[:,1]
  # x, h = cg!(x, Pperp*prec.A*Pperp, Pperp*b, Pl=Pperpf, log=true, verbose=false, reltol=1E-14, maxiter=10000)
  # x, h = lsmr!(x, Pperp*prec.A*Pperp, Pperp*b, log=true, verbose=false, maxiter=10000)
  # x, h = cg(Pperp*prec.A*Pperp, Pperp*b, log=true, verbose=false, reltol=1E-14, maxiter=10000)
  @info h1
  @info h2
  # return Pperp*x

  #! 1) linear algebra with basis
  # return prec.VV*x1 + prec.UU*x2
  #! 2) projector style w.o. basis
  return prec.Pperp*x1 + prec.P*x2

  # return prec.A \ b
end

prec = ApproximateEigenSchurPrecond7(Atilde, massmat, UU)

@info "end preconditioner"

σ = eigval2

# @time eigvec, eigval, errors, info = IP(Aa*stiffnessmat, Aa*massmat, σ, 1, 1, 1E-10, maxit, 1)
@time eigvec, eigval, errors, info = LOPCG(stiffnessmat, massmat, prec, 1, 1, 1E-8, maxit, 1)
# @time eigvec, eigval, errors, info = SD(stiffnessmat, massmat, Aa, 1, 1, 1E-10, maxit, 1)


@info "end eigensolve"

writevtk(model, "model")

uh = FEFunction(op1.trial, eigvec);
writevtk(Ω, "results", cellfields = ["uh" => uh])

Vgrid = Gridap.FESpaces.interpolate(V, V0)
writevtk(Ω, "pot", cellfields = ["pot" => Vgrid])


plotErrors(errors, "Errors")
