using LinearAlgebra
using IterativeSolvers

using ddEigenLab




function solve(A, maxit)
  N = size(A)[1]
  x = ones(N)
  fac = 0.1/5.2/2.8
  for i=1:maxit
    K = hcat(A[:,1:N-1], -x)
    # @show K
    @show cond(Array(A))
    @show cond(Array(K))
    b = -fac*Array(A[:,end])

    y = K \ b
    # y, h = cg(K, b, log=true); @show h
    # y, h = gmres(K, b, log=true); @show h
    # y = cg(K, b, abstol=1E-14)

    x = vcat(y[1:end-1], fac)

    res = A*x - (x'*A*x)/(x'*x) * x
    @show norm(res)
  end
  return x
end

Lx = 20
Ly = 1
ρ = 3
Ω = Rectangle2D(Lx, Ly, ρ, ρ)
Vxy(x,y) = 0
V0 = 0
Vx(x) = 0
Vy(y) = 0
L = NegativeLaplacePlusPotential(V0, Vx, Vy, Vxy)
bcs = Dict(:x => :d, :y => :d)
Lh = FD2D(L, Ω, bcs)

lambdainf = eigen(Array(Lh.Ay)).values[1]
# A = Lh.A
A = Lh.A - lambdainf * I(size(Lh.A)[1])


# A = 1.0*[2 -1; -1 3]
eigvec = solve(A, 10)
