using Printf
using LinearAlgebra
using SparseArrays
using IterativeSolvers
using Plots

using ddEigenLab
using ddEigenLab.Geometries

LyRange = [2^i for i=1:5]
iterDict = Dict()

for Ly in LyRange

  LxRange = [2^i for i=1:10]
  iterArray = []
  for Lx in LxRange
    Ω = Rectangle2D(Lx, Ly, 1, 1)
    V0 = 0
    Vx(x) = 0
    Vy(y) = 0
    L = NegativeLaplacePlusPotential(V0, Vx, Vy)
    Lh = FD2D(L, Ω)

    N = size(Lh.A)[1]
    f = ones(N)

    c, log = cg(Lh.A, f, Pl = Identity(), tol=1E-10, log=true, verbose=true)
    print(log)

    push!(iterArray, log.iters)
  end

  iterDict[Ly] = iterArray

end

plot(
  LxRange,
  [iterDict[Ly] for Ly in LyRange],
  title = "Solve FD: -Laplace(u)=1 on [0,Lx]x[0,Ly] with CG (no prec.)",
  xaxis = (:log,"Lx"),
  yaxis = (:log,"iterations to converge (tol=1E-10)"),
  marker = (:circle, 4),
  label = hcat(["Ly=$(Ly)" for Ly in LyRange]...),
  legend=:bottomright
)
