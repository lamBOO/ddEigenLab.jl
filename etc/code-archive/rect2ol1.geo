delta = 0.1;

Point(1) = {0, 1, 0, 0.1};
Point(2) = {1-delta, 1, 0, 0.1};
Point(3) = {1+delta, 1, 0, 0.1};
Point(4) = {2, 1, 0, 0.1};
Point(5) = {2, 0, 0, 0.1};
Point(6) = {1+delta, 0, 0, 0.1};
Point(7) = {1-delta, 0, 0, 0.1};
Point(8) = {0, 0, 0, 0.1};
Physical Point("outer")     = {1:8};


Line(11) = {1, 2};
Line(12) = {2, 7};
Line(13) = {7, 8};
Line(14) = {8, 1};

Line(21) = {2, 3};
Line(22) = {3, 6};
Line(23) = {6, 7};

Line(31) = {3, 4};
Line(32) = {4, 5};
Line(33) = {5, 6};

Physical Line("outer")     = {11,21,31,32,33,23,13,14};
Physical Line("ol")     = {12,22};

Line Loop(11) = {11:14};
Plane Surface(11) = {11};
Line Loop(21) = {21:23,-12};
Plane Surface(21) = {21};
Line Loop(31) = {31:33,-22};
Plane Surface(31) = {31};

Physical Surface("ld",1) = {11};
Physical Surface("ol",2) = {21};
Physical Surface("rd",3) = {31};
