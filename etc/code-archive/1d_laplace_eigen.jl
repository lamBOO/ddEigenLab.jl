using LinearAlgebra
using Plots

function λ(x, A)
    ((x' * A * x)/(x' * x))[1]
end
a = 0
b = 10
L = b-a # size
N = 5*L # number of cells!
h = L/N # grid spacing
x = range(a, stop=b, length=N+1)'

dA = Diagonal(2I(N-1))
dAp1 = diagm(1 => -ones(N-2))
dAm1 = diagm(-1 => -ones(N-2))
A = (dA + dAp1 + dAm1)
A = A/h^2
# display(A)

# Preconditioner
# P = I(N-1) # DOESNT WORK, practical preconditioner, approx stiffnes L=I [Knyazev1998] doesnt work
# P = inv(diag(diag(A)))
# P = inv(A - 0.25 * eye(N-1)) % perfect preconditioner [Knyazev1998]
P = inv(A)

delta=1
tau=1

x_i = ones(N-1, 1);
x_i = x_i/norm(x_i, 2);

print("Outer iterations: ")
for i=1:100
  print(i, " ")
  global x_i

  # 1) Collapsed inverse power
  # x_i = A\x_i; % x_i = A^(-1) x_i
  # x_i = x_i/norm(x_i,2);

  # 2) Residual based inverse power
  x_i = tau * x_i - delta * P * (A * x_i - λ(x_i, A) * x_i);
  x_i = x_i/norm(x_i, 2);

  # Plotting
  # display(plot(x',[0; x_i; 0]))
  # sleep(0.9)
end

# Error
eigen_A = eigen(A)

error_val = eigen_A.values[1] - λ(x_i, A)
Linf_val = max(broadcast(abs,error_val))

error_vec = eigen_A.vectors[:,1]-x_i
L1_vec = sum(broadcast(abs, error_vec))
L2_vec = sqrt(sum(error_vec.^2))
Linf_vec = max(broadcast(abs, error_vec)...)

println("\nERRORS:\n")
println("L∞(λ)=", Linf_val)
println("L1(v)=", L1_vec)
println("L2(v)=", L2_vec)
println("L∞(v)=", Linf_vec)