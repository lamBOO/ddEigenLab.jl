using Gridap
using LinearAlgebra
using TickTock

function main(d :: Integer)
# Manufactured solution
u_ex_expression = x->x[1]^3
ddu_ex_expression = x->-6*x[1]

# Geometric setup
@info "Start geometry", tick()
ρ = 3  # 1/h
# d = 4
L = 1
domain = tuple(vcat([[0,L] for dd=1:d]...)...)  # [0,L]^d
partition = tuple(vcat([ρ for dd=1:d])...)  # points per dimension
model = CartesianDiscreteModel(domain, partition)
labels = get_face_labeling(model)
add_tag_from_tags!(labels, "diri", collect(1:3^d-1))
refel = ReferenceFE(lagrangian, Float64, 1)
V = TestFESpace(model, refel; conformity=:H1, dirichlet_tags = ["diri"])
U = TrialFESpace(V, x->u_ex_expression(x))
Ω = Triangulation(model)
dΩ = Measure(Ω, 2)
@info "End geometry", tock()

# bilinear form % linear functional
@info "Start assemble", tick()
a(u,v) = ∫( ∇(v) ⋅ ∇(u) ) * dΩ
b(v) = ∫( ddu_ex_expression * v ) * dΩ
op = AffineFEOperator(a, b, U, V)
@info "End assemble", tock()

# solution process: direct LU solver
@info "Start solution", tick()
ls = LUSolver()
solver = LinearFESolver(ls)
uh = Gridap.solve(solver,op)
@info "End solution", tock()

# Output solution for Paraview
# writevtk(Ω,"results",cellfields=["uh"=>uh])

# relative errors
# @info "Start errors", tick()
# tick()
# u_ex_interp = interpolate(u_ex_expression, V)
# e = uh - u_ex_expression
# e_l2 = sqrt(sum(
#   ∫( e*e )*dΩ
# ))./sqrt(sum(
#   ∫( u_ex_interp*u_ex_interp )*dΩ
# ))
# e_h1 = sqrt(sum(
#   ∫( e*e + ∇(e)⋅∇(e) )*dΩ
# ))./sqrt(sum(
#   ∫( u_ex_interp*u_ex_interp + ∇(u_ex_interp)⋅∇(u_ex_interp) )*dΩ
# ))
# @show e_l2, e_h1;
# @info "End errors", tock()

return uh
end

@time result = main(6)
