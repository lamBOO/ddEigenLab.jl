using LinearAlgebra

A = [1 2 3; 2 4 5; 3 5 6] + I(3)

X = Array(I(3)[:,1:2])
D = zeros(2)
for k=1:10
  global X,D
  X, _ = qr(A * X)
  X = Array(X)
  D = X' * A * X


  X = X[:,1:2]
end

@show X
@show eigen(A).vectors




# # VV, RR = qr(Z)

# Y = A * VV
# H = VV' * Y

# res = Y - VV * H

# # for k=1:10
# # end
