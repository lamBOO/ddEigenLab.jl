using IterativeSolvers
using AlgebraicMultigrid

AA = poisson(20)
N = size(AA)[1]
x0 = I(N)[:,1]
P = x0*x0'
PP = I(N) - P

bb = ones(N)

x1,h1 = cg(AA, bb, log=true)
x2,h2 = cg(PP*AA*PP, PP*bb, log=true)
x3,h3 = cg(P*AA*P, P*bb, log=true)
