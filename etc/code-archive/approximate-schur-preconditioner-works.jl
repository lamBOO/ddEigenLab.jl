using Gridap
using LinearAlgebra
using SparseArrays
using LinearMaps
using Distributed

using ddEigenLab

Lx = 16
Ly = 1
ρ = 10

d = 2

Z = 10
# V(x) = 0
# V(x) = Z^2 * (sin(x[1]*π)^2 * 10*sin(x[2]*π)^2)  # works
V(x) = Z^2 * (sin(x[1]*π)^2 * sin(x[2]*π)^2 + 1*(x[1] % 1))  # doesnt work
# ZZ = 2
# alpha = 5
# # V(x) = Z^2 * sum([exp(-(x-(i-0.5))^2) for i=1:Lx])
# V(x) = 1 + ZZ^2 * (
#   -sum([
#     # exp(sum(-([x.data...]-[i-0.5,0.5]).^2))
#     1/(norm(-([x.data...]-[i-0.5,0.5]),2) + 0.1) * exp(- alpha * norm(-([x.data...]-[i-0.5,0.5]),2))
#    for i=1:Lx])
# )

Lxp = Lyp = 1
domain = (0, 1, 0, Lyp)
partition = (Lxp * ρ, Lyp * ρ)
model2 = CartesianDiscreteModel(domain, partition; isperiodic=(true, false))
labels2 = get_face_labeling(model2)
add_tag_from_tags!(labels2, "diri", collect(1:3^d-1))
order = 1
reffe2 = ReferenceFE(lagrangian, Float64, order)
V02 = TestFESpace(model2, reffe2; conformity=:H1, dirichlet_tags = ["diri"])
Ug2 = TrialFESpace(V02, 0)
degree = 2
Ω2 = Triangulation(model2)
dΩ2 = Measure(Ω2, degree)

b2(v) = ∫( 1 * v ) * dΩ2  # dummy
a11(u,v) = ∫( ∇(v) ⋅ ∇(u) + V * u * v ) * dΩ2
op11 = AffineFEOperator(a11, b2, Ug2, V02)
stiffnessmat2 = assemble_matrix(a11, Ug2, V02)
a22(u,v) = ∫( v * u ) * dΩ2
op22 = AffineFEOperator(a22, b2, Ug2, V02)
massmat2 = assemble_matrix_and_vector(a22, b2, Ug2, V02)[1]

@info "start eigensolve periodic cell"

eigvec2, eigval2, errors, info = IP(stiffnessmat2, massmat2, 0, 1, 1, 1E-10, 1000)

uper = FEFunction(op22.trial, eigvec2);
writevtk(Ω2, "uper", cellfields = ["uper" => uper])

@info "end eigensolve periodic cell"

domain = nothing
partition = nothing
if d==2
  domain = (0, Lx, 0, Ly)
  partition = (Lx * ρ, Ly * ρ)
elseif d==3
  domain = (0, Lx, 0, Ly, 0, Ly)
  partition = (Lx * ρ, Ly * ρ, Ly * ρ)
else
  error("we need d=2,3")
end
model = CartesianDiscreteModel(domain, partition; isperiodic=(false, false))  # quads
# model = simplexify(CartesianDiscreteModel(domain, partition))  # tets

labels = get_face_labeling(model)
add_tag_from_tags!(labels, "diri", collect(1:3^d-1))

order = 1
reffe = ReferenceFE(lagrangian, Float64, order)
V0 = TestFESpace(model, reffe; conformity=:H1, dirichlet_tags = ["diri"])

Ug = TrialFESpace(V0, 0)

degree = 2
Ω = Triangulation(model)
dΩ = Measure(Ω,degree)

b(v) = ∫( 1 * v ) * dΩ  # dummy

a1(u,v) = ∫( ∇(v) ⋅ ∇(u) + V * u * v ) * dΩ
op1 = AffineFEOperator(a1, b, Ug, V0)
stiffnessmat = assemble_matrix(a1, Ug, V0)

a2(u,v) = ∫( v * u ) * dΩ
op2 = AffineFEOperator(a2, b, Ug, V0)
massmat = assemble_matrix_and_vector(a2, b, Ug, V0)[1]

@info "start eigensolve"

maxit=100

u0 = Gridap.FESpaces.interpolate(
  # x->sin(1*pi*x[1]/Lx)*sin(1*pi*x[2]/Ly)
  # x->sin(1*pi*x[2]/Ly)
  x->sin(1*pi*x[1]/Lx) * uper(VectorValue(x.data .% 1 .+ [0,0]))
  # x->uper(VectorValue(x.data .% 1))
  # x->1
, V0)
x0 = u0.free_values
# x0 = x0 ./ sqrt(dot(x0, massmat, x0))
# x0 = x0 ./ norm(x0)

u1 = Gridap.FESpaces.interpolate(
  # x->sin(1*pi*x[1]/Lx)*sin(1*pi*x[2]/Ly)
  # x->sin(1*pi*x[2]/Ly)
  x->sin(2*pi*x[1]/Lx) * uper(VectorValue(x.data .% 1 .+ [0,0]))
  # x->uper(VectorValue(x.data .% 1))
  # x->1
, V0)
x1 = u1.free_values
# x1 = x1 ./ sqrt(dot(x1, massmat, x1))
# x1 = x1 ./ norm(x1)

u2 = Gridap.FESpaces.interpolate(
  # x->sin(1*pi*x[1]/Lx)*sin(1*pi*x[2]/Ly)
  # x->sin(1*pi*x[2]/Ly)
  x->sin(3*pi*x[1]/Lx) * uper(VectorValue(x.data .% 1 .+ [0,0]))
  # x->uper(VectorValue(x.data .% 1))
  # x->1
, V0)
x2 = u2.free_values
# x2 = x2 ./ sqrt(dot(x2, massmat, x2))
# x2 = x2 ./ norm(x2)

u3 = Gridap.FESpaces.interpolate(
  # x->sin(1*pi*x[1]/Lx)*sin(1*pi*x[2]/Ly)
  # x->sin(1*pi*x[2]/Ly)
  x->sin(4*pi*x[1]/Lx) * uper(VectorValue(x.data .% 1 .+ [0,0]))
  # x->uper(VectorValue(x.data .% 1))
  # x->1
, V0)
x3 = u3.free_values
# x2 = x2 ./ sqrt(dot(x2, massmat, x2))
# x3 = x3 ./ norm(x3)

u4 = Gridap.FESpaces.interpolate(
  # x->sin(1*pi*x[1]/Lx)*sin(1*pi*x[2]/Ly)
  # x->sin(1*pi*x[2]/Ly)
  x->sin(5*pi*x[1]/Lx) * uper(VectorValue(x.data .% 1 .+ [0,0]))
  # x->uper(VectorValue(x.data .% 1))
  # x->1
, V0)
x4 = u4.free_values
# x2 = x2 ./ sqrt(dot(x2, massmat, x2))
# x4 = x4 ./ norm(x4)

u5 = Gridap.FESpaces.interpolate(
  # x->sin(1*pi*x[1]/Lx)*sin(1*pi*x[2]/Ly)
  # x->sin(1*pi*x[2]/Ly)
  x->sin(6*pi*x[1]/Lx) * uper(VectorValue(x.data .% 1 .+ [0,0]))
  # x->uper(VectorValue(x.data .% 1))
  # x->1
, V0)
x5 = u5.free_values
# x2 = x2 ./ sqrt(dot(x2, massmat, x2))
# x5 = x5 ./ norm(x5)

u6 = Gridap.FESpaces.interpolate(
  # x->sin(1*pi*x[1]/Lx)*sin(1*pi*x[2]/Ly)
  # x->sin(1*pi*x[2]/Ly)
  x->sin(7*pi*x[1]/Lx) * uper(VectorValue(x.data .% 1 .+ [0,0]))
  # x->uper(VectorValue(x.data .% 1))
  # x->1
, V0)
x6 = u6.free_values
# x6 = x6 ./ sqrt(dot(x6, massmat, x6))
# x6 = x6 ./ norm(x6)

# u3 = Gridap.FESpaces.interpolate(
#   # x->sin(1*pi*x[1]/Lx)*sin(1*pi*x[2]/Ly)
#   # x->sin(1*pi*x[2]/Ly)
#   x->sin(4*pi*x[1]/Lx) * uper(VectorValue(x.data .% 1 .+ [0,0]))
#   # x->uper(VectorValue(x.data .% 1))
#   # x->1
# , V0)
# x3 = u3.free_values
# x3 = x3 ./ sqrt(dot(x3, massmat, x3))

u0 = FEFunction(op1.trial, x0);
writevtk(Ω, "results_u0", cellfields = ["u0" => u0])
u1 = FEFunction(op1.trial, x1);
writevtk(Ω, "results_u1", cellfields = ["u1" => u1])
u2 = FEFunction(op1.trial, x2);
writevtk(Ω, "results_u2", cellfields = ["u2" => u2])
u3 = FEFunction(op1.trial, x3);
writevtk(Ω, "results_u3", cellfields = ["u3" => u3])
u4 = FEFunction(op1.trial, x4);
writevtk(Ω, "results_u4", cellfields = ["u4" => u4])



# Approximate Schur
P = (
  + x0*(x0)' / (x0'*(x0))
  + x1*(x1)' / (x1'*(x1))
  + x2*(x2)' / (x2'*(x2))
  + x3*(x3)' / (x3'*(x3))
  + x4*(x4)' / (x4'*(x4))
  + x5*(x5)' / (x5'*(x5))
  + x6*(x6)' / (x6'*(x6))
)
UU = svd(P).U[:,1:rank(P)]
# UU = UU[:,[2,1,3]]  # Get order right, lowest first
PP = I(size(massmat)[1]) - (UU * UU')
# UU = hcat(x0,x1,x2,x3,x4)
UU = hcat(x0,x1,x2,x3,x4,x5,x6)
VV = svd(PP).U[:,1:rank(PP)]
Q = hcat(UU, VV)

Atilde = inv(Array(massmat))*Array(stiffnessmat - eigval2 * massmat)


# best would be to use 1/L^2 * nu^{(m)} I woudl say
# Negative Aai eigenvalue lead to no conv for LOPCG
Aai = vcat(
  # hcat(diagm(diag(UU' * (Atilde) * UU)), UU' * (Atilde) * VV),  # doesnt work
  hcat(diagm([x' * (Atilde) * x for x in [x0,x1,x2,x3,x4,x5,x6]]), 0 * UU' * (Atilde) * VV),
  # hcat(diagm(test), 0 * UU' * (Atilde) * VV),  # doesnt work
  # hcat(diagm([x' * (Atilde) * x for x in [x0,x1]]), UU' * (Atilde) * VV),
  # hcat(UU' * (Atilde) * UU, UU' * (Atilde) * VV),
  hcat(0 * VV' * (Atilde) * UU, VV' * (Atilde) * VV),
)
Aa = Q * Aai * inv(Q)

@show eigen(Atilde).values[1]
@show eigen((UU' * (Atilde) * UU)).values[1]
@show eigen((UU' * (Atilde) * UU)).values[end]
@show eigen((VV' * (Atilde) * VV)).values[1]
@show norm(inv(Aa) - inv(Atilde))







σ = eigval2
# # σ = 0
# # @time eigvec, eigval, errors, info = IP((I(size(stiffnessmat)[1]) - eigval*x0 * (massmat * x0)') * stiffnessmat, massmat, σ, 1, 1, 1E-10, maxit, 1, xᵢ=x0)
# @time eigvec, eigval, errors, info = IP(stiffnessmat, massmat, σ, 1, 1, 1E-10, maxit, 1, xᵢ=x0)
# # @time eigvec, eigval, errors, info = RQI(stiffnessmat, massmat, 1E-8, maxit, xᵢ=x0)
# # @time eigvec, eigval, errors, info = IP(stiffnessmat - (stiffnessmat * x0) * (massmat * x0)', massmat, σ, 1, 1, 1E-10, maxit, 1)
# # @time eigvec, eigval, errors, info = IP((I(size(stiffnessmat)[1]) - 2*x0 * (massmat * x0)') * stiffnessmat * inv(Array(I(size(stiffnessmat)[1]) - 2*x0 * (massmat * x0)')), massmat, σ, 1, 1, 1E-10, maxit, 1)
# # @time eigvec, eigval, errors, info = IP((x0 * (massmat * x0)') * stiffnessmat, massmat, σ, 1, 1, 1E-10, maxit, 1)
# # @time eigvec, eigval, errors, info = IP(2*stiffnessmat - x0 * (massmat * x0)', massmat, σ, 1, 1, 1E-10, maxit, 1, xᵢ=x0)
# # @time eigvec, eigval, errors, info = IP((stiffnessmat - σ * massmat) * inv(Array(stiffnessmat + σ * massmat)), massmat, 0, 1, 1, 1E-10, maxit, 1, xᵢ=x0)
# # @time eigvec, eigval, errors, info = IP((stiffnessmat - 0.5σ * massmat) * inv(Array(stiffnessmat - σ * massmat)), massmat, 0, 1, 1, 1E-10, maxit, 1, xᵢ=x0)
# # @time eigvec, eigval, errors, info = IP(stiffnessmat, massmat, σ, 1, 1, 1E-10, maxit, 1, xᵢ=x0)
@time eigvec, eigval, errors, info = LOPCG(stiffnessmat, massmat, massmat * Aa, 1, 1, 1E-10, maxit, 1)
# # @time eigvec, eigval, errors, info = LOPCG((stiffnessmat - σ * massmat), massmat, stiffnessmat, 1, 1, 1E-10, maxit, 1)
# # @time eigvec, eigval, errors, info = LOPCG(stiffnessmat, massmat, (I(size(stiffnessmat)[1]) + 2 * x0 * (massmat*x0)') * (stiffnessmat - σ * massmat), 1, 1, 1E-10, maxit, 1)
# # @time eigvec, eigval, errors, info = LOPCG(stiffnessmat, massmat, (2I(size(stiffnessmat)[1]) - (massmat*x0) * x0')'*stiffnessmat*((2I(size(stiffnessmat)[1]) - (massmat*x0) * x0')), 1, 1, 1E-10, maxit, 1)
# # @time eigvec, eigval, errors, info = LOPCG(stiffnessmat, massmat, (stiffnessmat - σ * massmat), 1, 1, 1E-10, maxit, 1, xᵢ=x0)
# # @time eigvec, eigval, errors, info = SD(stiffnessmat, massmat, (stiffnessmat - σ * massmat), 1, 1, 1E-10, maxit, 1)
# # @time eigvec, eigval, errors, info = SD(stiffnessmat, massmat, (1*I(size(stiffnessmat)[1]) - 0.5*x0 * (massmat * x0)'), 1, 1, 1E-10, maxit, 1)
# # @time eigvec, eigval, errors, info = SD(stiffnessmat, massmat, (1*x0 * (massmat * x0)'), 1, 1, 1E-10, maxit, 1)

# @info "end eigensolve"

# writevtk(model, "model")

# uh = FEFunction(op1.trial, eigvec);
# writevtk(Ω, "results", cellfields = ["uh" => uh])

# uh_res = FEFunction(op1.trial, stiffnessmat*x0 - raycoeffg(x0, stiffnessmat, massmat) * massmat * x0);
# writevtk(Ω, "results_res", cellfields = ["uh_res" => uh_res])

# Vgrid = Gridap.FESpaces.interpolate(V, V0)
# writevtk(Ω, "pot", cellfields = ["pot" => Vgrid])


# plotErrors(errors, "Errors")
