using Gridap
using LinearAlgebra
using SparseArrays
using LinearMaps
using SymPy
using DataFrames
using Plots
using IterativeSolvers

using ddEigenLab

howmany = 1

A(x) = (sin(π*x[2])^2*(cos(π*x[1])^2 + 1.1) - sin(π*x[2])^2*1.0*sin(π*x[2])^2)^2

function Adiffx(x)
  # too slow:
  # @vars xx yy
  # eval(Meta.parse(replace(replace(string(SymPy.diff(A([xx,yy]),xx).subs(xx,"xxx").subs(yy,"yyy")), "xxx"=>"$x[1]"), "yyy"=>"$x[2]")))
  -4*pi*((cos(pi*x[1])^2 + 1.1)*sin(pi*x[2])^2 - 1.0*sin(pi*x[2])^4)*sin(pi*x[1])*sin(pi*x[2])^2*cos(pi*x[1])
end

function solve_corrector_problem()
  Lx = 1
  Ly = 1
  ρ = 50
  tol = 1E-8

  domain = (0, Lx, 0, Ly)
  partition = (Lx * ρ, Ly * ρ)
  model = CartesianDiscreteModel(domain, partition; isperiodic=(true, false))  # quads
  # model = simplexify(CartesianDiscreteModel(domain, partition))  # tets

  labels = get_face_labeling(model)
  # add_tag_from_tags!(labels, "diri", collect(1:3^d-1))
  add_tag_from_tags!(labels, "diri", [1])

  order = 2
  reffe = ReferenceFE(lagrangian, Float64, order)
  V0 = TestFESpace(model, reffe; conformity=:H1, dirichlet_tags = ["diri"])

  Ug = TrialFESpace(V0, 1)

  degree = 3
  Ω = Triangulation(model)
  dΩ = Measure(Ω,degree)

  e1 = VectorValue(1.0,0.0)
  # d = TensorValue(1,0,0,Lx^2)
  d = TensorValue(1,0,0,1)

  b(v) = ∫( Adiffx * v ) * dΩ

  a1(u,v) = ∫( ∇(u) ⋅ ∇(v) * A ) * dΩ
  op1 = AffineFEOperator(a1, b, Ug, V0)

  ls = LUSolver()
  solver = LinearFESolver(ls)

  θ = Gridap.solve(solver, op1)

  writevtk(Ω, "results_theta", cellfields = ["θ" => θ])

  Cbar = sum(∫( A * ( 1 + ∇(θ) ⋅ VectorValue(1,0) )) * dΩ)
  Dbar = sum(∫( A ) * dΩ)

  ahom(u,v) = ∫( ∇(u) ⋅ ∇(v) * Cbar ) * dΩ
  bhom(v) = ∫( Dbar * v ) * dΩ
  ophom = AffineFEOperator(ahom, bhom, Ug, V0)
  u0 = Gridap.solve(solver, ophom)
  writevtk(Ω, "results_u0", cellfields = ["u0" => u0])

  return [Cbar, Dbar]
end

function solve_homogenized_problem(Lx)
  Cbar, Dbar = solve_corrector_problem()
  use_shortcut = false
  if use_shortcut
    λ0 = Cbar/Dbar * pi^2
    @info λ0
    return [λ0*i^2 for i=1:howmany]
  else
    Ly = 1
    ρ = 50
    tol = 1E-8

    domain = (0, Lx, 0, Ly)
    partition = (Lx * ρ, Ly * ρ)
    model = CartesianDiscreteModel(domain, partition; isperiodic=(false, false))  # quads
    # model = simplexify(CartesianDiscreteModel(domain, partition))  # tets

    labels = get_face_labeling(model)
    # add_tag_from_tags!(labels, "diri", collect(1:3^d-1))
    add_tag_from_tags!(labels, "diri", [1,2,3,4,7,8])

    order = 2
    reffe = ReferenceFE(lagrangian, Float64, order)
    V0 = TestFESpace(model, reffe; conformity=:H1, dirichlet_tags = ["diri"])

    Ug = TrialFESpace(V0, 0)

    degree = 3
    Ω = Triangulation(model)
    dΩ = Measure(Ω,degree)

    b(v) = ∫( 1 * v ) * dΩ  # dummy

    e1 = VectorValue(1.0,1.0)
    # d = TensorValue(1,0,0,Lx^2)
    d = TensorValue(1,0,0,1)

    a1(u,v) = ∫( d ⋅ ∇(u) ⋅ ∇(v) * Cbar * Lx^2 ) * dΩ
    op1 = AffineFEOperator(a1, b, Ug, V0)
    stiffnessmat = assemble_matrix(a1, Ug, V0)

    a2(u,v) = ∫( u * v * Dbar ) * dΩ
    op2 = AffineFEOperator(a2, b, Ug, V0)
    massmat = assemble_matrix_and_vector(a2, b, Ug, V0)[1]

    @info "start eigensolve"

    maxit=1000
    σ = 0
    @time eigvec, eigval, errors, info = IP(stiffnessmat, massmat, σ, 1, 1, tol, maxit, 1)
    # @time eigvec, eigval, errors, info = IOI(stiffnessmat, massmat .+ 1E-10*I(size(massmat)[1]), σ, howmany, 1, 1, tol, maxit, 1)
    # @time eigvec, eigval, errors, info = IOI(stiffnessmat, massmat, σ, howmany, 1, 1, tol, maxit, 1)

    # @assert info.isconverged

    @info "end eigensolve"
    @info eigval

    writevtk(Ω, "results_hom", cellfields = ["uh$i" => FEFunction(op1.trial, eigvec[:,i]) for i=1:howmany])

    return [eigval]
  end
end

function solve_nonhomogenized_problem(Lx)
  Ly = 1
  ρ = 50
  tol = 1E-8

  domain = (0, Lx, 0, Ly)
  partition = (Lx * ρ, Ly * ρ)
  model = CartesianDiscreteModel(domain, partition; isperiodic=(false, false))  # quads
  # model = simplexify(CartesianDiscreteModel(domain, partition))  # tets

  labels = get_face_labeling(model)
  # add_tag_from_tags!(labels, "diri", collect(1:3^d-1))
  add_tag_from_tags!(labels, "diri", [1,2,3,4,7,8])

  order = 2
  reffe = ReferenceFE(lagrangian, Float64, order)
  V0 = TestFESpace(model, reffe; conformity=:H1, dirichlet_tags = ["diri"])

  Ug = TrialFESpace(V0, 0)

  degree = 3
  Ω = Triangulation(model)
  dΩ = Measure(Ω,degree)

  b(v) = ∫( 1 * v ) * dΩ  # dummy

  e1 = VectorValue(1.0,1.0)
  # d = TensorValue(1,0,0,Lx^2)
  d = TensorValue(1,0,0,1)

  a1(u,v) = ∫( d ⋅ ∇(u) ⋅ ∇(v) * A * Lx^2 ) * dΩ
  op1 = AffineFEOperator(a1, b, Ug, V0)
  stiffnessmat = assemble_matrix(a1, Ug, V0)

  a2(u,v) = ∫( u * v * A ) * dΩ
  op2 = AffineFEOperator(a2, b, Ug, V0)
  massmat = assemble_matrix_and_vector(a2, b, Ug, V0)[1]

  @info "start eigensolve"

  maxit=1000
  σ = 0
  @time eigvec, eigval, errors, info = IP(stiffnessmat, massmat, σ, 1, 1, tol, maxit, 1)
  # @time eigvec, eigval, errors, info = IOI(stiffnessmat, massmat .+ 1E-10*I(size(massmat)[1]), σ, howmany, 1, 1, tol, maxit, 1)
  # @time eigvec, eigval, errors, info = IOI(stiffnessmat, massmat, σ, howmany, 1, 1, tol, maxit, 1)

  # @assert info.isconverged

  @info "end eigensolve"

  writevtk(model, "model")
  writevtk(Ω, "results_nonhom", cellfields = ["uh$i" => FEFunction(op1.trial, eigvec[:,i]) for i=1:howmany])

  return eigval

end

function execute(Lx)
  Ly = 1
  ρ = 50
  tol = 1E-8
  maxit=1000
  σ = 0

  domain = (0, Lx, 0, Ly)  # 1)
  # domain = (0, 1, 0, Ly)  # 2)
  partition = (Lx * ρ, Ly * ρ)
  model = CartesianDiscreteModel(domain, partition; isperiodic=(false, false))  # quads
  # model = simplexify(CartesianDiscreteModel(domain, partition))  # tets

  labels = get_face_labeling(model)
  # add_tag_from_tags!(labels, "diri", collect(1:3^d-1))
  add_tag_from_tags!(labels, "diri", [1,2,3,4,7,8])

  order = 2
  reffe = ReferenceFE(lagrangian, Float64, order)
  V0 = TestFESpace(model, reffe; conformity=:H1, dirichlet_tags = ["diri"])

  Ug = TrialFESpace(V0, 0)

  degree = 3
  Ω = Triangulation(model)
  dΩ = Measure(Ω,degree)

  b(v) = ∫( 1 * v ) * dΩ  # dummy


  # non homogenized
  d = TensorValue(1, 0, 0, 1)  # 1)
  # d = TensorValue(1, 0, 0, Lx^2)  # 2)
  Anew(x) = A(x)  # 1)
  # Anew(x) = A([Lx*x[1],x[2]])  # 2)
  lhs(u,v) = ∫( d ⋅ ∇(u) ⋅ ∇(v) * (x->Anew(x)) ) * dΩ
  lhs_op_nonhom = AffineFEOperator(lhs, b, Ug, V0)
  stiffm_nonhom = assemble_matrix(lhs, Ug, V0)

  rhs_nonhom(u,v) = ∫( u * v * (x->Anew(x)) ) * dΩ
  rhs_op_nonhom = AffineFEOperator(rhs_nonhom, b, Ug, V0)
  massmat_nonhom = assemble_matrix_and_vector(rhs_nonhom, b, Ug, V0)[1]

  @info "start eigensolve"
  @time eigvec, eigval, errors, info = IP(stiffm_nonhom, massmat_nonhom, σ, 1, 1, tol, maxit, 1)
  uh_nonhom = FEFunction(lhs_op_nonhom.trial, eigvec[:,1])
  @info "end eigensolve hom"

  writevtk(Ω, "results_nonhom", cellfields = ["uh" => uh_nonhom])


  # homogenized
  Cbar, Dbar = solve_corrector_problem()

  lhs_hom(u,v) = ∫( ∇(u) ⋅ ∇(v) * Cbar ) * dΩ
  lhs_op_hom = AffineFEOperator(lhs_hom, b, Ug, V0)
  stiffm_hom = assemble_matrix(lhs_hom, Ug, V0)

  rhs_hom(u,v) = ∫( u * v * Dbar ) * dΩ
  rhs_op_hom = AffineFEOperator(rhs_hom, b, Ug, V0)
  massmat_hom = assemble_matrix_and_vector(rhs_hom, b, Ug, V0)[1]

  @info "start eigensolve hom"
  @time eigvec_hom, eigval_hom, errors_hom, info_hom = IP(stiffm_hom, massmat_hom, σ, 1, 1, tol, maxit, 1)
  uh_hom = FEFunction(lhs_op_hom.trial, eigvec_hom[:,1])
  @info "end eigensolve"
  @info eigval


  writevtk(Ω, "results_hom", cellfields = ["uh" => uh_hom])

  # Eigenfunction Error
  e = uh_nonhom - uh_hom
  el2 = sqrt(sum( ∫( e*e )*dΩ ))
  eh1 = sqrt(sum( ∫( e*e + ∇(e)⋅∇(e) )*dΩ ))
  return (el2, eh1)

end

df = DataFrame(
  Lx = Float64[],
  errorsl2 = Float64[],
  errorsh1 = Float64[],
)

# for Lx = [i for i=5:0.1:8]
for Lx = [2^i for i=0:5]
  @info Lx
  el2, eh1 = execute(Lx)
  push!(df, (Lx, abs(el2), abs(eh1)))
end

plot(
  df.Lx,
  [
    [df.Lx.^-1],
    [df.Lx.^-2],
    df.errorsl2,
    df.errorsh1,
  ], axis=:log, marker=:circle,
  labels=["order 1" "order 2" "l2 error" "h1 error"]
)
