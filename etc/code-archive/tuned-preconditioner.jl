using LinearAlgebra
using IterativeSolvers
using ddEigenLab
Lx = 2^5
Lh = FD2D(
  NegativeLaplacePlusPotential(0, x->0, x->0, (x,y)->0),
  Rectangle2D(Lx, 1, 5, 5),
  Dict(:x => :d, :y => :d)
)
A = Symmetric(Lh.A - eigen(Array(Lh.Ay)).values[1] * I(size(Lh.A)[1]))
# A = Lh.A


# b = ones(size(A)[1])
# xreal = A \ b
# P = Symmetric(inv(Array(A + 1 * I(size(Lh.A)[1]))))
# xprecond = (P * A) \ (P * b)
# error = xreal - xprecond


# eigsolve(Symmetric(A), 1, :SR)
# eigsolve(Symmetric(A), 1, :SR)

# xreal = eigen(Array(A)).vectors[:,1]

# x = normalize(xreal + 1/Lx * rand(size(A)[1]),2)
x = ones(size(A)[1])
res = 1
@time for i=1:1000
  global x, res
  # P = A
  # P = I(size(A)[1])
  # P = zeros(size(A))
  PP = I(size(A)[1])
  # PP = Array(Lh.Ay)
  # PP = eigen(Array(Lh.Ay)).values[1] * I(size(Lh.A)[1])
  # PP = P + (A - P) * x * x'
  # PP = I(size(A)[1]) + 0.01*x * x'
  # display(PP)
  invPP = inv(PP)
  # invPP = I(size(A)[1])
  # x = (A*invPP) \ x
  b=x; x,h = cg!(x, A*invPP, b, log=true); @info h
  x = invPP \ x


  x = x / sqrt(dot(x,x))
  res = (A - (x' * A * x)/(x' * x) * I(size(A)[1]) ) * x |> norm
  @info i, res
  if (res < 1E-8) break end
end
