#!/bin/bash

for N in 1 2 4 8 16 32 64
do
  gmsh -2 full_structured_nodefects.geo -o full_structured_nodefects_"$N".msh -setnumber N "$N"
  gmsh -2 full_structured_defects.geo -o full_structured_defects_"$N".msh -setnumber N "$N"
  gmsh -2 periodic_structured.geo -o periodic_structured.msh
done
