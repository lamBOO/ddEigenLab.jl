using Gridap
using GridapGmsh
using GridapDistributed
using Gridap.CellData
using Gridap.Arrays
using SparseArrays
import Base.\
using IterativeSolvers
using Metis

using ddEigenLab

# run(Cmd(`sh create_meshes.sh`))

R = 1  # radius of disks
a = 0.0001  # cutoff radisu
Z = 1  # strength
S = 0  # shift
p = 8  # inverse mesh size exponent

pot1(x) =
  if (
    (sqrt(x[1]^2 + x[2]^2) <= R) &&
    (sqrt(x[1]^2 + x[2]^2) <= a)
  )
    -Z * 1 / a + S
  elseif (
    (sqrt(x[1]^2 + x[2]^2) <= R) &&
    (sqrt(x[1]^2 + x[2]^2) > a)
  )
    -Z * 1 / sqrt(x[1]^2 + x[2]^2) + S
  else
    0
  end

pot(x, Ns) = sum([
  pot1(x - VectorValue([p, 0]))
  for p in [1 + (i - 1) * 1.8 for i in Ns]
])

pot(x, Ns) = 0


# Z = 10
# pot(x) = Z^2 * (sin(x[1]*4*π/1.8)^2 * sin(x[2]*4*π/1.8)^2 + 5*(((10+x[1]-0.25)%1.8)^2 ))
# pot(x) = Z^2 * (10 + 5 * (((10 + x[1] - 0.25) % 1.8)^2))


# periodic unit cell
mshfile = joinpath("periodic_structured.msh")
model = GmshDiscreteModel(mshfile)
reffe = ReferenceFE(lagrangian, Float64, 1)
Vs = TestFESpace(model, reffe, dirichlet_tags=["top", "bot"])
U = TrialFESpace(Vs, 0)
Ω = Triangulation(model)
dΩ = Measure(Ω, 2)
a1(u, v) = ∫(∇(u) ⋅ ∇(v) + (x -> pot(x, 0:2)) * u * v)dΩ
# a1(u, v) = ∫(∇(u) ⋅ ∇(v) + (x -> pot(x)) * u * v)dΩ
a2(u, v) = ∫(u * v)dΩ
b(v) = ∫(1 * v)dΩ
op = AffineFEOperator(a1, b, U, Vs)
writevtk(model, "model")

stiffnessmat = assemble_matrix(a1, U, Vs)
massmat = assemble_matrix_and_vector(a2, b, U, Vs)[1]
# eigvec, eigval, errors, info = IP(stiffnessmat, massmat, 0, 1, 1, 1E-10, 1000, 1)
eigvec, eigval, errors, info = LOPCG(
  stiffnessmat, massmat, Pl = (stiffnessmat - 0 * massmat), tol = 1E-10,
  maxiter = 1000
)
uh = FEFunction(op.trial, eigvec);
writevtk(Ω, "gmshpart_periodic", cellfields=["uh" => uh])


n = 2
# mshfile2 = joinpath("full_structured_defects_$(n).msh")
mshfile2 = joinpath("full_structured_nodefects_$(n).msh")
model2 = GmshDiscreteModel(mshfile2)
g = GridapDistributed.compute_cell_graph(model2)
writevtk(model2, "model2")


reffe2 = ReferenceFE(lagrangian, Float64, 1)
V2 = TestFESpace(model2, reffe2, dirichlet_tags=["outer"])
U2 = TrialFESpace(V2, 0)
Ω2 = Triangulation(model2)
dΩ2 = Measure(Ω2, 2)
a12(u, v) = ∫(∇(u) ⋅ ∇(v) + (x -> pot(x, 0:n)) * u * v)dΩ2
# a12(u, v) = ∫(∇(u) ⋅ ∇(v) + (x -> pot(x)) * u * v)dΩ2
a22(u, v) = ∫(u * v)dΩ2
b2(v) = ∫(1 * v)dΩ2
op2 = AffineFEOperator(a12, b2, U2, V2)
@info "End problem"


## 230105: Fails since the meshes don't have EXACT coords...
function ref(x)
  @info x
  return VectorValue(mod(x[1] - 0.1, 1.8) + 0.1, x[2] * (1.0 - 1E-4))
end
function ref2(x)
  # @info x
  return (mod(x[1] - 0.1, 1.8) + 0.1, x[2] * (1.0 - 1E-4))
end
helper3(x) = uh(VectorValue(mod(x[1] - 0.1, 1.8) + 0.1, x[2] * (1.0 - 1E-4)))
function helper2(x::VectorValue)::Float64
  # @info x
  return uh(VectorValue(mod(x[1] - 0.1, 1.8) + 0.1, x[2] * (1.0 - 1E-4)))
end

search_method = KDTreeSearch(num_nearest_vertices=5)
uhnew = Interpolable(uh; searchmethod=search_method)
const cache5 = return_cache(uhnew, Point(0.0, 0.0))
function helper4(x)
  return evaluate!(cache5, uhnew, Point(mod(x[1] - 0.1, 1.8) + 0.1, x[2] * (1.0 - 1E-4)))
end

search_method = KDTreeSearch(num_nearest_vertices=5)

@time uext = Gridap.FESpaces.interpolate(helper4, V2)

@info "Start matrices"
stiffnessmat2 = assemble_matrix(a12, U2, V2)
massmat2 = assemble_matrix_and_vector(a22, b2, U2, V2)[1]
rhs = assemble_matrix_and_vector(a22, b2, U2, V2)[2]

uhvec2 = stiffnessmat2 \ (rhs)
uh2 = FEFunction(op2.trial, uhvec2);

uhreal2 = Gridap.solve(op2)

eigvec2, eigval2, errors2, info2 = LOPCG(
  stiffnessmat2, massmat2, Pl = (stiffnessmat2 - eigval * massmat2),
  tol = 1E-10, maxiter = 1000
)
eigfun2 = FEFunction(op2.trial, eigvec2);



# @time poth = Gridap.FESpaces.interpolate(pot, V2)
# writevtk(Ω2, "poth", cellfields=["poth" => poth])


# DD
# partition = gmsh_partition(model2, "untitled" .* ["$i" for i = cat(1:n+2, dims=1)])  # defects
# partition = gmsh_partition(model2, "untitled" .* ["$i" for i = cat(1:n, dims=1)])
partition = Metis.partition(GridapDistributed.compute_cell_graph(model2), n)
writevtk(Ω2, "partition", cellfields=["partition" => partition])
Vconst = TestFESpace(model2, ReferenceFE(lagrangian, Float64, 0))
partitionfunc = FEFunction(Vconst, Float64.(partition));
writevtk(Ω2, "partition", cellfields=["partition" => partition])

# npar = n
# decomp = create_decomposition(model2, "untitled" .* ["$i" for i=1:npar], 1, V2, epart=Metis.partition(GridapDistributed.compute_cell_graph(model2), n))

alpha = 0.01/n

pu = PartitionOfUnity(partition, 1, V2)
par = Partition(partition, 1, V2)
nocc = NoCoarseCorrection()
P = stiffnessmat2 - eigval * massmat2
cc = GeneralizedNicolaides(P, pu, uext.free_values[:, :])
ASM1 = PSM(P, par, nocc)
ASM2 = PSM(P, par, cc)
RAS1 = PSM(P, pu, nocc)
RAS2 = PSM(P, pu, cc)
res = ddEigenLab.solve(RAS2, alpha*ones(size(ASM1.A)[2]), x0=alpha*ones(V2.nfree), maxiter=400, reltol=1E-8, debug=true)
uhdd = FEFunction(op2.trial, res[1]);

puvecs = [pu.Ri[i]' * pu.Di[i] * pu.Ri[i] * ones(size(stiffnessmat2)[1]) for i=1:length(pu.Di)]
pufuns = [FEFunction(op2.trial, puvecs[i]) for i=1:length(pu.Di)]
parvecs = [pu.Ri[i]' * pu.Ri[i] * ones(size(stiffnessmat2)[1]) for i=1:length(pu.Di)]
parfuns = [FEFunction(op2.trial, parvecs[i]) for i=1:length(pu.Di)]
writevtk(Ω2, "gmshpart", cellfields=["uh2" => uh2, "uhreal2" => uhreal2, "uhdd" => uhdd, (["pu_$i" for i=1:length(pu.Di)] .=> pufuns)..., (["par_$i" for i=1:length(pu.Di)] .=> parfuns)..., "uext" => uext, "eigfun2" => eigfun2])




# kmax = 10
res_array = Vector{Float64}[]
its = 10
# for its = 0:kmax
xis, _ = ddEigenLab.solveAS(ASM1, alpha*ones(size(ASM1.A)[2]), zeros(size(ASM1.A)[2]), its, 1E-8)
funcs = [FEFunction(op2.trial, xis[i]) for i=1:length(xis)]
for i = 1:length(xis)
  @info norm(xis[i]-res[1])
  writevtk(Ω2, "dd_results_$i", cellfields=["f" => funcs[i]])
end
# end

map(x->@sprintf("%.2E",x), map(x->norm(x-res[1]), xis)
