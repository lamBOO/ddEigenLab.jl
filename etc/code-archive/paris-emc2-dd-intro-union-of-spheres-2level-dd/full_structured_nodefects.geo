// Command line Parameters
If(!Exists(p))
  p = 3;
EndIf
If(!Exists(N))
  N = 4;
EndIf

pts = 2^(p)+1;

// Settings
res = 100;
Mesh.CharacteristicLengthMax = 1.0 * 2^(-p);
// Mesh.MshFileVersion = 4.1;
Mesh.MshFileVersion = 2.2;
d = 0.1;
D = 2 - 2 * 0.1;

// Left
Point(100) = {0 + 0.1, +Sin(Acos(-0.9)), 0, p};
Point(200) = {0 + 0.1, -Sin(Acos(-0.9)), 0, p};

// Middle
For i In {1:N}

  Point(000 + i) = {1 + (i-1) * D, 0, 0, p};
  Point(100 + i) = {2 -  0.1 + (i-1) * D, +Sin(Acos(-0.9)), 0, p};
  Point(200 + i) = {2 -  0.1 + (i-1) * D, -Sin(Acos(-0.9)), 0, p};

  Line (1000 + i) = {100 + i, 200 + i};
  Transfinite Curve(1000 + i) = pts;

  Circle (1100 + i) = {100 + i - 1, 000 + i, 100 + i};
  Circle (1200 + i) = {200 + i, 000 + i, 200 + i - 1};
  Transfinite Curve(1100 + i) = Round(1.2*pts/2)*2+1;
  Transfinite Curve(1200 + i) = Round(1.2*pts/2)*2+1;

EndFor

// Left
Line (1000) = {100, 200};
Transfinite Curve(1000) = pts;
// Transfinite Curve(1000) = pts;
// Circle (1100) = {200, 1, 100};
// Curve Loop(2000 + N  + 1) = {1000,1100};
// Plane Surface(3000 + N  + 1) = {2000 + N  + 1};

// // Right
// Circle (1300) = {100 + N, N, 200 + N};
// Curve Loop(2000 + N  + 2) = {1000 + N,-1300};
// Plane Surface(3000 + N  + 2) = {2000 + N  + 2};

For i In {1:N}
  Curve Loop(2000 + i) = {-(1000 + i - 1),1100 + i,1000 + i,1200 + i};
  Plane Surface(3000 + i) = {2000 + i};
  Transfinite Surface(3000 + i) = {} AlternateLeft;
  // Transfinite Surface(3000 + i) = {} Left;
EndFor

// Curve Loop(2100) = {-1100, -1201:-(1200 + N), -1300, -(1100 + N):-1101};
Physical Line("outer") = {-1000, -1201:-(1200 + N), -1000-N, -(1100 + N):-1101};

Physical Point("outer") = {100:100+N, 200:200+N};

// Physical Surface("mesh",1000) = {3001:3000 + N + 2};

// Physical Surface("test1",1) = {};
// Physical Surface("test2",2) = {};
// Physical Surface("test3",3) = {};

For i In {1:N}
  Printf("%f",i);
  // pi = 3.14;
  // // s = Printf(Sprintf("model%f", pi));
  // a = 1; b = 2; c = 10;
  // test = Printf(Sprintf("model%g_%g_%g.inp", a, b, c));
  // Printf(s);
  // Printf(StrCat("name is ", s));
  Physical Surface(i) = {3000+i};
EndFor

// Mesh.Algorithm = 1; // Delaunay 2D
