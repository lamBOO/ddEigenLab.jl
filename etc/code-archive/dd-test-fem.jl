using Gridap
using GridapGmsh
using LinearAlgebra
using SparseArrays
using LinearMaps

using ddEigenLab

run(`gmsh -2 examples/fem/dd-test/rect2ol1.geo -o examples/fem/dd-test/mesh.msh`)
mshfile = joinpath("examples/fem/dd-test/mesh.msh")
model = GmshDiscreteModel(mshfile)

labels = get_face_labeling(model)

el = ReferenceFE(lagrangian, Float64, 1)
test = TestFESpace(model, el; conformity=:H1, dirichlet_tags=["outer"])
trial = TrialFESpace(test, 0)
Ω = Triangulation(model)
dΩ = Measure(Ω, 2)

b(v) = ∫( 1 * v ) * dΩ
a(u,v) = ∫( ∇(v) ⋅ ∇(u) ) * dΩ
op = AffineFEOperator(a, b, trial, test)
A = assemble_matrix(a, trial, test)
B = assemble_vector(b, test)

x = A \ B

u = FEFunction(op.trial, x);
writevtk(Ω,"u",cellfields=["u"=>u])
