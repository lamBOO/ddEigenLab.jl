using LinearAlgebra
import Base: iterate
using Base.Iterators # so we can use `take` and `enumerate`
using Printf # so we can use the `@printf` macro


struct CGIterable{TA, Tb, Tx}
  A::TA
  b::Tb
  x0::Tx
end

cgiterable(A::TA, b::Tb; x0::Tx=nothing) where {TA, Tb, Tx} = CGIterable{TA, Tb, Tx}(A, b, x0)

mutable struct CGState{R, Tb}
  x::Tb
  r::Tb
  rs::R
  rsprev::R
  p::Tb
  Ap::Tb
end



function iterate(iter::CGIterable{TA, Tb, Tx}) where
    {R, TA, Tb <: AbstractVector{R}, Tx}
    if iter.x0 === nothing
        x = zero(iter.b)
        r = copy(iter.b)
    else
        x = copy(iter.x0)
        r = iter.A*x
        r .*= -one(R)
        r .+= iter.b
    end
    rs = dot(r, r)
    p = copy(r)
    Ap = similar(r)
    state = CGState{R, Tb}(x, r, rs, zero(rs), p, Ap)
    return state, state
end

function iterate(iter::CGIterable{TA, Tb, Tx}, state::CGState{R, Tb}) where
    {R, TA, Tb <: AbstractVector{R}, Tx}
    mul!(state.Ap, iter.A, state.p)
    alpha = state.rs / dot(state.p, state.Ap)
    state.x .+= alpha .* state.p
    state.r .-= alpha .* state.Ap
    state.rsprev = state.rs
    state.rs = dot(state.r, state.r)
    state.p .= state.r .+ (state.rs / state.rsprev) .* state.p
    return state, state
end



struct HaltingIterable{I, F}
  iter::I
  fun::F
end

function iterate(iter::HaltingIterable)
  next = iterate(iter.iter)
  return dispatch(iter, next)
end

function iterate(iter::HaltingIterable, (instruction, state))
  if instruction == :halt return nothing end
  next = iterate(iter.iter, state)
  return dispatch(iter, next)
end

function dispatch(iter::HaltingIterable, next)
  if next === nothing return nothing end
  return next[1], (iter.fun(next[1]) ? :halt : :continue, next[2])
end

halt(iter::I, fun::F) where {I, F} = HaltingIterable{I, F}(iter, fun)




struct TeeIterable{I, F}
  iter::I
  fun::F
end

function iterate(iter::TeeIterable, args...)
  next = iterate(iter.iter, args...)
  if next !== nothing iter.fun(next[1]) end
  return next
end

tee(iter::I, fun::F) where {I, F} = TeeIterable{I, F}(iter, fun)





struct SamplingIterable{I}
  iter::I
  period::UInt
end

function iterate(iter::SamplingIterable, state=iter.iter)
  current = iterate(state)
  if current === nothing return nothing end
  for i = 1:iter.period-1
      next = iterate(state, current[2])
      if next === nothing return current[1], rest(state, current[2]) end
      current = next
  end
  return current[1], rest(state, current[2])
end

sample(iter::I, period) where I = SamplingIterable{I}(iter, period)





struct StopwatchIterable{I}
  iter::I
end

function iterate(iter::StopwatchIterable)
  t0 = time_ns()
  next = iterate(iter.iter)
  return dispatch(iter, t0, next)
end

function iterate(iter::StopwatchIterable, (t0, state))
  next = iterate(iter.iter, state)
  return dispatch(iter, t0, next)
end

function dispatch(iter::StopwatchIterable, t0, next)
  if next === nothing return nothing end
  return (time_ns()-t0, next[1]), (t0, next[2])
end

stopwatch(iter::I) where I = StopwatchIterable{I}(iter)





function loop(iter)
  x = nothing
  for y in iter
    x = y
  end
  return x
end


function cg(
  A::TA, b::Tb;
  x0::Tx=nothing,
  tol=1e-6,
  maxit=max(100000,size(A,2)),
  period=div(size(A,2),10)
  ) where {TA, Tb, Tx}

  stop(state) = sqrt(state.rs) <= tol
  disp(state) = @printf "%5d | %.3f | %.3e\n" state[2][1] state[1]/1e9 sqrt(state[2][2].rs)

  iter = cgiterable(A, b, x0=x0)
  iter = halt(iter, stop)
  iter = take(iter, maxit)
  iter = enumerate(iter)
  iter = sample(iter, period)
  iter = stopwatch(iter)
  iter = tee(iter, disp)

  (_, (it, state)) = loop(iter)

  return state.x, it
end


# ******************************************************************
# test cg
# using Random

# Random.seed!(12345)
# n = 100; L = rand(n, n); A = L*L'; b = rand(n);
# x, it = cg(A, b, tol=1e-8, period=30);
# norm(A*x - b)
# ******************************************************************






abstract type EigensolverIterable end

function eigensolver(iter::TIter, A::TA; x0::Tx=nothing,
  tol=1e-6, maxit=max(1000,size(A,2)),
  period=div(size(A,2),10)) where {TIter <: EigensolverIterable, TA, Tx}

  stop(state) = norm(state.r) <= tol
  disp(state) = @printf "%5d | %.3e | %.3e\n" state[2][1] state[1]/1e9 sqrt(state[2][2].rs)

  iter = halt(iter, stop)
  iter = take(iter, maxit)
  iter = enumerate(iter)
  iter = sample(iter, period)
  iter = stopwatch(iter)
  iter = tee(iter, disp)

  (_, (it, state)) = loop(iter)

  return state.x, it
end


struct PMIterable{TA, Tx} <: EigensolverIterable
  A::TA
  x0::Tx
end
pmiterable(A::TA; x0::Tx=nothing) where {TA, Tx} = PMIterable{TA, Tx}(A, x0)
mutable struct PMState{R, Tx}
  x::Tx
  r::Tx
  rs::R
end
function iterate(
  iter::PMIterable{TA, Tx},
  state::PMState{R, Tx} = PMState{R, Tx}(
    copy(iter.x0), Inf*ones(size(iter.A)[2]), Inf
  )
) where {R, TA, Tx <: AbstractVector{R}}
    Ax = iter.A * state.x
    state.x = normalize(Ax, 2)
    state.r = Ax - state.x'*Ax/(state.x'*state.x) * state.x
    state.rs = dot(state.r, state.r)
    return state, state
end
function pm(A::TA; x0::Tx=nothing, tol=1e-6, maxit=max(1000,size(A,2)),
  period=div(size(A,2),10)
) where {TA, Tx}
  eigensolver(pmiterable(A, x0=x0), A, x0=x0, tol=tol, maxit=maxit, period=period)
end


# # ******************************************************************
# # test pm
# using Random

# Random.seed!(12345)
# println("setup start")
# n = 100; L = rand(n, n); A = L*L'; b = rand(n);
# println("setup end")
# x, it = @time pm(A, x0=ones(size(A,2)), tol=1e-10, period=1, maxit=1000);
# norm(x + eigen(A).vectors[:,end])
# # ******************************************************************



struct IPMIterable{TA, Tx} <: EigensolverIterable
  A::TA
  x0::Tx
end
ipmiterable(A::TA; x0::Tx=nothing) where {TA, Tx} = IPMIterable{TA, Tx}(A, x0)
mutable struct IPMState{R, Tx}
  x::Tx
  r::Tx
  rs::R
end
function iterate(
  iter::IPMIterable{TA, Tx},
  state::IPMState{R, Tx} = IPMState{R, Tx}(
    copy(iter.x0), Inf*ones(size(iter.A)[2]), Inf
  )
) where {R, TA, Tx <: AbstractVector{R}}
    Ax = iter.A * state.x
    state.x = normalize(iter.A \ state.x, 2)
    state.r = Ax - state.x' * Ax * state.x
    # display(state.r)
    state.rs = dot(state.r, state.r)
    return state, state
end
function ipm(A::TA; x0::Tx=nothing, tol=1e-6, maxit=max(1000,size(A,2)),
  period=div(size(A,2),10)
) where {TA, Tx}
  eigensolver(ipmiterable(A, x0=x0), A, x0=x0, tol=tol, maxit=maxit, period=period)
end

# # ******************************************************************
# # test ipm
# using Random
# Random.seed!(12345)
# println("setup start")
# n = 1000; L = rand(n, n); A = L*L'; b = rand(n);
# println("setup end")
# x, it = @time ipm(A, x0=ones(size(A,2)), tol=1e-8, period=1, maxit=1000);
# norm(x + eigen(A).vectors[:,1])
# # ******************************************************************







# struct FibonacciIterable{I}
#   s0::I
#   s1::I
# end

# import Base: iterate
# iterate(iter::FibonacciIterable) = iter.s0, (iter.s0, iter.s1)
# iterate(iter::FibonacciIterable, state) = state[2], (state[2], sum(state))

# for F in FibonacciIterable(BigInt(0), BigInt(1))
#   println(F)
#   if F > 50 break end
# end