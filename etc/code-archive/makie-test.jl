println("Start loading")
@time using Makie
println("End loading")

x = range(0, stop = 2pi, length = 40)
f(x) = sin.(x)
y = f(x)
scene = lines(x, y, color = :blue)