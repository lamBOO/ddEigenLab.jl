using Gridap
using LinearAlgebra
using SparseArrays
using LinearMaps
using SymPy
using DataFrames
using Plots
using IterativeSolvers
using CSV
using FresnelIntegrals
using SpecialFunctions
using Dates

using ddEigenLab

howmany = 4

struct MatrixPrecond{TD}
  diagonal::TD
end

LinearAlgebra.ldiv!(y, P::MatrixPrecond, x) = y .= P.diagonal \ x

"Input string needs x[1],x[2],..."
struct WeightFunction5
  exp :: String
  A :: Function
  DA :: Function
  Aeffz :: Function
  DAeffz :: Function
  function WeightFunction5(exp)
    @info "Start weight"
    @vars xx yy zz
    xsym = [xx, yy, zz]

    A = eval(Meta.parse("x->"*exp))

    DA = eval(Meta.parse(
      "x->["*
      join([replace(
        string(SymPy.diff(Base.invokelatest(A, xsym),ξ)),
        (string.(xsym).=>["x[$i]" for i=1:3])...) for ξ in xsym
      ], ",")
      *"]"
    ))

    Aeffz = (
      eval(Meta.parse(
        "x->"*replace(
          string(SymPy.integrate(Base.invokelatest(A, xsym),(zz,0,1))),
          (string.(xsym).=>["x[$i]" for i=1:3])...
        )
      ))
    )

    DAeffz = eval(Meta.parse(
      "x->["*
      join(replace.(
        string.([SymPy.integrate(Base.invokelatest(DA, xsym)[i],(zz,0,1)) for i=1:2]),
        (string.(xsym).=>["x[$i]" for i=1:3])...)
      , ",")
      *"]"
    ))
    @info "End weight"

    return new(
      exp, A, DA, Aeffz, DAeffz
    )
  end
end

if !@isdefined w
  # w = WeightFunction5("(sin(π*x[3])^2*( 10*cos(π*x[1])^2 + 10*cos(π*x[2])^2 + 1.1 - sin(π*x[3])^2 ))^2") #! before review
  w = WeightFunction5("(27*x[3]^2*(1-x[3])/4*( 10*cos(π*x[1])^2 + 10*cos(π*x[2])^2 + 1.1 - sin(π*x[3])^2 ))^2")
end

function solve_corrector_problem_2d()
  Lx = 1
  Ly = 1
  ρ = 300
  tol = 1E-8

  domain = (0, Lx, 0, Lx)
  partition = (Lx * ρ, Lx * ρ)
  model = CartesianDiscreteModel(domain, partition; isperiodic=(true, true))  # quads
  # Tets don't work with periodic
  # https://github.com/gridap/Gridap.jl/issues/586
  # model = simplexify(CartesianDiscreteModel(domain, partition))  # tets

  labels = get_face_labeling(model)
  add_tag_from_tags!(labels, "diri", [1,2,3,4,5,6,7,8,10])

  order = 2
  reffe = ReferenceFE(lagrangian, Float64, order)
  V0 = TestFESpace(model, reffe; conformity=:H1)
  # V0 = TestFESpace(model, reffe; conformity=:H1, dirichlet_tags = ["diri"])
  Ug = TrialFESpace(V0, 0)

  degree = 10
  Ω = Triangulation(model)
  dΩ = Measure(Ω,degree)

  ls = LUSolver()
  solver = LinearFESolver(ls)

  e1 = VectorValue(1.0,0.0)
  d = TensorValue(1,0,0,1)

  CbarA = zeros(2, 2)
  Dbar = 0
  for i=1:2
    b(v) = ∫( (x->w.DAeffz(x)[i]) * v ) * dΩ

    a1(u,v) = ∫( ∇(u) ⋅ ∇(v) * w.Aeffz ) * dΩ
    op1 = AffineFEOperator(a1, b, Ug, V0)

    @info "corrector solve: start"
    @time θ = Gridap.solve(solver, op1)
    @info "corrector solve: end"

    write_vtk = false
    if write_vtk
      writevtk(Ω, "results_theta_$i", cellfields = ["θ" => θ])
      writevtk(Ω, "results_gradtheta_$i", cellfields = ["∇(θ)" => ∇(θ)])
      writevtk(Ω, "results_atimesgradtheta_$i", cellfields = ["A*∇(θ)" =>
        w.Aeffz * ∇(θ)
      ])
      writevtk(Ω, "results_atimesgradtheta+ei_$i", cellfields = ["A*∇(θ)" =>
        w.Aeffz * ( ∇(θ) + VectorValue(Int.(I(2)[:,i])) )
      ])
    end

    for j=1:2
      Cbar = sum(∫( w.Aeffz * ( Int(i==j) + ∇(θ) ⋅ VectorValue(Int.(I(2)[:,j])) )) * dΩ)
      CbarA[i,j] = Cbar
    end

    Dbar = sum(∫( w.Aeffz ) * dΩ)
  end

  @show CbarA, Dbar

  Cbar = TensorValue(CbarA)

  return [Cbar, Dbar]
end


function execute(Lx, Cbar, Dbar)
  Ly = 1
  ρx = 10
  ρy = 6
  tol = 1E-8
  maxit = 1000
  σ = 0

  domain = (0, 1, 0, 1, 0, Ly)
  partition = (round(Int, Lx * ρx), round(Int, Lx * ρx), round(Int, Ly * ρy))
  model = CartesianDiscreteModel(domain, partition; isperiodic=(false, false, false))  # quads
  # model = simplexify(CartesianDiscreteModel(domain, partition))  # tets
  writevtk(model, "model_$(replace(string(round(Lx,digits=2)),"."=>""))")

  labels = get_face_labeling(model)
  add_tag_from_tags!(labels, "diri", filter!(e->!(e∈[21,22]), collect(1:3^3-1)))

  order = 2
  reffe = ReferenceFE(lagrangian, Float64, order)
  V0 = TestFESpace(model, reffe; conformity=:H1, dirichlet_tags = ["diri"])
  Ug = TrialFESpace(V0, 0)

  degree = 3
  Ω = Triangulation(model)
  dΩ = Measure(Ω,degree)

  b(v) = ∫( 1 * v ) * dΩ  # dummy

  # B) analytical solution homogenized
  eigval_mat = [pi^2 * ((Cbar[1,1] * i^2 + Cbar[2,2] * j^2) / Dbar) for i=1:10, j=1:10]
  eigval_hom = sort(vec(eigval_mat))[1:howmany]
  eigval_indices = [findall(x->x==eigval_hom[i], eigval_mat)[1] for i=1:howmany]
  eigfuncs = [
    x->-2/sqrt(Dbar) * sin(1*pi*x[1])*sin(1*pi*x[2]),
    x->-2/sqrt(Dbar) * sin(2*pi*x[1])*sin(1*pi*x[2]),
    x->-2/sqrt(Dbar) * sin(1*pi*x[1])*sin(2*pi*x[2]),
    x->-2/sqrt(Dbar) * sin(2*pi*x[1])*sin(2*pi*x[2]),
  ]  # TODO: Works only for howmany=4
  # TODO: Make parametric with anisotropic Cbar

  global uh_hom = [
    Gridap.FESpaces.interpolate(eigfuncs[i], V0)
    for i=1:howmany
  ]
  @info eigval_hom
  @info "end eigensolve hom"
  writevtk(Ω, "results_hom_$(replace(string(round(Lx,digits=2)),"."=>""))", cellfields = [
    "uh$i" => uh_hom[i] for i=1:howmany
  ])

  # non homogenized
  d = TensorValue(1, 0, 0, 0, 1, 0, 0, 0, Lx^2)
  Anew(x) = w.A([Lx*x[1], Lx*x[2], x[3]])
  lhs(u,v) = ∫( d ⋅ ∇(u) ⋅ ∇(v) * (x->Anew(x)) ) * dΩ
  lhs_op_nonhom = AffineFEOperator(lhs, b, Ug, V0)
  stiffm_nonhom = assemble_matrix(lhs, Ug, V0)

  rhs_nonhom(u,v) = ∫( u * v * (x->Anew(x)) ) * dΩ
  rhs_op_nonhom = AffineFEOperator(rhs_nonhom, b, Ug, V0)
  massmat_nonhom = assemble_matrix_and_vector(rhs_nonhom, b, Ug, V0)[1]

  @info "start eigensolve nonhom"
  # @time eigvec_nonhom, eigval_nonhom, errors_nonhom, info_nonhom = IP(stiffm_nonhom, massmat_nonhom, σ, 1, 1, tol, maxit, 1)
  # @time eigvec_nonhom, eigval_nonhom, errors_nonhom, info_nonhom = IOI(stiffm_nonhom, massmat_nonhom, σ, howmany, 1, 1, tol, maxit, 1, zeros(size(stiffm_nonhom)[1]), true, hcat(map(e->e.free_values, uh_hom)...))
  # @time eigvec_nonhom, eigval_nonhom, errors_nonhom, info_nonhom = IOI(stiffm_nonhom, massmat_nonhom, σ, howmany, 1, 1, tol, maxit, 1)
  @time res = lobpcg(stiffm_nonhom, massmat_nonhom, false, howmany, P=MatrixPrecond(factorize(stiffm_nonhom)), log=true, maxiter=maxit)
  @show res.converged
  @assert res.converged
  eigvec_nonhom, eigval_nonhom = res.X, res.λ
  eigvec_nonhom = eigvec_nonhom
  @show res.residual_norms
  @info eigval_nonhom

  # rotate degenerate eigenstates and flip sign
  # TODO: Make adaptive and detect double eigenvals
  eigvec_nonhom_tmp = nothing
  rotate = true
  if rotate
    eigvec_nonhom_tmp = [
      eigvec_nonhom[:,1]' * uh_hom[1].free_values * eigvec_nonhom[:,1],
      (eigvec_nonhom[:,2]' * uh_hom[2].free_values * eigvec_nonhom[:,2] + eigvec_nonhom[:,3]' * uh_hom[2].free_values * eigvec_nonhom[:,3]),
      (eigvec_nonhom[:,2]' * uh_hom[3].free_values * eigvec_nonhom[:,2] + eigvec_nonhom[:,3]' * uh_hom[3].free_values * eigvec_nonhom[:,3]),
      eigvec_nonhom[:,4]' * uh_hom[4].free_values * eigvec_nonhom[:,4],
    ]
    eigvec_nonhom_tmp = [
      eigvec_nonhom_tmp[1] / sqrt(dot(eigvec_nonhom_tmp[1], massmat_nonhom, eigvec_nonhom_tmp[1])),
      eigvec_nonhom_tmp[2] / sqrt(dot(eigvec_nonhom_tmp[2], massmat_nonhom, eigvec_nonhom_tmp[2])),
      eigvec_nonhom_tmp[3] / sqrt(dot(eigvec_nonhom_tmp[3], massmat_nonhom, eigvec_nonhom_tmp[3])),
      eigvec_nonhom_tmp[4] / sqrt(dot(eigvec_nonhom_tmp[4], massmat_nonhom, eigvec_nonhom_tmp[4]))
    ]
  else
    eigvec_nonhom_tmp = [
      eigvec_nonhom[:,1],
      eigvec_nonhom[:,2],
      eigvec_nonhom[:,3],
      eigvec_nonhom[:,4]
    ]
    eigvec_nonhom_tmp = [
      eigvec_nonhom_tmp[1] / sqrt(dot(eigvec_nonhom_tmp[1], massmat_nonhom, eigvec_nonhom_tmp[1])),
      eigvec_nonhom_tmp[2] / sqrt(dot(eigvec_nonhom_tmp[2], massmat_nonhom, eigvec_nonhom_tmp[2])),
      eigvec_nonhom_tmp[3] / sqrt(dot(eigvec_nonhom_tmp[3], massmat_nonhom, eigvec_nonhom_tmp[3])),
      eigvec_nonhom_tmp[4] / sqrt(dot(eigvec_nonhom_tmp[4], massmat_nonhom, eigvec_nonhom_tmp[4]))
    ]
  end
  uh_nonhom = [
    FEFunction(lhs_op_nonhom.trial, eigvec_nonhom_tmp[1]),
    FEFunction(lhs_op_nonhom.trial, eigvec_nonhom_tmp[2]),
    FEFunction(lhs_op_nonhom.trial, eigvec_nonhom_tmp[3]),
    FEFunction(lhs_op_nonhom.trial, eigvec_nonhom_tmp[4])
  ]
  @info "end eigensolve nonhom"
  writevtk(Ω, "results_nonhom_$(replace(string(round(Lx,digits=2)),"."=>""))", cellfields = [
    "uh$i" => uh_nonhom[i] for i=1:howmany
  ])
  writevtk(Ω, "results_gradnonhom_$(replace(string(round(Lx,digits=2)),"."=>""))", cellfields = [
    "grad(uh)$i" => ∇(uh_nonhom[i]) for i=1:howmany
  ])

  # Relative Eigenfunction Error
  e = uh_nonhom .- uh_hom
  el2 = [collect(
    [sqrt(sum( ∫( e[i]*e[i] )*dΩ ))./sqrt(sum( ∫( uh_hom[i]*uh_hom[i] )*dΩ )) for i=1:howmany])...
  ]
  eh1 = [collect(
    [sqrt(sum( ∫( e[i]*e[i] + ∇(e[i])⋅∇(e[i]) )*dΩ ))./sqrt(sum( ∫( uh_hom[i]*uh_hom[i] + ∇(uh_hom[i])⋅∇(uh_hom[i]) )*dΩ )) for i=1:howmany])...
  ]

  # Relative Eigenvalues Error
  eevals = [collect((eigval_nonhom .- eigval_hom)./eigval_hom)...]

  # Eigenvalues into array
  eigval_nonhom = [collect(eigval_nonhom)...]
  eigval_hom = [collect(eigval_hom)...]

  return (el2, eh1, eevals, eigval_nonhom, eigval_hom, res.converged)

end

df = DataFrame(
  Lx = Float64[],
  converged = Bool[],
  time = String[],
  errorsl2 = Vector{Float64}[],
  errorsh1 = Vector{Float64}[],
  errorsevals = Vector{Float64}[],
  eigval_nonhom = Vector{Float64}[],
  eigval_hom = Vector{Float64}[],
)

if (!@isdefined Cbar) || (!@isdefined Dbar)
  global Cbar, Dbar = solve_corrector_problem_2d()
end

# for Lx = [i for i=5:0.1:8]
# for Lx = [2.2]
# for Lx = vcat([2^1+0.2*i for i=1:2^3*5])
for Lx = vcat([1+0.1*i for i=0:(2^4-1)*10])
# for Lx = vcat([2^i for i=0:3], [2^3+0.1*i for i=1:2^3*10])
# for Lx = [2^i for i=0:4]
  @info Lx
  el2, eh1, errorsevals, eigval_nonhom, eigval_hom, converged = execute(Lx, Cbar, Dbar)
  push!(df, (Lx, converged, string(now()), abs.(el2), abs.(eh1), abs.(errorsevals), eigval_nonhom, eigval_hom))
  CSV.write("homogenization-3d-df", df)
end

CSV.write("errorsh1.csv",
  DataFrame(
    Dict("Lx" => df.Lx,
    ["errorsh1-$j" => [df.errorsh1[i][j] for i=1:size(df)[1]] for j=1:howmany]...)
  )
)
CSV.write("errorsl2.csv",
  DataFrame(
    Dict("Lx" => df.Lx,
    ["errorsl2-$j" => [df.errorsl2[i][j] for i=1:size(df)[1]] for j=1:howmany]...)
  )
)
CSV.write("errorsevals.csv",
  DataFrame(
    Dict("Lx" => df.Lx,
    ["errorsevals-$j" => [df.errorsevals[i][j] for i=1:size(df)[1]] for j=1:howmany]...)
  )
)

ratios = map(vals -> [vals[j] / vals[j+1] for j=1:howmany-1], df.eigval_nonhom)
CSV.write("ratioevals.csv",
  DataFrame(
    Dict(
      "Lx" => df.Lx,
      ["rat$j" => [ratios[i][j] for i=1:size(ratios)[1]] for j=1:howmany-1]...
    )
  )
)

CSV.write("eigval_hom.csv",
DataFrame(
  Dict("Lx" => df.Lx,
  ["eigval_hom-$j" => [df.eigval_hom[i][j] for i=1:size(df)[1]] for j=1:howmany]...)
  )
)
CSV.write("eigval_nonhom.csv",
DataFrame(
  Dict("Lx" => df.Lx,
  ["eigval_nonhom-$j" => [df.eigval_nonhom[i][j] for i=1:size(df)[1]] for j=1:howmany]...)
  )
)


plotpairs = collect(zip(
  [
    [map(e -> e[i], df.errorsl2,) for i=1:howmany],
    [map(e -> e[i], df.errorsh1) for i=1:howmany],
    [map(e -> e[i], df.errorsevals) for i=1:howmany]
  ],
  [
    ["L2 error$i" for i=1:howmany],
    ["H1 error$i" for i=1:howmany],
    ["lambda error$i" for i=1:howmany]
  ]
))

for pp in plotpairs
  display(plot(
    df.Lx,
    [
      [df.Lx.^-1],
      [0.2*df.Lx.^-2],
      pp[1]...
    ], axis=:log, marker=:circle,
    labels=reshape([
      "order 1"
      "order 2"
      pp[2]...
    ], 1, :)
  ))
end

plot(
  df.Lx[2:end],
  [[map(e -> e[i], df.eigval_nonhom) for i=1:howmany][j][2:end] for j=1:howmany],
  marker=:circle,
  labels=reshape([
    ["lambda$i" for i=1:howmany]...
  ], 1, :)
)
