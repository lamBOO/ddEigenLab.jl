using Gridap
using LinearAlgebra
using SparseArrays
using LinearMaps
using SymPy
using DataFrames
using Plots
using IterativeSolvers

using ddEigenLab

howmany = 2

A(x) = (sin(π*x[2])^2*(cos(π*x[1])^2 + 1.1) - sin(π*x[2])^2*1.0*sin(π*x[2])^2)^2

function Adiffx(x)
  # too slow:
  # @vars xx yy
  # eval(Meta.parse(replace(replace(string(SymPy.diff(A([xx,yy]),xx).subs(xx,"xxx").subs(yy,"yyy")), "xxx"=>"$x[1]"), "yyy"=>"$x[2]")))
  -4*pi*((cos(pi*x[1])^2 + 1.1)*sin(pi*x[2])^2 - 1.0*sin(pi*x[2])^4)*sin(pi*x[1])*sin(pi*x[2])^2*cos(pi*x[1])
end

function solve_homogenized_problem()
  Lx = 1
  Ly = 1
  ρ = 50
  tol = 1E-8

  domain = (0, Lx, 0, Ly)
  partition = (Lx * ρ, Ly * ρ)
  model = CartesianDiscreteModel(domain, partition; isperiodic=(true, false))  # quads
  # model = simplexify(CartesianDiscreteModel(domain, partition))  # tets

  labels = get_face_labeling(model)
  # add_tag_from_tags!(labels, "diri", collect(1:3^d-1))
  add_tag_from_tags!(labels, "diri", [1])

  order = 2
  reffe = ReferenceFE(lagrangian, Float64, order)
  V0 = TestFESpace(model, reffe; conformity=:H1, dirichlet_tags = ["diri"])

  Ug = TrialFESpace(V0, 1)

  degree = 3
  Ω = Triangulation(model)
  dΩ = Measure(Ω,degree)

  e1 = VectorValue(1.0,0.0)
  # d = TensorValue(1,0,0,Lx^2)
  d = TensorValue(1,0,0,1)

  b(v) = ∫( Adiffx * v ) * dΩ

  a1(u,v) = ∫( ∇(u) ⋅ ∇(v) * A ) * dΩ
  op1 = AffineFEOperator(a1, b, Ug, V0)

  ls = LUSolver()
  solver = LinearFESolver(ls)

  θ = Gridap.solve(solver, op1)

  writevtk(Ω, "results_theta", cellfields = ["θ" => θ])

  Cbar = sum(∫( A * ( 1 + ∇(θ) ⋅ VectorValue(1,0) )) * dΩ)
  Dbar = sum(∫( A ) * dΩ)
  λ0 = Cbar/Dbar * pi^2

  return [λ0*i^2 for i=1:howmany]
end

function solve_nonhomogenized_problem(Lx)
  Ly = 1
  ρ = 50
  tol = 1E-8

  domain = (0, Lx, 0, Ly)
  partition = (Lx * ρ, Ly * ρ)
  model = CartesianDiscreteModel(domain, partition; isperiodic=(false, false))  # quads
  # model = simplexify(CartesianDiscreteModel(domain, partition))  # tets

  labels = get_face_labeling(model)
  # add_tag_from_tags!(labels, "diri", collect(1:3^d-1))
  add_tag_from_tags!(labels, "diri", [1,2,3,4,7,8])

  order = 2
  reffe = ReferenceFE(lagrangian, Float64, order)
  V0 = TestFESpace(model, reffe; conformity=:H1, dirichlet_tags = ["diri"])

  Ug = TrialFESpace(V0, 0)

  degree = 3
  Ω = Triangulation(model)
  dΩ = Measure(Ω,degree)

  b(v) = ∫( 1 * v ) * dΩ  # dummy

  e1 = VectorValue(1.0,1.0)
  # d = TensorValue(1,0,0,Lx^2)
  d = TensorValue(1,0,0,1)

  a1(u,v) = ∫( d ⋅ ∇(u) ⋅ ∇(v) * A * Lx^2 ) * dΩ
  op1 = AffineFEOperator(a1, b, Ug, V0)
  stiffnessmat = assemble_matrix(a1, Ug, V0)

  a2(u,v) = ∫( u * v * A ) * dΩ
  op2 = AffineFEOperator(a2, b, Ug, V0)
  massmat = assemble_matrix_and_vector(a2, b, Ug, V0)[1]

  @info "start eigensolve"

  maxit=1000
  σ = 0
  # @time eigvec, eigval, errors, info = IP(stiffnessmat, massmat, σ, 1, 1, tol, maxit, 1)
  # @time eigvec, eigval, errors, info = IOI(stiffnessmat, massmat .+ 1E-10*I(size(massmat)[1]), σ, howmany, 1, 1, tol, maxit, 1)
  @time eigvec, eigval, errors, info = IOI(stiffnessmat, massmat, σ, howmany, 1, 1, tol, maxit, 1)

  # @assert info.isconverged

  @info "end eigensolve"

  writevtk(model, "model")
  writevtk(Ω, "results_$Lx", cellfields = ["uh$i" => FEFunction(op1.trial, eigvec[:,i]) for i=1:howmany])

  return eigval

end

df = DataFrame(
  Lx = Float64[],
  errors = Vector{Float64}[],
  λε = Vector{Float64}[],
  fratios = Float64[],
)

λ0 = solve_homogenized_problem()
# for Lx = [i for i=5:0.1:8]
for Lx = [2^i for i=0:5]
  @info Lx
  λε = solve_nonhomogenized_problem(Lx)
  push!(df, (Lx, abs.(λ0 .- λε), λε, λε[1]/λε[2]))
end

plot(df.Lx,[[df.Lx.^-2], [map(e -> e[i], df.errors) for i=1:howmany]...], axis=:log, marker=:circle, labels=["2nd order" ["lambda$i" for i=1:howmany]... ])
