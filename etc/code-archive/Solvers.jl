using PyPlot
using Optim
using LinearAlgebra


# **************************************************************************** #
N = 2000
xs = range(0, 1, length=N+1)[1:N]
h = xs[2] - xs[1]
#V(x) = -1000*exp(-cos(π*x)^2)
V(x) = 2-exp(-cos(π*x)^2)

A = diagm(0 => 2*ones(N), 1 => -1*ones(N-1), -1 => -1*ones(N-1))
A[1, end] = -1
A[end, 1] = -1
A /= (xs[2]-xs[1])^2
Vx = V.(xs)

H = Symmetric(A + diagm(Vx))
# **************************************************************************** #



# # **************************************************************************** #
# # PROBLEM DESCRIPTION
# # **************************************************************************** #
# using SparseArrays
# Lx = 200;
# Ly = 3; # CONST

# nx = Lx+1;
# ny = 5;
# N = (nx-2) * (ny-2);
# dx = Lx/(nx-1);
# dy = Ly/(ny-1);
# x = range(0, stop=Lx, step=dx)';
# y = range(0, stop=Ly, step=dy)';

# # Laplacian matrix:
# # See: https://en.wikipedia.org/wiki/Kronecker_sum_of_discrete_Laplacians
# Ex = sparse(2:nx-2, 1:nx-3,1, nx-2, nx-2);
# Ax = - Ex - Ex' + 2 * sparse(I(nx-2));
# Ey = sparse(2:ny-2, 1:ny-3, 1, ny-2, ny-2);
# Ay = - Ey - Ey' + 2 * sparse(I(ny-2));
# H = Array(Ay/dy^2 ⊗ I(nx-2) + I(ny-2) ⊗ Ax/dx^2);
# # **************************************************************************** #



project_tangent(G, X) = (XG = X'G; G - X*((XG .+ XG')./2))


function InvPower(X0, tol, maxiter)
    println("-- Inverse Power Iteration --")
    x = X0/norm(X0)
    norms = []
    for n = 1:maxiter
        z = inv(H)*x
        x = z/norm(z)
        lambda = tr(x'*H*x)
        res = norm(H*x-lambda*x)
        println(n,"\t",res, "\t", tr(x'*H*x))
        res < tol && break
        push!(norms, res)
    end
    norms
end

function RQI(X0, tol, maxiter)
    println("-- RQI --")
    x = X0/norm(X0)
    lambda = tr(x'*H*x)
    norms = []
    Eye = Matrix{Float64}(I, size(x,1), size(x,1))
    for n = 1:maxiter
        P = inv(H-lambda*Eye)
        z = P*x
        x = z/norm(z)
        lambda = tr(x'*H*x)
        res = norm(H*x-lambda*x)
        println(n,"\t",res, "\t", tr(x'*H*x))
        res < tol && break
        push!(norms, res)
    end
    norms
end

function Newton(X0, tol, maxiter)
    println("-- Newton --")
    x = X0/norm(X0)
    lambda = tr(x'*H*x)
    norms = []
    Eye = Matrix{Float64}(I, size(x,1), size(x,1))
    for n = 1:maxiter
        xold = x
        P = inv(H-lambda*Eye)
        y = P*x
        x = y/norm(y)
        lambda = tr(xold'*H*x)/tr(xold'*x)
        # this is equivalent to commented below:
        #alpha = xold'*y
        #a = 1/alpha[1]
        #lambda = lambda + a

        res = norm(H*x-lambda*x)
        println(n,"\t",res, "\t", tr(x'*H*x))
        res < tol && break
        push!(norms, res)
    end
    norms
end

function SteepestDescent(X0, tol, maxiter)
    println("-- Steepest Descent --")
    alpha = 1                   # step size
    x = X0/norm(X0)
    lambda = tr(x'*H*x)
    norms = []
#    Eye = Matrix{Float64}(I, size(x,1), size(x,1))
    for n = 1:maxiter
#        D = x*x'
#        Dperp = Eye - D
        P = inv(H)           # preconditioner
        r = H*x - lambda*x      # residual
        z = P*r
        z = x - alpha*z
        x = z/norm(z)
        lambda = tr(x'*H*x)
        res = norm(H*x-lambda*x)
        println(n,"\t",res, "\t", tr(x'*H*x))
        res < tol && break
        push!(norms, res)
    end
    norms
end

function MetricGradientFlow(X0, tol, maxiter)
    println("-- Metric Gradient Flow --")
    x = X0/norm(X0)
    lambda = tr(x'*H*x)
    norms = []
    Eye = Matrix{Float64}(I, size(x,1), size(x,1))
    res = norm(H*x-lambda*x)
    r = H*x-lambda*x
    for n = 1:maxiter
        xold = x
#        D = x*x'
#        Dperp = Eye - D
        P = inv(H) #-lambda*Eye)       # Define metric
#        y = P*x
#        z = P*H*x                   # Sobolev gradient

        # inverse InvPower
        if(n==1)
            x = P*x
            x = x/norm(x)
            lambda = tr(x'*H*x)
        end

#        ht = x
        #ht = inv(H)*x
        #ht = inv(H-x*x')*x
        ht = inv(H-lambda*Eye)*x
        at = tr(x'*ht)
        dx = -(x - ht/at)  #xt/at - x
        #M = H
        #G = Gz(M,x)
        #Pr = project(z,G,x)
        #dx = -Pr

        #dx = -z + tr(z'*x)/tr(x'*y)*y

        # Version 1
        #dx = P*(-H*x + tr(z'*x)/tr(x'*y)*x)

        x = x + dx
#        x = P*x
        x = x/norm(x)
        #lambda = tr(x'*H*x)
        lambda = tr(xold'*H*x)/tr(xold'*x)


        res = norm(H*x-lambda*x)
        println(n,"\t",res, "\t", tr(x'*H*x))
        res < tol && break
        push!(norms, res)
    end
    norms
end

## Sent through Slack
function GradientFlow(X0, tol, maxiter)
    println("-- Gradient Flow --")
    x = X0/norm(X0)
    lambda = tr(x'*H*x)
    norms = []
    Eye = Matrix{Float64}(I, size(x,1), size(x,1))
    res = norm(H*x-lambda*x)
    r = H*x-lambda*x
    for n = 1:maxiter
        xold = x
        G = Gz(H,x)
        Pr = project(xold,G,x) # xold was z before
        dx = -Pr
        x = x + dx
        x = x/norm(x)
        lambda = tr(x'*H*x)/tr(x'*x)
        res = norm(H*x-lambda*x)
        println(n,"\t",res, "\t", tr(x'*H*x))
        res < tol && break
        push!(norms, res)
    end
    norms
end


function project(v,Gz,z)
    v - tr(z'*v)/tr(Gz'*z)*Gz
end

function Gz(M,z)
    inv(M)*z
end


function test(X0, tol, maxiter)
    println("-- Test --")
    x = X0/norm(X0)
    lambda = tr(x'*H*x)
    norms = []
    Eye = Matrix{Float64}(I, size(x,1), size(x,1))
    r = H*x-lambda*x
    res = norm(r)
    for n = 1:maxiter
        xold = x
        r = H*x-lambda*x
        Htilde = H - lambda*Eye

        D = x*x'
        Dperp = Eye - D
        Hess = (H-lambda*Eye)
        p = x
        #p = Dperp*p
        #println(p)
        p = p/norm(p)
        z = inv(Hess)*p

        x = z/norm(z)
        lambda = tr(x'*H*x)
        lambda = tr(xold'*H*x)/tr(xold'*x)

        res = norm(H*x-lambda*x)
        println(n,"\t",res, "\t", tr(x'*H*x))
        res < tol && break
        push!(norms, res)
    end
    norms
end

Nvp = 1
Nconv = Nvp
α = 5
using Random
Random.seed!(0)
X0 = randn(N, Nvp)
println(X0)
println("------------------- start ---------------------")
res1 = InvPower(X0, 1e-8, 200)
#res2 = RQI(X0, 1e-8, 200)
res4 = SteepestDescent(X0, 1e-8, 200)
#res3 = Newton(X0, 1e-8, 200)
res5 = MetricGradientFlow(X0, 1e-8, 200)
res6 = GradientFlow(X0, 1e-8, 200)
println("------------------- end --------------------")
L = eigvals(H)
println(minimum(L))
println(maximum(L))




