using LinearAlgebra
using SparseArrays
using Plots
using LaTeXStrings
using IterativeSolvers
using AlgebraicMultigrid

const ⊗ = kron

nsd = 50
lsd = 3
ol = 1

Lx = nsd*lsd-(nsd-1)*ol;
Ly = 2; # CONST

nx = Lx+1; # h_x = const
ny = Ly+1; # h_y = const
N = (nx-2) * (ny-2);
dx = Lx/(nx-1);
dy = Ly/(ny-1);
xVals = range(0, stop=Lx, step=dx)';
yVals = range(0, stop=Ly, step=dy)';

# Laplacian matrix:
# See: https://en.wikipedia.org/wiki/Kronecker_sum_of_discrete_Laplacians
println("Start assemble")

# Negative Laplacian:
Ex = sparse(2:nx-2, 1:nx-3,1, nx-2, nx-2);
Axl = - Ex - Ex' + 2 * sparse(I, nx-2, nx-2);
Ey = sparse(2:ny-2, 1:ny-3, 1, ny-2, ny-2);
Ayl = - Ey - Ey' + 2 * sparse(I, ny-2, ny-2);
Ax = Hermitian(I(ny-2) ⊗ Axl/dx^2)
Ay = Hermitian(Ayl/dy^2 ⊗ I(nx-2))

# # Potential:
# # V(x,y) = 0
# # V(x,y) = 10000
# V(x,y) = 1 + ((x-Lx/2)^2+(y-Ly/2)^2) / ((Lx/2)^2+(Ly/2)^2)
# V(x,y) = 0.00000001 * (1 + ((x-Lx/2)^2+(y-Ly/2)^2) / ((Lx/2)^2+(Ly/2)^2))
# # V(x,y) = 1/(sqrt((x-Lx/2)^2+(y-Ly/2)^2)+1)
# # V(x,y) = 0.1 * exp(-( (x-Lx/2)^2/(2*(Lx/4)^2) + (y-Ly/2)^2/(2*(Ly/4)^2) )) # TODO: Why scaling with LOPCG? Due to collapsing first two eigenstates!!
# # V(x,y) = -0.1 * exp(-( (x-Lx/2)^2/(2*(Lx/4)^2) + (y-Ly/2)^2/(2*(Ly/4)^2) )) + 0.2
# # V(x,y) = - 1/(sqrt((x-Lx/2)^2+(y-Ly/2)^2) + 10) + 2
# # V(x,y) = Lx^2
# # V(x,y) = 0.01 * ( (x-Lx/2)^2+(y-Ly/2)^2 + 1 )
# # V(x,y) = 0.01 * ( ((2x-Lx)/Lx)^2+((2y-Ly)/Ly)^2 + 1 )
# # V(x,y) = 0
# values = [[xx,yy] for xx in xVals[2:end-1] for yy in yVals[2:end-1]]
# Apot = Diagonal(diagm(V.([values[i][1] for i=1:N],[values[i][2] for i=1:N])))

# Splitted Potential
V0 = 0
Vx(x) = 0
Vy(y) = 0
# Vx(x) = ((x-Lx/2)^2) / ((Lx/2)^2+(Ly/2)^2) #! works
# Vy(y) = ((y-Ly/2)^2) / ((Lx/2)^2+(Ly/2)^2)
# Vx(x) = ((x-Lx/2)^2) / ((Lx/2)^2) #! fails
# Vy(y) = ((y-Ly/2)^2) / ((Ly/2)^2)
# Vx(x) = exp(-( (x-Lx/2)^2/(2*(1/4)^2))) #! fails
# Vy(y) = exp(-( (y-Ly/2)^2/(2*(1/4)^2)))
# Vx(x,y) = x^2
# Vy(x,y) = y^2 #! SCALES, also for IP with P=I because no collapsing gap
# Vx(x) = abs(x-Lx/2)
# Vy(y) = abs(y-Ly/2)
# Vx(x) = (x-Lx/2)^2
# Vy(y) = (y-Ly/2)^2 #! SCALES because no collapsing gap

V(x,y) = V0 + Vx(x) + Vy(y)
values = [[xx,yy] for xx in xVals[2:end-1] for yy in yVals[2:end-1]] # FIXME: ob
AV0 = I(nx-2) ⊗ V0 ⊗ I(ny-2)
AV0y = V0 ⊗ I(ny-2)
AV0x = I(nx-2) ⊗ V0
AVyl = Diagonal(diagm(Vy.(yVals[2:end-1])))
AVy = AVyl ⊗ I(nx-2)
AVxl = Diagonal(diagm(Vx.(xVals[2:end-1])))
AVx = I(ny-2) ⊗ AVxl
# AVx = Diagonal(diagm(Vx.([values[i][1] for i=1:N],[values[i][2] for i=1:N])))
# AVy = Diagonal(diagm(Vy.([values[i][1] for i=1:N],[values[i][2] for i=1:N])))
Apot = AV0 + AVx + AVy

A = Hermitian(Ax + Ay + Apot);
Ainitial = A

println("End assemble")


# 210316: New test both scalability: eigen and dd?
# 210526: DOESNT WORK SINCE inv(Array(Lx^2*(A-λinf*I(size(A)[1])))) is required. This is the system that we actually want to solve.. :(
λinf = eigen(Array(Ay+AVy+AV0)).values[1]
# λinf = 0
# A = inv(inv(Array(Lx^2*(A-λinf*I(size(A)[1]))))+I(size(A)[1]))
# A = inv(Array(Lx^2*(A-λinf*I(size(A)[1]))))+I(size(A)[1])

# A = inv(Array(Ainitial))' * Hermitian(A-λinf*I(size(A)[1])) *inv(Array(Ainitial))

# A = inv(Array(Ainitial))*Hermitian(A-λinf*I(size(A)[1]))


# C = cholesky(Array(Ainitial))
# L, Lstar = Array(C.L), Array(C.U)
# A = inv(L) * A * inv(Lstar)

# A = inv(Array(A)) * (I(size(A)[1]) - λinf * inv(Array(A)))
# eigenvec = eigen(Array(A)).vectors[:,1]
# Pi = I(size(A)[1]) - eigenvec * eigenvec'
# r = A * eigenvec - λinf * eigenvec
# A = Pi * (A-λinf*I(size(A)[1])) * Pi
# A = inv(inv(Array(Lx^2*(A-λinf*I(size(A)[1]))))+Lx^2/λinf*I(size(A)[1]))
A = A - λinf * I(size(A)[1])

# Laplace
# A = A # works

# dxx dyy u
# A = Ax + 1*I(N) # works
# A = Ax*Ay + 1*I(N) # works
# A = Ax*Ay + 4*I(N) # works
# A = Ax*Ay + diagm(0 => [x for x in 1:N]) # works
# A = Ax*Ay + diagm(V.(xVals[2:end-1])) # works for good potential
# A = Ax*Ay + Ax # fails
# A = Ax*Ay + Ax + Ay # fails
# A = Ax*Ay + Ax*Ax + Ay*Ay # works
# A = Ax*Ay # fails
# A = Ax # fails
# A = Ax + I(N) # works
# A = - Ax - I(N) # works
# A = - Ax + I(N) # fails
# A = Ax + 0.1*I(N) # works but slows down
# A = Ax*Ay + Ay # works
# A = Ay * inv(Ax + Ay) * Ax # fails
# A = inv(inv(Ax) * inv(Ay))
# A = Ax^0.5 + Ax*Ay + Ay^0.5
# A = Ax+inv(Ay) # works
# A = Ax+I(N) # works
# A = A-inv(Ay) # works
# A = (1/Lx)*Ax+Ay # works
# A = 1/cond(Ax)*Ax+1/cond(Ay)*Ay # works
# A = Lx*Ax+Ly*Ay # fails
# A = inv(A)*Ax # fails
# A = A-Ay # fails
# A = Ax+Ay*Ay # fails
# A = Ax*Ax+Ay*Ax # fails
# A = Ax*Ax*Ay+Ay*Ax*Ay # fails
# A = 9*I(N) - Ax # fails
# A = Ax # work for rhs = A*rhs (preconditioning)
# A = inv(inv(Ax)+inv(Ay)) # fails
# A = Ax*Ax + Ay*Ay
# A = 1/10 * I(N) + A # works for good potential positive
# A = Ay*A*Ax # fails
# A = Ay*A*Ax + I(N) # works
# A = (Ax+Ay)*inv(Ay) # works
# A = (Lx*Ax+Ly*Ay)/(Lx+Ly) # fails
# A = A*inv(Ax)+A*inv(Ay) # fails
# A = inv(inv(Ax)+I(N))+I(N)
# A = -1I(N)-Ax
# A = I(N)+(Lx^2*Ax+Ly^2*Ay)/(Lx^2+Ly^2) # works
# A = I(N)/(Lx^2+Ly^2)+(Lx^2*Ax+Ly^2*Ay)/(Lx^2+Ly^2) # fails
# A = (Lx^2*Ax+Ly^2*Ay) # fails
# A = 1/(Lx^2)*I(N)+Ax # fails
# A = Ax*Ay # fails
# A = (Lx^2*Ax+Ly^2*Ay)/(Lx^2+Ly^2) # fails
# A = (Array(Ax)+AVx)*A

# Δt = 1E4
# PDelta = inv(1/Lx^2 * I(N) + (Lx^2*Ax+Ly^2*Ay)/(Lx^2+Ly^2)) # works
# PV = inv(Lx^2*I(N) + Apot)
# PC1 = PV * PDelta
# A = inv(PC1)


f = normalize(ones(N), 2)
# f = inv(L) * normalize(ones(N), 2)
# f = r
# f = zeros(N)
# f = inv(L) * eigen(Array(Ainitial)).vectors[:,1]
# f = eigen(Array(Ainitial)).vectors[:,2]
# f = zeros(N)
sol = A\f
# sol = Hermitian(Ainitial-λinf*I(size(A)[1])) \ f


Ri = Matrix{Float64}[]
Ai = Matrix{Float64}[]
for sd = 1:nsd
  println(sd)
  nysd = (ny-2)
  if sd==1
    nxsd = lsd
    Rsd = SparseMatrixCSC(zeros(nxsd*nysd,N))
    Rsd[1:nxsd,1:nxsd*nysd] = I(nxsd*nysd)
    push!(Ri, Rsd)
    push!(Ai, Rsd * A * Rsd')
    # display(Ri[sd])
    # display(Ai[sd])
  elseif sd==nsd
    nxsd = lsd
    Rsd = SparseMatrixCSC(zeros(nxsd*nysd,N))
    Rsd[1:nxsd,(sd-1)*lsd-(sd-1)*ol:end] = I(nxsd*nysd)
    push!(Ri, Rsd)
    push!(Ai, Rsd * A * Rsd')
    # display(Ri[sd])
    # display(Ai[sd])
  else
    nxsd = lsd+1
    Rsd = SparseMatrixCSC(zeros(nxsd*nysd,N))
    Rsd[1:nxsd,(sd-1)*lsd-(sd-1)*ol:sd*lsd-(sd-1)*ol] = I(nxsd*nysd)
    push!(Ri, Rsd)
    push!(Ai, Rsd * A * Rsd')
    # display(Ri[sd])
    # display(Ai[sd])
  end
end

# Nicolaides coarse space, p129 ddbook 2015
Z = Array(zeros(N,nsd))
for sd = 1:nsd
  Z[:,sd] = Ri[sd]'*Ri[sd]*ones(N)
end
R0 = Z'
A0 = R0*A*R0'
coarse_correction = false

println("start iter")
# x_i = sol +. rand(N)
x_i = sol + 100*sin.(0.5*pi*xVals[2:end-1]./lsd) # works!!
# x_i = rand(N)
# x_i = zeros(N)
# x_i = ones(N)
# x_i = eigen(Array(A)).vectors[:,1]

display(plot(xVals[2:end-1], x_i))


for it = 1:10
  global x_i
  if coarse_correction
    x_i = x_i + R0'*inv(A0)*R0 * (f-A*x_i)
  end
  for sd = 1:nsd
    x_i = x_i + Ri[sd]' * inv(Ai[sd]) * Ri[sd] * (f-A*x_i)
    # display(x_i)
  end
  display(plot(xVals[2:end-1],[x_i,sol],label = ["x_i" "sol"]))
  error = norm(sol-x_i,2)
  # error = norm(sol-inv(Array(Lstar))*x_i,2)
  rel_l2_error = norm(error, 2)/norm(sol, 2)
  println("iter ", it, " abs l2 error: ", error, " rel l2 error: ", rel_l2_error)
end
println("end iter")

# x_i = inv(Array(Lstar)) * x_i



# println("Start AMG preconditioner")
# ml = ruge_stuben(A) # Construct a Ruge-Stuben solver
# println("End AMG preconditioner")

# N = size(A)[1]
# f = normalize(ones(N), 2)
# p = aspreconditioner(ml)
# # p = I(N)

# x_i, log = cg(A, f, Pl = p, log=true, verbose=true, maxiter=5)
# # minres!(x_i, A, f, maxiter=30)



error = sol - x_i
rel_error = error / norm(sol, 2)
rel_l2_error = norm(error, 2)/norm(sol, 2)
println("relative error: ", rel_l2_error)
plot_errors = false
if plot_errors
  plot(xVals[2:end-1],[error,rel_error],label = ["error" "rel_error"], layout = (2, 1))
end




# heatmap(xVals[2:end-1],yVals[2:end-1],hcat([x_i[(i-1)*(nx-2)+1:i*(nx-2)] for i=1:(ny-2)]...)')
display(plot(xVals[2:end-1],[x_i,sol],label = ["x_i" "sol"]))

# surface(xVals[2:end-1],yVals[2:end-1],hcat([x_i[(i-1)*(nx-2)+1:i*(nx-2)] for i=1:(ny-2)]...)')
