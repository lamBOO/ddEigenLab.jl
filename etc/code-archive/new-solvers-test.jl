using Printf
using LinearAlgebra
using SparseArrays

using ddEigenLab
using ddEigenLab.Geometries


# ## Setup Long Chain Domain
Ω = Rectangle2D(10000, 10, 1, 1)
V0 = 0
Vx(x) = x -> 0
Vy(y) = 0
L = NegativeLaplacePlusPotential(V0, Vx, Vy)
Lh = FD2D(L, Ω)

imax = 100
tol = 1E-14
x_i, lambda_i, errors = @time ddEigenLab.IP(Lh.A, 0, 1, 1, tol, imax)

x, it = @time ddEigenLab.ipm(Lh.A, x0=ones(size(Lh.A,2)), tol=tol, period=1, maxit=imax);
