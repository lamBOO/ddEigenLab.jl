using LinearAlgebra

A=[1 0;0 2]

lambda=1
x0 = 1/sqrt(2) * ones(2)

x1 = x0 - pinv(A-lambda*I(2)) * (A-((x0' * A * x0)/(x0' * x0))*I(2)) * x0
x2 = x1 - pinv(A-lambda*I(2)) * (A-((x1' * A * x1)/(x1' * x1))*I(2)) * x1