# # 2D Laplacian plus Potential
# Solves the Laplacian eigenvalue problem with zero Dirichlet boudnary conditoy
# on a rectangular domain (x,y) ∈ Ω = [0,Lx]×[0,Ly]

using Printf
using LinearAlgebra
using SparseArrays
using DataFrames
using CSV

using ddEigenLab
using ddEigenLab.Geometries





verbose = false
imax = 100000
tol = 1E-10
Ly = 2^3
outputfolder = "."

if !isdir(outputfolder) mkdir(outputfolder) end


Lxs_exps = 3:6
Lxs = 2 .^Lxs_exps
rhos = [1,2,3]
# rhos = [1]
potentials = Dict(
  "squared-sinus" => Dict(),
  "sawtooth" => Dict(),
  "smooth-bumps" => Dict(),
  "hydrogen-helium-period" => Dict(),
  "symmetric-perturbed-hyhe" => Dict(),
)
xperiods = Dict(
  "squared-sinus" => 1,
  "sawtooth" => 1,
  "smooth-bumps" => 1,
  "hydrogen-helium-period" => 2,
  "symmetric-perturbed-hyhe" => 1,
)
methods = [
  "IP-noshift",
  "IP-optimalshift",
]

dfs = Dict(
  [methodname => Dict(
    [potname => DataFrame(
      "Lx" => Lxs,
      [string(rho) => ones(size(Lxs)[1]) for rho in rhos]...
    ) for (potname,_) in potentials]
  ) for methodname in methods]
)

for (iLx, Lx) in enumerate(Lxs)

  for rho in rhos

    Ω = Rectangle2D(Lx, Ly, rho, rho, Dict(:y => :d, :x => :d))
    Z = 10
    potentials["squared-sinus"] = Dict(
      "V0" => 0,
      "Vx" => x -> 0,
      "Vy" => y -> 0,
      "Vxy" => (x,y) -> Z*sin(pi*Ω.Lx)^2 + y^2
      )
    potentials["sawtooth"] = Dict(
      "V0" => 0,
      "Vx" => x -> 0,
      "Vy" => y -> 0,
      "Vxy" => (x,y) -> Z*(x-floor(x)) + y^2
      )
    R=(1/2)^-1
    ciarray=[i-0.5 for i=1:Ω.Lx]
    potentials["smooth-bumps"] = Dict(
      "V0" => 0,
      "Vx" => x -> 0,
      "Vy" => y -> 0,
      "Vxy" => (x,y) -> Z - ( Z*sum(exp(-((x-ci)^2 + (y-Ω.Ly/2)^2)/((1/R)^2 - ((x-ci)^2 + (y-Ω.Ly/2)^2))) * (sqrt((x-ci)^2 + (y-Ω.Ly/2)^2) < 1/R) for ci in ciarray) )
    )
    potentials["hydrogen-helium-period"] = Dict(
      "V0" => 0,
      "Vx" => x -> 0,
      "Vy" => y -> 0,
      "Vxy" => (x,y) -> Z - ( (Z-Z/4*(abs(x%2-0.5)<=0.5 ? 1 : 0))*sum(exp(-((x-ci)^2 + (y-Ω.Ly/2)^2)/((1/R)^2 - ((x-ci)^2 + (y-Ω.Ly/2)^2))) * (sqrt((x-ci)^2 + (y-Ω.Ly/2)^2) < 1/R) for ci in ciarray) )
    )
    potentials["symmetric-perturbed-hyhe"] = Dict(
      "V0" => 0,
      "Vx" => x -> 0,
      "Vy" => y -> 0,
      "Vxy" => (x,y) -> Z - ( (Z-Z/2*(abs(x-Ω.Lx/2)<=1.0))*sum(exp(-((x-ci)^2 + (y-Ω.Ly/2)^2)/((1/R)^2 - ((x-ci)^2 + (y-Ω.Ly/2)^2))) * (sqrt((x-ci)^2 + (y-Ω.Ly/2)^2) < 1/R) for ci in ciarray) )
    )

    for (potname, pot) in potentials
      L = NegativeLaplacePlusPotential(pot["V0"], pot["Vx"], pot["Vy"], pot["Vxy"])
      Lh = FD2D(L, Ω)

      λ∞ = eigen(Array(FD2DXPeriodic(L, Rectangle2D(xperiods[potname], Ω.Ly, rho, rho, Dict(
        :y => :d, :x => :p
      ))).A)).values[1]

      methodname = "IP-noshift"
      x_i, lambda_i, errors, info = ddEigenLab.IP(Lh.A, 0, 1, 1, tol, imax, 0, 0*ones(size(Lh.A)[1]), verbose)
      println(string(Lx) * " " * string(rho) * " " * methodname * " " * potname * " " * string(info.it))
      dfs[methodname][potname][!, string(rho)][iLx] = info.it
      if !info.isconverged throw(ErrorException("not converged")) end

      methodname = "IP-optimalshift"
      x_i, lambda_i, errors, info = ddEigenLab.IP(Lh.A, λ∞, 1, 1, tol, imax, 0, 0*ones(size(Lh.A)[1]), verbose)
      println(string(Lx) * " " * string(rho) * " " * methodname * " " * potname * " " * string(info.it))
      dfs[methodname][potname][!, string(rho)][iLx] = info.it
      if !info.isconverged throw(ErrorException("not converged")) end

      # methodname = "IP-changedmatrix"
      # x_i, lambda_i, errors, info = ddEigenLab.IP((Lh.Ax+Lh.AVx)*(Lh.Ay+Lh.AVy)*Lh.A, 0, 1, 1, tol, imax, 0, 0*ones(size(Lh.A)[1]), verbose)
      # println(string(Lx) * " " * string(rho) * " " * methodname * " " * potname * " " * string(info.it))
      # dfs[methodname][potname][!, string(rho)][iLx] = info.it
      # if !info.isconverged throw(ErrorException("not converged")) end

      # methodname = "SD-optimalshift"
      # x_i, lambda_i, errors, info = ddEigenLab.SD(Lh.A, P_λ∞, 1, 1, tol, imax, 0, 0*ones(size(Lh.A)[1]), verbose)
      # println(string(Lx) * " " * string(rho) * " " * methodname * " " * potname * " " * string(info.it))
      # dfs[methodname][potname][!, string(rho)][iLx] = info.it
      # if !info.isconverged throw(ErrorException("not converged")) end

      # methodname = "SD-changedmatrix"
      # x_i, lambda_i, errors, info = ddEigenLab.SD(inv(P_λ∞), P_λ∞, 1, 1, tol, imax, 0, 0*ones(size(Lh.A)[1]), verbose)
      # println(string(Lx) * " " * string(rho) * " " * methodname * " " * potname * " " * string(info.it))
      # dfs[methodname][potname][!, string(rho)][iLx] = info.it
      # if !info.isconverged throw(ErrorException("not converged")) end

      # methodname = "SD-split1"
      # x_i, lambda_i, errors, info = ddEigenLab.SD_split([Lh.Ax+Lh.AVx, Lh.Ay+Lh.AVy, Lh.AV0], [Px, Py, P0], 1, 1, tol, imax, 0, 0*ones(size(Lh.A)[1]), verbose)
      # println(string(Lx) * " " * string(rho) * " " * methodname * " " * potname * " " * string(info.it))
      # dfs[methodname][potname][!, string(rho)][iLx] = info.it
      # if !info.isconverged throw(ErrorException("not converged")) end

      # methodname = "LOPCG"
      # x_i, lambda_i, errors, info = ddEigenLab.LOPCG(Lh.A, P_λ∞, 1, 1, tol, imax, 0, 0*ones(size(Lh.A)[1]), verbose)
      # println(string(Lx) * " " * string(rho) * " " * methodname * " " * potname * " " * string(info.it))
      # dfs[methodname][potname][!, string(rho)][iLx] = info.it
      # if !info.isconverged throw(ErrorException("not converged")) end

      # methodname = "SD-split2"
      # x_i, lambda_i, errors, info = ddEigenLab.SD_split2([Lh.Ax+Lh.AVx, Lh.Ay+Lh.AVy, Lh.AV0], [Px, Py, P0], 1, 1, tol, imax, 0, 0*ones(size(Lh.A)[1]), verbose)
      # println(string(Lx) * " " * string(rho) * " " * methodname * " " * potname * " " * string(info.it))
      # dfs[methodname][potname][!, string(rho)][iLx] = info.it
      # if !info.isconverged throw(ErrorException("not converged")) end

    end

  end

end


for (method, data) in dfs
  for (potname, _) in potentials
    println(method * "_" * potname)
    display(dfs[method][potname])
    CSV.write(outputfolder * "/" * method * "_" * potname * ".csv", dfs[method][potname])
  end
end


