using Gridap
using Gridap.Fields

ρ = 20

domain = (0, 1, 0, 1)
partition = (1 * ρ, 1 * ρ)
model = CartesianDiscreteModel(domain, partition)
reffe = ReferenceFE(lagrangian, Float64, 1)
V = TestFESpace(model, reffe)

# Periodic function in unit cell
f(x) = sin(pi*x[1]) * sin(pi*x[2])
fh = interpolate(f, V)

domain1 = (0, 2, 0, 2)
partition1 = (2 * ρ, 2 * ρ)
model1 = CartesianDiscreteModel(domain1, partition1)
reffe1 = ReferenceFE(lagrangian, Float64, 1)
V1 = TestFESpace(model1, reffe1)

# interpolate(fh, V1)  # error, triangulations don't have same background model

@time interpolate(
  x -> fh(VectorValue(mod.(get_array(x),1))), V1
)
# 🔝 16.918525 seconds (12.97 M allocations: 6.106 GiB, 15.72% gc time, 3.19% compilation time)

j(x) = fh(VectorValue(mod.(get_array(x),1)))
@time interpolate(
  j, V1
)
# 🔝 16.918525 seconds (12.97 M allocations: 6.106 GiB, 15.72% gc time, 3.19% compilation time)

# @time interpolate(
#   x->norm(x), V1
# )
# # 0.566446 seconds (1.05 M allocations: 55.431 MiB, 11.19% gc time, 99.06% compilation time)

# h(x) = norm(x)
# @time interpolate(
#   h, V1
# )
# # 0.001022 seconds (171 allocations: 94.562 KiB)

# # h(x) = norm(x)
# @time interpolate(
#   norm, V1
# )
# # 0.001022 seconds (171 allocations: 94.562 KiB)

cache = return_cache(fh, Point(0,0))
const cache3 = return_cache(fh,Point(0.,0.))
i(x) = evaluate!(cache3,fh,Point(mod.(get_array(x),1) .* [1,1-1E-6]))
@time interpolate(
  i, V1
)
# 0.001022 seconds (171 allocations: 94.562 KiB)
