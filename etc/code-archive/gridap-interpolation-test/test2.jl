using Gridap
using GridapGmsh
using Gridap.Fields
using Gridap.Geometry

model = GmshDiscreteModel("examples/fem/gridap-interpolation-test/SimpleBox3D.msh") # range [-2:2, -3:3, -0.5:0.5]

u(x) = x

order = 0
reffe = ReferenceFE(nedelec, Float64, order)

V = TestFESpace(model, reffe, vector_type = Vector{ComplexF64})

Ω = Triangulation(model)
uh3d = interpolate(u,V)

const cache2 = return_cache(uh3d,Point(0.,0.,0.))
f((x,y)) = evaluate!(cache2,uh3d,Point(x,y,0))
writevtk(Ω, "sol2d", nsubcells=4, cellfields=["uh3d"=>f])
