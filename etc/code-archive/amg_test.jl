using AlgebraicMultigrid
using IterativeSolvers

using LinearAlgebra
using SparseArrays
using Plots
using LaTeXStrings

const ⊗ = kron

nsd = 99
lsd = 2
ol = 1

Lx = nsd*lsd-(nsd-1)*ol;
Ly = 2; # CONST

nx = Lx+1; # h_x = const
ny = Ly+1; # h_y = const
N = (nx-2) * (ny-2);
dx = Lx/(nx-1);
dy = Ly/(ny-1);
xVals = range(0, stop=Lx, step=dx)';
yVals = range(0, stop=Ly, step=dy)';

# Laplacian matrix:
# See: https://en.wikipedia.org/wiki/Kronecker_sum_of_discrete_Laplacians
println("Start assemble")
Ex = sparse(2:nx-2, 1:nx-3, 1, nx-2, nx-2);
Axl = - Ex - Ex' + 2 * sparse(I(nx-2));
Ey = sparse(2:ny-2, 1:ny-3, 1, ny-2, ny-2);
Ayl = - Ey - Ey' + 2 * sparse(I(ny-2));
Ax = SparseMatrixCSC(I(ny-2) ⊗ Axl/dx^2)
Ay = SparseMatrixCSC(Ayl/dy^2 ⊗ I(nx-2))
A = SparseMatrixCSC(Ax + Ay);

# Potential
V(x,y) = 0
# V(x,y) = Lx^2
# V(x,y) = 0.01 * ( (x-Lx/2)^2+(y-Ly/2)^2 + 1 )
# V(x,y) = 0.01 * ( ((2x-Lx)/Lx)^2+((2y-Ly)/Ly)^2 + 1 )
# V(x,y) = 0
values = [[xx,yy] for xx in xVals[2:end-1] for yy in yVals[2:end-1]]
Apot = Diagonal(diagm(V.([values[i][1] for i=1:N],[values[i][2] for i=1:N])))
# A += Apot
println("End assemble")

println("Start matrix")
mat = poisson(10000)
# mat = A
println("End matrix")

println("Start AMG preconditioner")
ml = ruge_stuben(mat) # Construct a Ruge-Stuben solver
println("End AMG preconditioner")

N = size(mat)[1]
f = normalize(ones(N), 2)
p = aspreconditioner(ml)
# p = I(N)

c, log = cg(mat, f, Pl = p, log=true, verbose=true)
# c, log = cg(mat, f, Pl = Identity(), log=true, verbose=true)

print(log)

make_plot = false
if make_plot
  plot(c)
end