"""
Solves the Laplacian eigenvalue problem with zero Dirichlet boudnary conditons
on a rectangular domain (x,y) ∈ Ω = [0,Lx]×[0,Ly]
"""

using Printf
using LinearAlgebra
using SparseArrays
using Plots
using LaTeXStrings

# **************************************************************************** #
# FUNCTIONS
# **************************************************************************** #

"Return Rayleigh coefficient for given vector x"
function λ(x, A)
  ((x' * A * x)/(x' * x))[1]
end

"Calculates the erorrs"
function calc_errors(eval_approx, eval_real, evec_approx, evec_real, A)
  error_val = eval_approx - eval_real
  Linf_val = max( abs(error_val) ) / abs(eval_real)

  # Use sum to charaterize sign of eigenvectors (sgn(1) fails for very low)
  evec_approx_sum = sum(evec_approx)
  evec_real_sum = sum(evec_real)

  evec_approx_positive = sign(evec_approx_sum) * evec_approx
  evec_real_positive = sign(evec_real_sum) * evec_real[:,1]
  error_vec = evec_approx_positive - evec_real_positive
  L1_vec = sum(broadcast(abs, error_vec))
  L2_vec = sqrt(sum(error_vec.^2))
  Linf_vec = broadcast(max, broadcast(abs, error_vec)...)
  error_res = A * evec_approx - eval_approx * evec_approx
  L1_res = sum(broadcast(abs, error_res))
  L2_res = norm(error_res, 2)
  Linf_res = broadcast(max, broadcast(abs, error_res)...)
  return Dict([
    ("Linf_val", Linf_val),
    ("L1_vec", L1_vec),
    ("L2_vec", L2_vec),
    ("Linf_vec", Linf_vec),
    ("L1_res", L1_res),
    ("L2_res", L2_res),
    ("Linf_res", Linf_res)
  ])
end

"Calculates the erorrs"
function print_errors(errors)
  for (etype, eval) in errors
    @printf "%s=%.5E " etype eval
  end
  @printf "\n"
end

"Map kron function"
const ⊗ = kron

function RQI(A, iₘₐₓ=100)
  println("Start RQI")

  N = size(A)[1]
  xᵢ = normalize(ones(N), 2)
  λᵢ = λ(xᵢ,A)
  errors = []

  let i=1
    while (i <= iₘₐₓ)
      @printf("[%03d] ", i)

      # # Intuitive implementation:
      # xᵢ = normalize(inv(A - λᵢ * I(N)) * xᵢ, 2)
      # λᵢ = λ(xᵢ,A)

      # Templates inplementation with xᵢ=v:
      y = inv(A - λᵢ * I(N)) * xᵢ
      θ = norm(y)
      λᵢ = λᵢ + (y'*xᵢ)/θ^2
      xᵢ = y/θ

      error = calc_errors(λᵢ, eigval[1], xᵢ, eigvec[:,1], A)
      @printf "λᵢ=%.4f " λᵢ
      print_errors(error)
      push!(errors, error)

      i += 1
    end
  end

  println("End RQI")
  return [xᵢ, λᵢ, errors]
end

function GF_BE(A, P, τ=1, α=1, εₜₒₗ=1E-4, iₘₐₓ=100)
  println("Start GF_BE")

  N = size(A)[1]
  xᵢ = normalize(ones(N), 2)
  λᵢ = λ(xᵢ,A)
  errors = []

  #* => Backward Euler AntoineLevitt2018
  # Δt = -1/λᵢ # RQI
  Δt = 1E-2
  alpha_V = 1/Δt
  alpha_Δ = 0.5

  # alpha = λᵢ # w.o. free parameter AntoineLevitt2018
  # PDelta = inv(1/Lx^2 * I(N) + (Lx^2*Ax+Ly^2*Ay)/(Lx^2+Ly^2)) # works
  # PDelta = inv(alpha * I(N) + Ax) # works

  PDelta = inv(alpha_Δ * I(N) + A)
  PV = inv(alpha_V * I(N) + Apot)
  PI = I(N)
  PC = PV^(1/2) * PDelta * PV^(1/2)
  P = PC

  let i=1
    while (i <= iₘₐₓ)
      @printf("[%03d] ", i)

      xᵢ = normalize(P * inv(I(N) + Δt*A) * xᵢ, 2) # xᵢ = A⁻¹ xᵢ₋₁
      λᵢ = λ(xᵢ,A)

      error = calc_errors(λᵢ, eigval[1], xᵢ, eigvec[:,1], A)
      @printf "λᵢ=%.4f " λᵢ
      print_errors(error)
      push!(errors, error)

      i += 1
    end
  end

  println("End GF_BE")
  return [xᵢ, λᵢ, errors]
end

function richardson(A, P, τ=1, α=1, εₜₒₗ=1E-4, iₘₐₓ=100)
  println("Start Richardson")

  N = size(A)[1]
  xᵢ = normalize(ones(N), 2)
  λᵢ = λ(xᵢ,A)

  errors = []

  let i=1
    while (i <= iₘₐₓ)
      @printf("[%03d] ", i)

      #* => Residual based inverse power: (actually also Richardson [Argentati2014])
      # P = pinv(A - λᵢ * I(N)) # Naive idea: Use eval guess (not groundstate)
      pᵢ = (A*xᵢ - λ(xᵢ,A)*xᵢ)
      xᵢ = normalize(τ * xᵢ - α * P * pᵢ, 2)
      # # Mix-in inverse power to set direction to groundstate
      # xᵢ = normalize(A\xᵢ, 2) # set dir  ection to groundstate
      λᵢ = λ(xᵢ,A)

      #* => Scaled inverse power: Neymeyr Habil p27
      # xᵢ = normalize((λᵢ * A)\xᵢ,2) # xᵢ = A⁻¹ xᵢ₋₁
      # λᵢ = λ(xᵢ,A)

      #* => Richardson (Knyazev): Has inverted coeffs
      # w_i = inv(A) * (A * xᵢ - λ(xᵢ, A) * xᵢ)
      # xᵢ = normalize(w_i - (1/λ(xᵢ, A)) * xᵢ, 2)

      # TODO: Implement gradient type method from Knyazev !!!

      error = calc_errors(λᵢ, eigval[1], xᵢ, eigvec[:,1], A)
      @printf "λᵢ=%.4f " λᵢ
      print_errors(error)
      push!(errors, error)

      i += 1
    end
  end

  println("End Richardson")
  return [xᵢ, λᵢ, errors]
end

"""
Fails for too large constant potential..
"""
function IP(A, P, τ=1, α=1, εₜₒₗ=1E-4, iₘₐₓ=100)
  println("Start Inverse Power")

  N = size(A)[1]
  xᵢ = normalize(ones(N), 2)
  λᵢ = λ(xᵢ,A)
  errors = []

  # P = I(N)
  # P = inv(Array(Ax)+AVx)*inv(Array(Ay)+AVy)
  # P = inv(I(N)+Array(A)) * inv(I(N)+Array(Apot))
  # P = inv((Lx^2*Ax+Ly^2*Ay)/(Lx^2+Ly^2))
  # shift = 0

  shift = eigen(Array(Ayl+AVyl+AV0yl)).values[1]
  P = I(N)

  let i=1
    while (i <= iₘₐₓ)
      @printf("[%03d] ", i)

      #* => Collapsed inverse power: Same as residual based IP for non-DD
      # P = I(N)
      # P = inv(A)
      # P = (inv(Apot))^(1/2)*inv(A)*(inv(Apot))^(1/2) # fails
      # P = inv(Ax+10000*I(N)) # fails
      # P = inv(A)*inv(A) # fails
      # P = inv(Ax + I(N)) # fails
      # P = inv(Ax) + I(N) # works
      # P = inv(-Ax) # works
      # P = inv(A*A*Ax) # works
      # P = inv(Ax+inv(Ay)) # fails
      # P = inv(Ax*A*inv(Ax)) # fails
      # P = Ax*inv(A) # works
      # P = inv(A-I(N)) # works but not groundstate
      # P = inv(Ax*Ay + 1*Ax) # works
      # P = inv(Ax*Ay + 1*Ay) # fails
      # P = inv(Ax*Ay + 1*I(N)) # fails
      # P = inv(Ax - 1*I(N)) # fails
      # P = inv(Ax + 0.001*I(N)) # works, but slows down
      # P = inv(Ax*Ay) # works
      # P = inv(Ax) * inv(Ay) # works
      # P = inv(2*Ax*Ay + Ax*Ax + Ay*Ay) # fails
      # P = inv(Ax) + inv(Ay) # works
      # P = inv(Ay) * inv(inv(Ax) + inv(Ay)) * inv(Ax) # doesnt work
      # P = inv(Ay * inv(Ax + Ay) * Ax) # works
      # P = inv(Ay * Ax * inv(Ax + Ay) * Ax * Ay) # works
      # P = inv(Ax * Ay * inv(Ax + Ay)) # works
      # P = inv(Ay)
      # P = inv(A-inv(Ay)) # fails but convreges to non ground
      # P = inv(Ay) # fails
      # P = inv(1/cond(Ax)*Ax+1/cond(Ay)*Ay) # fails
      # P = inv(Lx*Ax+Ly*Ay) # fails
      # P = inv(Ax+Ay*Ay) # fails
      # P = inv(Ax)+inv(Ay) # fails
      # P = inv(0.1 * I(N) - Ax) # fails
      # P = inv(0.1 * I(N) - Ax*Ay) # fails
      # P = inv((Ax+Ay)*inv(Ay)+inv(Ax)) # fails
      # P = inv(Ax)+inv(Ay)
      # P = I(N)
      # P = inv(Ax+I(N))inv(Apot+I(N))
      # P = inv(A*inv(Ax)+A*inv(Ay))
      # P = inv(0.5I(N)-Ax) # works!
      # P = inv(0.5I(N)-Ax) # works but not ground
      # P = inv(Ax+0.1I(N)) # fails
      # P = inv((Lx^2*Ax+Ly^2*Ay)/(Lx^2+Ly^2)) # works!
      # P = inv(I(N)+(Lx^2*Ax+Ly^2*Ay)) # works!
      # P = inv(I(N)+(Lx*Ax+Ly*Ay)) # fails

      xᵢ = normalize(P * ((A-shift*I(N))\xᵢ), 2) # xᵢ = A⁻¹ xᵢ₋₁
      λᵢ = λ(xᵢ,A)

      error = calc_errors(λᵢ, eigval[1], xᵢ, eigvec[:,1], A)
      # error = calc_errors(λᵢ, 0, xᵢ, zeros(N))
      @printf "λᵢ=%.4f " λᵢ
      print_errors(error)
      push!(errors, error)

      i += 1
    end
  end

  println("End Inverse Power")
  return [xᵢ, λᵢ, errors]
end

function SD(A::AbstractArray, P, τ::Real=1, α=1, εₜₒₗ=1E-4, iₘₐₓ=100)
  println("Start SD")

  N = size(A)[1]
  xᵢ = normalize(ones(N), 2)
  λᵢ = λ(xᵢ,A)
  res = Inf
  errors = []

  # Lower bounding preconditioner
  λLimit = eigen(Array(Ayl+AVyl+AV0yl)).values[1]
  print("λLimit: ",λLimit)
  P = inv(Array(A-λLimit*I(N))) # A - eigen(Ay)[1]*I(N) # best in limit # TODO: Use kro

  # P = pinv(A - λᵢ * I(N)) # Naive idea: Use eval guess (not groundstate)
  # P = I(N)
  # P = inv(Ax)
  # P = inv(A)
  # P = inv(A-I(N))
  # P = inv(1*I(N)+Ax*A*Ay) # fails

  let i=1
    while (i <= iₘₐₓ && norm(res,2) > εₜₒₗ)
      @printf("[%03d] ", i)

      res = A*xᵢ - λᵢ*xᵢ
      pᵢ = P * res

      α = RR(A,[xᵢ,pᵢ])[1] # for convention
      # α = -0.5 # Neymeyr1991 PINVIT alpha=-2
      xᵢ = normalize(xᵢ + α * pᵢ, 2)
      λᵢ = λ(xᵢ,A)

      error = calc_errors(λᵢ, eigval[1], xᵢ, eigvec[:,1], A)
      @printf "λᵢ=%.4f " λᵢ
      print_errors(error)
      push!(errors, error)

      i += 1
    end
  end

  println("End SD")
  return [xᵢ, λᵢ, errors]
end

function RR(A, subspace)

  left = Hermitian(hcat([v' for v in subspace]) * hcat([(A*v)' for v in subspace])')
  right = Hermitian(hcat([v' for v in subspace]) * hcat([v' for v in subspace])')

  # Ax = A * x
  # Ap = A * p
  # left = [
  #   x'*Ax x'*Ap
  #   p'*Ax p'*Ap
  # ]
  # right = [
  #   x'*x x'*p
  #   p'*x p'*p
  # ]

  edecomp = eigen(left, right)
  alpha = edecomp.vectors[1,1]
  beta = edecomp.vectors[2,1]

  stepwdiths = [edecomp.vectors[i,1]/edecomp.vectors[1,1] for i=2:length(subspace)]
  return stepwdiths
end

"""
Doesnt work with potential...
Another idea:
pᵢ = Py * (Ax*xᵢ - (1/2)*λᵢ*xᵢ) + Px * (Ay*xᵢ - (1/2)*λᵢ*xᵢ)
"""
function SD_split(A, P, τ=1, α=1, εₜₒₗ=1E-4, iₘₐₓ=100)
  println("Start SD_split")

  N = size(A)[1]
  xᵢ = normalize(ones(N), 2)
  λᵢ = λ(xᵢ,A)
  errors = []

  println("Start Prec")

  # Px = inv(Array(Ax + AVy))
  # Py = inv(Array(Ay + AVx))
  # PV0 = inv(Array(AV0))

  Px = inv(Array(Ax+AVx))
  Py = inv(Array(Ay+AVy))
  PV0 = inv(Array(AV0))

  println("End Prec")

  let i=1
    while (i <= iₘₐₓ)
      @printf("[%03d] ", i)

      pᵢ = (
        + Px * ((Ax+AVx)*xᵢ - λ(xᵢ,Ax+AVx)*xᵢ)
        + Py * ((Ay+AVy)*xᵢ - λ(xᵢ,Ay+AVy)*xᵢ)
        + PV0 * (AV0*xᵢ - λ(xᵢ,AV0)*xᵢ)
      )
      # pᵢ = (
      #   + Px * ((Ax)*xᵢ - λ(xᵢ,Ax)*xᵢ)
      #   + Py * ((Ay)*xᵢ - λ(xᵢ,Ay)*xᵢ)
      #   + PV0 * (Apot*xᵢ - λ(xᵢ,Apot)*xᵢ)
      # )

      α = RR(A,[xᵢ,pᵢ])[1]

      xᵢ = normalize(xᵢ + α * pᵢ, 2)
      λᵢ = λ(xᵢ,A)

      error = calc_errors(λ(xᵢ,A), eigval[1], xᵢ, eigvec[:,1], A)
      @printf "λᵢ=%.4f " λᵢ
      print_errors(error)
      push!(errors, error)

      i += 1
    end
  end

  println("End SD_split")
  return [xᵢ, λᵢ, errors]
end

"""
Doesnt work with potential...
"""
function SD_split2(A, P, τ=1, α=1, εₜₒₗ=1E-4, iₘₐₓ=100)
  println("Start SD_split2")

  N = size(A)[1]
  xᵢ = normalize(ones(N), 2)
  λᵢ = λ(xᵢ,A)
  errors = []

  # fails:
  # Px = I(N)
  # Py = I(N)

  # fails:
  # Px = inv(Array(A))
  # Py = inv(Array(A))

  # works:
  Px = inv(Array(Ax+AVx))
  Py = inv(Array(Ay+AVy))
  PV0 = inv(Array(AV0))

  # fails:
  # Px = inv(I(N)+(Lx^2*Ax)/(Lx^2+Ly^2))
  # Py = inv(I(N)+(Ly^2*Ay)/(Lx^2+Ly^2))

  let i=1
    while (i <= iₘₐₓ)
      @printf("[%03d] ", i)

      px = Px * ((Ax+AVx)*xᵢ - λ(xᵢ,(Ax+AVx))*xᵢ)
      py = Py * ((Ay+AVy)*xᵢ - λ(xᵢ,(Ay+AVy))*xᵢ)
      ppot = PV0 * (AV0*xᵢ - λ(xᵢ,AV0)*xᵢ)
      # printstyled(norm(px), " ", norm(py), " ", norm(ppot), color=:red)

      subspace = [
        xᵢ,px,py
        ,ppot
      ]
      α = RR(A,subspace)[1]
      β = RR(A,subspace)[2]
      γ = RR(A,subspace)[3]
      # printstyled(α, β, γ, color=:green)

      xᵢ = normalize(
        xᵢ
        + α * px
        + β * py
        + γ * ppot
      , 2)
      λᵢ = λ(xᵢ,A)

      error = calc_errors(λ(xᵢ,A), eigval[1], xᵢ, eigvec[:,1], A)
      @printf "λᵢ=%.16f " λᵢ
      print_errors(error)
      push!(errors, error)

      i += 1
    end
  end

  println("End SD_split2")
  return [xᵢ, λᵢ, errors]
end

function LOPCG(A, P, τ=1, α=1, εₜₒₗ=1E-4, iₘₐₓ=100)
  println("Start LOPCG")

  N = size(A)[1]
  xᵢ = normalize(ones(N), 2)
  λᵢ = λ(xᵢ,A)
  p = Array(I(N)[:,1])  # different from xᵢ but unique
  res = Inf
  errors = []

  # fails:
  # P = I(N)

  # fails
  # P = I(N)

  # works:
  # P = inv(Array(A))

  # Lower bounding preconditioner
  λLimit = eigen(Array(Ayl+AVyl+AV0yl)).values[1]
  print("λLimit: ",λLimit)
  P = inv(Array(A-λLimit*I(N))) # A - eigen(Ay)[1]*I(N) # best in limit # TODO: Use kro

  let i=1
    while (i <= iₘₐₓ && norm(res, 2) > εₜₒₗ)
      @printf("[%03d] ", i)

      res = A*xᵢ - λ(xᵢ,A)*xᵢ
      g = P * res

      α = RR(A,[xᵢ,g,p])[1]
      β = RR(A,[xᵢ,g,p])[2]
      p = α * g + β * p

      xᵢ = normalize(xᵢ + p, 2)
      λᵢ = λ(xᵢ,A)

      error = calc_errors(λ(xᵢ,A), eigval[1], xᵢ, eigvec[:,1], A)
      @printf "λᵢ=%.4f " λᵢ
      print_errors(error)
      push!(errors, error)

      i += 1
    end
  end

  println("End LOPCG")
  return [xᵢ, λᵢ, errors]
end
# **************************************************************************** #

# **************************************************************************** #
# PROBLEM DESCRIPTION
# **************************************************************************** #
Lx = 100
Ly = 10 # CONST

rho = 1

nx = rho*Lx+1 # h_x = const
ny = rho*Ly+1 # h_y = const
N = (nx-2) * (ny-2)
dx = Lx/(nx-1)
dy = Ly/(ny-1)
xVals = range(0, stop=Lx, step=dx)'
yVals = range(0, stop=Ly, step=dy)'

# Laplacian matrix:
# See: https://en.wikipedia.org/wiki/Kronecker_sum_of_discrete_Laplacians
println("Start assemble")

# Negative Laplacian:
Ex = sparse(2:nx-2, 1:nx-3,1, nx-2, nx-2)
Axl = - Ex - Ex' + 2 * sparse(I, nx-2, nx-2)
Ey = sparse(2:ny-2, 1:ny-3, 1, ny-2, ny-2)
Ayl = - Ey - Ey' + 2 * sparse(I, ny-2, ny-2)
Ax = I(ny-2) ⊗ Axl/dx^2
Ay = Ayl/dy^2 ⊗ I(nx-2)

# # Potential:
# # V(x,y) = 0
# # V(x,y) = 10000
# V(x,y) = 1 + ((x-Lx/2)^2+(y-Ly/2)^2) / ((Lx/2)^2+(Ly/2)^2)
# V(x,y) = 0.00000001 * (1 + ((x-Lx/2)^2+(y-Ly/2)^2) / ((Lx/2)^2+(Ly/2)^2))
# # V(x,y) = 1/(sqrt((x-Lx/2)^2+(y-Ly/2)^2)+1)
# # V(x,y) = 0.1 * exp(-( (x-Lx/2)^2/(2*(Lx/4)^2) + (y-Ly/2)^2/(2*(Ly/4)^2) )) # TODO: Why scaling with LOPCG? Due to collapsing first two eigenstates!!
# # V(x,y) = -0.1 * exp(-( (x-Lx/2)^2/(2*(Lx/4)^2) + (y-Ly/2)^2/(2*(Ly/4)^2) )) + 0.2
# # V(x,y) = - 1/(sqrt((x-Lx/2)^2+(y-Ly/2)^2) + 10) + 2
# # V(x,y) = Lx^2
# # V(x,y) = 0.01 * ( (x-Lx/2)^2+(y-Ly/2)^2 + 1 )
# # V(x,y) = 0.01 * ( ((2x-Lx)/Lx)^2+((2y-Ly)/Ly)^2 + 1 )
# # V(x,y) = 0
# values = [[xx,yy] for xx in xVals[2:end-1] for yy in yVals[2:end-1]]
# Apot = Diagonal(diagm(V.([values[i][1] for i=1:N],[values[i][2] for i=1:N])))

# Splitted Potential
V0 = 1.8
# Vx(x) = 0
# Vy(y) = 0
# Vx(x) = ((x-Lx/2)^2) / ((Lx/2)^2+(Ly/2)^2)
# Vy(y) = ((y-Ly/2)^2) / ((Lx/2)^2+(Ly/2)^2)
# Vx(x) = ((x-Lx/2)^2) / ((Lx/2)^2)
# Vy(y) = ((y-Ly/2)^2) / ((Ly/2)^2)
sigma_x = Lx^(0.5)
sigma_y = Ly^(0.5)
Vx(x) = 0.1 - 0.1 * exp(-( (x-2*Lx/4)^2/(2*sigma_x^2)))
Vy(y) = 0.1 - 0.1 * exp(-( (y-Ly/2)^2/(2*sigma_y^2)))
# Vx(x,y) = x^2
# Vy(x,y) = y^2
# Vx(x,y) = (x-Lx/2)^2
# Vy(x,y) = (y-Ly/2)^2

V(x,y) = V0 + Vx(x) + Vy(y)
values = [[xx,yy] for xx in xVals[2:end-1] for yy in yVals[2:end-1]] # FIXME: ob
AV0 = I(nx-2) ⊗ V0 ⊗ I(ny-2)
AV0yl = I(ny-2) ⊗ V0
AV0xl = V0 ⊗ I(nx-2)
AVyl = spdiagm(0 => Vy.(yVals[2:end-1]))
AVy = AVyl ⊗ I(nx-2)
AVxl = spdiagm(0 => Vx.(xVals[2:end-1]))
AVx = I(ny-2) ⊗ AVxl
# AVx = Diagonal(diagm(Vx.([values[i][1] for i=1:N],[values[i][2] for i=1:N])))
# AVy = Diagonal(diagm(Vy.([values[i][1] for i=1:N],[values[i][2] for i=1:N])))
Apot = AV0 + AVx + AVy

A = Ax + Ay + Apot

println("End assemble")

println("Start real spectrum")
eigval, eigvec = eigen(Array(A))
println("End real spectrum")
# **************************************************************************** #



# **************************************************************************** #
# PRECONDITIONERS
# **************************************************************************** #
#* Practical preconditioner: Approx stiffnes L=I [Knyazev1998] doesnt work
println("Start assemble preconditioner")

P = I(N)

# Doesnt work
# P = A

#* Jacobi Preconditioner?: Works similar to inv(A)
# P = inv(Diagonal(diag(A)))

#* Continous perfect preconditioner [Knyazev1998]: Doesnt work
# P = inv(A - pi^2*(1^2/Lx^2 + 1^2/Ly^2) * I(N))

#* Algebraic perfect preconditioner [Knyazev1998]: Is perfect, trivial Richards.
# ϵ = 0.00 # perturbation, small perturbation still works for convergence!
# λₑ = eigval[1] # exact eigenvalue
# P = pinv(A - (λₑ+ϵ) * I(N))

# Testing
# P = inv(A - 0.1 * I(N)) # Just for testing

#* Our choice: Leads to inverse power method
# P = inv(A)

println("End assemble preconditioner")
# **************************************************************************** #



# **************************************************************************** #
# CACULATION
# **************************************************************************** #
imax = 50
tol = 1E-14
# x_i, lambda_i, errors = RQI(A, imax)
# x_i, lambda_i, errors = GF_BE(A, P, 1, 1, tol, imax)
# x_i, lambda_i, errors = richardson(A, P, 1, 1, tol, imax)
# x_i, lambda_i, errors = IP(A, P, 1, 1, tol, imax)
# x_i, lambda_i, errors = SD(A, P, 1, 1, tol, imax)
# x_i, lambda_i, errors = SD_split(A, P, 1, 1, tol, imax)
x_i, lambda_i, errors = SD_split2(A, P, 1, 1, tol, imax)
# x_i, lambda_i, errors = LOPCG(A, P, 1, 1, tol, imax)
# display(errors)

# Add one machine precision to avoid error in semilog plot
Linf_val = [errors[i]["Linf_val"] for i=1:length(errors)] .+ eps(Float64)
L1_vec = [errors[i]["L1_vec"] for i=1:length(errors)] .+ eps(Float64)
L2_vec = [errors[i]["L2_vec"] for i=1:length(errors)] .+ eps(Float64)
Linf_vec = [errors[i]["Linf_vec"] for i=1:length(errors)] .+ eps(Float64)
L1_res = [errors[i]["L1_res"] for i=1:length(errors)] .+ eps(Float64)
L2_res = [errors[i]["L2_res"] for i=1:length(errors)] .+ eps(Float64)
Linf_res = [errors[i]["Linf_res"] for i=1:length(errors)] .+ eps(Float64)

# Plot errors
display(plot(
  [i for i=1:length(errors)],
  [Linf_val, L1_vec, L2_vec, Linf_vec, L1_res, L2_res, Linf_res],
  yaxis=:log,
  # xaxis=:log,
  label=[L"\|e_{\lambda_1}\|_{L^\infty}" L"\|e_{u_1}\|_{L^1}" L"\|e_{u_1}\|_{L^2}" L"\|e_{u_1}\|_{L^\infty}" L"\|r\|_{L^1}" L"\|r\|_{L^2}" L"\|r\|_{L^\infty}"],
  title="Groundstate-errors for (Lx,Ly)=($(Lx),$(Ly))", marker = (:circle, 2),
  xaxis="iteration",
  legendfontsize=12
))
savefig("images/($(Lx),$(Ly))")

display("Spectrum statisctis:")
display("max gap $(findmax([eigval[i+1]-eigval[i] for i=1:length(eigval)-1]))")
display("min gap $(findmin([eigval[i+1]-eigval[i] for i=1:length(eigval)-1]))")
display("first gap $(eigval[2]-eigval[1])")
display("first rat $(eigval[1]/eigval[2])")
# **************************************************************************** #

# Plot solution
# TODO: Make subplots
plot_solution = false
if plot_solution
  # Wave function
  display(heatmap(xVals[2:end-1],yVals[2:end-1],hcat([x_i[(i-1)*(nx-2)+1:i*(nx-2)] for i=1:(ny-2)]...)'))
  display(surface(xVals[2:end-1],yVals[2:end-1],hcat([x_i[(i-1)*(nx-2)+1:i*(nx-2)] for i=1:(ny-2)]...)',camera=(30,70)))

  # Density
  display(surface(xVals[2:end-1],yVals[2:end-1],hcat([x_i[(i-1)*(nx-2)+1:i*(nx-2)].^2 for i=1:(ny-2)]...)',camera=(30,70)))
  display(heatmap(xVals[2:end-1],yVals[2:end-1],hcat([x_i[(i-1)*(nx-2)+1:i*(nx-2)].^2 for i=1:(ny-2)]...)',camera=(30,70)))
  display(plot(xVals[2:end-1],yVals[2:end-1],hcat([x_i[(i-1)*(nx-2)+1:i*(nx-2)].^2 for i=1:(ny-2)]...)',camera=(30,70),st=:contourf))
end

plot_potential = false
if plot_potential
  display("Start plot potential")
  potential = [V(xx,yy) for xx in xVals', yy in yVals']
  display(heatmap(xVals',yVals',[V(xx,yy) for xx in xVals', yy in yVals']',title="Pot"))
  display(surface(xVals',yVals',[V(xx,yy) for xx in xVals', yy in yVals']',title="Pot"))
  display("End plot potential")
end

plot_real_solution = false
if plot_real_solution
  display(heatmap(xVals[2:end-1],yVals[2:end-1],sign(eigvec[1,1])*hcat([eigvec[:,1][(i-1)*(nx-2)+1:i*(nx-2)] for i=1:(ny-2)]...)'))
  display(surface(xVals[2:end-1],yVals[2:end-1],sign(eigvec[1,1])*hcat([eigvec[:,1][(i-1)*(nx-2)+1:i*(nx-2)] for i=1:(ny-2)]...)'))
end
