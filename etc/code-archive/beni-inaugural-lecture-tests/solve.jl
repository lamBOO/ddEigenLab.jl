using Gridap
using GridapDistributed
using Gridap.CellData
using Gridap.Arrays
using SparseArrays
import Base.\
using IterativeSolvers
using LinearAlgebra
using Metis

using ddEigenLab


CASE_NAME = "dd-rectangle-dd"

R = 1  # radius of disks
a = 0.0001  # cutoff radius
Z = 1  # strength
S = 0  # shift
p = 8  # inverse mesh size exponent

pot(x) = 0

ρ = 40
Lx = 8
Ly = 1.0
domain2 = (0, Lx, 0, Ly)
partition2 = (Lx * ρ, Ly * ρ)
model2 = CartesianDiscreteModel(domain2, partition2; isperiodic=(false, false))
labels2 = get_face_labeling(model2)
add_tag_from_tags!(labels2, "diri", collect(1:3^2-1))
g = GridapDistributed.compute_cell_graph(model2)


reffe2 = ReferenceFE(lagrangian, Float64, 1)
V2 = TestFESpace(model2, reffe2, dirichlet_tags=["diri"])
U2 = TrialFESpace(V2, 0)
Ω2 = Triangulation(model2)
dΩ2 = Measure(Ω2, 2)
a12(u, v) = ∫(∇(u) ⋅ ∇(v) + (x -> pot(x)) * u * v)dΩ2
# a12(u, v) = ∫(∇(u) ⋅ ∇(v) + (x -> pot(x)) * u * v)dΩ2
a22(u, v) = ∫(u * v)dΩ2
b2(v) = ∫(1 * v)dΩ2
op2 = AffineFEOperator(a12, b2, U2, V2)
@info "End problem"

@info "Start matrices"
stiffnessmat2 = assemble_matrix(a12, U2, V2)
massmat2 = assemble_matrix_and_vector(a22, b2, U2, V2)[1]
rhs = assemble_matrix_and_vector(a22, b2, U2, V2)[2]

uhvec2 = stiffnessmat2 \ (rhs)
uh2 = FEFunction(op2.trial, uhvec2);

uhreal2 = Gridap.solve(op2)


partition = Metis.partition(GridapDistributed.compute_cell_graph(model2), 32)
writevtk(Ω2, "partition", cellfields=["partition" => partition])
Vconst = TestFESpace(model2, ReferenceFE(lagrangian, Float64, 0))
partitionfunc = FEFunction(Vconst, Float64.(partition));
writevtk(Ω2, "partition", cellfields=["partition" => partition])

ol = 1
pu = PartitionOfUnity(partition, ol, V2)
nocc = NoCoarseCorrection()
RAS1 = PSM(stiffnessmat2, pu, nocc)

res = ddEigenLab.solve(RAS1, rhs, ones(V2.nfree), 400, 1E-6, debug=true)
uhdd = FEFunction(op2.trial, res[1]);


puvecs = [pu.Ri[i]' * pu.Di[i] * pu.Ri[i] * ones(size(stiffnessmat2)[1]) for i=1:length(pu.Di)]
pufuns = [FEFunction(op2.trial, puvecs[i]) for i=1:length(pu.Di)]
parvecs = [pu.Ri[i]' * pu.Ri[i] * ones(size(stiffnessmat2)[1]) for i=1:length(pu.Di)]
parfuns = [FEFunction(op2.trial, parvecs[i]) for i=1:length(pu.Di)]
writevtk(Ω2, "gmshpart", cellfields=["uh2" => uh2, "uhreal2" => uhreal2, "uhdd" => uhdd, (["pu_$i" for i=1:length(pu.Di)] .=> pufuns)..., (["par_$i" for i=1:length(pu.Di)] .=> parfuns)...])
