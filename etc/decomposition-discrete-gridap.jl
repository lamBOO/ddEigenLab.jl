using Gridap
using LinearAlgebra
using SparseArrays

Lx = 4
Ly = 1
ρ = 10

d = 2


Z = 10
V(x) = Z^2 * sin(x[1]*π)^2 * sin(x[2]*π)^2
# V(x) = 0


Lxp = 1
Lyp = 1
domain = (0, Lxp, 0, Lyp)
partition = (Lxp * ρ, Lyp * ρ)
model2 = CartesianDiscreteModel(domain, partition; isperiodic=(true, false))
labels2 = get_face_labeling(model2)
add_tag_from_tags!(labels2, "diri", collect(1:3^d-1))
order = 1
reffe2 = ReferenceFE(lagrangian, Float64, order)
V02 = TestFESpace(model2, reffe2; conformity=:H1, dirichlet_tags = ["diri"])
Ug2 = TrialFESpace(V02, 0)
degree = 2
Ω2 = Triangulation(model2)
dΩ2 = Measure(Ω2, degree)

b2(v) = ∫( 1 * v ) * dΩ2  # dummy
a11(u,v) = ∫( ∇(v) ⋅ ∇(u) + V * u * v ) * dΩ2
op11 = AffineFEOperator(a11, b2, Ug2, V02)
stiffnessmat2 = assemble_matrix(a11, Ug2, V02)
a22(u,v) = ∫( v * u ) * dΩ2
op22 = AffineFEOperator(a22, b2, Ug2, V02)
massmat2 = assemble_matrix_and_vector(a22, b2, Ug2, V02)[1]

@info "start eigensolve periodic cell"

x0 = ones(size(massmat2)[1])
print("IPM")
for i=1:20
  global x0
  print(".")
  x0 = stiffnessmat2 \ (massmat2 * x0)
  x0 = x0 / sqrt((x0' * massmat2 * x0))
end
print("\n")
eigvec2 = sign(sum(x0)) * x0
eigval2 = (x0' * stiffnessmat2 * x0) / sqrt((x0' * massmat2 * x0))
residual2 = (stiffnessmat2 - eigval2 * massmat2) * eigvec2
@show norm(residual2, 2)
@show norm(residual2, Inf)
uhp = FEFunction(op11.trial, eigvec2);

@info "end eigensolve periodic cell"



domain = nothing
partition = nothing
if d==2
  domain = (0, Lx, 0, Ly)
  partition = (Lx * ρ, Ly * ρ)
elseif d==3
  domain = (0, Lx, 0, Ly, 0, Ly)
  partition = (Lx * ρ, Ly * ρ, Ly * ρ)
else
  error("we need d=2,3")
end
model = CartesianDiscreteModel(domain, partition; isperiodic=(false, false))  # quads
# model = simplexify(CartesianDiscreteModel(domain, partition))  # tets

labels = get_face_labeling(model)
add_tag_from_tags!(labels, "diri", filter!(e->!(e∈[5,6]), collect(1:3^d-1)))

order = 1
reffe = ReferenceFE(lagrangian, Float64, order)
V0 = TestFESpace(model, reffe; conformity=:H1, dirichlet_tags = ["diri"])

Ug = TrialFESpace(V0, 0)

degree = 2
Ω = Triangulation(model)
dΩ = Measure(Ω,degree)

# uhp = Gridap.FESpaces.interpolate(uhp, V0)

b(v) = ∫( 1 * v ) * dΩ  # dummy

tmp(x)=(uhp(VectorValue(x[1]%1,x[2]%1)))^2

a1(u,v) = ∫( (x->tmp(x)) * ∇(v) ⋅ ∇(u) ) * dΩ
# a1(u,v) = ∫( ∇(v) ⋅ ∇(u) + uhp * u * v ) * dΩ
op1 = AffineFEOperator(a1, b, Ug, V0)
stiffnessmat = assemble_matrix(a1, Ug, V0)

a2(u,v) = ∫( (x->tmp(x)) * v * u ) * dΩ
op2 = AffineFEOperator(a2, b, Ug, V0)
massmat = assemble_matrix_and_vector(a2, b, Ug, V0)[1]

@info "start eigensolve"

# σ = eigval2
σ = 0
x0 = ones(size(massmat)[1])
print("IPM")
for i=1:10
  global x0
  print(".")
  x0 = (stiffnessmat - σ * massmat) \ (massmat * x0)
  x0 = x0 / sqrt((x0' * massmat * x0))
end
print("\n")
eigvec = sign(sum(x0)) * x0
eigval = (x0' * stiffnessmat * x0) / sqrt((x0' * massmat * x0))
residual = (stiffnessmat - eigval * massmat) * eigvec
@show norm(residual, 2)
@show norm(residual, Inf)

@info "end eigensolve"

writevtk(model, "model")
uh = FEFunction(op1.trial, eigvec); writevtk(Ω, "results", cellfields = ["uh" => uh])


