<img src="media/logo.png" width="300px" />

# ddEigenLab.jl: A Domain-Decomposition Eigenvalue Problem Lab

[![zenodo link](https://zenodo.org/badge/DOI/10.5281/zenodo.6576197.svg)](https://doi.org/10.5281/zenodo.6576197)
[![pipeline status](https://git.rwth-aachen.de/lamBOO/ddEigenLab.jl/badges/master/pipeline.svg)](https://git.rwth-aachen.de/lamBOO/ddEigenLab.jl/-/commits/master)
[![coverage report](https://git.rwth-aachen.de/lamBOO/ddEigenLab.jl/badges/master/coverage.svg)](https://git.rwth-aachen.de/lamBOO/ddEigenLab.jl/-/commits/master)
[![documentation](https://img.shields.io/badge/docs-master-blue.svg)](https://lamboo.pages.rwth-aachen.de/ddEigenLab.jl/)

This package provides a lab for algorithms to solve PDE-based eigenvalue problems based on Schrödinger operators. Some key features include:
- Finite Difference Discretizations
- Finite Element Discretizations using [Gridap.jl](https://github.com/gridap/Gridap.jl)
- Optimally preconditioned eigenvalue solvers, including:
   - inverse power method (IP),
   - Steepest-descent method for the Rayleigh quotient (SD)
   - Locally-optimal conjugate gradient method (LOPCG)
- Linear Periodic Schrödinger Equations or multi-dimensional Poisson problems
- Domain-decomposition (DD) solution strategies and preconditioners
- Efficient spectral and factorization preconditioners to use within the DD framework

The repository contains supplemental material (numerical examples) used in the following publications:

- Paper1: Benjamin Stamm and Lambert Theisen. *A Quasi-Optimal Factorization Preconditioner for Periodic Schrödinger Eigenstates in Anisotropically Expanding Domains*. SIAM Journal on Numerical AnalysisVol. 60, Iss. 5 (2022). [10.1137/21M1456005](https://doi.org/10.1137/21M1456005).
- Paper2: Lambert Theisen and Benjamin Stamm. *A Scalable Two-Level Domain Decomposition Eigensolver for Periodic Schrödinger Eigenstates in Anisotropically Expanding Domains*. arXiv preprint. [10.48550/arXiv.2311.08757](
https://doi.org/10.48550/arXiv.2311.08757).


## Installation and execution

1. Clone the repository and open the folder
```bash
git clone git@git.rwth-aachen.de:lamBOO/ddEigenLab.jl.git
cd ddEigenLab.jl
```

2. Build and start the container (after installing [Docker](https://www.docker.com/)), then open Julia.
```bash
# BEFORE: Install Docker and Docker-Compose and start daemon
docker compose run ddeigenlab bash  # optional but recommended for dependencies
cd shared/ddEigenLab
```

3. Install all Julia dependencies
```bash
julia --project -e 'import Pkg; Pkg.instantiate()'
```

4. Run examples by navigating to the folder and execute the examples from the shell:
```bash
cd examples/fem/paper2/dd-rectangle-2d/
julia --project solve.jl

