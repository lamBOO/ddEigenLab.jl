using Test

using ddEigenLab, Documenter, LinearAlgebra

@testset "Test Examples" begin
  exampleFolder = joinpath(@__DIR__, "..", "examples")
  for (root, dirs, files) in walkdir(exampleFolder)
    for file in files
      fullpath = joinpath(root, file)
      if isfile(fullpath) && endswith(file, ".jl")
        blacklist = [
          "examples/fd",
          "examples/misc",
          "perturbed-periodic-potential/perturbed-periodic-potential.jl",
          "1d-poisson",
        ]
        whitelist = [
          "",  # comment out to activate whitelist
          # "examples/fem/etc/geneo-test",
          # "examples/fem/etc/friess-gilbert-scheichl-rqi-test",
        ]
        if any([contains(fullpath, p) for p in blacklist]) ||
          all([!contains(fullpath, p) for p in whitelist])
          @info "Skip example $(fullpath)"
          continue
        end
        @info "Test example $(fullpath)"
        dir, _ = splitdir(fullpath)
        @testset "$(fullpath)" begin
          cd(dir) do
          @eval Module() begin
            # to avoid putting all examples in a separte main()
            # to avoid conflicting redefinitions of variables/functions
            # see: https://github.com/JuliaLang/julia/issues/40189
            Base.include(@__MODULE__, $fullpath)
          end
        end
        end
      end
    end
  end
end

@testset "ddEigenLab" begin
    doctest(ddEigenLab; manual=false)
end
