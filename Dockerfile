# Use Julia base image
FROM julia:1.10.2-bookworm

# Descriptions
LABEL maintainer="Lambert Theisen <lambert.theisen@rwth-aachen.de>"
LABEL description="ddEigenlab.jl"

# Specify software versions
ENV GMSH_VERSION 4.8.4

# Download Install Gmsh Requirements (apt gmsh is old, just use it for deps)
RUN export DEBIAN_FRONTEND=noninteractive && \
    apt-get -qq update && \
    apt-get -y install \
        wget \
        libglu1 \
        libxcursor-dev \
        libxinerama1 \
        git \
        git-lfs \
        gmsh && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN cd /usr/local && \
    wget -nc http://gmsh.info/bin/Linux/gmsh-${GMSH_VERSION}-Linux64-sdk.tgz && \
    tar -xf gmsh-${GMSH_VERSION}-Linux64-sdk.tgz
ENV PATH=/usr/local/gmsh-${GMSH_VERSION}-Linux64-sdk/bin:$PATH

# apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 648ACFD622F3D138 && \
# apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 0E98404D386FA1D9 && \
# apt-key adv --keyserver keyserver.ubuntu.com --recv-keys F8D2585B8783D481 && \
# apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 0E98404D386FA1D9 && \
# apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 6ED0E7B82643E131 && \
# apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 54404762BBB6E853 && \
# apt-key adv --keyserver keyserver.ubuntu.com --recv-keys BDE6D2B9216EC7A8 && \
