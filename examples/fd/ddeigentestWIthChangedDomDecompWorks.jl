# # 2D Laplacian plus Potential
# Solves the Laplacian eigenvalue problem with zero Dirichlet boudnary conditons
# on a rectangular domain (x,y) ∈ Ω = [0,Lx]×[0,Ly]

using LinearAlgebra
using SparseArrays
using Statistics
using Plots
using Distributed
gr()

using ddEigenLab


# ## Setup Long Chain Domain
const Lx = 10
const Ly = 1
const ρ = 10
const Ω = Rectangle2D(Lx, Ly, ρ, ρ)


Z = 10
# Vxy(x,y) = Z*sin(Ω.Ly*pi*y/Ω.Ly)^2 + Z*sin(Ω.Lx*pi*x/Ω.Lx)^2 # works for A
# Vxy(x,y) = Z^2 * (sin(x*π)^2 * sin(y*π)^2 + 1*(((x% 1)-0.25)^2 ))
Vxy(x,y) = 0
V(x,y) = Vxy(x,y)
V0 = 0
Vx(x) = 0
Vy(y) = 0
L = NegativeLaplacePlusPotential(V0, Vx, Vy, Vxy)


# ## Discretize Operators Using Finite Differences
# Lh = FD2D(L, Ω)
bcs = Dict(:x => :d, :y => :d)
Lh = FD2D(L, Ω, bcs)






# ## Setup Preconditioners
@info "Preconditioners start"
Lxp = 1
Lyp = 1
Ωp = Rectangle2D(Lxp, Lyp, ρ, ρ)
bcsp = Dict(:x => :p, :y => :d)
cellsolution = eigen(Symmetric(Array(FD2D(L, Ωp, bcsp).A)), 1:1)
λ∞ = cellsolution.values[1]
psi = cellsolution.vectors[:, 1]
iP_λ∞ = Lh.A-λ∞*I(Ω.N)
psifull = vcat(
  [
    vcat(
      [[psi[i*(length(Ωp.xVals)-1)+1 : (i+1)*(length(Ωp.xVals)-1)] for i=0:length(Ωp.yVals)-2-1][k] for j=1:Lx]...
    )[2:end] for k=1:(length(Ωp.yVals)-2)
  ]...
)
psifullfloating = vcat(
  [
    vcat(
      [0 for i=1:(length(Ωp.xVals)-2)]...,
      vcat([[psi[i*(length(Ωp.xVals)-1)+1 : (i+1)*(length(Ωp.xVals)-1)] for i=0:length(Ωp.yVals)-2-1][k] for j=2:Lx]...)[1:end-(length(Ωp.xVals)-2)],
      [0 for i=1:(length(Ωp.xVals)-2)]...,
    )[1:end] for k=1:(length(Ωp.yVals)-2)
  ]...,
)
writeVtk(psi, Ωp, bcsp, "psi")
writeVtk(psifull, Ω, bcs, "psifull")
@info "Preconditioners end"

sinyinf(x,y) = sin(pi * y / Ly)
sinyinfvec = vec(sinyinf.(Ω.xVals[2:end-1], Ω.yVals[2:end-1]'))

# sininf(x,y) = sin(pi * x / Lx) * sin(pi * y / Ly)
sininf(x,y) = sin(pi * x / Lx)
sininfvec = vec(sininf.(Ω.xVals[2:end-1], Ω.yVals[2:end-1]'))
# sininfvec = sininfvec / norm(sininfvec)

uinfvec = sininfvec .* psifull
uinfvec = uinfvec / norm(uinfvec)

sinyinffloating = sinyinfvec .* (!isequal).(psifullfloating, 0)
sinyinffloating = sinyinffloating / norm(sinyinffloating)






function ipm(solver::PSM, x0::Vector{Float64}, A::SparseMatrixCSC{Float64, Int64})
  x_i = copy(x0)
  for i=1:100

    # x_i = solver.A \ x_i
    x_i = solve(solver, x_i, x_i, 20000, 1E-10, debug=false)
    x_i = x_i / norm(x_i)

    λ = ddEigenLab.raycoeff(x_i, A)
    resnorm = norm(A * x_i - λ * x_i)
    @show i, λ, resnorm
    if resnorm < 1E-8
      break
    end
  end
  return x_i
end

@time solver = PSM(Ω, iP_λ∞, :nico, uinfvec[:,:])
res = solve(solver, uinfvec, uinfvec, 100, 1E-10, debug=true)

ddEigenLab.raycoeff(res, Lh.A)


# change b-Ax to lx-Ax eigen residual
