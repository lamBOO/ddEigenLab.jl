using LinearAlgebra

using ddEigenLab

for Lx=1:10
# Lx = 10
Ly = 1
ρ = 10
Ω = Rectangle2D(Lx, Ly, ρ, ρ)
L = NegativeLaplacePlusPotential(0, x->0, y->0, (x,y)->0)
bcs = Dict(:x => :d, :y => :d)
Lh = FD2D(L, Ω, bcs)

eigval, eigvec = eigen(Symmetric(Array(Lh.A)), 1:1)

aeigval = (1^2/Lx^2+1^2/Ly^2)*pi^2

abserror = abs(eigval[1]-aeigval)
relerror = abs(eigval[1]-aeigval)/aeigval
@info abserror
# @info relerror

end
