using LinearAlgebra
using SparseArrays
using Plots

L = 10
# L = 0.5 # [Cances2021]
Nx = 100
x = range(-L,L,Nx)
# x = range(-2,2,Nx)
# V(x) = 0.25 * x^2 + sin(2 * pi * x)^2
# V(x) = -1*(1/(x+0.01) - 100)
# V(x) = - 20 * ( exp(-30*cos(pi*((x+0.5)-0.2))^2 ) + 2 * exp(-30*cos(pi*((x+0.5)+0.25))^2) )  # [p263, Cances2021]
# V(x) = 1*sin(2 * pi * x)^2
# V(x) = sum([-30*exp(-2*(x-i)^2) for i=-L:L]) + 50
# V(x) = 0
V(x) = x^2
# β = 5
β = 100
# β = 50 # [Cances2021]

# [Cances2021] doeesn't have Dirichlet zero BCs!

function A(u, x, β, V)
  N = length(x)-2
  minuslaplace = (1/abs(x[1] - x[2])^2) * sparse(Tridiagonal(fill(-1, N-1), fill(2, N), fill(-1, N-1)))
  potential = spdiagm(0 => vec(V.(x[2:end-1])))
  betausquared = β * spdiagm(0 => vec(u.^2))
  return minuslaplace + potential + betausquared
end

# inverse iteration genealized
function iigen(u0, kmax, tol, x, β, V, σ, μ)
  uk = copy(u0)
  λk = 0
  for i=1:kmax

    # damped generalized inverse power
    # α = 1
    # uk = (1-α) * uk + α * ( A(uk, x, β, V) - σ * I(length(uk)) ) \ ( (μ * A(uk, x, β, V) + I(length(uk))) * uk)

    # generalize Inverse power
    uk = A(uk, x, β, V) \ uk

    uk = uk / sqrt(sum(uk.^2) * abs(x[1] - x[2]))
    # uk = uk / norm(uk)


    λk = uk' * A(uk, x, β, V) * uk / (uk' * uk)
    r = A(uk, x, β, V) * uk - λk * uk
    @show i, λk, norm(r)
    if norm(r) < tol
      break
    end
  end
  return uk
end

function scf(u0, kmax, tol, x, β, V, σ, μ)
  uk = copy(u0)
  λk = 0
  for i=1:kmax

    uk = eigen(Array(A(uk, x, β, V))).vectors[:,1]
    uk = uk / sqrt(sum(uk.^2) * abs(x[1] - x[2]))
    λk = uk' * A(uk, x, β, V) * uk / (uk' * uk)
    r = A(uk, x, β, V) * uk - λk * uk
    @show i, λk, norm(r)
    if norm(r) < tol
      break
    end
  end
  return uk
end

# res = iigen(ones(length(x)-2), 10000, 1E-8, x, β, V, 0, 0)
res = scf(ones(length(x)-2), 10000, 1E-8, x, β, V, 0, 0)
Plots.plot(x, V.(x), labels="V")
Plots.plot(x, vcat(0, res.^2, 0), labels="ρ")  # ρ [p265, Cances2021]
Plots.plot(x, V.(x) + 50*vcat(0, res.^2, 0), labels="Veff")  # V_eff [p265, Cances2021]
Plots.plot(x, vcat(0, res, 0), labels="ψ")
