using LinearAlgebra
using AlgebraicMultigrid
using CairoMakie
CairoMakie.activate!(type = "svg")
using SparseArrays

using Random
Random.seed!(42)

N = 5

# h is the same!
Aper = poisson(N-1) - sparse([1; N-1],[N-1; 1],[1; 1])
Aneu = poisson(N) - sparse([1; N],[1; N],[1; 1])

eigvecneu = eigen(Array(Aneu)).vectors
eigvecperext = vcat(eigen(Array(Aper)).vectors,eigen(Array(Aper)).vectors[1:1,:])

x = rand(N)

rdaggereucl = sum([(x' * eigvecperext[:,i]) * eigvecperext[:,i] for i in 1:N-1])
rdaggereuclperp = x - rdaggereucl

rdaggerA = sum([(x' * Aneu * eigvecperext[:,i]) / (eigvecperext[:,i]' * Aneu * eigvecperext[:,i]) * eigvecperext[:,i] for i in 2:N-1])
rdaggerAperp = x - rdaggerA


f = Figure()
Axis(f[1, 1])

xs = 0:N-1
scatterlines!(xs, x, color = :red, label="x")
scatterlines!(xs, rdaggereucl, color = :blue, label="r_#e x")
scatterlines!(xs, rdaggereuclperp, color = :blue, linestyle=:dash, label="r_#e⟂ x")
scatterlines!(xs, rdaggerA, color = :green,  label="r_#A x")
scatterlines!(xs, rdaggerAperp, color = :green, linestyle=:dash, label="r_#A⟂ x")
# scatterlines!(xs, rdaggerA .+ 0.54, color = :yellow, label="tesdt")

for i = 1:N
  # scatterlines!(xs, eigvecneu[:,i], color = :red)
end
f
axislegend(position = :lb); f
