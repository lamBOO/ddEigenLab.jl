# # 2D Laplacian plus Potential
# Solves the Laplacian eigenvalue problem with zero Dirichlet boudnary conditons
# on a rectangular domain (x,y) ∈ Ω = [0,Lx]×[0,Ly]

using LinearAlgebra
using Statistics
using Plots
gr()

using ddEigenLab


# ## Setup Long Chain Domain
Lx = 5
Ly = 1
ρ = 10
Ω = Rectangle2D(Lx, Ly, ρ, ρ)


# ## Define Potential
# $V(x,y) = V_0 + V_x(x) + V_y(y)$
# V0 = 0.1
## V0 = 0
## Vx(x) = 0
## Vy(y) = 0
## Vx(x) = ((x-Ω.Lx/2)^2) / ((Ω.Lx/2)^2+(Ω.Ly/2)^2)
## Vy(y) = ((y-Ω.Ly/2)^2) / ((Ω.Lx/2)^2+(Ω.Ly/2)^2)
# Vx(x) = ((x-Ω.Lx/2)^2) / ((Ω.Lx/2)^2)
# Vy(y) = ((y-Ω.Ly/2)^2) / ((Ω.Ly/2)^2)
## sigma_x = Ω.Lx^(0.5)
## sigma_y = Ω.Ly^(0.5)
## sigma_x = 10
## sigma_y = 10
## Vx(x) = 1.0 - 1.0 * exp(-( (x-2*Ω.Lx/4)^2/(2*sigma_x^2)))
## Vy(y) = 1.0 - 1.0 * exp(-( (y-Ω.Ly/2)^2/(2*sigma_y^2)))
## Vx(x) = x^2
## Vy(y) = y^2
## Vx(x) = x/Ω.Lx
## Vy(y) = y/Ω.Ly
# Vx(x) = abs(x-Ω.Lx/2)/(Ω.Lx/2)
# Vy(y) = abs(y-Ω.Ly/2)/(Ω.Ly/2)
## Vx(x) = 0.1*(x-Ω.Lx/2)^2
## Vy(y) = (y-Ω.Ly/2)^2
## Vx(x) = abs(x-1Ω.Lx/4) + abs(x-3Ω.Lx/4)
## Vy(y) = (y-Ω.Ly/2)^2
## Vx(x) = (x-1Ω.Lx/4)^2*(x-3Ω.Lx/4)^2 / ((0-1Ω.Lx/4)^2*(0-3Ω.Lx/4)^2)
## Vy(y) = (y-Ω.Ly/2)^2
## Vx(x) = (x-1Ω.Lx/4)^2*(x-3Ω.Lx/4)^2
## Vy(y) = (y-Ω.Ly/2)^2 / ((Ω.Ly/2)^2)
## a = 0.9
## Vx(x) = (abs(x-1Ω.Lx/4))^a / (Ω.Lx/2)^a + (abs(x-3Ω.Lx/4))^a / (Ω.Lx/2)^a
## Vy(y) = ((y-Ω.Ly/2)^2) / ((Ω.Ly/2)^2)
# V(x,y) = V0 + Vx(x) + Vy(y)
# Vxy(x,y) = 0

sigmax = 0.1
sigmay = 0.1
Z = 10
# Vxy(x,y) = (x-Ω.Lx/2)^2 + (y-Ω.Ly/2)^2
# Vxy(x,y) = (x-Ω.Lx/2)^2 + (y-Ω.Ly/2)^2
# Vxy(x,y) = -Z*sum(exp(-((x-(ci-1)-0.5)^2/sigmax+(y-Ω.Ly/2)^2/sigmay)) for ci=1:Ω.Lx)
# Vxy(x,y) = Z*sum(exp(-((x-(ci-1)-0.5)^2/sigmax+(y-Ω.Ly/2)^2/sigmay)) for ci=1:Ω.Lx)
# Vxy(x,y) = (x-Ω.Lx/2)^2 / (Ω.Lx/2)^2 * (y-Ω.Ly/2)^2 / (Ω.Ly/2)^2 # works with d_yy only
# Vxy(x,y) = sum(exp(-((x-ci+0.5)^2 + (y-Ω.Ly/2)^2)) for ci=1:Ω.Lx)
# Vxy(x,y) = exp(-((x-Ω.Lx/2)^2 + (y-Ω.Ly/2)^2))  # 1: works
# Vxy(x,y) = sin(pi * x/Ω.Lx) * sin(pi * y/Ω.Ly)  # 2: works
# Vxy(x,y) = 1 - sin(pi * x/Ω.Lx) * sin(pi * y/Ω.Ly)  # 3: works
# Vxy(x,y) = 1 * (abs(x-Ω.Lx/2)<=1)  # 2: works
# Vxy(x,y) = exp(-(x-Ω.Lx/2)^2/((1/R)^2 - (x-Ω.Lx/2)^2)) * (abs(x-Ω.Lx/2) <= 1/R)  # 2: works
# Vxy(x,y) = 2 * exp(-(x-Ω.Lx/2)^2/((1/R)^2 - (x-Ω.Lx/2)^2)) * (abs(x-Ω.Lx/2) <= 1/R) + exp(-(x-Ω.Lx/2 - 1)^2/((1/R)^2 - (x-Ω.Lx/2 - 1)^2)) * (abs(x-Ω.Lx/2 - 1) <= 1/R) + exp(-(x-Ω.Lx/2 + 1)^2/((1/R)^2 - (x-Ω.Lx/2 + 1)^2)) * (abs(x-Ω.Lx/2 + 1) <= 1/R)  # 2: works TODO Strange because lambda infty is not the limit..
# Vxy(x,y) = 2 - (2 * exp(-(x-Ω.Lx/2)^2/((1/R)^2 - (x-Ω.Lx/2)^2)) * (abs(x-Ω.Lx/2) <= 1/R) + exp(-(x-Ω.Lx/2 - 1)^2/((1/R)^2 - (x-Ω.Lx/2 - 1)^2)) * (abs(x-Ω.Lx/2 - 1) <= 1/R) + exp(-(x-Ω.Lx/2 + 1)^2/((1/R)^2 - (x-Ω.Lx/2 + 1)^2)) * (abs(x-Ω.Lx/2 + 1) <= 1/R))  # 3: works
R = (1/2)^-1
ciarray = [i-0.5 for i=1:Ω.Lx]
# Vxy(x,y) = 1 - ( sum(exp(-(x-ci)^2/((1/R)^2 - (x-ci)^2)) * (abs(x-ci) <= 1/R) for ci in ciarray) )
# Vxy(x,y) = Z - ( Z*sum(exp(-((x-ci)^2 + (y-Ω.Ly/2)^2)/((1/R)^2 - ((x-ci)^2 + (y-Ω.Ly/2)^2))) * (sqrt((x-ci)^2 + (y-Ω.Ly/2)^2) < 1/R) for ci in ciarray) ) # smooth bump
# Vxy(x,y) = Z - ( (Z-Z/2*(abs(x-Ω.Lx/2)<=1.0))*sum(exp(-((x-ci)^2 + (y-Ω.Ly/2)^2)/((1/R)^2 - ((x-ci)^2 + (y-Ω.Ly/2)^2))) * (sqrt((x-ci)^2 + (y-Ω.Ly/2)^2) < 1/R) for ci in ciarray) ) # perturbed
# Vxy(x,y) = 3Z/2 - ( (Z+Z/2*(abs(x-Ω.Lx/2)<=1.0))*sum(exp(-((x-ci)^2 + (y-Ω.Ly/2)^2)/((1/R)^2 - ((x-ci)^2 + (y-Ω.Ly/2)^2))) * (sqrt((x-ci)^2 + (y-Ω.Ly/2)^2) < 1/R) for ci in ciarray) )  # TODO: Doesnt work... Works with 0,2 period periodic middle 🤔 or 0,1 with period both because its min x = x_0 allaire2002!
# Vxy(x,y) = Z - ( (Z-Z/4*(abs(x%2-0.5)<=0.5 ? 1 : 0))*sum(exp(-((x-ci)^2 + (y-Ω.Ly/2)^2)/((1/R)^2 - ((x-ci)^2 + (y-Ω.Ly/2)^2))) * (sqrt((x-ci)^2 + (y-Ω.Ly/2)^2) < 1/R) for ci in ciarray) )
# Vxy(x,y) = Z * abs(x-floor(x)-0.5)
# Vxy(x,y) = Z * abs(x-floor(x)-0.5) * sin(pi*x/Ω.Lx)
# Vxy(x,y) = Z * (0.5-abs(x-floor(x)-0.5))
# Vxy(x,y) = 0
# Vxy(x,y) = sqrt(2/Ω.Lx)*sin(Ω.Lx*pi*x/Ω.Lx)^2
# Vxy(x,y) = (y-Ω.Ly/2)^2 + Z*sin(Ω.Lx*pi*x/Ω.Lx)^2
Vxy(x,y) = Z*sin(Ω.Ly*pi*y/Ω.Ly)^2 + Z*sin(Ω.Lx*pi*x/Ω.Lx)^2
# Vxy(x,y) = 100*Z*y + Z*sin(Ω.Lx*pi*x/Ω.Lx)^2  # non y-periodic
# Vxy(x,y) = 1
# Vxy(x,y) = ((Ω.Ly/2)^2-(y-Ω.Ly/2)^2) * Z*sin(Ω.Lx*pi*x/Ω.Lx)^2
# Vxy(x,y) = y * Z*sin(Ω.Lx*pi*x/Ω.Lx)^2 # works
# Vxy(x,y) = (y-Ω.Ly/2)^2 + Z*(x-floor(x)) # sawtooth
# Vxy(x,y) = Z*(y-Ω.Ly/2)^2 + 10 * cos(pi*x/Ω.Lx)^2 * sin(Ω.Lx*pi*x/Ω.Lx)^2
# Vxy(x,y) = (y-Ω.Ly/2)^2 + exp(-(x-Ω.Lx/2)^2)
V(x,y) = Vxy(x,y)

V0 = 0
Vx(x) = 0
Vy(y) = 0
# Vx(x) = -Z*sum(exp(-((x-(ci-1)-0.5)^2/sigmax+(0.5-Ω.Ly/2)^2/sigmay)) for ci=1:Ω.Lx) + V0
# Vx(x) = -Z*sqrt(pi*sigmay)*erf(1/(2*sqrt(sigmay)))*exp(-pi^2*sigmay/4)*sum(exp(-((x-(ci-1)-0.5)^2/sigmax+(0.5-Ω.Ly/2)^2/sigmay)) for ci=1:Ω.Lx) + 2/pi * V0
# Vy(y) = -Z*exp(-((y-Ω.Ly/2)^2/sigmay)) + V0
# Vy(y) = sum(exp(-((y-Ω.Ly/2)^2)) for ci=1:Ω.Lx)
# Vy(y) = exp(-((0-Ω.Lx/2)^2 + (y-Ω.Ly/2)^2))  # 1: works
# Vy(y) = Vxy(0,y)  # 2: works
# Vy(y) = Vxy(Ω.Lx/2,y)  # 3: works
# Vy(y) = Vxy(Ω.Lx/2+0.5,y)  # 4: works
# Vy(y) = Vxy(Ω.Lx/2+0.5,y)  # 2: works
# Vy(y) = Z/4  # for chainsaw: Vy=average in x direction!!! geht nicht mit z=50 und above :-(

# ## Define Operator
# $x \mapsto Lx = (-\Delta + V(x,y))x$
L = NegativeLaplacePlusPotential(V0, Vx, Vy, Vxy)


# ## Discretize Operators Using Finite Differences
# Lh = FD2D(L, Ω)
bcs = Dict(:x => :d, :y => :d)
Lh = FD2D(L, Ω, bcs)


# ## Calculate Ground Truth for Comparison
@info "Groundtruth start"
eigval, eigvec = eigen(Symmetric(Array(Lh.A)), 1:2)
# eigval, eigvec = zeros(size(Lh.A)[1]),zeros(size(Lh.A))
@info "Groundtruth end"


# ## Setup Preconditioners
@info "Preconditioners start"
# λ∞ = 0
# λ∞ = eigen(Array(Lh.Ax+Lh.Ay+Lh.AVx)).values[1]
# λ∞ = eigen(Array(Lh.Ax+Lh.AVx)).values[1]
# λ∞ = eigen(Array(Lh.Ax+Lh.Ay+Lh.AVx)).values[1]
# λ∞ = eigen(Array(Lh.Ay)).values[1]
# λ∞ = eigen(Array(Lh.Ay+Lh.AVy)).values[1]
# λ∞ = eigen(Array(FD2DXPeriodic(NegativeLaplacePlusPotential(V0, Vx, Vy, (x,y)->Vxy(x+Ω.Lx/2-1,y)), Rectangle2D(2, 1, ρ, ρ, Dict(
#   :x => :p, :y => :d
# ))).A)).values[1]
Lxp = 1
Lyp = 1
cellsolution = eigen(Symmetric(Array(FD2D(L, Rectangle2D(Lxp, Lyp, ρ, ρ), Dict(:x => :p, :y => :d)).A)), 1:1)
λ∞ = cellsolution.values[1]
psi = cellsolution.vectors[:, 1]
# λ∞ = 14.8637  # Mathematica
# λ∞ = pi^2
# λ∞ = eigen(Array(Lh.Ayl+Lh.AVyl+Lh.AV0yl)).values[1]
## λ∞x = eigen(Array(Lh.Axl+Lh.AVxl)).values[1]
## P_λ∞Inv = inv(Array((Lh.A-λ∞*I(Ω.N))*(Lh.A-λ∞x*I(Ω.N))))
iP_λ∞ = Array(Lh.A-λ∞*I(Ω.N))
## P_λ∞ = inv(Array(Lh.A-λ∞*I(Ω.N)))
## P_I = I(Ω.N)
## P_invA = inv(Array(Lh.A))
## Px = inv(Array(Lh.Ax+Lh.AVx))
## Py = inv(Array(Lh.Ay+Lh.AVy))
## P0 = inv(Array(Lh.AV0))
@info "Preconditioners end"

@info "Cellproblem start"
cellproblem = eigen(Array(FD2D(L, Rectangle2D(Lxp, Lyp, ρ, ρ), Dict(:x => :p, :y => :p)).A))
λcell = cellproblem.values[1]
psi2 = cellproblem.vectors[:, 1]
psiSpatial     = hcat(reshape(psi2, ρ, ρ)'[:,2:end], [reshape(psi2, ρ, ρ)' for i=1:Ω.Lx-1]...)
psiSpatialFull = hcat([reshape(psi2, ρ, ρ)' for i=1:Ω.Lx]..., reshape(psi2, ρ, ρ)'[:,1])
psiSpatialFull = vcat(psiSpatialFull, psiSpatialFull[1:1,:])
psiSpatialFullSquared = psiSpatialFull.^2

psiSpatialInner = psiSpatialFull[2:end-1,2:end-1]
psiSpatialInnerSquared = psiSpatialInner.^2
psiSpatialInnerSquaredDiag = diagm(vcat([psiSpatialInnerSquared[i,:] for i=size(psiSpatialInnerSquared)[1]:-1:1]...))

D = psiSpatialFullSquared

DmeanY = kron(ones(size(D)[1]), [Statistics.mean(D[:,j]) for j=1:size(D)[2]]')
DmeanYInner = DmeanY[2:end-1,2:end-1]
DmeanYInnerDiag = diagm(vcat([DmeanYInner[i,:] for i=size(DmeanYInner)[1]:-1:1]...))

DintY = kron(ones(size(D)[1]), [sum(D[:,j]) for j=1:size(D)[2]]')
DintYInner = DintY[2:end-1,2:end-1]
DintYInnerDiag = diagm(vcat([DintYInner[i,:] for i=size(DintYInner)[1]:-1:1]...))

DintX = kron(ones(size(D)[2])', [sum(D[i,:]) for i=1:size(D)[1]])
DintXInner = DintX[2:end-1,2:end-1]
DintXInnerDiag = diagm(vcat([DintXInner[i,:] for i=size(DintXInner)[1]:-1:1]...))

correctorYMean = eigen(Array(FD2DAnisotrop(L, DmeanY, Rectangle2D(Lx, Ly, ρ, ρ), Dict(:x => :d, :y => :d)).Ax), DmeanYInnerDiag)
correctorYInt = eigen(Array(FD2DAnisotrop(L, DintY, Rectangle2D(Lx, Ly, ρ, ρ), Dict(:x => :d, :y => :d)).Ax), DintYInnerDiag)
correctorXInt = eigen(Array(FD2DAnisotrop(L, DintX, Rectangle2D(Lx, Ly, ρ, ρ), Dict(:x => :d, :y => :d)).Ay), DintXInnerDiag)

corrector = eigen(Array(FD2DAnisotrop(L, D, Rectangle2D(Lx, Ly, ρ, ρ), Dict(:x => :d, :y => :d)).A), psiSpatialInnerSquaredDiag)
@info "Cellproblem end"

# ## Solve Eigenproblem with Preconditoned Iterative Solver
imax = 1000
tol = 1E-08
## x_i, lambda_i, errors, info = RQI(Lh.A, tol, imax, eigval[1], eigvec[:,1])
## x_i, lambda_i, errors, info = GF_BE(Lh.A, P_I, 1, 1, tol, imax, eigval[1], eigvec[:,1])
## x_i, lambda_i, errors, info = richardson(Lh.A, P_invA, 1, 1, tol, imax, eigval[1], eigvec[:,1])
## x_i, lambda_i, errors, info = IP(Lh.A, λ∞, 1, 1, tol, imax, eigval[1], eigvec[:,1])
## x_i, lambda_i, errors, info = SD(Lh.A, P_λ∞, 1, 1, tol, imax, eigval[1], eigvec[:,1])
## x_i, lambda_i, errors, info = SD_split([Lh.Ax+Lh.AVx, Lh.Ay+Lh.AVy, Lh.AV0], [Px, Py, P0], 1, 1, tol, imax, eigval[1], eigvec[:,1])
## x_i, lambda_i, errors, info = SD_split2([Lh.Ax+Lh.AVx, Lh.Ay+Lh.AVy, Lh.AV0], [Px, Py, P0], 1, 1, tol, imax, eigval[1], eigvec[:,1])
x_i, lambda_i, errors, info = LOPCG(Lh.A, iP_λ∞, 1, 1, tol, imax, eigval[1], eigvec[:,1])

## x_i, lambda_i, errors, info = IP((Lh.A-λ∞*I(Ω.N))*Lh.A, 0, 1, 1, tol, imax, eigval[1], eigvec[:,1])
## x_i, lambda_i, errors, info = IP((Lh.Ax+Lh.AVx)*(Lh.Ay+Lh.AVy)*Lh.A, 0, 1, 1, tol, imax, eigval[1], eigvec[:,1])
## x_i, lambda_i, errors, info = SD(inv(P_λ∞), P_λ∞, 1, 1, tol, imax, eigval[1], eigvec[:,1])

## x_i, lambda_i, errors, info = IP(Lh.A, 0, 1, 1, tol, imax, eigval[1], eigvec[:,1])
## x_i, lambda_i, errors, info = SD(Lh.A, inv(Array(Lh.AVx+Lh.AV0)), 1, 1, tol, imax, eigval[1], eigvec[:,1])

## Scalable DD and Eigen!!! TODO
# x_i, lambda_i, errors, info = IP(inv(inv(Array(Ω.Lx^2*(Lh.A-1*I(size(Lh.A)[1]))))+1*I(size(Lh.A)[1])), 0, 1, 1, tol, imax, eigval[1], eigvec[:,1])

# ## Plot Errors
plotErrors(errors, "Errors for (Ω.Lx,Ω.Ly)=($(Ω.Lx),$(Ω.Ly))")


# ## Postprocess Results
displaySpectrumStats(eigval, eigvec)
## plotSolution(x_i, Ω.xVals, Ω.yVals, Ω.nx, Ω.ny, "approx")
## plotSolution(eigvec, Ω.xVals, Ω.yVals, Ω.nx, Ω.ny, "real")
## plotPotential(V, Ω.xVals, Ω.yVals)

## @show eigen(Array((Lh.Axl+Lh.AVxl))).values[1]/eigen(Array((Lh.Axl+Lh.AVxl))).values[2]

## @show eigen(Array((Lh.Ayl+Lh.AVyl+Lh.AV0yl))).values[1]/eigen(Array((Lh.Ayl+Lh.AVyl+Lh.AV0yl))).values[2]
## @show eigen(Array((Lh.Ay+Lh.AVy+Lh.AV0))).values[1]/eigen(Array((Lh.Ay+Lh.AVy+Lh.AV0))).values[2]

## @show (eigen(Array((Lh.A))).values[1] - λ∞)/(eigen(Array((Lh.A))).values[2] - λ∞)

## @show eigen(Array((Lh.Axl+Lh.AVxl))).values[1]/(eigen(Array((Lh.Axl+Lh.AVxl))).values[2] - eigen(Array((Lh.Axl+Lh.AVxl))).values[1])
## @show eigen(Array((Lh.Axl+Lh.AVxl))).values[1]/ eigen(Array((Lh.AVxl))).values[2]
## @show eigen(Array((Lh.Ayl+Lh.AVyl))).values[1]/eigen(Array((Lh.Ayl+Lh.AVyl))).values[2]
## @show eigen(Array((Lh.Ax+Lh.AVx)*(Lh.Ay+Lh.AVy)*Lh.A)).values[1]/eigen(Array((Lh.Ax+Lh.AVx)*(Lh.Ay+Lh.AVy)*Lh.A)).values[2]
## @show eigen(Array(Lh.A)).values[1]/eigen(Array(Lh.A)).values[2]

## @show eigen(Array(Lh.A)).values[1]
## @show eigen(Array(Lh.Ax+Lh.AVx)).values[1]
## @show eigen(Array(Lh.Ay+Lh.AVy)).values[1]
## @show eigen(Array((Lh.Ax+Lh.AVx)*(Lh.Ay+Lh.AVy)*Lh.A)).values[1]
## @show eigen(Array(Lh.A)).values[1]*eigen(Array(Lh.Ax+Lh.AVx)).values[1]*eigen(Array(Lh.Ay+Lh.AVy)).values[1] - eigen(Array((Lh.Ax+Lh.AVx)*(Lh.Ay+Lh.AVy)*Lh.A)).values[1]

## Mind the [1] vs [2]
## @show eigen(Array(Lh.A)).values[2]
## @show eigen(Array(Lh.Axl+Lh.AVxl)).values[2]
## @show eigen(Array(Lh.Ayl+Lh.AVyl)).values[1]
## @show eigen(Array((Lh.Ax+Lh.AVx)*(Lh.Ay+Lh.AVy)*Lh.A)).values[2]
## @show eigen(Array(Lh.A)).values[2]*eigen(Array(Lh.Axl+Lh.AVxl)).values[2]*eigen(Array(Lh.Ayl+Lh.AVyl)).values[1] - eigen(Array((Lh.Ax+Lh.AVx)*(Lh.Ay+Lh.AVy)*Lh.A)).values[2]

## writeErrorsToCsv(errors, "errors")
plotErrors(errors, "Errors for (Ω.Lx,Ω.Ly)=($(Ω.Lx),$(Ω.Ly))")

# writeVtk(x_i, Ω, bcs)
