# # H20 Test

using LinearAlgebra
using Statistics
using ddEigenLab

struct Molecule7
  atoms::Array{Int}
  x::Array{Pair{Int, Int}}
  bonds::Vector{Tuple{Int, Int, Int}}
end
Molecule = Molecule7

function shift(mol::Molecule, shift::Pair{Int, Int})
  xshifted = map(i -> mol.x[i][1] + shift[1] => mol.x[i][2] + shift[2] , 1:length(mol.atoms))
  return Molecule(mol.atoms, xshifted, mol.bonds)
end

function molPotential(mol::Molecule, atompot::Function)
  return (x,y) -> sum(map(i -> mol.atoms[i] * atompot(x-mol.x[i][1],y-mol.x[i][2]), 1:length(mol.atoms)))
end

function getParaviewSource(mol)
  atomlines = map(i -> "output.AppendAtom($(mol.atoms[i]),$(mol.x[i][1]),$(mol.x[i][2]),0)", 1:length(mol.atoms))
  bondlines = map(i -> "output.AppendBond($(mol.bonds[i][1]),$(mol.bonds[i][2]),$(mol.bonds[i][3]))", 1:length(mol.bonds))
  return join(vcat([atomlines, bondlines]...), ";")
end

h2o = Molecule(
  [1, 8, 1],
  [
    1 => 2,
    2 => 1,
    3 => 2,
  ],
  [
    (0, 1, 2)
    (2, 1, 2)
  ]
)

mols = [h2o, shift(h2o, 3=>0)]


Lx = 7
Ly = 3
ρ = 50
Ω = Rectangle2D(Lx, Ly, ρ, ρ)

# pot(x,y) = 1
pot = (x,y) -> sum(map(i -> molPotential(mols[i], (x,y)->3-exp(-sqrt(x^2+y^2)))(x,y), 1:length(mols)))
L = NegativeLaplacePlusPotential(0, x->0, y->0, pot)

bcs = Dict(:x => :d, :y => :d)
Lh = FD2D(L, Ω, bcs)

x_i, lambda_i, errors, info = IP(Lh.A, 0, 1, 1, 1E-08, 1000, 0, zeros(Ω.N))

plotSolution(x_i, Ω, bcs, "approx", false)
plotSolution(x_i, Ω, bcs, "approx", true)
# plotPotential(V, Ω.xVals, Ω.yVals)

writeVtk(x_i, Ω, bcs, "phi")
writeVtk(x_i.^2, Ω, bcs, "phiSquared")
