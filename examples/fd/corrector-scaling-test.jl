using Printf
using LinearAlgebra
using SparseArrays
using Plots
gr()

using ddEigenLab

const Lxarray = 1:1:20
const epsarray = Float64[]
eigvalarray = Float64[]
lambdainfarray = Float64[]

for Lx = Lxarray
  Ly = 1
  ρ = 5
  Ω = Rectangle2D(Lx, Ly, ρ, ρ)

  Z = 1000
  # Vxy(x,y) = ((Ω.Ly/2)^2-(y-Ω.Ly/2)^2) + Z*sin(Ω.Lx*pi*x/Ω.Lx)^2  # y-periodic
  Vxy(x,y) = 100*Z*y + Z*sin(Ω.Lx*pi*x/Ω.Lx)^2  # non y-periodic
  L = NegativeLaplacePlusPotential(0, x->0, x->0, Vxy)
  Lh = FD2D(L, Ω, Dict(:x => :d, :y => :d))
  eigval, eigvec = eigen(Array(Lh.A))

  Lxp = 1
  Lyp = 1
  λinfty = eigen(Array(FD2D(L, Rectangle2D(Lxp, Ly, ρ, ρ), Dict(:x => :p, :y => :d)).A)).values[1]
  λr = eigen(Array(FD2D(NegativeLaplacePlusPotential(0,x->0,y->0,(x,y)->0), Rectangle2D(Lx, Ly, ρ, ρ), Dict(:x => :d, :y => :p)).Ax)).values[1]

  println(Lx)
  push!(eigvalarray, eigval[1])
  push!(lambdainfarray, λinfty)

  eps = eigval[1] - λinfty
  push!(epsarray, eps)
end

plot(Lxarray, [epsarray, epsarray[end]*Lxarray[end]^2*[Lx^-2 for Lx in Lxarray]], yaxis=:log, xaxis=:log, label=["lambdaReal - lambdaInf" "Lx^-2"], marker=:circle, title="lambdaInf is solution to Dirichlet-0-y and Periodic-x cell problem")
