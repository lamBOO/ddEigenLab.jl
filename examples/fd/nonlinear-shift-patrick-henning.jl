using LinearAlgebra
using Plots

# Works a little bit: SOme improvement with nonlienarity can be observed
# But we don't get independece of parameter tau

# Construct matrix with collapsing gap
eps = 1E-0
A = eps * [
  2 -1 0
  -1 2 -1
  0 -1 2
] + I(3)

function ipm(A; x0=ones(size(A)[1]), kmax=10, tol=1E-8, alpha=2.4)
  errors = Float64[]
  # x = abs.(randn(size(x0)[1]))
  @info x
  x = copy(x0)
  for k=1:kmax

    # nonlinear shift, quadratic GPE term with vanishing prefactor
    Q = diagm(x.^2)
    res = (A - (x' * A * x) / (x' * x) * I(size(A)[1])) * x
    tau = alpha * norm(res)
    # @info tau
    # tau = alpha * (x' * A * x) / (x' * x)
    # tau = 0.001
    AQ = A + tau * Q

    x = AQ \ x
    normalize!(x, )
    # x = x / (sqrt(x' * (I(size(A)[1]) + tau*Q) * x))
    res = (A - (x' * A * x) / (x' * x) * I(size(A)[1])) * x
    resnorm = norm(res)
    @info k, resnorm
    push!(errors, resnorm)
    if resnorm < tol
      return (x, errors)
      break
    end
  end
  return (x, errors)
end

errors_array = Array{Array{Float64}}(undef,0)
# alpha_array = [10.0^i for i=-2:1//3:1-1//3]
alpha_array = [i for i=0:0.25:4]
for alph in alpha_array
  @info alph
  # x, errors = ipm(A, kmax=1000, x0=normalize!([2.,1,1]), alpha=alph)  # TODO: No positive effect at all :-(
  x, errors = ipm(A, kmax=1000, x0=[2.,1,1], alpha=alph)  # TODO: SOme slight positive effect
  push!(errors_array, errors)
end
display(plot(errors_array, yaxis=:log, markersize=1.5, markerstrokewidth=0, marker=:circle, labels="α=" .* string.(round.(alpha_array', digits=5))))
@info x, (x' * A * x) / (x' * x)

# x, errors = ipm(A, kmax=1000, x0=[2,1,1])
# display(plot(errors, yaxis=:log, markersize=1.5, markerstrokewidth=0, marker=:circle))
# @info x, (x' * A * x) / (x' * x)
