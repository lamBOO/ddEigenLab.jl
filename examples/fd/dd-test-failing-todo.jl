using LinearAlgebra
using SparseArrays
using Statistics
using Plots
using Distributed
using IterativeSolvers
using CSV, Tables
gr()

using ddEigenLab




# Something is wrong here I think. When increasing the Lx, the inner iterations
# go up, but only for oberlap large. Investigate!!!!
# For the first symmetric potential, it works however...


# ## Setup Long Chain Domain^1
const Lx = 40
const Ly = 1
const ρ = 10
const Ω = Rectangle2D(Lx, Ly, ρ, ρ)


Z = 10

# Vxy(x,y) = Z*sin(Ω.Ly*pi*y/Ω.Ly)^2 + Z*sin(Ω.Lx*pi*x/Ω.Lx)^2 # works for A
# Vxy(x,y) = Z^2 * sin((x+0.25)*π)^2 * sin(y*π)^2
# Vxy(x,y) = Z^2 * (sin(x*π)^2 * sin(y*π)^2 + 1*(((x% 1)-0.25)^2 ))
# Vxy(x,y) = Z^2 * (sin(x*π)^2 * sin(y*π)^2 + 5*(((x% 1)-0.25)^2 )) # doesnt stay constant!!
Vxy(x,y) = Z^2 * (sin(x*π)^2 * sin(y*π)^2 + 5*(((10+x-0.25)%1)^2 )) # doesnt stay constant!!
# Vxy(x,y) = 0
V(x,y) = Vxy(x,y)
V0 = 0
Vx(x) = 0
Vy(y) = 0
L = NegativeLaplacePlusPotential(V0, Vx, Vy, Vxy)


# ## Discretize Operators Using Finite Differences
# Lh = FD2D(L, Ω)
bcs = Dict(:x => :d, :y => :d)
Lh = FD2D(L, Ω, bcs)





# ## Setup Preconditioners
@info "Preconditioners start"
Lxp = 1
Lyp = 1
Ωp = Rectangle2D(Lxp, Lyp, ρ, ρ)
bcsp = Dict(:x => :p, :y => :d)
# @time cellsolution = eigen(Symmetric(Array(FD2D(L, Ωp, bcsp).A)), 1:1)
# λ∞ = cellsolution.values[1]
# psi = cellsolution.vectors[:, 1]
A = FD2D(L, Ωp, bcsp).A
@time psi,λ∞ = IP(A, 0, 1, 1, 1E-10, 1000, 0, zeros(size(A)[1]), false)

# psi = vcat([
#   [psi[(length(Ωp.xVals)-2)*(i-1)+1:(length(Ωp.xVals)-2)*i]; 0] for i=1:length(Ω.yVals)-2
# ]...)

iP_λ∞ = Lh.A-λ∞*I(Ω.N)
psifull = vcat(
  [
    vcat(
      [[psi[i*(length(Ωp.xVals)-1)+1 : (i+1)*(length(Ωp.xVals)-1)] for i=0:length(Ωp.yVals)-2-1][k] for j=1:Lx]...
    )[2:end] for k=1:(length(Ωp.yVals)-2)
  ]...
)
psifullfloating = vcat(
  [
    vcat(
      [0 for i=1:(length(Ωp.xVals)-2)]...,
      vcat([[psi[i*(length(Ωp.xVals)-1)+1 : (i+1)*(length(Ωp.xVals)-1)] for i=0:length(Ωp.yVals)-2-1][k] for j=2:Lx]...)[1:end-(length(Ωp.xVals)-2)],
      [0 for i=1:(length(Ωp.xVals)-2)]...,
    )[1:end] for k=1:(length(Ωp.yVals)-2)
  ]...,
)
writeVtk(psi, Ωp, bcsp, "psi")
writeVtk(psifull, Ω, bcs, "psifull")
@info "Preconditioners end"

sinyinf(x,y) = sin(pi * y / Ly)
sinyinfvec = vec(sinyinf.(Ω.xVals[2:end-1], Ω.yVals[2:end-1]'))

# sininf(x,y) = sin(pi * x / Lx) * sin(pi * y / Ly)
sininf(x,y) = sin(pi * x / Lx)
sininfvec = vec(sininf.(Ω.xVals[2:end-1], Ω.yVals[2:end-1]'))
# sininfvec = sininfvec / norm(sininfvec)

uinfvec = sininfvec .* psifull
uinfvec = uinfvec / norm(uinfvec)

sinyinffloating = sinyinfvec .* (!isequal).(psifullfloating, 0)
sinyinffloating = sinyinffloating / norm(sinyinffloating)

onesfloating = ones(Ω.N) .* (!isequal).(psifullfloating, 0)

writeVtk(uinfvec, Ω, bcs, "uinf") ##




function eigensolve(solver::PSM, x0::Vector{Float64}, A::SparseMatrixCSC{Float64, Int64}; m::Int = 1)
  @info "Start"
  x_i = copy(x0)
  # x_i1 = copy(x0)
  # x_i1 = normalize(x_i1 .* rand(size(x_i1)))
  # x_i1 = normalize(rand(size(x_i)[1]))
  x_i1 = Array(I(size(x_i)[1])[:,1])

  resnorms = Float64[]

  outer_its = -1
  inner_its = Int64[]
  inner_tol = 1E-8
  outer_maxiter = 100
  inner_maxiter = 1000

  λ = ddEigenLab.raycoeff(x_i, A)
  resnorm = norm(A * x_i - λ * x_i)

  for i=1:outer_maxiter
    if m == 1  # IP_BACKSLASH
      x_i = solver.A \ x_i
    elseif m==2  # IP_RAS1
      x_i, info = solve(RAS1, x_i, x_i, inner_maxiter, inner_tol, debug=false)
      @show info.iters
      push!(inner_its, info.iters)
    elseif m==3  # IP_RAS2
      x_i, info = ddEigenLab.solve(RAS2, x_i, x_i, inner_maxiter, inner_tol, debug=false)
      @show info.iters
      push!(inner_its, info.iters)
    elseif m==4  # IP_CGASM2
      x_i, info = cg(solver.A, x_i, verbose=false, log=true, Pl=ASM2, maxiter=inner_maxiter, reltol=inner_tol)
      if i==1 CSV.write("$(Lx).csv",  Tables.table(hcat([1:length(info[:resnorm]),info[:resnorm]]...)), writeheader=false) end
      @info info
      push!(inner_its, info.iters)
    elseif m==5  # IP_GMRESRAS2
      x_i, info = gmres(solver.A, x_i, verbose=false, log=true, Pl=RAS2, maxiter=inner_maxiter, reltol=inner_tol, restart=size(A, 2))
      @info info
      push!(inner_its, info.iters)
    elseif m==6  # LOPCG_RAS2
      tmp = x_i
      p, info = solve(RAS2, A * x_i - ddEigenLab.raycoeff(x_i, A) * x_i, x_i, inner_maxiter, inner_tol, debug=false)
      @info p
      # p = solver \ (A * x_i - ddEigenLab.raycoeff(x_i, A) * x_i)  # new1
      x_i = ddEigenLab.RRg(A, sparse(I(size(A)[1])), [x_i, p, x_i1])
      x_i1 = tmp
      @info info.iters
      push!(inner_its, info.iters)
    elseif m==7
      tmp = x_i
      p, info = cg(solver.A, (A * x_i - ddEigenLab.raycoeff(x_i, A) * x_i), verbose=false, log=true, Pl=ASM2, maxiter=inner_maxiter, reltol=inner_tol)
      x_i = ddEigenLab.RRg(A, sparse(I(size(A)[1])), [x_i, p, x_i1])
      x_i1 = tmp
      @info info.iters
      push!(inner_its, info.iters)
    elseif m==8
      tmp = x_i
      p, info = gmres(solver.A, (A * x_i - ddEigenLab.raycoeff(x_i, A) * x_i), verbose=false, log=true, Pl=solver, maxiter=inner_maxiter, reltol=inner_tol, restart=size(A, 2))
      # p, info = gmres(solver.A, (A * x_i - ddEigenLab.raycoeff(x_i, A) * x_i), verbose=false, log=true, Pl=RAS2, maxiter=inner_maxiter, reltol=minimum([0.5,resnorm]), restart=size(A, 2))
      x_i = ddEigenLab.RRg(A, sparse(I(size(A)[1])), [x_i, p, x_i1])
      x_i1 = tmp
      @info info
      push!(inner_its, info.iters)
    elseif m==9  # LOPCG_RAS2_NOIO
      tmp = x_i
      p = solver \ (A * x_i - ddEigenLab.raycoeff(x_i, A) * x_i)  # new1
      x_i = ddEigenLab.RRg(A, sparse(I(size(A)[1])), [x_i, p, x_i1])
      x_i1 = tmp
      push!(inner_its, 1)
    end

    x_i = x_i / norm(x_i)

    λ = ddEigenLab.raycoeff(x_i, A)
    resnorm = norm(A * x_i - λ * x_i)
    push!(resnorms, resnorm)
    @show i, λ, resnorm
    if resnorm < 1E-7
      outer_its = i
      break
    end
  end
  return (x_i, (outer_its = outer_its, inner_its = inner_its, resnorms=resnorms))
end


ol = 5

pu = PartitionOfUnity(Ω, ol)
par = Partition(Ω, ol)
cc = GeneralizedNicolaides(iP_λ∞, pu, psifull[:,:])
ccA = GeneralizedNicolaides(Lh.A, pu, psifull[:,:])
nico = GeneralizedNicolaides(iP_λ∞, pu, ones(Ω.N)[:,:])
nocc = NoCoarseCorrection()
ef = Efendiev(iP_λ∞, pu, psifull[:,:])
sicc = ShiftAndInvertCoarseCorrection(Lh.A, pu, psifull[:,:])

evals, evec = eigen(Array(sicc.Z' * Lh.A * sicc.Z), Array(sicc.Z' * sicc.Z))
λ0 = evals[1]

# @time ASM2 = PSM(Lh.A, par, sicc)
@time ASM2 = PSM(iP_λ∞, par, sicc)
@time ASM1 = PSM(iP_λ∞, par, nocc)
@time RAS2 = PSM(iP_λ∞, pu, cc)
@time RAS1 = PSM(iP_λ∞, pu, nocc)

# assert valid PU
@assert sum([pu.Ri[i]' * pu.Di[i] * pu.Ri[i] for i=1:length(pu.Di)]) == I(size(pu.Ri[1])[2])

# @time stRAS2 = StationaryMethod(RAS2, normalize(ones(Ω.N), 2), 1000, 1E-10, debug=false);
# @time stRAS1 = StationaryMethod(RAS1, normalize(ones(Ω.N), 2), 1000, 1E-10, debug=false);
# @time stASM2 = StationaryMethod(ASM2, normalize(ones(Ω.N), 2), 1000, 1E-10, debug=false);
# @time LOPCG(Lh.A, I(size(Lh.A)[1]), stRAS2, 1, 1, 1E-7, 100, 1);
# @time LOPCG(Lh.A, I(size(Lh.A)[1]), stRAS1, 1, 1, 1E-7, 100, 1);
# @time LOPCG(Lh.A, I(size(Lh.A)[1]), stASM2, 1, 1, 1E-7, 100, 1);

# x0 = normalize(rand(Ω.N), 2)
# x0 = normalize(ones(Ω.N), 2);
x0 = ones(Ω.N)
# x0 = normalize(sininfvec, 2)
# x0 = normalize(uinfvec, 2)
# x0 = normalize(psifull, 2)
# x0 = normalize(sinyinfvec, 2)
# x0 = normalize(sininfvec, 2)

# @time eigvec = eigensolve(ASM2, x0, Lh.A)
# @time eigvec, info = eigensolve(RAS2, x0, Lh.A, m=9)
@time eigvec = eigensolve(RAS2, x0, Lh.A, m=8)



## plot: inner iterations vs. spectral residual
## resnorm < 1E-7
## changes to 8:
## p, info = gmres(solver.A, (A * x_i - ddEigenLab.raycoeff(x_i, A) * x_i), verbose=false, log=true, Pl=RAS2, maxiter=inner_maxiter, reltol=minimum([0.01,resnorm]), restart=size(A, 2))
# # @time eigvec, info = eigensolve(RAS2, x0, Lh.A, m=8)  # 1+2) exact+inexact GMRES
# @time eigvec, info = eigensolve(RAS2, x0, Lh.A, m=9)  # 3) fused m=9
# initres = norm(Lh.A * x0 - ddEigenLab.raycoeff(x0, Lh.A) * x0)
# sress = reduce(vcat, ones.(vcat([info[:inner_its]..., 1])) .* vcat(initres, info[:resnorms]...))
# # CSV.write("1i.csv",  Tables.table(hcat([0:length(sress)-1,sress]...)), writeheader=false)  # exact GMRES
# # CSV.write("2i.csv",  Tables.table(hcat([0:length(sress)-1,sress]...)), writeheader=false)  # inexact GMRES
# CSV.write("3i.csv",  Tables.table(hcat([0:length(sress)-1,sress]...)), writeheader=false)  # fused m=9
# # CSV.write("1.csv",  Tables.table(hcat([1:length(info[:resnorms]),vcat(initres, info[:resnorms]...)]...)), writeheader=false)  # exact GMRES
# # CSV.write("2.csv",  Tables.table(hcat([1:length(info[:resnorms]),vcat(initres, info[:resnorms]...)]...)), writeheader=false)  # inexact GMRES
# CSV.write("3.csv",  Tables.table(hcat([0:length(info[:resnorms]),vcat(initres, info[:resnorms]...)]...)), writeheader=false)  # fused m=9

# new Idea: This works without shifting, only coarse space needed
# psifullfloating doesn work
# no shift operator, only Lh.A!!!
# only ol=1,2 work, ol=3 doesnt work, solution doesnt seem to be in H1 anymore
# LOPCG with preconditioning
# do 1x:
# itsarray = Tuple{Int,Int}[]
# begin
# sicc = ShiftAndInvertCoarseCorrection(Lh.A, pu, psifull[:,:])
# @time ASM1 = PSM(Ω, iP_λ∞, par, sicc)
# @time eigvec, info = eigensolve(ASM1, x0, Lh.A, m=9)
# push!(itsarray, (Lx, info[:outer_its]))
# end
# CSV.write("coarseshift-nocc-$(ol).csv",  Tables.table(hcat([getindex.(itsarray, i) for i=1:2]...)), writeheader=false)  # only at end!

# Genrate Table: Execute multiple times
# data = String[]
# for i=2:8
#   @info i
#   if i==2 && Lx>=16
#     info = (outer_its=-1, inner_its=[-1])
#   else
#     @time eigvec, info = eigensolve(RAS2, x0, Lh.A, m=i)
#   end
#   oi = info.outer_its
#   ii_sum = sum(info.inner_its)
#   ii_maxits = maximum(info.inner_its)
#   push!(data, "$(oi)/$(ii_maxits)/$(ii_sum)")
# end
# datamat = hcat(["$(2^i)" for i=1:7], vcat(permutedims.([data2, data4, data8, data16, data32, data64, data128])...))
# print(join([join(datamat[i,:], "&") * "\\\\\n" for i=1:size(datamat)[1]]))


# plot total number of inner solves
# p_ar = Float64[]
# inner_its_max_ar = Int[]
# inner_its_sum_ar = Int[]
# outer_its_ar = Int[]
# for p=0:0.05:1
#   ol = 1
#   pu = PartitionOfUnity(Ω, ol)
#   par = Partition(Ω, ol)
#   cc = GeneralizedNicolaides(iP_λ∞, pu, psifullfloating[:,:])
#   nico = GeneralizedNicolaides(iP_λ∞, pu, ones(Ω.N)[:,:])
#   nocc = NoCoarseCorrection()
#   @time ASM2 = PSM(Ω, Lh.A-p*λ∞*I(Ω.N), par, cc)
#   @time ASM1 = PSM(Ω, Lh.A-p*λ∞*I(Ω.N), par, nocc)
#   @time eigvec, info = eigensolve(ASM1, x0, Lh.A, m=4)
#   push!(p_ar, p)
#   push!(inner_its_max_ar, maximum(info.inner_its))
#   push!(inner_its_sum_ar, sum(info.inner_its))
#   push!(outer_its_ar, info.outer_its)
# end



# writeVtk(eigvec[1], Ω, bcs, "x_i") ##
# writeVtk(psi, Rectangle2D(Lxp, Lyp, ρ, ρ), Dict(:x => :p, :y => :d), "psi") ##

