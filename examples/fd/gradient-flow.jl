using LinearAlgebra

A = [
  1 0
  0 2
]
n = size(A)[1]

R(A, x) = x' * A * x / (x' * x)
∇R(A,x) = 2 * (A * x - R(A,x) * x) / (x' * x)

# Hessian of Rayleigh
# [yang 1989 eq (10)]
HR(A,x) = 2 * (A - ∇R(A,x) * x' - x * ∇R(A,x)' - R(A,x) * I(n)) / (x' * x)

# [heid,stamm 2021 eq (7)]
# B is "metric" / "weight"
G(B,x) = B \ x

# [heid,stamm 2021 eq (8)]
P(B,G,x) = x - (x' * x) / (x' * B * x) * (B \ x)

# [heid,stamm 2021 eq (14b)]
function gfi(x0;τ=0.01,kmax=1)
  x = copy(x0)
  resnorms = Float64[]
  for k=1:kmax
    @show x
    @show ∇R(A,x)
    @show B = HR(A,x)
    # @show B = I(n)
    @show G = (A \ x)
    @show (x' * B * x)
    @show (G' * B * G)
    @show resnorm = norm(A * x - R(A,x) * x)
    push!(resnorms,resnorm)
    # [heid,stamm 2021 eq (8)]
    x = (1-τ)*x + τ / (G' * B * G) * G
    normalize!(x)
  end
  return x,resnorms
end
