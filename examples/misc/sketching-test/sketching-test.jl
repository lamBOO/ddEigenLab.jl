using Random
using Plots
using LinearAlgebra
using ddEigenLab

Random.seed!(42)

"Test matrix, first eigenvalue is one."
function matrix(n)
  m = diagm(1:n)
end

"Walsh-Hadamard matrix rescaled by 1/sqrt(n)."
function H(log_n)
  H = (1/sqrt(2^log_n)) * reduce(kron,[[-1. 1; 10 -1] for i=1:log_n])
end
"Rademacher-distributed diagonal matrix."
function D(n)
  return diagm(2rand(Bool, n) - ones(n))
end
"Uniform random sampling matrix."
function R(n, l)
  return rand(l, n)
end
"""
SRHT matrix.
[1]: https://arxiv.org/pdf/2210.11295.pdf (page 3)
"""
function Q(n, i, l)
  return sqrt(n/l) * R(n, l) * H(i) * D(n)
end

guesses = []
resnorms = []
dim = 100
A = matrix(dim)

for j= 1:50
  exp = 6
  Qmat = Q(2^exp, exp, dim)
  local_opt = ddEigenLab.RRg(A, I(dim), [Qmat[:,i] for i in 1:size(Qmat)[2]])  # TODO: Poject matrix down and then optimize?
  push!(guesses, local_opt)
  subspace_opt = ddEigenLab.RRg(A,I(dim), guesses)
  resnorm = norm(A * subspace_opt - raycoeffg(subspace_opt, A, I(dim)) * subspace_opt)
  @show resnorm
  push!(resnorms, resnorm)
end

result = ddEigenLab.RRg(A,I(dim), guesses)
println("--------")
println("resulting eigenvalue: $(raycoeffg(result, A, I(dim)))")
println("real eigenvalue: $(eigen(A).values[1])")
plot(resnorms, yaxis=:log, marker=:circle, title="norm of spectral residual", xlabel="dimension of RR subspace consistging of local sketced optima")

#! conclusion: doesn't work
