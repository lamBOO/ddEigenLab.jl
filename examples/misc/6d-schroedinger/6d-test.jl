using Gridap
using LinearAlgebra
using SparseArrays
using LinearMaps

using ddEigenLab

ρ = 20
d = 4
L = 5
domain = (0, L, 0, L, 0, L, 0, L)
partition = (ρ, ρ, ρ, ρ)
model2 = CartesianDiscreteModel(domain, partition)
labels2 = get_face_labeling(model2)
add_tag_from_tags!(labels2, "diri", collect(1:3^d-1))
order = 1
reffe2 = ReferenceFE(lagrangian, Float64, order)
V02 = TestFESpace(model2, reffe2; conformity=:H1, dirichlet_tags = ["diri"])
# V02 = TestFESpace(model2, reffe2; conformity=:H1)  # neumann zero
Ug2 = TrialFESpace(V02, 0)
degree = 2
Ω2 = Triangulation(model2)
dΩ2 = Measure(Ω2, degree)

α = 1000
β = 0.0001
prot = [L/2, L/2]
# Vext(x) = α * ((x[1]-0.5)^2 + (x[2]-0.5)^2)
Vext(x) = - 2 * (
    1/(sqrt((x[1]-prot[1])^2 + (x[2]-prot[2])^2 + β))
  + 1/(sqrt((x[3]-prot[1])^2 + (x[4]-prot[2])^2 + β))
)
ee(x) = 1/(sqrt((x[1]-x[3])^2 + (x[2]-x[4])^2 + β))


b2(v) = ∫( 1 * v ) * dΩ2  # dummy
a11(u,v) = ∫( ∇(v) ⋅ ∇(u) + Vext * u * v + ee * u * v) * dΩ2
op11 = AffineFEOperator(a11, b2, Ug2, V02)

# ls = LUSolver()
# solver = LinearFESolver(ls)
# uh = Gridap.solve(solver,op11)

@info "assmble start"
left = assemble_matrix(a11, Ug2, V02)
a22(u,v) = ∫( v * u ) * dΩ2
op22 = AffineFEOperator(a22, b2, Ug2, V02)
right = assemble_matrix_and_vector(a22, b2, Ug2, V02)[1]
@info "assmble end"

# psi0, eigval, errors, info = IP(left, right, 0, 1, 1, 1E-10, 1000)
K = 6
@info "eigsolve start"
@time res = lobpcg(left, right, false, ones(size(left)[1]), K)
@info "eigsolve end"

display(heatmap(reshape(psi0[1:(ρ-1)^2],(ρ-1,ρ-1))))
[display(heatmap(reshape(res.X[:,i][1:(ρ-1)^2],(ρ-1,ρ-1)), title="λ$(i)=$(res.λ[i])")) for i=1:K]
@info "finished"


