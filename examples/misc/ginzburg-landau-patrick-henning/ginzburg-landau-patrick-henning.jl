using Gridap

κ = 1

model = CartesianDiscreteModel((0,1,0,1), (10,10))

order = 1
reffe = ReferenceFE(lagrangian, Float64, order)
V = TestFESpace(model, reffe, vector_type = Vector{ComplexF64})
U = V   # mathematically equivalent to TrialFESpace(V,0)

degree = 2
Ω = Triangulation(model)
dΩ = Measure(Ω, degree)

Γ_s = BoundaryTriangulation(model) # Source line
dΓ_s = Measure(Γ_s, degree)

# a(u,v) = ∫(  (∇.*(Λf*v))  ⊙((ξ∘τ)*(Λf.*∇(u))) - (k^2*(v*u))  )dΩ

A(x) = sqrt(2) * VectorValue(
  sin(pi*x[1]) * cos(pi*x[2]),
  -cos(pi*x[1]) * sin(pi*x[2])
)  # [Doehrig Henning 23]
magnetic(x) = im * κ * A(x)

uold = 1

test = Gridap.interpolate(x->0.5+0.5im, V)

function a_A(w, A, κ)
  a(u, v) = ∫(
    (∇(u) + (x->im * κ * A(x))) ⋅ (∇(v) + (x->im * κ * A(x)))
    + (
      (x->κ^2 * (abs2(w(x))^2 - 1))
    ) * u * v
  )dΩ
  return assemble_matrix(a, U, V)
end

a(u, v) = ∫(
  (∇(u) + (x->im*κ*A(x))) ⋅ (∇(v) + (x->im*κ*A(x)))
  + (
    (x->κ^2 * (abs2(test(x))^2 - 1))
  ) * u * v
)dΩ
b(v) = ∫( 1 * v )dΩ

op = AffineFEOperator(a, b, U, V)
uh = Gridap.solve(op)



function gradflow(u0)

  return 0
end

writevtk(Ω, "ginzburg",
  cellfields = [
    "Real" => real(uh),
    "Imag"=>imag(uh),
    "Norm"=>abs2(uh),
  ]
)

# writevtk(Ω,"demo",cellfields=["Real"=>real(uh),
#         "Imag"=>imag(uh),
#         "Norm"=>abs2(uh),
#         "Real_t"=>real(uh_t),
#         "Imag_t"=>imag(uh_t),
#         "Norm_t"=>abs2(uh_t),
#         "Difference"=>abs(uh_t-uh)])
