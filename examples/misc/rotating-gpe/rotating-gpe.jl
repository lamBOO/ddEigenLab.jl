using Gridap
using ddEigenLab
using LinearAlgebra

κ = 1

L = 10
nelems = 50

model = CartesianDiscreteModel((-L,L,-L,L), (nelems,nelems))

labels = get_face_labeling(model)
add_tag_from_tags!(labels,"botleftpoint",[1,])
add_tag_from_tags!(labels,"botrightpoint",[2,])
add_tag_from_tags!(labels,"topleftpoint",[3,])
add_tag_from_tags!(labels,"toprightpoint",[4,])
add_tag_from_tags!(labels,"botline",[5,])
add_tag_from_tags!(labels,"topline",[6,])
add_tag_from_tags!(labels,"leftline",[7,])
add_tag_from_tags!(labels,"rightline",[8,])
add_tag_from_tags!(labels,"all",[1,2,3,4,5,6,7,8])

order = 1
reffe = ReferenceFE(lagrangian, Float64, order)
V = TestFESpace(model, reffe, vector_type = Vector{ComplexF64}, dirichlet_tags = ["all"])
# U = V   # mathematically equivalent to TrialFESpace(V,0)
U = TrialFESpace(V,0)

degree = 2
Ω = Triangulation(model)
dΩ = Measure(Ω, degree)

Γ_s = BoundaryTriangulation(model) # Source line
dΓ_s = Measure(Γ_s, degree)

# a(u,v) = ∫(  (∇.*(Λf*v))  ⊙((ξ∘τ)*(Λf.*∇(u))) - (k^2*(v*u))  )dΩ

A(x) = sqrt(2) * VectorValue(
  sin(pi*x[1]) * cos(pi*x[2]),
  -cos(pi*x[1]) * sin(pi*x[2])
)  # [Doehrig Henning 23]
magnetic(x) = im * κ * A(x)

R(x) = 1*VectorValue([x[2],-x[1]])  # altmann21

uold = 1

test = Gridap.interpolate(x->0.5+0.5im, V)

function a_A(w, A, κ)
  a(u, v) = ∫(
    (∇(u) + (x->im * κ * A(x))) ⋅ (∇(v) + (x->im * κ * A(x)))
    + (
      (x->κ^2 * (abs2(w(x))^2 - 1))
    ) * u * v
  )dΩ
  return assemble_matrix(a, U, V)
end

omega = 0.6345
W(x) = 1/2 * (x[1]^2 + x[2]^2)
Wr(x) = W(x) -1/4 * omega^2 * (x[1]^2 + x[2]^2)

a(u, v) = ∫(
  (∇(u) + (x->im*omega*R(x))*u) ⋅ (∇(v) + (x->im*omega*R(x))*v)
  + Wr * u * v
)dΩ
b(v) = ∫( 1 * v )dΩ
c(u,v) = ∫( u * v )dΩ
stiffnessmat = assemble_matrix(a, U, V)
massmat = assemble_matrix(c, U, V)
stiffnessmat_r = real.(stiffnessmat)
massmat_r = real.(massmat)

op = AffineFEOperator(a, b, U, V)
uh = Gridap.solve(op)

shift = -53
evecc, evall, _, _ = LOPCG(stiffnessmat_r, massmat_r, Pl = stiffnessmat_r-shift*massmat_r, tol = 1E-10, maxiter = 1000)

function gradflow(u0)

  return 0
end

writevtk(Ω, "ginzburg",
  cellfields = [
    "Real" => real(uh),
    "Imag"=>imag(uh),
    "Norm"=>abs2(uh),
    "evecc"=>real(FEFunction(V, Complex.(evecc))),
  ]
)

# writevtk(Ω,"demo",cellfields=["Real"=>real(uh),
#         "Imag"=>imag(uh),
#         "Norm"=>abs2(uh),
#         "Real_t"=>real(uh_t),
#         "Imag_t"=>imag(uh_t),
#         "Norm_t"=>abs2(uh_t),
#         "Difference"=>abs(uh_t-uh)])
