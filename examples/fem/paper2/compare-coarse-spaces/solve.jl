using Gridap
import GridapDistributed: compute_cell_graph
import Gridap.CellData: KDTreeSearch,Interpolable,return_cache
using Metis
using Plots

using ddEigenLab

cd(@__DIR__)

# Parameters
shift = 0.0
pot(x) = 100 * cos(pi * (x.data[1] - shift))^2 * cos(pi * x.data[2])^2
# pot(x) = 0
ρ = 10
Lxp = Lyp = 1.0
Lx = 4
f = (x->1)

# Periodic cell
domain = (0, Lxp, 0, Lyp)
partition = (Lxp * ρ, Lyp * ρ)
model = CartesianDiscreteModel(domain, partition; isperiodic=(true, false))
labels = get_face_labeling(model)
neu_tags = []  # [7,8] for x-direction
add_tag_from_tags!(labels, "diri", filter!(e->!(e∈neu_tags), collect(1:3^2-1)))
reffe = ReferenceFE(lagrangian, Float64, 1)
VV = TestFESpace(model, reffe, dirichlet_tags=["diri"])
U = TrialFESpace(VV, 0)
Ω = Triangulation(model)
dΩ = Measure(Ω, 2)
a1(u, v) = ∫(∇(u) ⋅ ∇(v) + (x -> pot(x)) * u * v)dΩ
a2(u, v) = ∫(u * v)dΩ
b(v) = ∫(1 * v)dΩ
stiffnessmat = assemble_matrix(a1, U, VV)
massmat = assemble_matrix(a2, U, VV)
eigvec, eigval, _ = LOPCG(
  stiffnessmat, massmat, Pl = stiffnessmat, tol = 1E-10, maxiter = 1000
)
psi = FEFunction(U, eigvec);
writevtk(model, "model")
writevtk(Ω, "psi", cellfields=["psi" => psi])

# full domain
domain2 = (0, Lx, 0, Lyp)
partition2 = (Lx * ρ, Lyp * ρ)
model2 = CartesianDiscreteModel(domain2, partition2; isperiodic=(false, false))
labels2 = get_face_labeling(model2)
add_tag_from_tags!(labels2, "diri", collect(1:3^2-1))
reffe2 = ReferenceFE(lagrangian, Float64, 1)
V2 = TestFESpace(model2, reffe2, dirichlet_tags=["diri"])
U2 = TrialFESpace(V2, 0)
Ω2 = Triangulation(model2)
dΩ2 = Measure(Ω2, 2)
a12(u, v) = ∫(∇(u) ⋅ ∇(v) + (x -> pot(x)) * u * v)dΩ2
a22(u, v) = ∫(u * v)dΩ2
b2(v) = ∫(f * v)dΩ2
stiffnessmat2 = assemble_matrix(a12, U2, V2)
massmat2 = assemble_matrix(a22, U2, V2)
rhs = assemble_vector(b2, V2)

# Create shifted matrix and solve
P = stiffnessmat2 - eigval * massmat2
x_exact = P \ rhs
u_ex = FEFunction(U2, x_exact);

# dummy space without boundary conditions
V_nobc = TestFESpace(model2, ReferenceFE(lagrangian, Float64, 1))

# Solve for groundstate
gs_x, gs_λ, _ = LOPCG(P, massmat2, Pl = stiffnessmat2, tol = 1E-10, maxiter = 1000)
gs_fun = FEFunction(U2, gs_x);
gs_fun_nobc = Gridap.FESpaces.interpolate(gs_fun, V_nobc)

# build coarse generator function
search_method = KDTreeSearch(num_nearest_vertices=5)
psi_helper = Interpolable(psi; searchmethod=search_method)
xshift = 0.0
const cache4 = return_cache(psi_helper, Point(0.0, 0.0))
function helper4(x)
  return evaluate!(cache4, psi_helper,
    Point(mod(x[1] - xshift, Lxp) + xshift, x[2] * (1.0 - 0))
  )
end
psi_pefa = Gridap.FESpaces.interpolate(helper4, V2)
psi_pefa_nobc = Gridap.FESpaces.interpolate(helper4, V_nobc)
psi_sin = Gridap.FESpaces.interpolate(x->sin(pi*x[2]), V2)
psi_sin_nobc = Gridap.FESpaces.interpolate(x->sin(pi*x[2]), V_nobc)
psi_nico_nobc = Gridap.FESpaces.interpolate(x->1, V_nobc)

u_ex_nobc = Gridap.FESpaces.interpolate(u_ex, V_nobc)

# Create partition
# par = Metis.partition(compute_cell_graph(model2), Lx)
par = kron(
  ones(Int32, ρ),
  vcat([kron(ones(Int32, 10),i) for i=convert.(Int32, [2,1,4,3])]...)
)  # for structured  decompotison (Metis didn't get it due to 0-BC)
pu = PartitionOfUnity(par, 1, V2)
nocc = NoCoarseCorrection()
cc_pefa = GeneralizedNicolaides(P, pu, psi_pefa.free_values[:, :])
cc_nico = GeneralizedNicolaides(P, pu, ones(length(psi_pefa.free_values))[:, :])
cc_sin = GeneralizedNicolaides(P, pu, psi_sin.free_values[:, :])
cc_diri = DirichletGenEO(P, pu, 0.1)
RAS_pefa = PSM(P, pu, cc_pefa)
RAS_nico = PSM(P, pu, cc_nico)
RAS_sin = PSM(P, pu, cc_sin)
RAS_diri = PSM(P, pu, cc_diri)

g = compute_cell_graph(model2)
elemsp = create_elements_partition(par, 4)
create_overlapping_elements_partition!(elemsp, g, 4, 1)
olparvecs = [zeros(Int32, length(par)[1]) for i=1:length(pu.Di)]
for ipar=1:length(pu.Di)
  olparvecs[ipar][elemsp[ipar]] .= 1
end
olparfuns = [FEFunction(U2, Float64.(olparvecs[i])) for i=1:length(pu.Di)]
allelems = vcat(elemsp...)
overlapelems = unique(filter(y->length(findall(x->x==y,allelems)) > 1, allelems))
overlapvec = zeros(Int32, length(par)[1])
overlapvec[overlapelems] .= 1

# Solve
ccs = [RAS_pefa, RAS_nico, RAS_sin, RAS_diri]
res = map(ras->ddEigenLab.solve(ras, rhs, x0=zeros(V2.nfree), maxiter=100, reltol=1E-6, debug=true, history=true, coarse_history=true), ccs)

hist_sol = map(x->x[2].solution_history, res)
hist_cc = map(x->x[2].coarse_updates, res)
hist_err = map(hist->map(y->abs.(y-x_exact), hist), hist_sol)
sols_cc = map(m->m.cc.Z * (m.cc.A0 \ (m.cc.Z' * rhs)), ccs)
nsols_cc = map(x-> x_exact'*x_exact / (x_exact' * x) * x, sols_cc)  # norm.

puvecs = [pu.Ri[i]' * pu.Di[i] * pu.Ri[i] * ones(size(stiffnessmat2)[1]) for i=1:length(pu.Di)]
pufuns = [FEFunction(U2, puvecs[i]) for i=1:length(pu.Di)]
parvecs = [pu.Ri[i]' * pu.Ri[i] * ones(size(stiffnessmat2)[1]) for i=1:length(pu.Di)]
parfuns = [FEFunction(U2, parvecs[i]) for i=1:length(pu.Di)]
writevtk(Ω2, "final", cellfields=[
  "par" => par,
  "overlap" => overlapvec,
  "u" => u_ex,
  "u_nobcs" => u_ex_nobc,
  "gs" => gs_fun,
  (["pu_$(lpad(i,3,"0"))" for i=1:length(pu.Di)] .=> pufuns)...,
  (["par_$(lpad(i,3,"0"))" for i=1:length(pu.Di)] .=> parfuns)...,
  "psi_pefa" => psi_pefa,
  "psi_pefa_nobc" => psi_pefa_nobc,
  "psi_nico_nobc" => psi_nico_nobc,
  "psi_sin_nobc" => psi_sin_nobc,
  "sol_pefa" => FEFunction(U2, cc_pefa.Z * (cc_pefa.A0 \ (cc_pefa.Z' * rhs))),
  "sol_nico" => FEFunction(U2, cc_nico.Z * (cc_nico.A0 \ (cc_nico.Z' * rhs))),
  "sol_sin" => FEFunction(U2, cc_sin.Z * (cc_sin.A0 \ (cc_sin.Z' * rhs))),
  "sol_diri" => FEFunction(U2, cc_diri.Z * (cc_diri.A0 \ (cc_diri.Z' * rhs))),
  "factor_pefa" => FEFunction(U2, gs_x ./ psi_pefa.free_values),
  "factor_sin" => FEFunction(U2, gs_x ./ psi_sin.free_values),
  "factor_nobc_pefa" => gs_fun_nobc / psi_pefa_nobc,
  (["cc_pefa_$i" for i=1:length(pu.Di)] .=>
    [FEFunction(U2, cc_pefa.Z[:,i]) for i=1:length(pu.Di)]
  )...,
  (["cc_nico_$i" for i=1:length(pu.Di)] .=>
    [FEFunction(U2, cc_nico.Z[:,i]) for i=1:length(pu.Di)]
  )...,
  (["cc_sin_$i" for i=1:length(pu.Di)] .=>
    [FEFunction(U2, cc_sin.Z[:,i]) for i=1:length(pu.Di)]
  )...,
  (["cc_diri_$i" for i=1:length(pu.Di)] .=>
    [FEFunction(U2, cc_diri.Z[:,i]) for i=1:length(pu.Di)]
  )...,
])

for (j, m) in enumerate(ccs)
  createpvd("cc-$j") do pvd
    @info pvd
    for i in 1:size(m.cc.Z)[2]
      pvd[i] = createvtk(Ω2,"cc-$(j)_$(i)"*".vtu",cellfields=[
        "cc" => FEFunction(U2, m.cc.Z[:,i]),
      ])
    end
  end
end

for (j, cc) in enumerate(ccs)
  createpvd("process-$j") do pvd
    @info pvd
    for i in 1:length(hist_sol[j])
      pvd[i] = createvtk(Ω2,"process-$(j)_$(i)"*".vtu",cellfields=[
        "u" => FEFunction(U2, hist_sol[j][i]),
        "err" => FEFunction(U2, hist_err[j][i]),
        "cc" => FEFunction(U2, hist_cc[j][i]),
        "sol" => FEFunction(U2, sols_cc[j]),
        "nsol" => FEFunction(U2, nsols_cc[j]),
      ])
    end
  end
end

# Plot errors over iterations
errors = map(errs->map(err->norm(err), errs), hist_err)
plot(errors,yaxis=:log)
