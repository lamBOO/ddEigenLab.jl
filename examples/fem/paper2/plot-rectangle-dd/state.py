# state file generated using paraview version 5.11.0
import paraview
paraview.compatibility.major = 5
paraview.compatibility.minor = 11

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# get the material library
materialLibrary1 = GetMaterialLibrary()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [3032, 2374]
renderView1.InteractionMode = '2D'
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.OrientationAxesVisibility = 0
renderView1.CenterOfRotation = [1.0, 0.5, 0.0]
renderView1.UseLight = 0
renderView1.StereoType = 'Crystal Eyes'
renderView1.CameraPosition = [0.9499326012275403, 0.41144607478349204, 4.319751617610021]
renderView1.CameraFocalPoint = [0.9499326012275403, 0.41144607478349204, 0.0]
renderView1.CameraFocalDisk = 1.0
renderView1.CameraParallelScale = 1.1180339887498945
renderView1.Background = [1.0, 1.0, 1.0]
renderView1.BackEnd = 'OSPRay raycaster'
renderView1.OSPRayMaterialLibrary = materialLibrary1

SetActiveView(None)

# ----------------------------------------------------------------
# setup view layouts
# ----------------------------------------------------------------

# create new layout object 'Layout #1'
layout1 = CreateLayout(name='Layout #1')
layout1.AssignView(0, renderView1)
layout1.SetSize(3032, 2374)

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'XML Unstructured Grid Reader'
resultvtu = XMLUnstructuredGridReader(registrationName='result.vtu', FileName=['/Users/lamberttheisen/Desktop/gits/ddEigenLab/examples/fem/plot-rectangle-dd/result.vtu'])
resultvtu.CellArrayStatus = ['cell']
resultvtu.PointArrayStatus = ['eigfun2', 'olpar_1', 'olpar_10', 'olpar_11', 'olpar_12', 'olpar_13', 'olpar_14', 'olpar_15', 'olpar_16', 'olpar_2', 'olpar_3', 'olpar_4', 'olpar_5', 'olpar_6', 'olpar_7', 'olpar_8', 'olpar_9', 'overlap', 'par_1', 'par_10', 'par_11', 'par_12', 'par_13', 'par_14', 'par_15', 'par_16', 'par_2', 'par_3', 'par_4', 'par_5', 'par_6', 'par_7', 'par_8', 'par_9', 'par_no_ol_1', 'par_no_ol_10', 'par_no_ol_11', 'par_no_ol_12', 'par_no_ol_13', 'par_no_ol_14', 'par_no_ol_15', 'par_no_ol_16', 'par_no_ol_2', 'par_no_ol_3', 'par_no_ol_4', 'par_no_ol_5', 'par_no_ol_6', 'par_no_ol_7', 'par_no_ol_8', 'par_no_ol_9', 'partition', 'pu_1', 'pu_10', 'pu_11', 'pu_12', 'pu_13', 'pu_14', 'pu_15', 'pu_16', 'pu_2', 'pu_3', 'pu_4', 'pu_5', 'pu_6', 'pu_7', 'pu_8', 'pu_9', 'uext', 'uh2', 'uhdd', 'uhreal2']
resultvtu.TimeArray = 'None'

# create a new 'Extract Component'
extractComponent1 = ExtractComponent(registrationName='ExtractComponent1', Input=resultvtu)
extractComponent1.InputArray = ['POINTS', 'overlap']

# create a new 'Threshold'
threshold2 = Threshold(registrationName='Threshold2', Input=extractComponent1)
threshold2.Scalars = ['POINTS', 'Result']
threshold2.LowerThreshold = 1.0
threshold2.UpperThreshold = 1.0

# create a new 'Threshold'
threshold3 = Threshold(registrationName='Threshold3', Input=resultvtu)
threshold3.Scalars = ['POINTS', 'olpar_13']
threshold3.LowerThreshold = 1.0
threshold3.UpperThreshold = 1.0

# create a new 'Calculator'
calculator1 = Calculator(registrationName='Calculator1', Input=resultvtu)
calculator1.Function = ''

# create a new 'Point Data to Cell Data'
pointDatatoCellData1 = PointDatatoCellData(registrationName='PointDatatoCellData1', Input=calculator1)
pointDatatoCellData1.PointDataArraytoprocess = ['olpar_1', 'olpar_10', 'olpar_11', 'olpar_12', 'olpar_13', 'olpar_14', 'olpar_15', 'olpar_16', 'olpar_2', 'olpar_3', 'olpar_4', 'olpar_5', 'olpar_6', 'olpar_7', 'olpar_8', 'olpar_9', 'overlap', 'par_1', 'par_10', 'par_11', 'par_12', 'par_13', 'par_14', 'par_15', 'par_16', 'par_2', 'par_3', 'par_4', 'par_5', 'par_6', 'par_7', 'par_8', 'par_9', 'par_no_ol_1', 'par_no_ol_10', 'par_no_ol_11', 'par_no_ol_12', 'par_no_ol_13', 'par_no_ol_14', 'par_no_ol_15', 'par_no_ol_16', 'par_no_ol_2', 'par_no_ol_3', 'par_no_ol_4', 'par_no_ol_5', 'par_no_ol_6', 'par_no_ol_7', 'par_no_ol_8', 'par_no_ol_9', 'partition', 'pu_1', 'pu_10', 'pu_11', 'pu_12', 'pu_13', 'pu_14', 'pu_15', 'pu_16', 'pu_2', 'pu_3', 'pu_4', 'pu_5', 'pu_6', 'pu_7', 'pu_8', 'pu_9']

# create a new 'Cell Data to Point Data'
cellDatatoPointData1 = CellDatatoPointData(registrationName='CellDatatoPointData1', Input=pointDatatoCellData1)
cellDatatoPointData1.CellDataArraytoprocess = ['cell', 'olpar_1', 'olpar_10', 'olpar_11', 'olpar_12', 'olpar_13', 'olpar_14', 'olpar_15', 'olpar_16', 'olpar_2', 'olpar_3', 'olpar_4', 'olpar_5', 'olpar_6', 'olpar_7', 'olpar_8', 'olpar_9', 'overlap', 'par_1', 'par_10', 'par_11', 'par_12', 'par_13', 'par_14', 'par_15', 'par_16', 'par_2', 'par_3', 'par_4', 'par_5', 'par_6', 'par_7', 'par_8', 'par_9', 'par_no_ol_1', 'par_no_ol_10', 'par_no_ol_11', 'par_no_ol_12', 'par_no_ol_13', 'par_no_ol_14', 'par_no_ol_15', 'par_no_ol_16', 'par_no_ol_2', 'par_no_ol_3', 'par_no_ol_4', 'par_no_ol_5', 'par_no_ol_6', 'par_no_ol_7', 'par_no_ol_8', 'par_no_ol_9', 'partition', 'pu_1', 'pu_10', 'pu_11', 'pu_12', 'pu_13', 'pu_14', 'pu_15', 'pu_16', 'pu_2', 'pu_3', 'pu_4', 'pu_5', 'pu_6', 'pu_7', 'pu_8', 'pu_9']
cellDatatoPointData1.PassCellData = 1
cellDatatoPointData1.PieceInvariant = 1

# create a new 'Clean to Grid'
cleantoGrid1 = CleantoGrid(registrationName='CleantoGrid1', Input=calculator1)

# create a new 'Contour'
contour5 = Contour(registrationName='Contour5', Input=cleantoGrid1)
contour5.ContourBy = ['POINTS', 'partition']
contour5.Isosurfaces = [1.0, 2.0, 3.0]
contour5.PointMergeMethod = 'Uniform Binning'

# create a new 'Contour'
contour4 = Contour(registrationName='Contour4', Input=calculator1)
contour4.ContourBy = ['POINTS', 'olpar_1']
contour4.Isosurfaces = [0.5]
contour4.PointMergeMethod = 'Uniform Binning'

# create a new 'Threshold'
threshold4 = Threshold(registrationName='Threshold4', Input=calculator1)
threshold4.Scalars = ['POINTS', 'partition']
threshold4.LowerThreshold = 13.0
threshold4.UpperThreshold = 13.0

# create a new 'Contour'
contour1 = Contour(registrationName='Contour1', Input=calculator1)
contour1.ContourBy = ['POINTS', 'par_13']
contour1.GenerateTriangles = 0
contour1.Isosurfaces = [2.0, 3.01, 3.01, 1.0, 1.0]
contour1.PointMergeMethod = 'Uniform Binning'

# create a new 'Clean Cells to Grid'
cleanCellstoGrid1 = CleanCellstoGrid(registrationName='CleanCellstoGrid1', Input=calculator1)

# create a new 'Threshold'
threshold1 = Threshold(registrationName='Threshold1', Input=cleanCellstoGrid1)
threshold1.Scalars = ['POINTS', 'partition']
threshold1.LowerThreshold = 13.0
threshold1.UpperThreshold = 13.0

# create a new 'Clean to Grid'
cleantoGrid2 = CleantoGrid(registrationName='CleantoGrid2', Input=threshold1)
cleantoGrid2.Tolerance = 0.01

# create a new 'Extract Region Surface'
extractRegionSurface1 = ExtractRegionSurface(registrationName='ExtractRegionSurface1', Input=cleantoGrid2)

# create a new 'Feature Edges'
featureEdges1 = FeatureEdges(registrationName='FeatureEdges1', Input=extractRegionSurface1)
featureEdges1.FeatureEdges = 0
featureEdges1.NonManifoldEdges = 0

# create a new 'Contour'
contour2 = Contour(registrationName='Contour2', Input=resultvtu)
contour2.ContourBy = ['POINTS', 'par_13']
contour2.Isosurfaces = [1.0]
contour2.PointMergeMethod = 'Uniform Binning'

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from resultvtu
resultvtuDisplay = Show(resultvtu, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
resultvtuDisplay.Representation = 'Outline'
resultvtuDisplay.AmbientColor = [0.0, 0.0, 0.0]
resultvtuDisplay.ColorArrayName = ['POINTS', '']
resultvtuDisplay.DiffuseColor = [0.0, 0.0, 0.0]
resultvtuDisplay.LineWidth = 2.0
resultvtuDisplay.SelectTCoordArray = 'None'
resultvtuDisplay.SelectNormalArray = 'None'
resultvtuDisplay.SelectTangentArray = 'None'
resultvtuDisplay.OSPRayScaleArray = 'eigfun2'
resultvtuDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
resultvtuDisplay.SelectOrientationVectors = 'None'
resultvtuDisplay.ScaleFactor = 0.2
resultvtuDisplay.SelectScaleArray = 'None'
resultvtuDisplay.GlyphType = 'Arrow'
resultvtuDisplay.GlyphTableIndexArray = 'None'
resultvtuDisplay.GaussianRadius = 0.01
resultvtuDisplay.SetScaleArray = ['POINTS', 'eigfun2']
resultvtuDisplay.ScaleTransferFunction = 'PiecewiseFunction'
resultvtuDisplay.OpacityArray = ['POINTS', 'eigfun2']
resultvtuDisplay.OpacityTransferFunction = 'PiecewiseFunction'
resultvtuDisplay.DataAxesGrid = 'GridAxesRepresentation'
resultvtuDisplay.PolarAxes = 'PolarAxesRepresentation'
resultvtuDisplay.ScalarOpacityUnitDistance = 0.3034811155014586
resultvtuDisplay.OpacityArrayName = ['POINTS', 'eigfun2']
resultvtuDisplay.SelectInputVectors = [None, '']
resultvtuDisplay.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
resultvtuDisplay.ScaleTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 1.4077810658421188, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
resultvtuDisplay.OpacityTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 1.4077810658421188, 1.0, 0.5, 0.0]

# show data from contour2
contour2Display = Show(contour2, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
contour2Display.Representation = 'Surface'
contour2Display.AmbientColor = [0.8509803921568627, 0.3254901960784314, 0.09803921568627451]
contour2Display.ColorArrayName = ['POINTS', '']
contour2Display.DiffuseColor = [0.8509803921568627, 0.3254901960784314, 0.09803921568627451]
contour2Display.LineWidth = 5.0
contour2Display.SelectTCoordArray = 'None'
contour2Display.SelectNormalArray = 'None'
contour2Display.SelectTangentArray = 'None'
contour2Display.OSPRayScaleArray = 'eigfun2'
contour2Display.OSPRayScaleFunction = 'PiecewiseFunction'
contour2Display.SelectOrientationVectors = 'None'
contour2Display.ScaleFactor = 0.15509202381530054
contour2Display.SelectScaleArray = 'eigfun2'
contour2Display.GlyphType = 'Arrow'
contour2Display.GlyphTableIndexArray = 'eigfun2'
contour2Display.GaussianRadius = 0.007754601190765027
contour2Display.SetScaleArray = ['POINTS', 'eigfun2']
contour2Display.ScaleTransferFunction = 'PiecewiseFunction'
contour2Display.OpacityArray = ['POINTS', 'eigfun2']
contour2Display.OpacityTransferFunction = 'PiecewiseFunction'
contour2Display.DataAxesGrid = 'GridAxesRepresentation'
contour2Display.PolarAxes = 'PolarAxesRepresentation'
contour2Display.SelectInputVectors = [None, '']
contour2Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
contour2Display.ScaleTransferFunction.Points = [0.6976387592955794, 0.0, 0.5, 0.0, 0.697760820388794, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
contour2Display.OpacityTransferFunction.Points = [0.6976387592955794, 0.0, 0.5, 0.0, 0.697760820388794, 1.0, 0.5, 0.0]

# show data from threshold3
threshold3Display = Show(threshold3, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
threshold3Display.Representation = 'Surface With Edges'
threshold3Display.AmbientColor = [0.0, 0.0, 0.0]
threshold3Display.ColorArrayName = [None, '']
threshold3Display.DiffuseColor = [0.0, 0.0, 0.0]
threshold3Display.Opacity = 0.25
threshold3Display.SelectTCoordArray = 'None'
threshold3Display.SelectNormalArray = 'None'
threshold3Display.SelectTangentArray = 'None'
threshold3Display.EdgeColor = [0.7499961852445258, 0.7499961852445258, 0.7499961852445258]
threshold3Display.OSPRayScaleArray = 'eigfun2'
threshold3Display.OSPRayScaleFunction = 'PiecewiseFunction'
threshold3Display.SelectOrientationVectors = 'None'
threshold3Display.ScaleFactor = 0.2
threshold3Display.SelectScaleArray = 'None'
threshold3Display.GlyphType = 'Arrow'
threshold3Display.GlyphTableIndexArray = 'None'
threshold3Display.GaussianRadius = 0.01
threshold3Display.SetScaleArray = ['POINTS', 'eigfun2']
threshold3Display.ScaleTransferFunction = 'PiecewiseFunction'
threshold3Display.OpacityArray = ['POINTS', 'eigfun2']
threshold3Display.OpacityTransferFunction = 'PiecewiseFunction'
threshold3Display.DataAxesGrid = 'GridAxesRepresentation'
threshold3Display.PolarAxes = 'PolarAxesRepresentation'
threshold3Display.ScalarOpacityUnitDistance = 0.1911811228329325
threshold3Display.OpacityArrayName = ['POINTS', 'eigfun2']
threshold3Display.SelectInputVectors = [None, '']
threshold3Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
threshold3Display.ScaleTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 1.3952775185911588, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
threshold3Display.OpacityTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 1.3952775185911588, 1.0, 0.5, 0.0]

# show data from extractComponent1
extractComponent1Display = Show(extractComponent1, renderView1, 'UnstructuredGridRepresentation')

# get 2D transfer function for 'overlap'
overlapTF2D = GetTransferFunction2D('overlap')

# get color transfer function/color map for 'overlap'
overlapLUT = GetColorTransferFunction('overlap')
overlapLUT.TransferFunction2D = overlapTF2D
overlapLUT.RGBPoints = [0.0, 0.231373, 0.298039, 0.752941, 0.5001220703125, 0.865003, 0.865003, 0.865003, 1.000244140625, 0.705882, 0.0156863, 0.14902]
overlapLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'overlap'
overlapPWF = GetOpacityTransferFunction('overlap')
overlapPWF.Points = [0.0, 0.0, 0.5, 0.0, 1.000244140625, 1.0, 0.5, 0.0]
overlapPWF.ScalarRangeInitialized = 1

# trace defaults for the display properties.
extractComponent1Display.Representation = 'Surface'
extractComponent1Display.AmbientColor = [0.0, 0.0, 0.0]
extractComponent1Display.ColorArrayName = ['POINTS', 'overlap']
extractComponent1Display.DiffuseColor = [0.0, 0.0, 0.0]
extractComponent1Display.LookupTable = overlapLUT
extractComponent1Display.SelectTCoordArray = 'None'
extractComponent1Display.SelectNormalArray = 'None'
extractComponent1Display.SelectTangentArray = 'None'
extractComponent1Display.OSPRayScaleArray = 'Result'
extractComponent1Display.OSPRayScaleFunction = 'PiecewiseFunction'
extractComponent1Display.SelectOrientationVectors = 'None'
extractComponent1Display.ScaleFactor = 0.2
extractComponent1Display.SelectScaleArray = 'None'
extractComponent1Display.GlyphType = 'Arrow'
extractComponent1Display.GlyphTableIndexArray = 'None'
extractComponent1Display.GaussianRadius = 0.01
extractComponent1Display.SetScaleArray = ['POINTS', 'Result']
extractComponent1Display.ScaleTransferFunction = 'PiecewiseFunction'
extractComponent1Display.OpacityArray = ['POINTS', 'Result']
extractComponent1Display.OpacityTransferFunction = 'PiecewiseFunction'
extractComponent1Display.DataAxesGrid = 'GridAxesRepresentation'
extractComponent1Display.PolarAxes = 'PolarAxesRepresentation'
extractComponent1Display.ScalarOpacityFunction = overlapPWF
extractComponent1Display.ScalarOpacityUnitDistance = 0.1911811228329325
extractComponent1Display.OpacityArrayName = ['POINTS', 'Result']
extractComponent1Display.SelectInputVectors = [None, '']
extractComponent1Display.WriteLog = ''

# show data from threshold2
threshold2Display = Show(threshold2, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
threshold2Display.Representation = 'Surface With Edges'
threshold2Display.AmbientColor = [0.0, 0.0, 0.0]
threshold2Display.ColorArrayName = ['POINTS', '']
threshold2Display.DiffuseColor = [0.0, 0.0, 0.0]
threshold2Display.Opacity = 0.2
threshold2Display.SelectTCoordArray = 'None'
threshold2Display.SelectNormalArray = 'None'
threshold2Display.SelectTangentArray = 'None'
threshold2Display.OSPRayScaleArray = 'Result'
threshold2Display.OSPRayScaleFunction = 'PiecewiseFunction'
threshold2Display.SelectOrientationVectors = 'None'
threshold2Display.ScaleFactor = 0.2
threshold2Display.SelectScaleArray = 'None'
threshold2Display.GlyphType = 'Arrow'
threshold2Display.GlyphTableIndexArray = 'None'
threshold2Display.GaussianRadius = 0.01
threshold2Display.SetScaleArray = ['POINTS', 'Result']
threshold2Display.ScaleTransferFunction = 'PiecewiseFunction'
threshold2Display.OpacityArray = ['POINTS', 'Result']
threshold2Display.OpacityTransferFunction = 'PiecewiseFunction'
threshold2Display.DataAxesGrid = 'GridAxesRepresentation'
threshold2Display.PolarAxes = 'PolarAxesRepresentation'
threshold2Display.ScalarOpacityUnitDistance = 0.2556679541537132
threshold2Display.OpacityArrayName = ['POINTS', 'Result']
threshold2Display.SelectInputVectors = [None, '']
threshold2Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
threshold2Display.ScaleTransferFunction.Points = [1.0, 0.0, 0.5, 0.0, 1.000244140625, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
threshold2Display.OpacityTransferFunction.Points = [1.0, 0.0, 0.5, 0.0, 1.000244140625, 1.0, 0.5, 0.0]

# show data from calculator1
calculator1Display = Show(calculator1, renderView1, 'UnstructuredGridRepresentation')

# get 2D transfer function for 'partition'
partitionTF2D = GetTransferFunction2D('partition')

# get color transfer function/color map for 'partition'
partitionLUT = GetColorTransferFunction('partition')
partitionLUT.TransferFunction2D = partitionTF2D
partitionLUT.RGBPoints = [1.0, 0.2081, 0.1663, 0.5292, 1.238095, 0.21162, 0.18978, 0.57768, 1.4761900000000001, 0.21225, 0.21377, 0.62697, 1.714285, 0.2081, 0.2386, 0.67709, 1.9523800000000002, 0.1959, 0.26446, 0.7279, 2.190475, 0.17073, 0.29194, 0.77925, 2.42857, 0.12527, 0.32424, 0.83027, 2.6666499999999997, 0.059133, 0.35983, 0.86833, 2.9047, 0.011695, 0.38751, 0.88196, 3.1429, 0.0059571, 0.40861, 0.88284, 3.3809500000000003, 0.016514, 0.4266, 0.87863, 3.619, 0.032852, 0.44304, 0.87196, 3.8572, 0.049814, 0.45857, 0.86406, 4.09525, 0.062933, 0.47369, 0.85544, 4.3332999999999995, 0.072267, 0.48867, 0.8467, 4.5715, 0.077943, 0.50399, 0.83837, 4.80955, 0.079348, 0.52002, 0.83118, 5.0476, 0.074943, 0.53754, 0.82627, 5.28565, 0.064057, 0.55699, 0.82396, 5.52385, 0.048771, 0.57722, 0.82283, 5.761900000000001, 0.034343, 0.59658, 0.81985, 5.99995, 0.0265, 0.6137, 0.8135, 6.23815, 0.02389, 0.62866, 0.80376, 6.4762, 0.02309, 0.64179, 0.79127, 6.71425, 0.022771, 0.65349, 0.77676, 6.952450000000001, 0.026662, 0.6642, 0.76072, 7.1905, 0.038371, 0.67427, 0.74355, 7.42855, 0.058971, 0.68376, 0.72539, 7.6666, 0.0843, 0.69283, 0.70617, 7.9048, 0.1133, 0.7015, 0.68586, 8.14285, 0.14527, 0.70976, 0.66463, 8.3809, 0.18013, 0.71766, 0.64243, 8.6191, 0.21783, 0.72504, 0.61926, 8.85715, 0.25864, 0.73171, 0.59543, 9.0952, 0.30217, 0.7376, 0.57119, 9.333400000000001, 0.34817, 0.74243, 0.54727, 9.57145, 0.39526, 0.7459, 0.52444, 9.8095, 0.44201, 0.74808, 0.50331, 10.04755, 0.48712, 0.74906, 0.48398, 10.28575, 0.53003, 0.74911, 0.46611, 10.523800000000001, 0.57086, 0.74852, 0.44939, 10.761849999999999, 0.60985, 0.74731, 0.43369, 11.00005, 0.6473, 0.7456, 0.4188, 11.238100000000001, 0.68342, 0.74348, 0.40443, 11.47615, 0.71841, 0.74113, 0.39048, 11.71435, 0.75249, 0.7384, 0.37681, 11.9524, 0.78584, 0.73557, 0.36327, 12.19045, 0.8185, 0.73273, 0.34979, 12.4285, 0.85066, 0.7299, 0.33603, 12.6667, 0.88243, 0.72743, 0.3217, 12.90475, 0.91393, 0.72579, 0.30628, 13.142800000000001, 0.94496, 0.72611, 0.28864, 13.381, 0.9739, 0.7314, 0.26665, 13.61905, 0.99377, 0.74546, 0.24035, 13.8571, 0.99904, 0.76531, 0.21641, 14.0953, 0.99553, 0.78606, 0.19665, 14.33335, 0.988, 0.8066, 0.17937, 14.5714, 0.97886, 0.82714, 0.16331, 14.80945, 0.9697, 0.84814, 0.14745, 15.047649999999999, 0.96259, 0.87051, 0.1309, 15.2857, 0.95887, 0.8949, 0.11324, 15.523750000000001, 0.95982, 0.92183, 0.094838, 15.761949999999999, 0.9661, 0.95144, 0.075533, 16.0, 0.9763, 0.9831, 0.0538]
partitionLUT.ColorSpace = 'RGB'
partitionLUT.NanColor = [1.0, 0.0, 0.0]
partitionLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'partition'
partitionPWF = GetOpacityTransferFunction('partition')
partitionPWF.Points = [1.0, 0.0, 0.5, 0.0, 16.0, 1.0, 0.5, 0.0]
partitionPWF.ScalarRangeInitialized = 1

# trace defaults for the display properties.
calculator1Display.Representation = 'Surface With Edges'
calculator1Display.ColorArrayName = ['POINTS', 'partition']
calculator1Display.LookupTable = partitionLUT
calculator1Display.SelectTCoordArray = 'None'
calculator1Display.SelectNormalArray = 'None'
calculator1Display.SelectTangentArray = 'None'
calculator1Display.EdgeColor = [0.2500038147554742, 0.2500038147554742, 0.2500038147554742]
calculator1Display.OSPRayScaleArray = 'olpar_1'
calculator1Display.OSPRayScaleFunction = 'PiecewiseFunction'
calculator1Display.SelectOrientationVectors = 'None'
calculator1Display.ScaleFactor = 0.2
calculator1Display.SelectScaleArray = 'None'
calculator1Display.GlyphType = 'Arrow'
calculator1Display.GlyphTableIndexArray = 'None'
calculator1Display.GaussianRadius = 0.01
calculator1Display.SetScaleArray = ['POINTS', 'olpar_1']
calculator1Display.ScaleTransferFunction = 'PiecewiseFunction'
calculator1Display.OpacityArray = ['POINTS', 'olpar_1']
calculator1Display.OpacityTransferFunction = 'PiecewiseFunction'
calculator1Display.DataAxesGrid = 'GridAxesRepresentation'
calculator1Display.PolarAxes = 'PolarAxesRepresentation'
calculator1Display.ScalarOpacityFunction = partitionPWF
calculator1Display.ScalarOpacityUnitDistance = 0.24087312099974903
calculator1Display.OpacityArrayName = ['POINTS', 'olpar_1']
calculator1Display.SelectInputVectors = [None, '']
calculator1Display.WriteLog = ''

# show data from threshold4
threshold4Display = Show(threshold4, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
threshold4Display.Representation = 'Surface With Edges'
threshold4Display.AmbientColor = [0.8509803921568627, 0.3254901960784314, 0.09803921568627451]
threshold4Display.ColorArrayName = [None, '']
threshold4Display.DiffuseColor = [0.8509803921568627, 0.3254901960784314, 0.09803921568627451]
threshold4Display.SelectTCoordArray = 'None'
threshold4Display.SelectNormalArray = 'None'
threshold4Display.SelectTangentArray = 'None'
threshold4Display.OSPRayScaleFunction = 'PiecewiseFunction'
threshold4Display.SelectOrientationVectors = 'None'
threshold4Display.ScaleFactor = -2.0000000000000002e+298
threshold4Display.SelectScaleArray = 'None'
threshold4Display.GlyphType = 'Arrow'
threshold4Display.GlyphTableIndexArray = 'None'
threshold4Display.GaussianRadius = -1e+297
threshold4Display.SetScaleArray = [None, '']
threshold4Display.ScaleTransferFunction = 'PiecewiseFunction'
threshold4Display.OpacityArray = [None, '']
threshold4Display.OpacityTransferFunction = 'PiecewiseFunction'
threshold4Display.DataAxesGrid = 'GridAxesRepresentation'
threshold4Display.PolarAxes = 'PolarAxesRepresentation'
threshold4Display.OpacityArrayName = [None, '']
threshold4Display.SelectInputVectors = [None, '']
threshold4Display.WriteLog = ''

# show data from contour1
contour1Display = Show(contour1, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
contour1Display.Representation = 'Surface'
contour1Display.AmbientColor = [0.0, 0.0, 0.0]
contour1Display.ColorArrayName = ['POINTS', '']
contour1Display.DiffuseColor = [0.0, 0.0, 0.0]
contour1Display.LineWidth = 5.0
contour1Display.SelectTCoordArray = 'None'
contour1Display.SelectNormalArray = 'None'
contour1Display.SelectTangentArray = 'None'
contour1Display.OSPRayScaleArray = 'par_no_ol_15'
contour1Display.OSPRayScaleFunction = 'PiecewiseFunction'
contour1Display.SelectOrientationVectors = 'None'
contour1Display.ScaleFactor = 0.045
contour1Display.SelectScaleArray = 'par_no_ol_15'
contour1Display.GlyphType = 'Arrow'
contour1Display.GlyphTableIndexArray = 'par_no_ol_15'
contour1Display.GaussianRadius = 0.00225
contour1Display.SetScaleArray = ['POINTS', 'par_no_ol_15']
contour1Display.ScaleTransferFunction = 'PiecewiseFunction'
contour1Display.OpacityArray = ['POINTS', 'par_no_ol_15']
contour1Display.OpacityTransferFunction = 'PiecewiseFunction'
contour1Display.DataAxesGrid = 'GridAxesRepresentation'
contour1Display.PolarAxes = 'PolarAxesRepresentation'
contour1Display.SelectInputVectors = [None, '']
contour1Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
contour1Display.ScaleTransferFunction.Points = [1.0, 0.0, 0.5, 0.0, 1.000244140625, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
contour1Display.OpacityTransferFunction.Points = [1.0, 0.0, 0.5, 0.0, 1.000244140625, 1.0, 0.5, 0.0]

# show data from pointDatatoCellData1
pointDatatoCellData1Display = Show(pointDatatoCellData1, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
pointDatatoCellData1Display.Representation = 'Surface With Edges'
pointDatatoCellData1Display.ColorArrayName = ['CELLS', 'partition']
pointDatatoCellData1Display.LookupTable = partitionLUT
pointDatatoCellData1Display.SelectTCoordArray = 'None'
pointDatatoCellData1Display.SelectNormalArray = 'None'
pointDatatoCellData1Display.SelectTangentArray = 'None'
pointDatatoCellData1Display.OSPRayScaleFunction = 'PiecewiseFunction'
pointDatatoCellData1Display.SelectOrientationVectors = 'None'
pointDatatoCellData1Display.ScaleFactor = 0.2
pointDatatoCellData1Display.SelectScaleArray = 'None'
pointDatatoCellData1Display.GlyphType = 'Arrow'
pointDatatoCellData1Display.GlyphTableIndexArray = 'None'
pointDatatoCellData1Display.GaussianRadius = 0.01
pointDatatoCellData1Display.SetScaleArray = [None, '']
pointDatatoCellData1Display.ScaleTransferFunction = 'PiecewiseFunction'
pointDatatoCellData1Display.OpacityArray = [None, '']
pointDatatoCellData1Display.OpacityTransferFunction = 'PiecewiseFunction'
pointDatatoCellData1Display.DataAxesGrid = 'GridAxesRepresentation'
pointDatatoCellData1Display.PolarAxes = 'PolarAxesRepresentation'
pointDatatoCellData1Display.ScalarOpacityFunction = partitionPWF
pointDatatoCellData1Display.ScalarOpacityUnitDistance = 0.1911811228329325
pointDatatoCellData1Display.OpacityArrayName = ['CELLS', 'cell']
pointDatatoCellData1Display.SelectInputVectors = [None, '']
pointDatatoCellData1Display.WriteLog = ''

# show data from contour4
contour4Display = Show(contour4, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
contour4Display.Representation = 'Surface'
contour4Display.ColorArrayName = [None, '']
contour4Display.SelectTCoordArray = 'None'
contour4Display.SelectNormalArray = 'None'
contour4Display.SelectTangentArray = 'None'
contour4Display.OSPRayScaleFunction = 'PiecewiseFunction'
contour4Display.SelectOrientationVectors = 'None'
contour4Display.ScaleFactor = -0.2
contour4Display.SelectScaleArray = 'None'
contour4Display.GlyphType = 'Arrow'
contour4Display.GlyphTableIndexArray = 'None'
contour4Display.GaussianRadius = -0.01
contour4Display.SetScaleArray = [None, '']
contour4Display.ScaleTransferFunction = 'PiecewiseFunction'
contour4Display.OpacityArray = [None, '']
contour4Display.OpacityTransferFunction = 'PiecewiseFunction'
contour4Display.DataAxesGrid = 'GridAxesRepresentation'
contour4Display.PolarAxes = 'PolarAxesRepresentation'
contour4Display.SelectInputVectors = [None, '']
contour4Display.WriteLog = ''

# show data from cellDatatoPointData1
cellDatatoPointData1Display = Show(cellDatatoPointData1, renderView1, 'UnstructuredGridRepresentation')

# get 2D transfer function for 'par_no_ol_1'
par_no_ol_1TF2D = GetTransferFunction2D('par_no_ol_1')

# get color transfer function/color map for 'par_no_ol_1'
par_no_ol_1LUT = GetColorTransferFunction('par_no_ol_1')
par_no_ol_1LUT.TransferFunction2D = par_no_ol_1TF2D
par_no_ol_1LUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'par_no_ol_1'
par_no_ol_1PWF = GetOpacityTransferFunction('par_no_ol_1')
par_no_ol_1PWF.ScalarRangeInitialized = 1

# trace defaults for the display properties.
cellDatatoPointData1Display.Representation = 'Surface'
cellDatatoPointData1Display.ColorArrayName = ['POINTS', 'par_no_ol_1']
cellDatatoPointData1Display.LookupTable = par_no_ol_1LUT
cellDatatoPointData1Display.SelectTCoordArray = 'None'
cellDatatoPointData1Display.SelectNormalArray = 'None'
cellDatatoPointData1Display.SelectTangentArray = 'None'
cellDatatoPointData1Display.OSPRayScaleArray = 'cell'
cellDatatoPointData1Display.OSPRayScaleFunction = 'PiecewiseFunction'
cellDatatoPointData1Display.SelectOrientationVectors = 'None'
cellDatatoPointData1Display.ScaleFactor = 0.2
cellDatatoPointData1Display.SelectScaleArray = 'None'
cellDatatoPointData1Display.GlyphType = 'Arrow'
cellDatatoPointData1Display.GlyphTableIndexArray = 'None'
cellDatatoPointData1Display.GaussianRadius = 0.01
cellDatatoPointData1Display.SetScaleArray = ['POINTS', 'cell']
cellDatatoPointData1Display.ScaleTransferFunction = 'PiecewiseFunction'
cellDatatoPointData1Display.OpacityArray = ['POINTS', 'cell']
cellDatatoPointData1Display.OpacityTransferFunction = 'PiecewiseFunction'
cellDatatoPointData1Display.DataAxesGrid = 'GridAxesRepresentation'
cellDatatoPointData1Display.PolarAxes = 'PolarAxesRepresentation'
cellDatatoPointData1Display.ScalarOpacityFunction = par_no_ol_1PWF
cellDatatoPointData1Display.ScalarOpacityUnitDistance = 0.1911811228329325
cellDatatoPointData1Display.OpacityArrayName = ['POINTS', 'cell']
cellDatatoPointData1Display.SelectInputVectors = [None, '']
cellDatatoPointData1Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
cellDatatoPointData1Display.ScaleTransferFunction.Points = [1.0, 0.0, 0.5, 0.0, 1600.0, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
cellDatatoPointData1Display.OpacityTransferFunction.Points = [1.0, 0.0, 0.5, 0.0, 1600.0, 1.0, 0.5, 0.0]

# show data from cleantoGrid1
cleantoGrid1Display = Show(cleantoGrid1, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
cleantoGrid1Display.Representation = 'Surface With Edges'
cleantoGrid1Display.ColorArrayName = ['POINTS', 'partition']
cleantoGrid1Display.LookupTable = partitionLUT
cleantoGrid1Display.SelectTCoordArray = 'None'
cleantoGrid1Display.SelectNormalArray = 'None'
cleantoGrid1Display.SelectTangentArray = 'None'
cleantoGrid1Display.OSPRayScaleArray = 'olpar_1'
cleantoGrid1Display.OSPRayScaleFunction = 'PiecewiseFunction'
cleantoGrid1Display.SelectOrientationVectors = 'None'
cleantoGrid1Display.ScaleFactor = 0.2
cleantoGrid1Display.SelectScaleArray = 'None'
cleantoGrid1Display.GlyphType = 'Arrow'
cleantoGrid1Display.GlyphTableIndexArray = 'None'
cleantoGrid1Display.GaussianRadius = 0.01
cleantoGrid1Display.SetScaleArray = ['POINTS', 'olpar_1']
cleantoGrid1Display.ScaleTransferFunction = 'PiecewiseFunction'
cleantoGrid1Display.OpacityArray = ['POINTS', 'olpar_1']
cleantoGrid1Display.OpacityTransferFunction = 'PiecewiseFunction'
cleantoGrid1Display.DataAxesGrid = 'GridAxesRepresentation'
cleantoGrid1Display.PolarAxes = 'PolarAxesRepresentation'
cleantoGrid1Display.ScalarOpacityFunction = partitionPWF
cleantoGrid1Display.ScalarOpacityUnitDistance = 0.1911811228329325
cleantoGrid1Display.OpacityArrayName = ['POINTS', 'olpar_1']
cleantoGrid1Display.SelectInputVectors = [None, '']
cleantoGrid1Display.WriteLog = ''

# show data from contour5
contour5Display = Show(contour5, renderView1, 'GeometryRepresentation')

# get 2D transfer function for 'olpar_1'
olpar_1TF2D = GetTransferFunction2D('olpar_1')

# get color transfer function/color map for 'olpar_1'
olpar_1LUT = GetColorTransferFunction('olpar_1')
olpar_1LUT.TransferFunction2D = olpar_1TF2D
olpar_1LUT.RGBPoints = [0.0, 0.231373, 0.298039, 0.752941, 0.5001220703125, 0.865003, 0.865003, 0.865003, 1.000244140625, 0.705882, 0.0156863, 0.14902]
olpar_1LUT.ScalarRangeInitialized = 1.0

# trace defaults for the display properties.
contour5Display.Representation = 'Surface'
contour5Display.ColorArrayName = ['POINTS', 'olpar_1']
contour5Display.LookupTable = olpar_1LUT
contour5Display.SelectTCoordArray = 'None'
contour5Display.SelectNormalArray = 'None'
contour5Display.SelectTangentArray = 'None'
contour5Display.OSPRayScaleArray = 'olpar_1'
contour5Display.OSPRayScaleFunction = 'PiecewiseFunction'
contour5Display.SelectOrientationVectors = 'None'
contour5Display.ScaleFactor = 0.045
contour5Display.SelectScaleArray = 'olpar_1'
contour5Display.GlyphType = 'Arrow'
contour5Display.GlyphTableIndexArray = 'olpar_1'
contour5Display.GaussianRadius = 0.00225
contour5Display.SetScaleArray = ['POINTS', 'olpar_1']
contour5Display.ScaleTransferFunction = 'PiecewiseFunction'
contour5Display.OpacityArray = ['POINTS', 'olpar_1']
contour5Display.OpacityTransferFunction = 'PiecewiseFunction'
contour5Display.DataAxesGrid = 'GridAxesRepresentation'
contour5Display.PolarAxes = 'PolarAxesRepresentation'
contour5Display.SelectInputVectors = [None, '']
contour5Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
contour5Display.ScaleTransferFunction.Points = [1.0, 0.0, 0.5, 0.0, 1.000244140625, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
contour5Display.OpacityTransferFunction.Points = [1.0, 0.0, 0.5, 0.0, 1.000244140625, 1.0, 0.5, 0.0]

# show data from cleanCellstoGrid1
cleanCellstoGrid1Display = Show(cleanCellstoGrid1, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
cleanCellstoGrid1Display.Representation = 'Surface'
cleanCellstoGrid1Display.ColorArrayName = ['POINTS', 'partition']
cleanCellstoGrid1Display.LookupTable = partitionLUT
cleanCellstoGrid1Display.SelectTCoordArray = 'None'
cleanCellstoGrid1Display.SelectNormalArray = 'None'
cleanCellstoGrid1Display.SelectTangentArray = 'None'
cleanCellstoGrid1Display.OSPRayScaleArray = 'olpar_1'
cleanCellstoGrid1Display.OSPRayScaleFunction = 'PiecewiseFunction'
cleanCellstoGrid1Display.SelectOrientationVectors = 'None'
cleanCellstoGrid1Display.ScaleFactor = 0.2
cleanCellstoGrid1Display.SelectScaleArray = 'None'
cleanCellstoGrid1Display.GlyphType = 'Arrow'
cleanCellstoGrid1Display.GlyphTableIndexArray = 'None'
cleanCellstoGrid1Display.GaussianRadius = 0.01
cleanCellstoGrid1Display.SetScaleArray = ['POINTS', 'olpar_1']
cleanCellstoGrid1Display.ScaleTransferFunction = 'PiecewiseFunction'
cleanCellstoGrid1Display.OpacityArray = ['POINTS', 'olpar_1']
cleanCellstoGrid1Display.OpacityTransferFunction = 'PiecewiseFunction'
cleanCellstoGrid1Display.DataAxesGrid = 'GridAxesRepresentation'
cleanCellstoGrid1Display.PolarAxes = 'PolarAxesRepresentation'
cleanCellstoGrid1Display.ScalarOpacityFunction = partitionPWF
cleanCellstoGrid1Display.ScalarOpacityUnitDistance = 0.24087312099974903
cleanCellstoGrid1Display.OpacityArrayName = ['POINTS', 'olpar_1']
cleanCellstoGrid1Display.SelectInputVectors = [None, '']
cleanCellstoGrid1Display.WriteLog = ''

# show data from threshold1
threshold1Display = Show(threshold1, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
threshold1Display.Representation = 'Surface'
threshold1Display.ColorArrayName = ['POINTS', 'partition']
threshold1Display.LookupTable = partitionLUT
threshold1Display.SelectTCoordArray = 'None'
threshold1Display.SelectNormalArray = 'None'
threshold1Display.SelectTangentArray = 'None'
threshold1Display.OSPRayScaleArray = 'olpar_1'
threshold1Display.OSPRayScaleFunction = 'PiecewiseFunction'
threshold1Display.SelectOrientationVectors = 'None'
threshold1Display.ScaleFactor = 0.05
threshold1Display.SelectScaleArray = 'None'
threshold1Display.GlyphType = 'Arrow'
threshold1Display.GlyphTableIndexArray = 'None'
threshold1Display.GaussianRadius = 0.0025
threshold1Display.SetScaleArray = ['POINTS', 'olpar_1']
threshold1Display.ScaleTransferFunction = 'PiecewiseFunction'
threshold1Display.OpacityArray = ['POINTS', 'olpar_1']
threshold1Display.OpacityTransferFunction = 'PiecewiseFunction'
threshold1Display.DataAxesGrid = 'GridAxesRepresentation'
threshold1Display.PolarAxes = 'PolarAxesRepresentation'
threshold1Display.ScalarOpacityFunction = partitionPWF
threshold1Display.ScalarOpacityUnitDistance = 0.16457849245112025
threshold1Display.OpacityArrayName = ['POINTS', 'olpar_1']
threshold1Display.SelectInputVectors = [None, '']
threshold1Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
threshold1Display.ScaleTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 1.1757813367477812e-38, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
threshold1Display.OpacityTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 1.1757813367477812e-38, 1.0, 0.5, 0.0]

# show data from cleantoGrid2
cleantoGrid2Display = Show(cleantoGrid2, renderView1, 'UnstructuredGridRepresentation')

# trace defaults for the display properties.
cleantoGrid2Display.Representation = 'Surface'
cleantoGrid2Display.ColorArrayName = ['POINTS', 'partition']
cleantoGrid2Display.LookupTable = partitionLUT
cleantoGrid2Display.SelectTCoordArray = 'None'
cleantoGrid2Display.SelectNormalArray = 'None'
cleantoGrid2Display.SelectTangentArray = 'None'
cleantoGrid2Display.OSPRayScaleArray = 'olpar_1'
cleantoGrid2Display.OSPRayScaleFunction = 'PiecewiseFunction'
cleantoGrid2Display.SelectOrientationVectors = 'None'
cleantoGrid2Display.ScaleFactor = 0.05
cleantoGrid2Display.SelectScaleArray = 'None'
cleantoGrid2Display.GlyphType = 'Arrow'
cleantoGrid2Display.GlyphTableIndexArray = 'None'
cleantoGrid2Display.GaussianRadius = 0.0025
cleantoGrid2Display.SetScaleArray = ['POINTS', 'olpar_1']
cleantoGrid2Display.ScaleTransferFunction = 'PiecewiseFunction'
cleantoGrid2Display.OpacityArray = ['POINTS', 'olpar_1']
cleantoGrid2Display.OpacityTransferFunction = 'PiecewiseFunction'
cleantoGrid2Display.DataAxesGrid = 'GridAxesRepresentation'
cleantoGrid2Display.PolarAxes = 'PolarAxesRepresentation'
cleantoGrid2Display.ScalarOpacityFunction = partitionPWF
cleantoGrid2Display.ScalarOpacityUnitDistance = 0.16457849245112025
cleantoGrid2Display.OpacityArrayName = ['POINTS', 'olpar_1']
cleantoGrid2Display.SelectInputVectors = [None, '']
cleantoGrid2Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
cleantoGrid2Display.ScaleTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 1.1757813367477812e-38, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
cleantoGrid2Display.OpacityTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 1.1757813367477812e-38, 1.0, 0.5, 0.0]

# show data from extractRegionSurface1
extractRegionSurface1Display = Show(extractRegionSurface1, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
extractRegionSurface1Display.Representation = 'Surface'
extractRegionSurface1Display.ColorArrayName = ['POINTS', 'partition']
extractRegionSurface1Display.LookupTable = partitionLUT
extractRegionSurface1Display.SelectTCoordArray = 'None'
extractRegionSurface1Display.SelectNormalArray = 'None'
extractRegionSurface1Display.SelectTangentArray = 'None'
extractRegionSurface1Display.OSPRayScaleArray = 'olpar_1'
extractRegionSurface1Display.OSPRayScaleFunction = 'PiecewiseFunction'
extractRegionSurface1Display.SelectOrientationVectors = 'None'
extractRegionSurface1Display.ScaleFactor = 0.05
extractRegionSurface1Display.SelectScaleArray = 'None'
extractRegionSurface1Display.GlyphType = 'Arrow'
extractRegionSurface1Display.GlyphTableIndexArray = 'None'
extractRegionSurface1Display.GaussianRadius = 0.0025
extractRegionSurface1Display.SetScaleArray = ['POINTS', 'olpar_1']
extractRegionSurface1Display.ScaleTransferFunction = 'PiecewiseFunction'
extractRegionSurface1Display.OpacityArray = ['POINTS', 'olpar_1']
extractRegionSurface1Display.OpacityTransferFunction = 'PiecewiseFunction'
extractRegionSurface1Display.DataAxesGrid = 'GridAxesRepresentation'
extractRegionSurface1Display.PolarAxes = 'PolarAxesRepresentation'
extractRegionSurface1Display.SelectInputVectors = [None, '']
extractRegionSurface1Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
extractRegionSurface1Display.ScaleTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 1.1757813367477812e-38, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
extractRegionSurface1Display.OpacityTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 1.1757813367477812e-38, 1.0, 0.5, 0.0]

# show data from featureEdges1
featureEdges1Display = Show(featureEdges1, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
featureEdges1Display.Representation = 'Surface'
featureEdges1Display.AmbientColor = [0.0, 0.0, 0.0]
featureEdges1Display.ColorArrayName = ['POINTS', '']
featureEdges1Display.DiffuseColor = [0.0, 0.0, 0.0]
featureEdges1Display.LineWidth = 5.0
featureEdges1Display.SelectTCoordArray = 'None'
featureEdges1Display.SelectNormalArray = 'None'
featureEdges1Display.SelectTangentArray = 'None'
featureEdges1Display.OSPRayScaleArray = 'olpar_1'
featureEdges1Display.OSPRayScaleFunction = 'PiecewiseFunction'
featureEdges1Display.SelectOrientationVectors = 'None'
featureEdges1Display.ScaleFactor = 0.05
featureEdges1Display.SelectScaleArray = 'None'
featureEdges1Display.GlyphType = 'Arrow'
featureEdges1Display.GlyphTableIndexArray = 'None'
featureEdges1Display.GaussianRadius = 0.0025
featureEdges1Display.SetScaleArray = ['POINTS', 'olpar_1']
featureEdges1Display.ScaleTransferFunction = 'PiecewiseFunction'
featureEdges1Display.OpacityArray = ['POINTS', 'olpar_1']
featureEdges1Display.OpacityTransferFunction = 'PiecewiseFunction'
featureEdges1Display.DataAxesGrid = 'GridAxesRepresentation'
featureEdges1Display.PolarAxes = 'PolarAxesRepresentation'
featureEdges1Display.SelectInputVectors = [None, '']
featureEdges1Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
featureEdges1Display.ScaleTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 1.1757813367477812e-38, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
featureEdges1Display.OpacityTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 1.1757813367477812e-38, 1.0, 0.5, 0.0]

# setup the color legend parameters for each legend in this view

# get color legend/bar for partitionLUT in view renderView1
partitionLUTColorBar = GetScalarBar(partitionLUT, renderView1)
partitionLUTColorBar.WindowLocation = 'Upper Left Corner'
partitionLUTColorBar.Position = [0.9261625989445911, 0.6613310867733783]
partitionLUTColorBar.Title = 'partition'
partitionLUTColorBar.ComponentTitle = ''

# set color bar visibility
partitionLUTColorBar.Visibility = 1

# get 2D transfer function for 'cell'
cellTF2D = GetTransferFunction2D('cell')

# get color transfer function/color map for 'cell'
cellLUT = GetColorTransferFunction('cell')
cellLUT.TransferFunction2D = cellTF2D
cellLUT.RGBPoints = [1.0, 0.231373, 0.298039, 0.752941, 800.5, 0.865003, 0.865003, 0.865003, 1600.0, 0.705882, 0.0156863, 0.14902]
cellLUT.ScalarRangeInitialized = 1.0

# get color legend/bar for cellLUT in view renderView1
cellLUTColorBar = GetScalarBar(cellLUT, renderView1)
cellLUTColorBar.Title = 'cell'
cellLUTColorBar.ComponentTitle = ''

# set color bar visibility
cellLUTColorBar.Visibility = 0

# get 2D transfer function for 'eigfun2'
eigfun2TF2D = GetTransferFunction2D('eigfun2')

# get color transfer function/color map for 'eigfun2'
eigfun2LUT = GetColorTransferFunction('eigfun2')
eigfun2LUT.TransferFunction2D = eigfun2TF2D
eigfun2LUT.RGBPoints = [0.0, 0.231373, 0.298039, 0.752941, 0.7038905329210594, 0.865003, 0.865003, 0.865003, 1.4077810658421188, 0.705882, 0.0156863, 0.14902]
eigfun2LUT.ScalarRangeInitialized = 1.0

# get color legend/bar for eigfun2LUT in view renderView1
eigfun2LUTColorBar = GetScalarBar(eigfun2LUT, renderView1)
eigfun2LUTColorBar.Title = 'eigfun2'
eigfun2LUTColorBar.ComponentTitle = ''

# set color bar visibility
eigfun2LUTColorBar.Visibility = 0

# get 2D transfer function for 'pu_1'
pu_1TF2D = GetTransferFunction2D('pu_1')

# get color transfer function/color map for 'pu_1'
pu_1LUT = GetColorTransferFunction('pu_1')
pu_1LUT.TransferFunction2D = pu_1TF2D
pu_1LUT.ScalarRangeInitialized = 1.0

# get color legend/bar for pu_1LUT in view renderView1
pu_1LUTColorBar = GetScalarBar(pu_1LUT, renderView1)
pu_1LUTColorBar.Title = 'pu_1'
pu_1LUTColorBar.ComponentTitle = ''

# set color bar visibility
pu_1LUTColorBar.Visibility = 0

# get 2D transfer function for 'par_1'
par_1TF2D = GetTransferFunction2D('par_1')

# get color transfer function/color map for 'par_1'
par_1LUT = GetColorTransferFunction('par_1')
par_1LUT.TransferFunction2D = par_1TF2D
par_1LUT.ScalarRangeInitialized = 1.0

# get color legend/bar for par_1LUT in view renderView1
par_1LUTColorBar = GetScalarBar(par_1LUT, renderView1)
par_1LUTColorBar.Title = 'par_1'
par_1LUTColorBar.ComponentTitle = ''

# set color bar visibility
par_1LUTColorBar.Visibility = 0

# get 2D transfer function for 'phi'
phiTF2D = GetTransferFunction2D('phi')

# get color transfer function/color map for 'phi'
phiLUT = GetColorTransferFunction('phi')
phiLUT.TransferFunction2D = phiTF2D
phiLUT.RGBPoints = [0.0, 0.231373, 0.298039, 0.752941, 0.42496783783595965, 0.865003, 0.865003, 0.865003, 0.8499356756719193, 0.705882, 0.0156863, 0.14902]
phiLUT.ScalarRangeInitialized = 1.0

# get color legend/bar for phiLUT in view renderView1
phiLUTColorBar = GetScalarBar(phiLUT, renderView1)
phiLUTColorBar.Title = 'phi'
phiLUTColorBar.ComponentTitle = ''

# set color bar visibility
phiLUTColorBar.Visibility = 0

# get 2D transfer function for 'pu_13'
pu_13TF2D = GetTransferFunction2D('pu_13')

# get color transfer function/color map for 'pu_13'
pu_13LUT = GetColorTransferFunction('pu_13')
pu_13LUT.TransferFunction2D = pu_13TF2D
pu_13LUT.ScalarRangeInitialized = 1.0

# get color legend/bar for pu_13LUT in view renderView1
pu_13LUTColorBar = GetScalarBar(pu_13LUT, renderView1)
pu_13LUTColorBar.WindowLocation = 'Upper Left Corner'
pu_13LUTColorBar.Title = 'pu_13'
pu_13LUTColorBar.ComponentTitle = ''

# set color bar visibility
pu_13LUTColorBar.Visibility = 0

# get 2D transfer function for 'par_13'
par_13TF2D = GetTransferFunction2D('par_13')

# get color transfer function/color map for 'par_13'
par_13LUT = GetColorTransferFunction('par_13')
par_13LUT.TransferFunction2D = par_13TF2D
par_13LUT.ScalarRangeInitialized = 1.0

# get color legend/bar for par_13LUT in view renderView1
par_13LUTColorBar = GetScalarBar(par_13LUT, renderView1)
par_13LUTColorBar.Title = 'par_13'
par_13LUTColorBar.ComponentTitle = ''

# set color bar visibility
par_13LUTColorBar.Visibility = 0

# get 2D transfer function for 'olpar_13'
olpar_13TF2D = GetTransferFunction2D('olpar_13')

# get color transfer function/color map for 'olpar_13'
olpar_13LUT = GetColorTransferFunction('olpar_13')
olpar_13LUT.TransferFunction2D = olpar_13TF2D
olpar_13LUT.ScalarRangeInitialized = 1.0

# get color legend/bar for olpar_13LUT in view renderView1
olpar_13LUTColorBar = GetScalarBar(olpar_13LUT, renderView1)
olpar_13LUTColorBar.Title = 'olpar_13'
olpar_13LUTColorBar.ComponentTitle = ''

# set color bar visibility
olpar_13LUTColorBar.Visibility = 0

# get 2D transfer function for 'olpar_12'
olpar_12TF2D = GetTransferFunction2D('olpar_12')

# get color transfer function/color map for 'olpar_12'
olpar_12LUT = GetColorTransferFunction('olpar_12')
olpar_12LUT.TransferFunction2D = olpar_12TF2D
olpar_12LUT.RGBPoints = [0.0, 0.231373, 0.298039, 0.752941, 5.878906683738906e-39, 0.865003, 0.865003, 0.865003, 1.1757813367477812e-38, 0.705882, 0.0156863, 0.14902]
olpar_12LUT.ScalarRangeInitialized = 1.0

# get color legend/bar for olpar_12LUT in view renderView1
olpar_12LUTColorBar = GetScalarBar(olpar_12LUT, renderView1)
olpar_12LUTColorBar.Title = 'olpar_12'
olpar_12LUTColorBar.ComponentTitle = ''

# set color bar visibility
olpar_12LUTColorBar.Visibility = 0

# get 2D transfer function for 'olpar_15'
olpar_15TF2D = GetTransferFunction2D('olpar_15')

# get color transfer function/color map for 'olpar_15'
olpar_15LUT = GetColorTransferFunction('olpar_15')
olpar_15LUT.TransferFunction2D = olpar_15TF2D
olpar_15LUT.RGBPoints = [0.0, 0.231373, 0.298039, 0.752941, 5.878906683738906e-39, 0.865003, 0.865003, 0.865003, 1.1757813367477812e-38, 0.705882, 0.0156863, 0.14902]
olpar_15LUT.ScalarRangeInitialized = 1.0

# get color legend/bar for olpar_15LUT in view renderView1
olpar_15LUTColorBar = GetScalarBar(olpar_15LUT, renderView1)
olpar_15LUTColorBar.Title = 'olpar_15'
olpar_15LUTColorBar.ComponentTitle = ''

# set color bar visibility
olpar_15LUTColorBar.Visibility = 0

# get color legend/bar for olpar_1LUT in view renderView1
olpar_1LUTColorBar = GetScalarBar(olpar_1LUT, renderView1)
olpar_1LUTColorBar.Title = 'olpar_1'
olpar_1LUTColorBar.ComponentTitle = ''

# set color bar visibility
olpar_1LUTColorBar.Visibility = 0

# get 2D transfer function for 'olpar_10'
olpar_10TF2D = GetTransferFunction2D('olpar_10')

# get color transfer function/color map for 'olpar_10'
olpar_10LUT = GetColorTransferFunction('olpar_10')
olpar_10LUT.TransferFunction2D = olpar_10TF2D
olpar_10LUT.ScalarRangeInitialized = 1.0

# get color legend/bar for olpar_10LUT in view renderView1
olpar_10LUTColorBar = GetScalarBar(olpar_10LUT, renderView1)
olpar_10LUTColorBar.Title = 'olpar_10'
olpar_10LUTColorBar.ComponentTitle = ''

# set color bar visibility
olpar_10LUTColorBar.Visibility = 0

# get 2D transfer function for 'olpar_11'
olpar_11TF2D = GetTransferFunction2D('olpar_11')

# get color transfer function/color map for 'olpar_11'
olpar_11LUT = GetColorTransferFunction('olpar_11')
olpar_11LUT.TransferFunction2D = olpar_11TF2D
olpar_11LUT.ScalarRangeInitialized = 1.0

# get color legend/bar for olpar_11LUT in view renderView1
olpar_11LUTColorBar = GetScalarBar(olpar_11LUT, renderView1)
olpar_11LUTColorBar.Title = 'olpar_11'
olpar_11LUTColorBar.ComponentTitle = ''

# set color bar visibility
olpar_11LUTColorBar.Visibility = 0

# get 2D transfer function for 'par_12'
par_12TF2D = GetTransferFunction2D('par_12')

# get color transfer function/color map for 'par_12'
par_12LUT = GetColorTransferFunction('par_12')
par_12LUT.TransferFunction2D = par_12TF2D
par_12LUT.ScalarRangeInitialized = 1.0

# get color legend/bar for par_12LUT in view renderView1
par_12LUTColorBar = GetScalarBar(par_12LUT, renderView1)
par_12LUTColorBar.Title = 'par_12'
par_12LUTColorBar.ComponentTitle = ''

# set color bar visibility
par_12LUTColorBar.Visibility = 0

# get color legend/bar for overlapLUT in view renderView1
overlapLUTColorBar = GetScalarBar(overlapLUT, renderView1)
overlapLUTColorBar.Title = 'overlap'
overlapLUTColorBar.ComponentTitle = ''

# set color bar visibility
overlapLUTColorBar.Visibility = 0

# get 2D transfer function for 'par_no_ol_13'
par_no_ol_13TF2D = GetTransferFunction2D('par_no_ol_13')

# get color transfer function/color map for 'par_no_ol_13'
par_no_ol_13LUT = GetColorTransferFunction('par_no_ol_13')
par_no_ol_13LUT.TransferFunction2D = par_no_ol_13TF2D
par_no_ol_13LUT.ScalarRangeInitialized = 1.0

# get color legend/bar for par_no_ol_13LUT in view renderView1
par_no_ol_13LUTColorBar = GetScalarBar(par_no_ol_13LUT, renderView1)
par_no_ol_13LUTColorBar.Title = 'par_no_ol_13'
par_no_ol_13LUTColorBar.ComponentTitle = ''

# set color bar visibility
par_no_ol_13LUTColorBar.Visibility = 0

# get 2D transfer function for 'par_no_ol_14'
par_no_ol_14TF2D = GetTransferFunction2D('par_no_ol_14')

# get color transfer function/color map for 'par_no_ol_14'
par_no_ol_14LUT = GetColorTransferFunction('par_no_ol_14')
par_no_ol_14LUT.TransferFunction2D = par_no_ol_14TF2D
par_no_ol_14LUT.ScalarRangeInitialized = 1.0

# get color legend/bar for par_no_ol_14LUT in view renderView1
par_no_ol_14LUTColorBar = GetScalarBar(par_no_ol_14LUT, renderView1)
par_no_ol_14LUTColorBar.Title = 'par_no_ol_14'
par_no_ol_14LUTColorBar.ComponentTitle = ''

# set color bar visibility
par_no_ol_14LUTColorBar.Visibility = 0

# get 2D transfer function for 'par_no_ol_15'
par_no_ol_15TF2D = GetTransferFunction2D('par_no_ol_15')

# get color transfer function/color map for 'par_no_ol_15'
par_no_ol_15LUT = GetColorTransferFunction('par_no_ol_15')
par_no_ol_15LUT.TransferFunction2D = par_no_ol_15TF2D
par_no_ol_15LUT.RGBPoints = [0.0, 0.231373, 0.298039, 0.752941, 0.5001220703125, 0.865003, 0.865003, 0.865003, 1.000244140625, 0.705882, 0.0156863, 0.14902]
par_no_ol_15LUT.ScalarRangeInitialized = 1.0

# get color legend/bar for par_no_ol_15LUT in view renderView1
par_no_ol_15LUTColorBar = GetScalarBar(par_no_ol_15LUT, renderView1)
par_no_ol_15LUTColorBar.Title = 'par_no_ol_15'
par_no_ol_15LUTColorBar.ComponentTitle = ''

# set color bar visibility
par_no_ol_15LUTColorBar.Visibility = 0

# get 2D transfer function for 'par_no_ol_11'
par_no_ol_11TF2D = GetTransferFunction2D('par_no_ol_11')

# get color transfer function/color map for 'par_no_ol_11'
par_no_ol_11LUT = GetColorTransferFunction('par_no_ol_11')
par_no_ol_11LUT.TransferFunction2D = par_no_ol_11TF2D
par_no_ol_11LUT.ScalarRangeInitialized = 1.0

# get color legend/bar for par_no_ol_11LUT in view renderView1
par_no_ol_11LUTColorBar = GetScalarBar(par_no_ol_11LUT, renderView1)
par_no_ol_11LUTColorBar.Title = 'par_no_ol_11'
par_no_ol_11LUTColorBar.ComponentTitle = ''

# set color bar visibility
par_no_ol_11LUTColorBar.Visibility = 0

# get 2D transfer function for 'Result'
resultTF2D = GetTransferFunction2D('Result')

# get color transfer function/color map for 'Result'
resultLUT = GetColorTransferFunction('Result')
resultLUT.TransferFunction2D = resultTF2D
resultLUT.RGBPoints = [0.0, 0.231373, 0.298039, 0.752941, 7.999999999999997, 0.865003, 0.865003, 0.865003, 16.0, 0.705882, 0.0156863, 0.14902]
resultLUT.ScalarRangeInitialized = 1.0

# get color legend/bar for resultLUT in view renderView1
resultLUTColorBar = GetScalarBar(resultLUT, renderView1)
resultLUTColorBar.WindowLocation = 'Upper Right Corner'
resultLUTColorBar.Title = 'Result'
resultLUTColorBar.ComponentTitle = ''

# set color bar visibility
resultLUTColorBar.Visibility = 0

# get 2D transfer function for 'par_no_ol_12'
par_no_ol_12TF2D = GetTransferFunction2D('par_no_ol_12')

# get color transfer function/color map for 'par_no_ol_12'
par_no_ol_12LUT = GetColorTransferFunction('par_no_ol_12')
par_no_ol_12LUT.TransferFunction2D = par_no_ol_12TF2D
par_no_ol_12LUT.ScalarRangeInitialized = 1.0

# get color legend/bar for par_no_ol_12LUT in view renderView1
par_no_ol_12LUTColorBar = GetScalarBar(par_no_ol_12LUT, renderView1)
par_no_ol_12LUTColorBar.WindowLocation = 'Upper Right Corner'
par_no_ol_12LUTColorBar.Title = 'par_no_ol_12'
par_no_ol_12LUTColorBar.ComponentTitle = ''

# set color bar visibility
par_no_ol_12LUTColorBar.Visibility = 0

# get color legend/bar for par_no_ol_1LUT in view renderView1
par_no_ol_1LUTColorBar = GetScalarBar(par_no_ol_1LUT, renderView1)
par_no_ol_1LUTColorBar.Title = 'par_no_ol_1'
par_no_ol_1LUTColorBar.ComponentTitle = ''

# set color bar visibility
par_no_ol_1LUTColorBar.Visibility = 0

# get 2D transfer function for 'par_no_ol_2'
par_no_ol_2TF2D = GetTransferFunction2D('par_no_ol_2')

# get color transfer function/color map for 'par_no_ol_2'
par_no_ol_2LUT = GetColorTransferFunction('par_no_ol_2')
par_no_ol_2LUT.TransferFunction2D = par_no_ol_2TF2D
par_no_ol_2LUT.ScalarRangeInitialized = 1.0

# get color legend/bar for par_no_ol_2LUT in view renderView1
par_no_ol_2LUTColorBar = GetScalarBar(par_no_ol_2LUT, renderView1)
par_no_ol_2LUTColorBar.Title = 'par_no_ol_2'
par_no_ol_2LUTColorBar.ComponentTitle = ''

# set color bar visibility
par_no_ol_2LUTColorBar.Visibility = 0

# get 2D transfer function for 'par_no_ol_4'
par_no_ol_4TF2D = GetTransferFunction2D('par_no_ol_4')

# get color transfer function/color map for 'par_no_ol_4'
par_no_ol_4LUT = GetColorTransferFunction('par_no_ol_4')
par_no_ol_4LUT.TransferFunction2D = par_no_ol_4TF2D
par_no_ol_4LUT.ScalarRangeInitialized = 1.0

# get color legend/bar for par_no_ol_4LUT in view renderView1
par_no_ol_4LUTColorBar = GetScalarBar(par_no_ol_4LUT, renderView1)
par_no_ol_4LUTColorBar.Title = 'par_no_ol_4'
par_no_ol_4LUTColorBar.ComponentTitle = ''

# set color bar visibility
par_no_ol_4LUTColorBar.Visibility = 0

# get 2D transfer function for 'par_no_ol_5'
par_no_ol_5TF2D = GetTransferFunction2D('par_no_ol_5')

# get color transfer function/color map for 'par_no_ol_5'
par_no_ol_5LUT = GetColorTransferFunction('par_no_ol_5')
par_no_ol_5LUT.TransferFunction2D = par_no_ol_5TF2D
par_no_ol_5LUT.ScalarRangeInitialized = 1.0

# get color legend/bar for par_no_ol_5LUT in view renderView1
par_no_ol_5LUTColorBar = GetScalarBar(par_no_ol_5LUT, renderView1)
par_no_ol_5LUTColorBar.Title = 'par_no_ol_5'
par_no_ol_5LUTColorBar.ComponentTitle = ''

# set color bar visibility
par_no_ol_5LUTColorBar.Visibility = 0

# get 2D transfer function for 'par_no_ol_6'
par_no_ol_6TF2D = GetTransferFunction2D('par_no_ol_6')

# get color transfer function/color map for 'par_no_ol_6'
par_no_ol_6LUT = GetColorTransferFunction('par_no_ol_6')
par_no_ol_6LUT.TransferFunction2D = par_no_ol_6TF2D
par_no_ol_6LUT.ScalarRangeInitialized = 1.0

# get color legend/bar for par_no_ol_6LUT in view renderView1
par_no_ol_6LUTColorBar = GetScalarBar(par_no_ol_6LUT, renderView1)
par_no_ol_6LUTColorBar.Title = 'par_no_ol_6'
par_no_ol_6LUTColorBar.ComponentTitle = ''

# set color bar visibility
par_no_ol_6LUTColorBar.Visibility = 0

# get 2D transfer function for 'par_no_ol_7'
par_no_ol_7TF2D = GetTransferFunction2D('par_no_ol_7')

# get color transfer function/color map for 'par_no_ol_7'
par_no_ol_7LUT = GetColorTransferFunction('par_no_ol_7')
par_no_ol_7LUT.TransferFunction2D = par_no_ol_7TF2D
par_no_ol_7LUT.ScalarRangeInitialized = 1.0

# get color legend/bar for par_no_ol_7LUT in view renderView1
par_no_ol_7LUTColorBar = GetScalarBar(par_no_ol_7LUT, renderView1)
par_no_ol_7LUTColorBar.Title = 'par_no_ol_7'
par_no_ol_7LUTColorBar.ComponentTitle = ''

# set color bar visibility
par_no_ol_7LUTColorBar.Visibility = 0

# get 2D transfer function for 'olpar_2'
olpar_2TF2D = GetTransferFunction2D('olpar_2')

# get color transfer function/color map for 'olpar_2'
olpar_2LUT = GetColorTransferFunction('olpar_2')
olpar_2LUT.TransferFunction2D = olpar_2TF2D
olpar_2LUT.ScalarRangeInitialized = 1.0

# get color legend/bar for olpar_2LUT in view renderView1
olpar_2LUTColorBar = GetScalarBar(olpar_2LUT, renderView1)
olpar_2LUTColorBar.Title = 'olpar_2'
olpar_2LUTColorBar.ComponentTitle = ''

# set color bar visibility
olpar_2LUTColorBar.Visibility = 0

# get 2D transfer function for 'par_2'
par_2TF2D = GetTransferFunction2D('par_2')

# get color transfer function/color map for 'par_2'
par_2LUT = GetColorTransferFunction('par_2')
par_2LUT.TransferFunction2D = par_2TF2D
par_2LUT.ScalarRangeInitialized = 1.0

# get color legend/bar for par_2LUT in view renderView1
par_2LUTColorBar = GetScalarBar(par_2LUT, renderView1)
par_2LUTColorBar.Title = 'par_2'
par_2LUTColorBar.ComponentTitle = ''

# set color bar visibility
par_2LUTColorBar.Visibility = 0

# get 2D transfer function for 'par_6'
par_6TF2D = GetTransferFunction2D('par_6')

# get color transfer function/color map for 'par_6'
par_6LUT = GetColorTransferFunction('par_6')
par_6LUT.TransferFunction2D = par_6TF2D
par_6LUT.ScalarRangeInitialized = 1.0

# get color legend/bar for par_6LUT in view renderView1
par_6LUTColorBar = GetScalarBar(par_6LUT, renderView1)
par_6LUTColorBar.Title = 'par_6'
par_6LUTColorBar.ComponentTitle = ''

# set color bar visibility
par_6LUTColorBar.Visibility = 0

# get 2D transfer function for 'par_14'
par_14TF2D = GetTransferFunction2D('par_14')

# get color transfer function/color map for 'par_14'
par_14LUT = GetColorTransferFunction('par_14')
par_14LUT.TransferFunction2D = par_14TF2D
par_14LUT.ScalarRangeInitialized = 1.0

# get color legend/bar for par_14LUT in view renderView1
par_14LUTColorBar = GetScalarBar(par_14LUT, renderView1)
par_14LUTColorBar.Title = 'par_14'
par_14LUTColorBar.ComponentTitle = ''

# set color bar visibility
par_14LUTColorBar.Visibility = 0

# get 2D transfer function for 'par_no_ol_9'
par_no_ol_9TF2D = GetTransferFunction2D('par_no_ol_9')

# get color transfer function/color map for 'par_no_ol_9'
par_no_ol_9LUT = GetColorTransferFunction('par_no_ol_9')
par_no_ol_9LUT.TransferFunction2D = par_no_ol_9TF2D
par_no_ol_9LUT.ScalarRangeInitialized = 1.0

# get color legend/bar for par_no_ol_9LUT in view renderView1
par_no_ol_9LUTColorBar = GetScalarBar(par_no_ol_9LUT, renderView1)
par_no_ol_9LUTColorBar.Title = 'par_no_ol_9'
par_no_ol_9LUTColorBar.ComponentTitle = ''

# set color bar visibility
par_no_ol_9LUTColorBar.Visibility = 0

# hide data in view
Hide(threshold3, renderView1)

# hide data in view
Hide(extractComponent1, renderView1)

# show color legend
calculator1Display.SetScalarBarVisibility(renderView1, True)

# hide data in view
Hide(threshold4, renderView1)

# hide data in view
Hide(contour1, renderView1)

# show color legend
pointDatatoCellData1Display.SetScalarBarVisibility(renderView1, True)

# hide data in view
Hide(pointDatatoCellData1, renderView1)

# hide data in view
Hide(contour4, renderView1)

# hide data in view
Hide(cellDatatoPointData1, renderView1)

# show color legend
cleantoGrid1Display.SetScalarBarVisibility(renderView1, True)

# hide data in view
Hide(cleantoGrid1, renderView1)

# hide data in view
Hide(contour5, renderView1)

# show color legend
cleanCellstoGrid1Display.SetScalarBarVisibility(renderView1, True)

# hide data in view
Hide(cleanCellstoGrid1, renderView1)

# show color legend
threshold1Display.SetScalarBarVisibility(renderView1, True)

# hide data in view
Hide(threshold1, renderView1)

# show color legend
cleantoGrid2Display.SetScalarBarVisibility(renderView1, True)

# hide data in view
Hide(cleantoGrid2, renderView1)

# show color legend
extractRegionSurface1Display.SetScalarBarVisibility(renderView1, True)

# hide data in view
Hide(extractRegionSurface1, renderView1)

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# get opacity transfer function/opacity map for 'olpar_10'
olpar_10PWF = GetOpacityTransferFunction('olpar_10')
olpar_10PWF.ScalarRangeInitialized = 1

# get opacity transfer function/opacity map for 'Result'
resultPWF = GetOpacityTransferFunction('Result')
resultPWF.Points = [0.0, 0.0, 0.5, 0.0, 16.0, 1.0, 0.5, 0.0]
resultPWF.ScalarRangeInitialized = 1

# get opacity transfer function/opacity map for 'olpar_2'
olpar_2PWF = GetOpacityTransferFunction('olpar_2')
olpar_2PWF.ScalarRangeInitialized = 1

# get opacity transfer function/opacity map for 'cell'
cellPWF = GetOpacityTransferFunction('cell')
cellPWF.Points = [1.0, 0.0, 0.5, 0.0, 1600.0, 1.0, 0.5, 0.0]
cellPWF.ScalarRangeInitialized = 1

# get opacity transfer function/opacity map for 'olpar_13'
olpar_13PWF = GetOpacityTransferFunction('olpar_13')
olpar_13PWF.ScalarRangeInitialized = 1

# get opacity transfer function/opacity map for 'par_no_ol_14'
par_no_ol_14PWF = GetOpacityTransferFunction('par_no_ol_14')
par_no_ol_14PWF.ScalarRangeInitialized = 1

# get opacity transfer function/opacity map for 'phi'
phiPWF = GetOpacityTransferFunction('phi')
phiPWF.Points = [0.0, 0.0, 0.5, 0.0, 0.8499356756719193, 1.0, 0.5, 0.0]
phiPWF.ScalarRangeInitialized = 1

# get opacity transfer function/opacity map for 'par_no_ol_5'
par_no_ol_5PWF = GetOpacityTransferFunction('par_no_ol_5')
par_no_ol_5PWF.ScalarRangeInitialized = 1

# get opacity transfer function/opacity map for 'par_no_ol_9'
par_no_ol_9PWF = GetOpacityTransferFunction('par_no_ol_9')
par_no_ol_9PWF.ScalarRangeInitialized = 1

# get opacity transfer function/opacity map for 'olpar_11'
olpar_11PWF = GetOpacityTransferFunction('olpar_11')
olpar_11PWF.ScalarRangeInitialized = 1

# get opacity transfer function/opacity map for 'par_no_ol_12'
par_no_ol_12PWF = GetOpacityTransferFunction('par_no_ol_12')
par_no_ol_12PWF.ScalarRangeInitialized = 1

# get opacity transfer function/opacity map for 'par_2'
par_2PWF = GetOpacityTransferFunction('par_2')
par_2PWF.ScalarRangeInitialized = 1

# get opacity transfer function/opacity map for 'eigfun2'
eigfun2PWF = GetOpacityTransferFunction('eigfun2')
eigfun2PWF.Points = [0.0, 0.0, 0.5, 0.0, 1.4077810658421188, 1.0, 0.5, 0.0]
eigfun2PWF.ScalarRangeInitialized = 1

# get opacity transfer function/opacity map for 'olpar_12'
olpar_12PWF = GetOpacityTransferFunction('olpar_12')
olpar_12PWF.Points = [0.0, 0.0, 0.5, 0.0, 1.1757813367477812e-38, 1.0, 0.5, 0.0]
olpar_12PWF.ScalarRangeInitialized = 1

# get opacity transfer function/opacity map for 'par_no_ol_15'
par_no_ol_15PWF = GetOpacityTransferFunction('par_no_ol_15')
par_no_ol_15PWF.Points = [0.0, 0.0, 0.5, 0.0, 1.000244140625, 1.0, 0.5, 0.0]
par_no_ol_15PWF.ScalarRangeInitialized = 1

# get opacity transfer function/opacity map for 'pu_13'
pu_13PWF = GetOpacityTransferFunction('pu_13')
pu_13PWF.ScalarRangeInitialized = 1

# get opacity transfer function/opacity map for 'par_no_ol_6'
par_no_ol_6PWF = GetOpacityTransferFunction('par_no_ol_6')
par_no_ol_6PWF.ScalarRangeInitialized = 1

# get opacity transfer function/opacity map for 'olpar_1'
olpar_1PWF = GetOpacityTransferFunction('olpar_1')
olpar_1PWF.Points = [0.0, 0.0, 0.5, 0.0, 1.000244140625, 1.0, 0.5, 0.0]
olpar_1PWF.ScalarRangeInitialized = 1

# get opacity transfer function/opacity map for 'par_12'
par_12PWF = GetOpacityTransferFunction('par_12')
par_12PWF.ScalarRangeInitialized = 1

# get opacity transfer function/opacity map for 'par_no_ol_2'
par_no_ol_2PWF = GetOpacityTransferFunction('par_no_ol_2')
par_no_ol_2PWF.ScalarRangeInitialized = 1

# get opacity transfer function/opacity map for 'par_6'
par_6PWF = GetOpacityTransferFunction('par_6')
par_6PWF.ScalarRangeInitialized = 1

# get opacity transfer function/opacity map for 'pu_1'
pu_1PWF = GetOpacityTransferFunction('pu_1')
pu_1PWF.ScalarRangeInitialized = 1

# get opacity transfer function/opacity map for 'olpar_15'
olpar_15PWF = GetOpacityTransferFunction('olpar_15')
olpar_15PWF.Points = [0.0, 0.0, 0.5, 0.0, 1.1757813367477812e-38, 1.0, 0.5, 0.0]
olpar_15PWF.ScalarRangeInitialized = 1

# get opacity transfer function/opacity map for 'par_no_ol_13'
par_no_ol_13PWF = GetOpacityTransferFunction('par_no_ol_13')
par_no_ol_13PWF.ScalarRangeInitialized = 1

# get opacity transfer function/opacity map for 'par_no_ol_11'
par_no_ol_11PWF = GetOpacityTransferFunction('par_no_ol_11')
par_no_ol_11PWF.ScalarRangeInitialized = 1

# get opacity transfer function/opacity map for 'par_no_ol_4'
par_no_ol_4PWF = GetOpacityTransferFunction('par_no_ol_4')
par_no_ol_4PWF.ScalarRangeInitialized = 1

# get opacity transfer function/opacity map for 'par_14'
par_14PWF = GetOpacityTransferFunction('par_14')
par_14PWF.ScalarRangeInitialized = 1

# get opacity transfer function/opacity map for 'par_no_ol_7'
par_no_ol_7PWF = GetOpacityTransferFunction('par_no_ol_7')
par_no_ol_7PWF.ScalarRangeInitialized = 1

# get opacity transfer function/opacity map for 'par_13'
par_13PWF = GetOpacityTransferFunction('par_13')
par_13PWF.ScalarRangeInitialized = 1

# get opacity transfer function/opacity map for 'par_1'
par_1PWF = GetOpacityTransferFunction('par_1')
par_1PWF.ScalarRangeInitialized = 1

# ----------------------------------------------------------------
# restore active source
SetActiveSource(resultvtu)
# ----------------------------------------------------------------


if __name__ == '__main__':
    # generate extracts
    SaveExtracts(ExtractsOutputDirectory='extracts')
