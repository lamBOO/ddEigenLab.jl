using Gridap
using GridapDistributed
using Metis

using ddEigenLab

ρ = 10
Lx = 2
Ly = 1.0
domain2 = (0, Lx, 0, Ly)
partition2 = (Lx * ρ, Ly * ρ)
model2 = CartesianDiscreteModel(domain2, partition2; isperiodic=(false, false)) |> simplexify
labels2 = get_face_labeling(model2)
add_tag_from_tags!(labels2, "diri", collect(1:3^2-1))



reffe2 = ReferenceFE(lagrangian, Float64, 1)
V2 = TestFESpace(model2, reffe2)
U2 = TrialFESpace(V2, 0)
Ω2 = Triangulation(model2)
dΩ2 = Measure(Ω2, 2)


N = 8
g = GridapDistributed.compute_cell_graph(model2)
partition = Metis.partition(g, N)
writevtk(Ω2, "partition", cellfields=["partition" => partition])

ol = 1
pu = PartitionOfUnity(partition, ol, V2)
pu_no_ol = PartitionOfUnity(partition, 0, V2)
par = Partition(partition, ol, V2)
par_no_ol = Partition(partition, 0, V2)

elemsp = create_elements_partition(partition, N)
create_overlapping_elements_partition!(elemsp, g, N, ol)
olparvecs = [zeros(Int32, length(partition)[1]) for i=1:length(pu.Di)]
for ipar=1:length(pu.Di)
  olparvecs[ipar][elemsp[ipar]] .= 1
end
olparfuns = [FEFunction(U2, Float64.(olparvecs[i])) for i=1:length(pu.Di)]
allelems = vcat(elemsp...)
overlapelems = unique(filter(y->length(findall(x->x==y,allelems)) > 1, allelems))
overlapvec = zeros(Int32, length(partition)[1])
overlapvec[overlapelems] .= 1

puvecs = [pu.Ri[i]' * pu.Di[i] * pu.Ri[i] * ones(size(pu.Ri[1])[2]) for i=1:length(pu.Di)]
pufuns = [FEFunction(U2, puvecs[i]) for i=1:length(pu.Di)]
parvecs = [pu.Ri[i]' * pu.Ri[i] * ones(size(pu.Ri[1])[2]) for i=1:length(pu.Di)]
parfuns = [FEFunction(U2, parvecs[i]) for i=1:length(pu.Di)]
par_no_ol_vecs = [pu_no_ol.Ri[i]' * pu_no_ol.Ri[i] * ones(size(pu_no_ol.Ri[1])[2]) for i=1:length(pu_no_ol.Di)]
par_no_ol_funs = [FEFunction(U2, par_no_ol_vecs[i]) for i=1:length(pu_no_ol.Di)]
writevtk(Ω2, "result", cellfields=[
  (["pu_$i" for i=1:length(pu.Di)] .=> pufuns)...,
  (["par_$i" for i=1:length(pu.Di)] .=> parfuns)...,
  (["par_no_ol_$i" for i=1:length(pu_no_ol.Di)] .=> par_no_ol_funs)...,
  (["olpar_$i" for i=1:length(pu.Di)] .=> olparvecs)...,
  "overlap" => overlapvec,
  "partition" => partition
])
