#!/bin/bash


for p in 3 5 7
do
  for N in 1 2 4 8 16 32 64 128
  do
    echo "$p"
    gmsh -2 full_structured_nodefects.geo -o full_structured_nodefects_"$p"_"$N".msh -setnumber N "$N" -setnumber p "$p"
    gmsh -2 full_structured_defects.geo -o full_structured_defects_"$p"_"$N".msh -setnumber N "$N" -setnumber p "$p"
  done
  gmsh -2 periodic_structured.geo -o periodic_structured_"$p".msh -setnumber p "$p"
done
