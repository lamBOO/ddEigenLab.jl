using Gridap
using GridapGmsh
using GridapDistributed
import Base.\
using IterativeSolvers
using LinearAlgebra
using Metis
using CSV
using DataFrames

using ddEigenLab

function main()

  function pot(x, Ns)
    function pot1(x)
      R = 1  # outer radius of disks
      r = 0.9  # inner radius
      a = 0.0001  # cutoff radius
      Z = 1  # strength
      S = 0  # shift
      scale = 1E+0  # scale of potential
      margin = 0.001  #!: Needed for precision with polygonal "circle"
      if (
        (abs(x[1]) <= r + margin) &&
        (sqrt(x[1]^2 + x[2]^2) <= a)
      )
        return -Z * 1 / (scale * a) + S
      elseif (
        (abs(x[1]) <= r + margin) &&
        (sqrt(x[1]^2 + x[2]^2) > a)
      )
        return -Z * 1 / (scale * sqrt(x[1]^2 + x[2]^2)) + S
      else
        return 0
      end
    end
    return  findmin([
      pot1(x - VectorValue([p, 0]))
      for p in [1 + (i - 1) * 1.8 for i in Ns]
    ])[1]
  end

  function solve_periodic(
    pot;
    p = 3  # 5 for paper
    # p = 5  # 5 for paper
  )
    mshfile = joinpath("periodic_structured_$(p).msh")
    mod = GmshDiscreteModel(mshfile)
    re = ReferenceFE(lagrangian, Float64, 1)
    V = TestFESpace(mod, re, dirichlet_tags=["top", "bot"])
    U = TrialFESpace(V, 0)
    Ω = Triangulation(mod)
    dΩ = Measure(Ω, 2)
    a(u, v) = ∫(∇(u) ⋅ ∇(v) + (x -> pot(x, 0:2)) * u * v)dΩ
    c(u, v) = ∫(u * v)dΩ
    A = assemble_matrix(a, U, V)
    M = assemble_matrix(c, U, V)

    eigvec, eigval, _, _ = LOPCG(
      A, M, Pl = A, tol = 1E-10, maxiter = 1000
    )

    writevtks = true
    if writevtks
      writevtk(mod, "periodic-model")
      writevtk(Ω, "periodic-result", cellfields=["uh" => FEFunction(U, eigvec)])
    end

    return (; eval=eigval, evec=eigvec, V=V)
  end

  function setup_full(
    n,
    pot::Function;
    p = 3,  # 5 for paper,
    # p = 5,  # 5 for paper,
    shift=0.0
  )
    mshfile = joinpath("full_structured_defects_$(p)_$(n).msh")
    mod = GmshDiscreteModel(mshfile)

    re = ReferenceFE(lagrangian, Float64, 1)
    V = TestFESpace(mod, re, dirichlet_tags=["outer"])
    U = TrialFESpace(V, 0)
    Ω = Triangulation(mod)
    dΩ = Measure(Ω, 2)
    a(u, v) = ∫(∇(u) ⋅ ∇(v) + (x -> pot(x, 0:n+1)) * u * v)dΩ
    c(u, v) = ∫(u * v)dΩ

    A = assemble_matrix(a, U, V)
    M = assemble_matrix(c, U, V)
    As = A - shift * M

    debug = true
    if debug
      Vnobc = TestFESpace(mod, re)
      writevtk(Ω, "potential-$(n)-$(p)", cellfields=["pot" => Gridap.FESpaces.interpolate(x->pot(x, 0:n+1), Vnobc)])
      @show "debug"
    end

    return (; A=A, M=M, As=As, V=V, U=U, mod=mod, Ω=Ω)
  end

  function solve_full(
    sol_per,
    setup_lin,
    n;
    overlap = 1,
    name = "result"
  )
    # dd setup
    psi = FEFunction(sol_per.V, sol_per.evec);
    @time psi_ext = periodic_extension(
      psi, setup_lin.V, unitcell=[0.1 1.9; -1 1], scaling = Diagonal([1,(1-1E-4)])
    );

    @info "Start partition"
    # 1) unstructured METIS decomp
    partition = gmsh_partition(
      setup_lin.mod, "untitled" .* ["$i" for i = cat(1:n+2, dims=1)]
    )  # defects
    # partition = gmsh_partition(
    #   setup_lin.mod, "untitled" .* ["$i" for i = cat(1:n, dims=1)]
    # )  # nodefects
    # partition = Metis.partition(
    #   GridapDistributed.compute_cell_graph(setup_lin.mod), n
    # )
    @time par = Partition(partition, overlap, setup_lin.V)
    @info "End partition"

    @time begin
      @info "start PU"
      # 1) equal weights pu
      pu = PartitionOfUnity(partition, overlap, setup_lin.V)
      @info "end PU"
    end

    @info "Start DDs and CCs"
    cc_none = NoCoarseCorrection()
    cc_pefa = GeneralizedNicolaides(setup_lin.As, pu, psi_ext.free_values[:, :])
    ASM1 = PSM(setup_lin.As, par, cc_none)
    ASM2 = PSM(setup_lin.As, par, cc_pefa)
    RAS1 = PSM(setup_lin.As, pu, cc_none)
    RAS2 = PSM(setup_lin.As, pu, cc_pefa)
    @info "End DDs and CCs"

    # for debug
    debug = true
    if debug
      writevtk(setup_lin.Ω, "partition-$(name)", cellfields=[
        "partition" => partition
      ])
      puvecs = [
        pu.Ri[i]' * pu.Di[i] * pu.Ri[i] * ones(size(setup_lin.A)[1])
        for i=1:length(pu.Di)
      ]
      pufuns = [FEFunction(setup_lin.V, puvecs[i]) for i=1:length(pu.Di)]
      writevtk(setup_lin.Ω, "pu-$(name)", cellfields=[
        (["pu_$i" for i=1:length(pu.Di)] .=> pufuns)...,
      ])
    end

    opts = (;verbose=false, log=true, maxiter=1000, reltol=1E-8,abstol=1E-300)
    opts2 = (;debug=false, maxiter=10000, reltol=1E-8, history=true)

    cgw = CGWrapper(ASM2, opts)
    gmresw = GMRESWrapper(RAS2, (opts..., restart=100))  #! check that enough
    psmw1 = PSMWrapper(RAS1, opts2)
    psmw2 = PSMWrapper(RAS2, opts2)

    # starting value x = psi_ext.free_values leads to "same" |r_0|
    opts_eig = (tol = 1E-8, maxiter = 1000)
    if n<=32
      @time res_psmw1 = LOPCG(setup_lin.A, setup_lin.M, Pl = psmw1; opts_eig...);
    end
    @time res_psmw2 = LOPCG(setup_lin.A, setup_lin.M, Pl = psmw2; opts_eig...);
    @time res_cgw = LOPCG(setup_lin.A, setup_lin.M, Pl = cgw; opts_eig...);
    @time res_gmresw = LOPCG(setup_lin.A, setup_lin.M, Pl = gmresw; opts_eig...);

    writevtks = true
    if writevtks
      writevtk(setup_lin.Ω, "result-$(n)", cellfields=[
        "uh" => FEFunction(setup_lin.U, res_cgw[1])
      ])
    end

    @show psmw1.iterations
    @show psmw2.iterations
    @show cgw.iterations
    @show gmresw.iterations

    return (;its = (;
      psmw1 = psmw1.iterations,
      psmw2 = psmw2.iterations,
      cgw = cgw.iterations,
      gmresw = gmresw.iterations,
    ))

  end


  df = DataFrame(
    "L" => Int[],
    ([
      "ras1",
      "ras2",
      "cgasm2",
      "gmresras2",
    ] |> f -> map(x->x.*"_".*["it_outer", "maxit_inner", "sumit_inner"], f) |> f-> vcat(f...) |> f->map(x->x=>Int[],f))...
  )
  n_arr = [2^i for i=0:7]
  sol_per = solve_periodic(pot);
  for n in n_arr
    set_full = setup_full(n, pot, shift=sol_per.eval);
    sol_full = solve_full(sol_per, set_full, n, name="full")
    @show sol_full
    push!(df,
    [
      n, (
        map(x->[length(x), findmax([0,x...])[1], sum([0,x...])], sol_full.its) |> collect |> f->reduce(vcat, f)
      )...
    ])
  end
  CSV.write("data-union.csv", df)
  @show Matrix(df)

  # to generate potential plot
  setup_full(4, pot, p=7);
  setup_full(4, pot, p=3);

  # output paraview string to use a program. source (output=vtkMolecule)
  println(getParaviewSource(Molecule(
    [6, 6, 6, 6],
    [
      (1.0, 0., -1.5),
      (2.8, 0., -1.5),
      (4.6, 0., -1.5),
      (6.4, 0., -1.5),
    ],
    [
      (0, 1, 2)
      (1, 2, 2)
      (2, 3, 2)
    ]
  )))

  return Matrix(df)
end
main()
