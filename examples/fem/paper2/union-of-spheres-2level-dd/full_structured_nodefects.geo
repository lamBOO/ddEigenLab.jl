// Command line Parameters
If(!Exists(p))
  p = 3;
EndIf
If(!Exists(N))
  N = 4;
EndIf

pts = 2^(p)+1;

// Settings
res = 100;
Mesh.CharacteristicLengthMax = 1.0 * 2^(-p);
// Mesh.MshFileVersion = 4.1;
Mesh.MshFileVersion = 2.2;
d = 0.1;
D = 2 - 2 * 0.1;

// Left
Point(1000) = {0 + 0.1, +Sin(Acos(-0.9)), 0, p};
Point(2000) = {0 + 0.1, -Sin(Acos(-0.9)), 0, p};

// Middle
For i In {1:N}

  Point(0000 + i) = {1 + (i-1) * D, 0, 0, p};
  Point(1000 + i) = {2 -  0.1 + (i-1) * D, +Sin(Acos(-0.9)), 0, p};
  Point(2000 + i) = {2 -  0.1 + (i-1) * D, -Sin(Acos(-0.9)), 0, p};

  Line (1000 + i) = {1000 + i, 2000 + i};
  Transfinite Curve(1000 + i) = pts;

  Circle (2000 + i) = {1000 + i - 1, 000 + i, 1000 + i};
  Circle (3000 + i) = {2000 + i, 000 + i, 2000 + i - 1};
  Transfinite Curve(2000 + i) = Round(1.2*pts/2)*2+1;
  Transfinite Curve(3000 + i) = Round(1.2*pts/2)*2+1;

EndFor

// // Left
Line (1000) = {1000, 2000};
Transfinite Curve(1000) = pts;
// Circle (2000) = {2000, 1, 1000};
// Curve Loop(2000 + N  + 1) = {1000,2000};
// Plane Surface(3000 + N  + 1) = {2000 + N  + 1};

// // Right
// Circle (4000) = {1000 + N, N, 2000 + N};
// Curve Loop(2000 + N  + 2) = {1000 + N,-4000};
// Plane Surface(3000 + N  + 2) = {2000 + N  + 2};

For i In {1:N}
  Curve Loop(2000 + i) = {-(1000 + i - 1),2000 + i,1000 + i,3000 + i};
  Plane Surface(3000 + i) = {2000 + i};
  Transfinite Surface(3000 + i) = {} AlternateLeft;
  // Transfinite Surface(3000 + i) = {} Left;
EndFor

// Curve Loop(2100) = {-2000, -3001:-(3000 + N), -4000, -(2000 + N):-2001};
Physical Line("outer") = {-1000, -3001:-(3000 + N), -1000-N, -(2000 + N):-2001};

Physical Point("outer") = {1000:1000+N, 2000:2000+N};

// Physical Surface("mesh",1000) = {3001:3000 + N + 2};

// Physical Surface("test1",1) = {};
// Physical Surface("test2",2) = {};
// Physical Surface("test3",3) = {};

For i In {1:N}
  Printf("%f",i);
  // pi = 3.14;
  // // s = Printf(Sprintf("model%f", pi));
  // a = 1; b = 2; c = 10;
  // test = Printf(Sprintf("model%g_%g_%g.inp", a, b, c));
  // Printf(s);
  // Printf(StrCat("name is ", s));
  Physical Surface(i) = {3000+i};
EndFor

// Mesh.Algorithm = 1; // Delaunay 2D
