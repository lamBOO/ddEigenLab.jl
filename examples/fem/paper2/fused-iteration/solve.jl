using Gridap
using GridapDistributed
using IterativeSolvers
using LinearAlgebra
using Metis
using CSV, Tables
using DataFrames

using ddEigenLab

function solve_eigen(
  dims,
  partition,
  pot::Function;
  isperiodic=(false, false),
  name="result",
  shift=0.0
)
  mod = CartesianDiscreteModel(dims, partition, isperiodic=isperiodic)
  labels = get_face_labeling(mod)
  add_tag_from_tags!(labels, "diri", collect(1:3^3-1))
  re = ReferenceFE(lagrangian, Float64, 1)
  V = TestFESpace(mod, re, dirichlet_tags=["diri"])
  U = TrialFESpace(V, 0)
  Ω = Triangulation(mod)
  dΩ = Measure(Ω, 2)

  a(u, v) = ∫(∇(v) ⋅ ∇(u) + pot * u * v)dΩ
  c(u, v) = ∫(v * u)dΩ
  A = assemble_matrix(a, U, V)
  M = assemble_matrix(c, U, V)

  evec, eval, _, info = LOPCG(
    A, M, Pl = A - shift * M, tol = 1E-10, maxiter = 1000
  )

  writevtk(Ω, "$(name)-out", cellfields=["out" => FEFunction(U, evec)])
  mod_tmp = CartesianDiscreteModel(dims, partition)
  V_out = TestFESpace(mod_tmp, re)
  V_grid = Gridap.FESpaces.interpolate(pot, V_out)
  writevtk(
    Triangulation(mod_tmp), "$(name)-pot", cellfields=["pot" => V_grid]
  )

  return (; eval=eval, evec=evec, it=info.it, V=V)
end

function setup_linear_system(
  dims,
  partition,
  pot::Function;
  isperiodic=(false, false, false),
  shift=0.0
)
  mod = CartesianDiscreteModel(dims, partition, isperiodic=isperiodic)
  labels = get_face_labeling(mod)
  add_tag_from_tags!(labels, "diri", collect(1:3^3-1))

  re = ReferenceFE(lagrangian, Float64, 1)
  V = TestFESpace(mod, re, dirichlet_tags=["diri"])
  U = TrialFESpace(V, 0)
  Ω = Triangulation(mod)
  dΩ = Measure(Ω, 2)
  a(u, v) = ∫(∇(v) ⋅ ∇(u) + pot * u * v)dΩ
  c(u, v) = ∫(u * v)dΩ

  A = assemble_matrix(a, U, V)
  M = assemble_matrix(c, U, V)
  As = A - shift * M

  debug = true
  if debug
    Vnobc = TestFESpace(mod, re)
    writevtk(Ω, "potential-$(dims[2])", cellfields=["pot" => Gridap.FESpaces.interpolate(x->pot(x), Vnobc)])
    @show "debug"
  end

  return (; A=A, M=M, As=As, V=V, U=U, mod=mod, Ω=Ω)
end

function pu_distance(x, lo, li, ro, ri)
  if x[1] < lo
    return 0.0
  elseif lo <= x[1] < li
    return (x[1]-lo) / (li-lo)
  elseif li <= x[1] < ri
    return 1.0
  elseif ri <= x[1] < ro
    return 1 - (x[1]-ri) / (ro-ri)
  else
    return 0.0
  end
end

function solve_full_new(
  sol_per,
  setup_lin,
  n;
  overlap = 1,
  name = "result"
)
  # dd setup
  psi = FEFunction(sol_per.V, sol_per.evec);
  @time psi_ext = periodic_extension(
    psi, setup_lin.V, unitcell=[0.0 1.0; 0.0 1.0; 0.0 1.0]
  );

  @info "Start partition"
  # 1) unstructured METIS decomp
  # partition = gmsh_partition(
  #   setup_lin.mod, "untitled" .* ["$i" for i = cat(1:n+2, dims=1)]
  # )  # defects
  # partition = gmsh_partition(
  #   setup_lin.mod, "untitled" .* ["$i" for i = cat(1:n, dims=1)]
  # )  # nodefects
  partition = Metis.partition(
    GridapDistributed.compute_cell_graph(setup_lin.mod), n^2
  )
  @time par = Partition(partition, overlap, setup_lin.V, check=false)
  @info "End partition"

  @time begin
    @info "start PU"
    # 1) equal weights pu
    pu = PartitionOfUnity(partition, overlap, setup_lin.V, check=false)
    @info "end PU"
  end

  # for debug
  debug = true
  if debug
    writevtk(setup_lin.Ω, "partition-$(name)-$(n)", cellfields=[
      "partition" => partition
    ])
    puvecs = [
      pu.Ri[i]' * pu.Di[i] * pu.Ri[i] * ones(size(setup_lin.A)[1])
      for i=1:length(pu.Di)
    ]
    pufuns = [FEFunction(setup_lin.V, puvecs[i]) for i=1:length(pu.Di)]
    writevtk(setup_lin.Ω, "pu-$(name)-$(n)", cellfields=[
      (["pu_$i" for i=1:length(pu.Di)] .=> pufuns)...,
    ])
  end

  # return pu

  @info "Start DDs and CCs"
  cc_none = NoCoarseCorrection()
  cc_pefa = GeneralizedNicolaides(setup_lin.As, pu, psi_ext.free_values[:, :])
  ASM1 = PSM(setup_lin.As, par, cc_none)
  ASM2 = PSM(setup_lin.As, par, cc_pefa)
  RAS1 = PSM(setup_lin.As, pu, cc_none)
  RAS2 = PSM(setup_lin.As, pu, cc_pefa)
  @info "End DDs and CCs"

  opts = (;verbose=false, log=true, maxiter=1000, reltol=1E-10)
  opts2 = (;debug=false, maxiter=1000, reltol=1E-10, history=true)

  cgw = CGWrapper(ASM2, opts)
  gmresw = GMRESWrapper(RAS2, (opts..., restart=100))  #! check that enough
  psmw1 = PSMWrapper(RAS1, opts2)
  psmw2 = PSMWrapper(RAS2, opts2)

  old = false
  if old
    opts_eig = (tol = 1E-8, maxiter = 1000)
    @time res_gmresw = LOPCG(setup_lin.A, setup_lin.M, Pl = gmresw; opts_eig...);
    @time res_gmresw = LOPCG(setup_lin.A, setup_lin.M, Pl = RAS2; opts_eig...);
  end

  function modified_lopcg(tol::Function, inner_maxiter::Int64, fused::Bool)
    x0 = normalize!(ones(size(setup_lin.A)[1]))
    x_i = copy(x0)
    x_i1 = Array(I(size(x_i)[1])[:,1])

    resnorms = Float64[]
    inner_its = Int64[]
    outer_tol = 1E-8
    outer_maxiter = 100

    initres = (setup_lin.A * x_i - ddEigenLab.raycoeffg(x_i, setup_lin.A, setup_lin.M) * setup_lin.M * x_i)
    push!(resnorms, norm(initres))

    for i=1:outer_maxiter
      tmp = x_i
      res = (setup_lin.A * x_i - ddEigenLab.raycoeffg(x_i, setup_lin.A, setup_lin.M) * setup_lin.M * x_i)

      if fused
        p = RAS2 \ res
        inner_it_count = 1
      else
        p, info = gmres(
          setup_lin.As, res, verbose=false, log=true, Pl=RAS2, maxiter=inner_maxiter,
          reltol= tol(norm(res)),
          # reltol= inner_tol,
          restart=100
        )
        @info info
        inner_it_count = info.iters
      end

      x_i = ddEigenLab.RRg(setup_lin.A, setup_lin.M, [x_i, p, x_i1])
      x_i = x_i / sqrt((x_i' * setup_lin.M * x_i))
      x_i1 = tmp
      λ = ddEigenLab.raycoeffg(x_i, setup_lin.A, setup_lin.M)
      resnorm = norm((setup_lin.A * x_i - λ * setup_lin.M * x_i))
      @info i, λ, resnorm
      push!(resnorms, resnorm)
      push!(inner_its, inner_it_count)
      if resnorm < outer_tol
        break
      end
    end
    return x_i, (;inner_its=inner_its, resnorms=resnorms)
  end

  inner_maxiter = 1000
  @time evec, fixed = modified_lopcg(x->1E-10, inner_maxiter, false)
  @time evec, adaptive = modified_lopcg(x->min(0.1,x), inner_maxiter, false)
  @time evec, fused = modified_lopcg(x->1E-10, 1, true)
  writevtk(
    setup_lin.Ω, "$(name)-$(n)-out", cellfields=[
      "out" => FEFunction(setup_lin.U, evec)
    ]
  )

  return (; fixed, adaptive, fused)

end

function main()
  ρ = 10
  Lxp = Lyp = Lzp = 1.0
  Lz = 1.0
  δ = 1

  # used for presi:
  pot(x) = 10 * (4 + sin(2*pi*x.data[1]) + sin(4*pi*x.data[1]) + 2sin(2*pi*x.data[2]) + 2sin(4*pi*x.data[2]) + x.data[3])

  # L_arr = [2^i for i=2:5]  # paper
  L_arr = [2^i for i=2:2]

  sol_per = solve_eigen(
    (0, Lxp, 0, Lyp, 0, Lyp), floor.(Int, (ρ*Lxp, ρ*Lyp, ρ*Lzp)), pot,
    isperiodic=(true, true, false), name="limit"
  );

  for L in L_arr
    @time setup_lin = setup_linear_system(
      (0, L, 0, L, 0, Lz), floor.(Int, (ρ*L, ρ*L, ρ*Lz)), pot,
      shift=sol_per.eval
    );
    it = solve_full_new(sol_per, setup_lin, L, overlap=δ, name="full");

    # output numbres
    @show it
    @info sum(it.fixed.inner_its), sum(it.adaptive.inner_its), sum(it.fused.inner_its)

    map(
      ((its, name),) -> CSV.write(
        "its_$(name)_$(L).csv", Tables.table(
          vcat(map(x->x[2]*ones(x[1]), zip([its.inner_its...,1], its.resnorms))...) |> x -> hcat(collect(0:length(x)-1), x)
        ), writeheader=false
      ) , [
        (it.fixed, "fixed"),
        (it.adaptive, "adaptive"),
        (it.fused, "fused"),
      ]
    )
  end
  return
end

main()
