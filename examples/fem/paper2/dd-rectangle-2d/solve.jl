using Gridap
using GridapDistributed
using IterativeSolvers
using LinearAlgebra
using Metis
using CSV, Tables
using DataFrames

using ddEigenLab

function solve_eigen(
  dims,
  partition,
  pot::Function;
  isperiodic=(false, false),
  name="result",
  shift=0.0
)
  mod = CartesianDiscreteModel(dims, partition, isperiodic=isperiodic)
  labels = get_face_labeling(mod)
  add_tag_from_tags!(labels, "diri", collect(1:3^2-1))
  re = ReferenceFE(lagrangian, Float64, 1)
  V = TestFESpace(mod, re, dirichlet_tags=["diri"])
  U = TrialFESpace(V, 0)
  Ω = Triangulation(mod)
  dΩ = Measure(Ω, 2)

  a(u, v) = ∫(∇(v) ⋅ ∇(u) + pot * u * v)dΩ
  c(u, v) = ∫(v * u)dΩ
  A = assemble_matrix(a, U, V)
  M = assemble_matrix(c, U, V)

  evec, eval, _, info = LOPCG(
    A, M, Pl = A - shift * M, tol = 1E-10, maxiter = 1000
  )

  writevtk(Ω, "$(name)-out", cellfields=["out" => FEFunction(U, evec)])
  mod_tmp = CartesianDiscreteModel(dims, partition)
  V_out = TestFESpace(mod_tmp, re)
  V_grid = Gridap.FESpaces.interpolate(pot, V_out)
  writevtk(
    Triangulation(mod_tmp), "$(name)-pot", cellfields=["pot" => V_grid]
  )

  return (; eval=eval, evec=evec, it=info.it, V=V)
end

function setup_linear_system(
  dims,
  partition,
  pot::Function,
  source::Function;
  isperiodic=(false, false),
  shift=0.0
)
  mod = CartesianDiscreteModel(dims, partition, isperiodic=isperiodic)
  labels = get_face_labeling(mod)
  add_tag_from_tags!(labels, "diri", collect(1:3^2-1))
  re = ReferenceFE(lagrangian, Float64, 1)
  V = TestFESpace(mod, re, dirichlet_tags=["diri"])
  U = TrialFESpace(V, 0)
  Ω = Triangulation(mod)
  dΩ = Measure(Ω, 2)

  a(u, v) = ∫(∇(v) ⋅ ∇(u) + pot * u * v - shift * u * v)dΩ
  b(v) = ∫(source * v)dΩ
  A, b = assemble_matrix_and_vector(a, b, U, V)

  return (; A=A, b=b, V=V, mod=mod, Ω=Ω)
end

function pu_distance(x, lo, li, ro, ri)
  if x[1] < lo
    return 0.0
  elseif lo <= x[1] < li
    return (x[1]-lo) / (li-lo)
  elseif li <= x[1] < ri
    return 1.0
  elseif ri <= x[1] < ro
    return 1 - (x[1]-ri) / (ro-ri)
  else
    return 0.0
  end
end

function solve_linear_system_cg_asm(
  sol_per,
  setup_lin,
  Lx,
  ρ;
  overlap = 1,
  name = "result"
)
  # dd setup
  psi = FEFunction(sol_per.V, sol_per.evec);
  @time psi_ext = periodic_extension(psi, setup_lin.V, unitcell=[0 1; 0 1]);

  @info "Start partition"
  factor = 1
  # 1) unstructured METIS decomp
  # partition = Metis.partition(
  #   GridapDistributed.compute_cell_graph(setup_lin.mod), factor*Lx
  # )
  # 2) structured decomp
  @assert mod(ρ, factor) == 0
  @time partition = kron(
    ones(Int32, ρ),
    vcat([kron(ones(Int32, convert(Int64,ρ//factor)),i) for i=convert.(Int32, 1:factor*Lx)]...)
  )
  @time par = Partition(partition, overlap, setup_lin.V)
  @info "End partition"

  @time begin
    @info "start PU"
    # 1) equal weights pu
    # pu = PartitionOfUnity(partition, overlap, setup_lin.V)
    # 2) distance-based PU
    delta = overlap * (1/ρ)
    pu_functions = vcat(
      [x->pu_distance(x, 0.0, 0.0, 1.0+delta, 1.0-delta)],
      [
        x->pu_distance(x, (i-1.0)-delta, (i-1.0)+delta, i+delta, i-delta)
        for i=2:Lx-1
      ],
      [x->pu_distance(x, (Lx-1.0)-delta, (Lx-1.0)+delta, Lx, Lx)],
    )
    @time pu_matrix = hcat([
      Gridap.FESpaces.interpolate(x->pu_functions[i](x), setup_lin.V).free_values
      for i=1:Lx
    ]...)
    @time pu = PartitionOfUnityGeneric(partition, overlap, setup_lin.V, pu_matrix)
    @info "end PU"
  end

  cc_none = NoCoarseCorrection()
  cc_pefa = GeneralizedNicolaides(setup_lin.A, pu, psi_ext.free_values[:, :])
  ASM1 = PSM(setup_lin.A, par, cc_none)
  ASM2 = PSM(setup_lin.A, par, cc_pefa)

  # RAS1 = PSM(setup_lin.A, pu, cc_none)
  # RAS2 = PSM(setup_lin.A, pu, cc_pefa)
  # ddEigenLab.solve(RAS1, setup_lin.b, ones(setup_lin.V.nfree), 1000, 1E-6, debug=true)

  # for debug
  debug = true
  if debug
    writevtk(setup_lin.Ω, "partition-$(name)", cellfields=[
      "partition" => partition
    ])
    puvecs = [
      pu.Ri[i]' * pu.Di[i] * pu.Ri[i] * ones(size(setup_lin.A)[1])
      for i=1:length(pu.Di)
    ]
    pufuns = [FEFunction(setup_lin.V, puvecs[i]) for i=1:length(pu.Di)]
    writevtk(setup_lin.Ω, "pu-$(name)", cellfields=[
      (["pu_$i" for i=1:length(pu.Di)] .=> pufuns)...,
    ])
  end

  x0 = fill!(similar(setup_lin.b),1.0)
  @show res0 = norm(setup_lin.b - setup_lin.A * x0)

  opts = (verbose=true, log=true, maxiter=1000, reltol=1E-8)
  xh, info_cg_asm1 = cg!(copy(x0), setup_lin.A, setup_lin.b; opts..., Pl=ASM1)
  xh, info_cg_asm2 = cg!(copy(x0), setup_lin.A, setup_lin.b; opts..., Pl=ASM2)
  if debug
    uh = FEFunction(setup_lin.V, xh);
    writevtk(setup_lin.Ω, "uh-$(name)", cellfields=["uh" => uh])
  end
  map(
    ((info, methodname),) -> CSV.write(
      "$(methodname)_$(name).csv",
      Tables.table(hcat([1:length(info[:resnorm]), info[:resnorm]./res0]...)),
      writeheader=false
    ) , [
      (info_cg_asm1, "cg_asm1"),
      (info_cg_asm2, "cg_asm2"),
    ]
  )
  return (;asm1=info_cg_asm1.iters, asm2=info_cg_asm2.iters)
end

function main()
  # ρ = 10
  Lxp = Lyp = 1.0
  Ly = 1.0

  # used for presi:
  pot(x) = 100 * cos(pi * x.data[1])^2 * cos(pi * x.data[2])^2
  # pot(x) = 0

  ρ_arr = [i*10 for i=1:3]
  δ_arr = [1,2,3]
  Lx_arr = [2^i for i=2:2]  # for paper: [2^i for i=1:8]

  its1 = zeros(Int64,length(Lx_arr),length(ρ_arr)*length(δ_arr))
  its2 = zeros(Int64,length(Lx_arr),length(ρ_arr)*length(δ_arr))

  for (ρ_i,ρ) in ρ_arr |> enumerate
    sol_per = solve_eigen(
      (0, Lxp, 0, Lyp), floor.(Int, (ρ*Lxp, ρ*Lyp)), pot,
      isperiodic=(true, false), name="limit"
    );
    open("sigma-$(ρ).txt", "w") do io
      write(io, string(sol_per.eval))
    end
    for (Lx_i,Lx) in Lx_arr |> enumerate
      # f = 1/Lx or 1/Lx^2 also possible since max(xh) to infty for f=1
      source(x) = 1
      @time setup_lin = setup_linear_system(
        (0, Lx, 0, Ly), floor.(Int, (ρ*Lx, ρ*Ly)), pot, source,
        shift=sol_per.eval
      );
      for (δ_i,δ) in δ_arr |> enumerate
        @show ρ, Lx, δ
        it = solve_linear_system_cg_asm(sol_per, setup_lin, Lx, ρ, overlap=δ, name="$(ρ)-$(δ)-$(Lx)")
        @show its1[Lx_i,(ρ_i-1)*length(δ_arr)+δ_i] = it.asm1
        @show its2[Lx_i,(ρ_i-1)*length(δ_arr)+δ_i] = it.asm2
      end
    end
  end
  data = hcat(string.(Lx_arr), string.(its1) .* "/" .* string.(its2))
  colnames = map(
    x->join(x,"-"),
    reshape(Base.Iterators.product(ρ_arr,δ_arr) |> collect |> permutedims, :, 1)
  )
  df = DataFrame(
    data,
    ["Lx",colnames...]
  )
  CSV.write("data-iteration-numbers.csv", df)
  return Lx_arr,its1,its2
end

main()
