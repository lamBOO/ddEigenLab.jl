using Gridap
using Plots
using LinearAlgebra

# function chi(x)
#   if x[1]<=0.5
#     return 0.0
#   elseif x[1]<=1.0
#     return x[1]-0.5
#   elseif x[1]<=2.0
#     return 1.0
#   elseif x[1]<=2.5
#     return 2.5-x[1]
#   else
#     return 0.0
#   end
# end

# V(x) = sin(0.25*8*pi*x[1])+sin(0.5*8*pi*x[1])+2
V(x) = 10*sin(2pi*x[1])+2*sin(4*pi*x[1])

Lx = 3
rho = 3
domain = (0, Lx)
model_p = CartesianDiscreteModel(domain,(Lx*rho), isperiodic=(true,))
model_n = CartesianDiscreteModel(domain,(Lx*rho))
model_d = CartesianDiscreteModel(domain,(Lx*rho))
Ω_p = Triangulation(model_p)
Ω_n = Triangulation(model_n)
Ω_d = Triangulation(model_d)
dΩ_p = Measure(Ω_p,4)
dΩ_n = Measure(Ω_n,4)
dΩ_d = Measure(Ω_d,4)

reffe = ReferenceFE(lagrangian,Float64,1)
V0_p = TestFESpace(model_p,reffe;conformity=:H1)
V0_n = TestFESpace(model_n,reffe;conformity=:H1)
V0_d = TestFESpace(model_d,reffe;conformity=:H1,dirichlet_tags = [1,2])
Ug_p = TrialFESpace(V0_p)
Ug_n = TrialFESpace(V0_n)
Ug_d = TrialFESpace(V0_d,[0,0])

a_p(u,v) = ∫(∇(u)⋅∇(v) + V*u*v)*dΩ_p
a_n(u,v) = ∫(∇(u)⋅∇(v) + V*u*v)*dΩ_n
a_d(u,v) = ∫(∇(u)⋅∇(v) + V*u*v)*dΩ_d
b_p(u,v) = ∫(u * v)*dΩ_p
b_n(u,v) = ∫(u * v)*dΩ_n
b_d(u,v) = ∫(u * v)*dΩ_d
A_p = assemble_matrix(a_p,Ug_p,V0_p)
A_n = assemble_matrix(a_n,Ug_n,V0_n)
A_d = assemble_matrix(a_d,Ug_d,V0_d)
B_p = assemble_matrix(b_p,Ug_p,V0_p)
B_n = assemble_matrix(b_n,Ug_n,V0_n)
B_d = assemble_matrix(b_d,Ug_d,V0_d)
eig_p = eigen(((A_p,B_p).|> Array)...)
eig_n = eigen(((A_n,B_n).|> Array)...)
eig_d = eigen(((A_d,B_d).|> Array)...)

# Observations:
@assert eig_n.values[1] < eig_p.values[1] "n smaller than p"
P = [eig_p.vectors[:,1]...,eig_p.vectors[:,1][1]] |> x-> (I - x * x')
@assert abs(eigen(P' * A_n * P).values[1] - eig_p.values[1]) > abs(eigen(P' * A_n * P).values[2] - eig_p.values[1]) "zero energy projection has smallest eigenvalue further away from origin than second smallest"

# plot(eig_n.vectors[:,1])
