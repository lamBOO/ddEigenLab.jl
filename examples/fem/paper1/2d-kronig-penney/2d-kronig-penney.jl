using Gridap
using LinearAlgebra
using DataFrames
using CSV

using ddEigenLab

function get_limit_eigenvalue(V)

  ρ = 10
  domain = (0, 1, 0, 1, 0, 1)
  partition = (1 * ρ, 1 * ρ, 1 * ρ)
  model = CartesianDiscreteModel(
    domain, partition; isperiodic=(true, true, false)
  )  # quads
  # model = simplexify(model)  # tets

  labels = get_face_labeling(model)
  add_tag_from_tags!(labels, "diri", collect(1:3^3-1))

  order = 1
  reffe = ReferenceFE(lagrangian, Float64, order)
  V0 = TestFESpace(model, reffe; conformity=:H1, dirichlet_tags = ["diri"])

  Ug = TrialFESpace(V0, 0)

  degree = 2
  Ω = Triangulation(model)
  dΩ = Measure(Ω,degree)

  b(v) = ∫( 1 * v ) * dΩ

  a1(u,v) = ∫( ∇(v) ⋅ ∇(u) + V * u * v ) * dΩ
  op1 = AffineFEOperator(a1, b, Ug, V0)
  stiffnessmat = assemble_matrix(a1, Ug, V0)

  a2(u,v) = ∫( v * u ) * dΩ
  op2 = AffineFEOperator(a2, b, Ug, V0)
  massmat = assemble_matrix_and_vector(a2, b, Ug, V0)[1]

  @info "start eigensolve"
  σlimit = 50.0
  # eigvec, eigval, errors, info = IP(
  #   stiffnessmat, massmat, σlimit, 1, 1, 1E-10, 10000, 1
  # )
  eigvec, eigval, errors, info = LOPCG(
    stiffnessmat, massmat, Pl = (stiffnessmat - σlimit * massmat), tol = 1E-10,
    maxiter = 10000
  )
  @info "start eigensolve"
  uh = FEFunction(op1.trial, eigvec);
  writevtk(Ω, "2d-kronig-limit-eigfunc", cellfields = ["uh" => uh])
  println("limit eigenvalue: $eigval")
  open("2d-kronig-limit-eigenval.txt", "w") do io
    write(io, string(eigval))
  end

  return eigval

end

function solve_full_system(Lx,V,σ)
  Lz = 1
  ρ = 10

  domain = (0, Lx, 0, Lx, 0, Lz)
  partition = (Lx * ρ, Lx * ρ, Lz * ρ)
  model = CartesianDiscreteModel(domain, partition)  # quads
  # model = simplexify(CartesianDiscreteModel(domain, partition))  # tets

  labels = get_face_labeling(model)
  add_tag_from_tags!(labels, "diri", collect(1:3^3-1))

  order = 1
  reffe = ReferenceFE(lagrangian, Float64, order)
  V0 = TestFESpace(model, reffe; conformity=:H1, dirichlet_tags = ["diri"])

  Ug = TrialFESpace(V0, 0)

  degree = 2
  Ω = Triangulation(model)
  dΩ = Measure(Ω,degree)

  b(v) = ∫( 1 * v ) * dΩ

  a1(u,v) = ∫( ∇(v) ⋅ ∇(u) + V * u * v ) * dΩ
  op1 = AffineFEOperator(a1, b, Ug, V0)
  stiffnessmat = assemble_matrix(a1, Ug, V0)

  a2(u,v) = ∫( v * u ) * dΩ
  op2 = AffineFEOperator(a2, b, Ug, V0)
  massmat = assemble_matrix_and_vector(a2, b, Ug, V0)[1]

  @info "using shift: $σ"

  @info "start eigensolve"
  eigvec, eigval, errors, info = LOPCG(
    stiffnessmat, massmat, Pl = (stiffnessmat - σ * massmat), tol = 1E-10,
    maxiter = 10000
  )
  # eigvec, eigval, errors, info = IP(
  #   stiffnessmat, massmat, σ, 1, 1, 1E-10, 10000, 1
  # )
  @info "end eigensolve"

  uh = FEFunction(op1.trial, eigvec);
  writevtk(Ω, "2d-kronig-result_$Lx", cellfields = ["uh" => uh])
  writevtk(
    Ω, "2d-kronig-pot_$Lx", cellfields = [
      "V" => Gridap.FESpaces.interpolate(
        V, TestFESpace(model, reffe; conformity=:H1)
      )
    ]
  )

  return (
    eigvec, eigval, errors, info, length(model.grid_topology.vertex_coordinates)
  )
end

df = DataFrame(
  L = Int[],
  nn = Int[],
  lambda1 = Float64[],
  maxPhi1 = Float64[],
  kit = Int[],
  t = Float64[],
  tperdofs = Float64[],
)


MAX_EXP = 2  # = 5 for publication
for i in 0:MAX_EXP
  L = 2^i

  # Kronig-Penney Potential
  R = 0.5
  V(x) = if (
    ((x[1]%1) - R)^2 < 0.25^2
    && ((x[2]%1) - R)^2 < 0.25^2
    && ((x[3]%1) - R)^2 < 0.25^2
  ) 0 else 100 end

  σ = get_limit_eigenvalue(V)
  # for precompiling
  if (i==0)
    solve_full_system(L, V, σ)
  end
  t = @elapsed eigvec, eigval, errors, info, nn = solve_full_system(L, V, σ)
  @info t
  push!(df,
    (
      L, nn, eigval,
      findmax(eigvec)[1], info.it, t, t/L^2
    )
  )
end

open("2d-kronig-penney.csv","w") do io
  CSV.write(io, df)
end
