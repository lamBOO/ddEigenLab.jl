// Command line Parameters
If(!Exists(p))
  p = 6;
EndIf

res = 100;
Mesh.CharacteristicLengthMax = 1.0 * 2^(-p);
Mesh.MshFileVersion = 2.0;

Point(0) = {1 + 0, 0, 0, p};
Point(1) = {1 + -0.9, Sin(Acos(-0.9)), 0, p};
Point(2) = {1 + -1, 0, 0, p};
Point(3) = {1 + -0.9, -Sin(Acos(-0.9)), 0, p};
Point(4) = {1 + 0.9, -Sin(Acos(0.9)), 0, p};
Point(5) = {1 + 1, 0, 0, p};
Point(6) = {1 + 0.9, Sin(Acos(0.9)), 0, p};

Circle (1000) = {3,0,4};
Circle (1001) = {6,0,1};
Physical Line("top") = {1001};
Physical Line("bot") = {1000};

Line(1100) = {1, 3};
Line(1101) = {4, 6};
Physical Line("left") = {1100};
Physical Line("right") = {1101};

Curve Loop(2000) = {1100, 1000, 1101, 1001};
Plane Surface(3000) = {2000};
Physical Surface("mesh") = {3000};

Periodic Line {1101} = {1100} Translate {1.8, 0, 0};

// Mesh.Algorithm = 1;
