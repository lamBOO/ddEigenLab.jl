using DataFrames: DataAPI
using Base: exprarray
using Gridap
using GridapGmsh
using Plots
using DataFrames
using LinearAlgebra
using CSV
using IterativeSolvers

using ddEigenLab

R = 1  # radius of disks
a = 0.0001  # cutoff radisu
Z = 1  # strength
S = 0  # shift
p = 8  # inverse mesh size exponent

pot1(x) = if (
  (sqrt(x[1]^2+x[2]^2) <= R) &&
  (sqrt(x[1]^2+x[2]^2) <= a)
)
  - Z * 1 / a + S
elseif (
  (sqrt(x[1]^2+x[2]^2) <= R) &&
  (sqrt(x[1]^2+x[2]^2) > a)
)
  - Z * 1 / sqrt(x[1]^2+x[2]^2) + S
else
  0
end

pot(x, Ns) = sum([
  pot1(x-VectorValue([p,0]))
  for p in [1 + (i-1) * 1.8 for i in Ns]
])

# run(`gmsh -2 periodic_structured.geo -o periodic_structured.msh -setnumber p $p`)
mshfile = joinpath("periodic_structured.msh")
model = GmshDiscreteModel(mshfile)
reffe = ReferenceFE(lagrangian, Float64, 1)
Vs = TestFESpace(model,reffe,dirichlet_tags=["top", "bot"])
U = TrialFESpace(Vs, 0)
Ω = Triangulation(model)
dΩ = Measure(Ω, 2)
a1(u,v) = ∫( ∇(u) ⋅ ∇(v) + (x->pot(x, 0:2)) * u * v )dΩ
a2(u,v) = ∫( u * v )dΩ
b(v) = ∫( 1 * v )dΩ
op = AffineFEOperator(a1, b, U, Vs)

stiffnessmat = assemble_matrix(a1, U, Vs)
massmat = assemble_matrix_and_vector(a2, b, U, Vs)[1]
# eigvec, eigval, errors, info = IP(stiffnessmat, massmat, 0, 1, 1, 1E-10, 1000, 1)
eigvec, eigval, errors, info = LOPCG(
  stiffnessmat, massmat, Pl = (stiffnessmat - 0 * massmat), tol = 1E-10,
  maxiter = 1000
)
phi = FEFunction(op.trial, eigvec);
writevtk(Ω,"union_basecell",cellfields=["phi"=>phi])
println("limit eigenvalue: $eigval")
open("barrier-limit-eigenval.txt", "w") do io
  write(io, string(eigval))
end

itarray = []
# exparray = 0:5  # for publication
exparray = 0:1  #

df = DataFrame(
  N = Int[],
  nn = Int[],
  lambda1 = Float64[],
  maxPhi1 = Float64[],
  kit = Int[],
  cond = Float64[],
  t = Float64[],
  tnrat = Float64[],
)

# generate meshes
# for i in exparray
#   N = 2^i
#   run(`gmsh -2 full_structured.geo -o full_structured_$N.msh -setnumber N $N -setnumber p $p`)
# end

for i in exparray
  N = 2^i

  @info "Start problem"
  mshfile2 = joinpath("full_structured_$N.msh")
  model2 = GmshDiscreteModel(mshfile2)
  reffe2 = ReferenceFE(lagrangian,Float64,1)
  V2 = TestFESpace(model2,reffe,dirichlet_tags=["outer"])
  U2 = TrialFESpace(V2,0)
  Ω2 = Triangulation(model2)
  dΩ2 = Measure(Ω2,2)
  a12(u,v) = ∫( ∇(u)⋅∇(v) + (x->pot(x, 0:N+1)) * u * v )dΩ2
  a22(u,v) = ∫( u * v )dΩ2
  b2(v) = ∫( 1 * v )dΩ2
  op2 = AffineFEOperator(a12,b2,U2,V2)
  @info "End problem"

  @info "Start matrices"
  stiffnessmat2 = assemble_matrix(a12, U2, V2)
  massmat2 = assemble_matrix_and_vector(a22, b2, U2, V2)[1]
  @info "End matrices"

  @info "Start eigensolver"
  σ = eigval
  # t = @elapsed eigvec2, eigval2, errors2, info2 = IP(stiffnessmat2, massmat2, σ, 1, 1, 1E-10, 1000, 1)
  t = @elapsed eigvec2, eigval2, errors2, info2 = LOPCG(
    stiffnessmat2, massmat2, Pl = (stiffnessmat2 - σ * massmat2), tol = 1E-10,
    maxiter = 1000
  )
  @info "End eigensolver"

  @info "Start output"
  phi2 = FEFunction(op2.trial, eigvec2);
  writevtk(Ω2,"union_$(i)",cellfields=["phi"=>phi2])
  @info "End output"

  @info "Start data"
  push!(itarray, info2.it)
  push!(df,
    (N, length(model2.grid_topology.vertex_coordinates), eigval2, findmax(eigvec2)[1], info2.it, lobpcg(stiffnessmat2 - σ * massmat2, true, 1, maxiter=1000, tol=1E-10).λ[1]/lobpcg(stiffnessmat2 - σ * massmat2, false, 1, maxiter=1000, tol=1E-10).λ[1], t, t/N)
  )
  @info "End data"
end

open("union-of-spheres.csv","w") do io
  CSV.write(io, df)
end
