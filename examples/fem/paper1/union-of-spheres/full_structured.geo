// Command line Parameters
If(!Exists(p))
  p = 6;
EndIf
If(!Exists(N))
  N = 3;
EndIf

pts = 2^(p)+1;

// Settings
res = 100;
Mesh.CharacteristicLengthMax = 1.0 * 2^(-p);
Mesh.MshFileVersion = 2.0;




// //+
// Point(0) = {1 + 0, 0, 0, p};
// Point(1) = {1 + -0.9, Sin(Acos(-0.9)), 0, p};
// Point(2) = {1 + -1, 0, 0, p};
// Point(3) = {1 + -0.9, -Sin(Acos(-0.9)), 0, p};
// Point(4) = {1 + 0.9, -Sin(Acos(0.9)), 0, p};
// Point(5) = {1 + 1, 0, 0, p};
// Point(6) = {1 + 0.9, Sin(Acos(0.9)), 0, p};



d = 0.1;
D = 2 - 2 * 0.1;

// Left
Point(100) = {0 + 0.1, +Sin(Acos(-0.9)), 0, p};
Point(200) = {0 + 0.1, -Sin(Acos(-0.9)), 0, p};

// Middle
For i In {1:N}

  Point(000 + i) = {1 + (i-1) * D, 0, 0, p};
  Point(100 + i) = {2 -  0.1 + (i-1) * D, +Sin(Acos(-0.9)), 0, p};
  Point(200 + i) = {2 -  0.1 + (i-1) * D, -Sin(Acos(-0.9)), 0, p};

  Line (1000 + i) = {100 + i, 200 + i};
  Transfinite Curve(1000 + i) = pts;

  // Point(10 * i + 0) = {1 + 0.8 * i + 0, 0, 0, p};
  // Point(10 * i + 1) = {1 + 0.8 * i + -0.9, Sin(Acos(-0.9)), 0, p};
  // Point(10 * i + 3) = {1 + 0.8 * i + -0.9, -Sin(Acos(-0.9)), 0, p};
  // Point(10 * i + 4) = {1 + 0.8 * i + 0.9, -Sin(Acos(0.9)), 0, p};
  // Point(10 * i + 6) = {1 + 0.8 * i + 0.9, Sin(Acos(0.9)), 0, p};
EndFor

For i In {1:N}
  Circle (1100 + i) = {100 + i - 1, 000 + i, 100 + i};
  Circle (1200 + i) = {200 + i, 000 + i, 200 + i - 1};
  Transfinite Curve(1100 + i) = Round(1.2*pts/2)*2+1;
  Transfinite Curve(1200 + i) = Round(1.2*pts/2)*2+1;
EndFor

// Left
Line (1000) = {100, 200};
Transfinite Curve(1000) = pts;
Circle (1100) = {200, 1, 100};
Curve Loop(2000 + N  + 1) = {1000,1100};
Plane Surface(3000 + N  + 1) = {2000 + N  + 1};

// Right
Circle (1300) = {100 + N, N, 200 + N};
Curve Loop(2000 + N  + 2) = {1000 + N,-1300};
Plane Surface(3000 + N  + 2) = {2000 + N  + 2};

For i In {1:N}
  Curve Loop(2000 + i) = {-(1000 + i - 1),1100 + i,1000 + i,1200 + i};
  Plane Surface(3000 + i) = {2000 + i};
  Transfinite Surface(3000 + i) = {} AlternateLeft;
  // Transfinite Surface(3000 + i) = {} Left;
EndFor

// Curve Loop(2100) = {-1100, -1201:-(1200 + N), -1300, -(1100 + N):-1101};
Physical Line("outer") = {-1100, -1201:-(1200 + N), -1300, -(1100 + N):-1101};

Physical Surface("mesh") = {3001:3000 + N + 2};

// Mesh.Algorithm = 1; // Delaunay 2D
