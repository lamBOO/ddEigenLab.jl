using Gridap
using LinearAlgebra
using SparseArrays
using LinearMaps

using ddEigenLab

ρ = 100

d = 2

Z = 10

# -> x and y periodic
# V(x) = Z^2 * sin(x[1]*π)^2 * sin(x[2]*π)^2

# -> not y-periodic but zero and same derivative value on boundary
# V(x) = Z^2 * sin(x[1]*π)^2 * (27*x[2]^2*(1-x[2])/4)^2

# -> no y-structure at all, different values, not periodic
V(x) = Z^2 * sin(x[1]*π)^2 * x[2]^2

Lxp = Lyp = 1
domain = (0, Lxp, 0, Lyp)
partition = (Lxp * ρ, Lyp * ρ)
model2 = CartesianDiscreteModel(domain, partition; isperiodic=(true, false))
labels2 = get_face_labeling(model2)
add_tag_from_tags!(labels2, "diri", collect(1:3^d-1))
order = 1
reffe2 = ReferenceFE(lagrangian, Float64, order)
V02 = TestFESpace(model2, reffe2; conformity=:H1, dirichlet_tags = ["diri"])
Ug2 = TrialFESpace(V02, 0)
degree = 2
Ω2 = Triangulation(model2)
dΩ2 = Measure(Ω2, degree)

b2(v) = ∫( 1 * v ) * dΩ2  # dummy
a11(u,v) = ∫( ∇(v) ⋅ ∇(u) + V * u * v ) * dΩ2
op11 = AffineFEOperator(a11, b2, Ug2, V02)
stiffnessmat2 = assemble_matrix(a11, Ug2, V02)
a22(u,v) = ∫( v * u ) * dΩ2
op22 = AffineFEOperator(a22, b2, Ug2, V02)
massmat2 = assemble_matrix_and_vector(a22, b2, Ug2, V02)[1]

@info "start eigensolve periodic cell"

eigvec2, eigval2, errors, info = IP(stiffnessmat2, massmat2, 0, 1, 1, 1E-10, 1000)

@info "end eigensolve periodic cell"

Ly = 1
# for Lx in vcat([32])
for Lx in vcat([2^i for i=0:4], [31.41])

  domain = nothing
  partition = nothing
  if d==2
    domain = (0, Lx, 0, Ly)
    partition = (Lx * ρ, Ly * ρ)
  elseif d==3
    domain = (0, Lx, 0, Ly, 0, Ly)
    partition = (Lx * ρ, Ly * ρ, Ly * ρ)
  else
    error("we need d=2,3")
  end
  model = CartesianDiscreteModel(domain, partition; isperiodic=(false, false))  # quads
  # model = simplexify(CartesianDiscreteModel(domain, partition))  # tets

  labels = get_face_labeling(model)
  add_tag_from_tags!(labels, "diri", collect(1:3^d-1))

  order = 1
  reffe = ReferenceFE(lagrangian, Float64, order)
  V0 = TestFESpace(model, reffe; conformity=:H1, dirichlet_tags = ["diri"])

  Ug = TrialFESpace(V0, 0)

  degree = 2
  Ω = Triangulation(model)
  dΩ = Measure(Ω,degree)

  b(v) = ∫( 1 * v ) * dΩ  # dummy

  a1(u,v) = ∫( ∇(v) ⋅ ∇(u) + V * u * v ) * dΩ
  op1 = AffineFEOperator(a1, b, Ug, V0)
  stiffnessmat = assemble_matrix(a1, Ug, V0)

  a2(u,v) = ∫( v * u ) * dΩ
  op2 = AffineFEOperator(a2, b, Ug, V0)
  massmat = assemble_matrix_and_vector(a2, b, Ug, V0)[1]

  @info "start eigensolve"

  σ = eigval2

  tol = 1E-10
  maxit = 20

  eigvec, eigval, errors, info = IP(stiffnessmat, massmat, 0, 1, 1, tol, maxit, 1)
  writeErrorsToCsv(errors, "errors_IP_000_$Lx")

  eigvec, eigval, errors, info = IP(stiffnessmat, massmat, 0.99*σ, 1, 1, tol, maxit, 1)
  writeErrorsToCsv(errors, "errors_IP_099_$Lx")

  eigvec, eigval, errors, info = IP(stiffnessmat, massmat, σ, 1, 1, tol, maxit, 1)
  writeErrorsToCsv(errors, "errors_IP_100_$Lx")

  eigvec, eigval, errors, info = LOPCG(
    stiffnessmat, massmat, Pl = (stiffnessmat - 0 * massmat), tol = tol,
    maxiter = maxit
  )
  writeErrorsToCsv(errors, "errors_LOPCG_000_$Lx")


  eigvec, eigval, errors, info = LOPCG(
    stiffnessmat, massmat, Pl = (stiffnessmat - 0.99*σ * massmat), tol = tol,
    maxiter = maxit
  )
  writeErrorsToCsv(errors, "errors_LOPCG_099_$Lx")


  eigvec, eigval, errors, info = LOPCG(
    stiffnessmat, massmat, Pl = (stiffnessmat - σ * massmat), tol = tol,
    maxiter = maxit
  )
  writeErrorsToCsv(errors, "errors_LOPCG_100_$Lx")

  @info "end eigensolve"

end
