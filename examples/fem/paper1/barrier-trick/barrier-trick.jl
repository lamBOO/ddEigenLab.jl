using Gridap
using LinearAlgebra
using Distributed

using ddEigenLab

Lx = 5.6
Ly = 1.0
ρ = 100
dim = 2

domain = nothing
partition = nothing
if dim==2
  domain = (0, Lx, -Ly, Ly)
  partition = (round(Lx * ρ), round(2*Ly * ρ))  # FIXME
elseif dim==3
  domain = (0, Lx, 0, Ly, 0, Ly)
  partition = (Lx * ρ, Ly * ρ, Ly * ρ)
else
  error("we need d=2,3")
end
model = CartesianDiscreteModel(domain, partition)  # quads
# model = simplexify(CartesianDiscreteModel(domain, partition))  # tets

labels = get_face_labeling(model)
add_tag_from_tags!(labels, "diri", collect(1:3^dim-1))

order = 1
reffe = ReferenceFE(lagrangian, Float64, order)
V0 = TestFESpace(model, reffe; conformity=:H1, dirichlet_tags = ["diri"])

Ug = TrialFESpace(V0, 0)

degree = 2
Ω = Triangulation(model)
dΩ = Measure(Ω,degree)

R = 1.0
d = 0.1
dist = 2*(R - d)

σ = 0
for i=0:5:25
  @info i

  global σ

  a = 2^i * 1.0

  ω = 9
  # sx = d + 2(R-d)  # Middle = Well
  sx = d  # Middle = Barrier
  sy = (R-d)
  V2(x) = 100 * (
    1 - sin(
      (ω*pi*(x[1]-sx))/(2*(R-d))
    )*sin(
      (ω*pi*(x[2]-sy))/(2*(R-d))
    )
  )

  V(x) = if (
      (sqrt((x[1] - (R + 0 * dist))^2+(x[2]-0.0)^2) <= R)
    || (sqrt((x[1] - (R + 1 * dist))^2+(x[2]-0.0)^2) <= R)
    || (sqrt((x[1] - (R + 2 * dist))^2+(x[2]-0.0)^2) <= R)
  ) V2([x[1],x[2]]) else V2([x[1],x[2]]) + a end

  writevtk(
    Ω, "barrier-pot_$i", cellfields = [
      "V" => Gridap.FESpaces.interpolate(
        V, TestFESpace(model, reffe; conformity=:H1)
      )
    ]
  )

  b(v) = ∫( 1 * v ) * dΩ

  a1(u,v) = ∫( ∇(v) ⋅ ∇(u) + V * u * v ) * dΩ
  op1 = AffineFEOperator(a1, b, Ug, V0)
  stiffnessmat = assemble_matrix(a1, Ug, V0)

  a2(u,v) = ∫( v * u ) * dΩ
  op2 = AffineFEOperator(a2, b, Ug, V0)
  massmat = assemble_matrix_and_vector(a2, b, Ug, V0)[1]

  @info "start eigensolve"

  eigvec, eigval, errors, info = LOPCG(
    stiffnessmat, massmat, Pl = (stiffnessmat - σ * massmat), tol = 1E-10,
    maxiter = 10000
  )
  # eigvec, eigval, errors, info = IP(
  #   stiffnessmat, massmat, σ, 1, 1, 1E-10, 10000, 1
  # )

  σ = eigval  # use eigval for next iteration, works by minmax principle and a>0

  @info "end eigensolve"

  # writevtk(model, "model")
  uh = FEFunction(op1.trial, eigvec);
  writevtk(Ω, "barrier-result_$i", cellfields = ["uh" => uh])
end
