# Idea
What happens if we solve cell problems for all subdomains and "glue" them together, apply Rayleigh Ritz and then use the resulting value as shift?

# Conclusion
Probably doesn't work in general since we need to be continuous between subdomains and for d>=2, that's not easy to enforce.
