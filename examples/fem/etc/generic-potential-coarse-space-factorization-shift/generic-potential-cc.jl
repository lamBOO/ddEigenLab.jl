using Gridap
using LinearAlgebra
using SparseArrays
using LinearMaps
using Random
Random.seed!(1234);

using ddEigenLab


# TODO
# Make from zero to Lx to avoid strange effects
# Make Rayleigh Ritz with cell solution to Neumann Operator since Dirichlet doesn't give right results

Lx = 20
ρ = 50

V(x) = cos(pi*(x[1]+0)/2)^2 + cos(pi*(x[1]+0)/4)^2
# V(x) = cos(pi*x[1])^2

cellsols = []
cellsolsvals = []

for i=1:2*Lx

  Lxp = 1
  domain = (-Lx+(i-1)*Lxp, -Lx+i*Lxp)
  partition = (Lxp * ρ)
  # model2 = CartesianDiscreteModel(domain, partition; isperiodic=(true,))
  model2 = CartesianDiscreteModel(domain, partition)
  labels2 = get_face_labeling(model2)

  reffe2 = ReferenceFE(lagrangian, Float64, 1)
  V02 = TestFESpace(model2, reffe2; conformity=:H1)
  Ug2 = TrialFESpace(V02, 0)
  Ω2 = Triangulation(model2)
  dΩ2 = Measure(Ω2, 2)

  b2(v) = ∫( 1 * v ) * dΩ2  # dummy
  a11(u,v) = ∫( ∇(v) ⋅ ∇(u) + V * u * v ) * dΩ2
  op11 = AffineFEOperator(a11, b2, Ug2, V02)
  stiffnessmat2 = assemble_matrix(a11, Ug2, V02)
  a22(u,v) = ∫( v * u ) * dΩ2
  op22 = AffineFEOperator(a22, b2, Ug2, V02)
  massmat2 = assemble_matrix_and_vector(a22, b2, Ug2, V02)[1]

  eigvec2, eigval2, errors, info = IP(stiffnessmat2, massmat2, 0, 1, 1, 1E-10, 1000)

  push!(cellsols, eigvec2)
  push!(cellsolsvals, eigval2)

  uh = FEFunction(op11.trial, eigvec2);
  writevtk(Ω2, "results_cell_$(i)", cellfields = ["uh" => uh])
end

# # Apply PU and extend
# cellsolsext = [zeros(2*Lx*ρ-1) for i=1:2*Lx]
# for i=1:2*Lx
#   if i == 1
#     # left
#     cellsolsext[i][(i-1)*ρ+1:i*ρ] = [cellsols[i][1:end-1];cellsols[i][1]]
#   elseif i != 2*Lx
#     cellsolsext[i][(i-1)*ρ:i*ρ] = [cellsols[i][1:end];cellsols[i][1]]
#   else
#     # right
#     cellsolsext[i][(i-1)*ρ:i*ρ-1] = cellsols[i]
#   end

#   # TODO FIXTHIS
#   # EXPORT RAYLEIGH RITZ SOLUTION, DISCOUNTINUOUS!
#   # Make normalized
#   if i == 1
#     # left
#     interface_mean = (cellsolsext[i][(i-1)*ρ+1] + cellsolsext[i+1][i*ρ])/4
#     cellsolsext[i][(i-1)*ρ+1] = interface_mean
#     cellsolsext[i+1][i*ρ] = interface_mean
#     @show interface_mean
#   elseif i != 2*Lx
#     interface_mean = (cellsolsext[i][(i-1)*ρ] + cellsolsext[i+1][i*ρ])/4
#     cellsolsext[i][(i-1)*ρ] = interface_mean
#     cellsolsext[i+1][i*ρ] = interface_mean
#     @show interface_mean
#   else
#     # right
#     # cellsolsext[i][(i-1)*ρ:i*ρ-1] = cellsols[i]
#   end
#   # if i != 2*Lx
#   #   interface_mean = (cellsolsext[i][end] + cellsolsext[i+1][1])/4
#   #   @show interface_mean
#   #   cellsolsext[i][end] = interface_mean
#   #   cellsolsext[i+1][1] = interface_mean
#   # end

# end
# Z = hcat([cellsolsext[i] for i=1:2*Lx]...)





domain = (-Lx, Lx+0)
partition = (Lx * 2 * ρ + 0*ρ)
model = CartesianDiscreteModel(domain, partition)
labels = get_face_labeling(model)
add_tag_from_tags!(labels, "diri", collect(1:2))
reffe = ReferenceFE(lagrangian, Float64, 1)
V0 = TestFESpace(model, reffe; conformity=:H1, dirichlet_tags = ["diri"])
# V0 = TestFESpace(model, reffe; conformity=:H1)
Ug = TrialFESpace(V0, 0)
Ω = Triangulation(model)
dΩ = Measure(Ω, 2)

b(v) = ∫( 1 * v ) * dΩ  # dummy

a1(u,v) = ∫( ∇(v) ⋅ ∇(u) + V * u * v ) * dΩ
op1 = AffineFEOperator(a1, b, Ug, V0)
stiffnessmat = assemble_matrix(a1, Ug, V0)

a2(u,v) = ∫( v * u ) * dΩ
op2 = AffineFEOperator(a2, b, Ug, V0)
massmat = assemble_matrix_and_vector(a2, b, Ug, V0)[1]

@info "start eigensolve"

maxit=1000
@time eigvec, eigval, errors, info = IP(stiffnessmat, massmat, 0, 1, 1, 1E-10, maxit, 1)

@info "end eigensolve"

writevtk(model, "model")
uh = FEFunction(op1.trial, eigvec);
writevtk(Ω, "results", cellfields = ["uh" => uh])

Vgrid = Gridap.FESpaces.interpolate(V, V0)
writevtk(Ω, "pot", cellfields = ["pot" => Vgrid])





# out = eigen(Z' * stiffnessmat * Z, Z' * massmat * Z)
# u0 = sum(out.vectors[:,1] .* cellsolsext)
# uh = FEFunction(op1.trial, u0);
# writevtk(Ω, "results_test", cellfields = ["uh" => uh])
