using Gridap
using LinearAlgebra
using SparseArrays
using LinearMaps
using Random
Random.seed!(1234);

using ddEigenLab


# TODO
# Make from zero to Lx to avoid strange effects
# Make Rayleigh Ritz with cell solution to Neumann Operator since Dirichlet doesn't give right results

ρ = 100

# V(x) = cos(pi*(x[1]+0)/2)^2 + cos(pi*(x[1]+0)/4)^2
# V(x) = 1
V(x) = cos(pi*x[1])^2
# V(x) = x[1]

Lxs = 0
Lxe = 1
Ly = 1
domain = (Lxs, Lxe, 0, 1)
partition = ((Lxe-Lxs) * ρ, Ly * ρ)

model2 = CartesianDiscreteModel(domain, partition)
# model2 = CartesianDiscreteModel(domain, partition; isperiodic=(true,false))
labels2 = get_face_labeling(model2)

add_tag_from_tags!(labels2, "diri", collect(1:6))  # 7,8 = xleft xright

reffe2 = ReferenceFE(lagrangian, Float64, 1)
V02 = TestFESpace(model2, reffe2; conformity=:H1, dirichlet_tags = ["diri"])
Ug2 = TrialFESpace(V02, 0)
Ω2 = Triangulation(model2)
dΩ2 = Measure(Ω2, 2)

Γ = BoundaryTriangulation(model2)
dΓ = Measure(Γ, 2)
n_Γ = get_normal_vector(Γ)

b2(v) = ∫( 1 * v ) * dΩ2  # dummy
a11(u,v) = ∫( ∇(v) ⋅ ∇(u) + V * u * v ) * dΩ2
# a11(u,v) = ∫( ∇(v) ⋅ ∇(u) + V * u * v ) * dΩ2 - ∫( v*(∇(u)⋅n_Γ) )*dΓ  # free BC
op11 = AffineFEOperator(a11, b2, Ug2, V02)
stiffnessmat2 = assemble_matrix(a11, Ug2, V02)
a22(u,v) = ∫( v * u ) * dΩ2
op22 = AffineFEOperator(a22, b2, Ug2, V02)
massmat2 = assemble_matrix_and_vector(a22, b2, Ug2, V02)[1]

eigvec2, eigval2, errors, info = IP(stiffnessmat2, massmat2, 0, 1, 1, 1E-10, 1000)

uh = FEFunction(op11.trial, eigvec2);
writevtk(Ω2, "results", cellfields = ["uh" => uh])

Vgrid = Gridap.FESpaces.interpolate(V, V02)
writevtk(Ω2, "pot", cellfields = ["pot" => Vgrid])





# out = eigen(Z' * stiffnessmat * Z, Z' * massmat * Z)
# u0 = sum(out.vectors[:,1] .* cellsolsext)
# uh = FEFunction(op1.trial, u0);
# writevtk(Ω, "results_test", cellfields = ["uh" => uh])
