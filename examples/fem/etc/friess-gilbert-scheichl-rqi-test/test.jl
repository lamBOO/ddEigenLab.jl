using ddEigenLab
using LinearAlgebra
import CairoMakie

"""
Recreate the figure 2 from [1] (page 14).
[1]: https://arxiv.org/pdf/2312.02847.pdf
"""

function A_mat(s)
  return diagm([-1,s,1])
end

function unit_simplex(p, q, r)
  x1 = [1,0,0]
  x2 = [0,1,0]
  x3 = [0,0,1]
  return p * x1 + q * x2 + r * x3
end

s1 = 0.1
s2 = 0.98
h = 0.005

pqr_arr = [[p, q, 1-p-q] for p=0:h:1 for q=0:h:1-p]
pts = hcat(map(pqr->unit_simplex(pqr...), pqr_arr)...)'
eigenvals_old_s1 = Vector{Float64}(undef, size(pts)[1])
eigenvals_old_s2 = Vector{Float64}(undef, size(pts)[1])
eigenvals_new_s1 = Vector{Float64}(undef, size(pts)[1])
eigenvals_new_s2 = Vector{Float64}(undef, size(pts)[1])

for i=1:size(pts)[1]
  # println("i = $i/$(size(pts)[1])")
  res = RQI_old(A_mat(s1), I(3), x=pts[i,:], verbose=false)
  eigenvals_old_s1[i] = res[2]
  res = RQI_old(A_mat(s2), I(3), x=pts[i,:], verbose=false)
  eigenvals_old_s2[i] = res[2]
  res = RQI_new(A_mat(s1), I(3), x=pts[i,:], verbose=false)
  eigenvals_new_s1[i] = res[2]
  res = RQI_new(A_mat(s2), I(3), x=pts[i,:], verbose=false)
  eigenvals_new_s2[i] = res[2]
end

B = [
  1 0.5 0
  0 1 0
]

pts2d = hcat(map(pqr->B*pqr, [pts[i,:] for i=1:size(pts)[1]])...)'

pts2d_sep_old_s1 = [
  pts2d[map(val -> findmin(abs.(val .- [-1,s1,1]))[2], eigenvals_old_s1) .== i, :]
  for i=1:3
]
pts2d_sep_old_s2 = [
  pts2d[map(val -> findmin(abs.(val .- [-1,s2,1]))[2], eigenvals_old_s2) .== i, :]
  for i=1:3
]
pts2d_sep_new_s1 = [
  pts2d[map(val -> findmin(abs.(val .- [-1,s1,1]))[2], eigenvals_new_s1) .== i, :]
  for i=1:3
]
pts2d_sep_new_s2 = [
  pts2d[map(val -> findmin(abs.(val .- [-1,s2,1]))[2], eigenvals_new_s2) .== i, :]
  for i=1:3
]

fig = CairoMakie.Figure(resolution = (1000, 750))
ax = CairoMakie.Axis(fig[1, 1],
  xlabel="barycentr. p",
  ylabel="barycentr. q",
  title="RQI classic [large gap]"
)
for i=1:3
  CairoMakie.scatter!(
    pts2d_sep_old_s1[i][:,1], pts2d_sep_old_s1[i][:,2],
    label="λ=$([-1,s1,1][i])",
    markersize=4,
  )
end
CairoMakie.axislegend()
ax = CairoMakie.Axis(fig[1, 2],
  xlabel="barycentr. p",
  ylabel="barycentr. q",
  title="RQI classic [small gap]"
)
for i=1:3
  CairoMakie.scatter!(
    pts2d_sep_old_s2[i][:,1], pts2d_sep_old_s2[i][:,2],
    label="λ=$([-1,s2,1][i])",
    markersize=4,
  )
end
CairoMakie.axislegend()
ax = CairoMakie.Axis(fig[2, 1],
  xlabel="barycentr. p",
  ylabel="barycentr. q",
  title="RQI-RR2 [large gap]"
)
for i=1:3
  CairoMakie.scatter!(
    pts2d_sep_new_s1[i][:,1], pts2d_sep_new_s1[i][:,2],
    label="λ=$([-1,s1,1][i])",
    markersize=4,
  )
end
CairoMakie.axislegend()
ax = CairoMakie.Axis(fig[2, 2],
  xlabel="barycentr. p",
  ylabel="barycentr. q",
  title="RQI-RR2 [small gap]"
)
for i=1:3
  CairoMakie.scatter!(
    pts2d_sep_new_s2[i][:,1], pts2d_sep_new_s2[i][:,2],
    label="λ=$([-1,s2,1][i])",
    markersize=4,
  )
end
CairoMakie.axislegend()
fig
