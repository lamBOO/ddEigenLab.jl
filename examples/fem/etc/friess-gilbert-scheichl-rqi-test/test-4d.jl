using ddEigenLab
using LinearAlgebra
import CairoMakie

"""
Recreate the figure 2 from [1] (page 14).
[1]: https://arxiv.org/pdf/2312.02847.pdf
"""

function eig_vals(s)
  return [s,3,4,5]
  # return [-1,0,s,1]  # TODO: That fails, maybe due to indefinite?
end

function A_mat(s)
  return diagm(eig_vals(s))
end

function unit_simplex(p, q, r, s)
  x1 = [1,0,0,0]
  x2 = [0,1,0,0]
  x3 = [0,0,1,0]
  x4 = [0,0,0,1]
  return p * x1 + q * x2 + r * x3 + s * x4
end

s = 2.98
h = 0.02

pqrs_arr = [[p, q, r, 1-p-q-r] for p=0:h:1 for q=0:h:1-p for r=0:h:1-p-q]
pts = hcat(map(pqr->unit_simplex(pqr...), pqrs_arr)...)'
eigenvals = Vector{Float64}(undef, size(pts)[1])

for i=1:size(pts)[1]
  # println("i = $i/$(size(pts)[1])")
  res = RQI_new(A_mat(s), I(size(A_mat(s))[1]), x=pts[i,:], verbose=false)
  # @show eigenvals[i] = res[2]
  # @show pts[i,:]
  if !(abs(res[2] - findmin(eig_vals(s))[1]) <= 1E-8) && !(pts[i,:][findmin(eig_vals(s))[2]] ≈ 0.0)
    error("not conv. to first eigenpair although x0 had compo. 1 in first eigenvector direction")
  end
end

B = [
  1 0.5 0 0
  0 1 0 0
]

pts2d = hcat(map(pqr->B*pqr, [pts[i,:] for i=1:size(pts)[1]])...)'

pts2d_sep_old_s1 = [
  pts2d[map(val -> findmin(abs.(val .- eig_vals(s)))[2], eigenvals) .== i, :]
  for i=1:4
]


fig = CairoMakie.Figure(resolution = (1000, 750))
ax = CairoMakie.Axis(fig[1, 1],
  xlabel="barycentr. p",
  ylabel="barycentr. q",
  title="RQI-2RR-KrylovEscape"
)
for i=1:4
  CairoMakie.scatter!(
    pts2d_sep_old_s1[i][:,1], pts2d_sep_old_s1[i][:,2],
    label="λ=$(eig_vals(s)[i])",
    markersize=10,
  )
end
CairoMakie.axislegend()
fig
