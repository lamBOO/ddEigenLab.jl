using Gridap

model = CartesianDiscreteModel((0,1,0,1), (5,5))

labels = get_face_labeling(model)
add_tag_from_tags!(labels, "diri", collect(1:8))

order = 1
reffe = ReferenceFE(lagrangian,Float64,order)
V0 = TestFESpace(model,reffe;conformity=:H1, dirichlet_tags="diri")
Ug = TrialFESpace(V0, 0.0)
Ω = Triangulation(model)
dΩ = Measure(Ω, 2)

a(u,v) = ∫( ∇(v)⋅∇(u) )*dΩ
b(u,v) = ∫( v * u )*dΩ

A = assemble_matrix(a, Ug, V0)
B = assemble_matrix(b, Ug, V0)
