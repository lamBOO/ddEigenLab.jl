using ddEigenLab
using Gridap, GridapGmsh


mshfile = joinpath("mesh.msh")
mod = GmshDiscreteModel(mshfile)
re = ReferenceFE(lagrangian, Float64, 1)
V = TestFESpace(mod, re, dirichlet_tags=["outer"])
U = TrialFESpace(V, 0)
Ω = Triangulation(mod)
dΩ = Measure(Ω, 2)

source(x) = 1
a(u, v) = ∫(∇(v) ⋅ ∇(u) )dΩ
f(v) = ∫(source * v)dΩ
A, b = assemble_matrix_and_vector(a, f, U, V)

partition = create_elements_partition_gmsh(mod, "untitled" .* ["$i" for i=1:3])
region_elements = create_elements_partition_gmsh(mod, "untitled" .* ["$i" for i=1:3])
elemsp = [
  vcat(region_elements[2],region_elements[i])  # second region is overlap
  for i in [1,3]
]
dofsp = create_dofs_partition(elemsp, V)
Ri, Di = create_pu_matrices(dofsp, V)
par = Partition(Ri)
pu = PartitionOfUnity(Ri, Di)

nocc = NoCoarseCorrection()
RAS1 = PSM(A, pu, nocc)
ASM1 = PSM(A, par, nocc)

for i=0:5
  res = ddEigenLab.solve(RAS1, b, maxiter=i)
  writevtk(Ω, "result-ras$(i)", cellfields=["sol" => Gridap.FEFunction(U, res[1])])
end

# res = ddEigenLab.solveAS(RAS1, b, zeros(size(b)), 3, 1E-10)
# for (i, step) in enumerate(res[1])
#   writevtk(Ω, "result-ras$(i)", cellfields=["sol" => Gridap.FEFunction(U, step)])
# end
