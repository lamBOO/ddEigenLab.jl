using Gridap
using LinearAlgebra
using SparseArrays
using LinearMaps

using ddEigenLab

Lx = 32
Ly = 1
ρ = 5
d = 2
Z = 5

# ******************************************************************************
# Explaination:

# For the potential 2), there's always a gap for the Lx-th to (Lx+1)-th
# However, it's some weird behavior since we have localized on the boundary (Lx)
# Probably need some weird potential with modulo operations...
# Doesn't work for no potential or the usual periodic potential
# => Conclusion: Not soo relevant for us

# Potentials:
# 1) Doesn't work
V(x) = Z^2 * (sin(x[1]*π)^2 * sin(x[2]*π)^2)
# 2) Works since Lx eigenfunction is very localized near boundary
# V(x) = Z^2 * (0 * sin(x[1]*π)^2 * sin(x[2]*π)^2 + 5*(((x[1]% 1)-0.25)^2 ))
# 3) Doesn't work
# V(x) = 0
# ******************************************************************************

domain = (0, Lx, 0, Ly)
  partition = (round(Int, Lx * ρ), round(Int, Ly * ρ))
model = CartesianDiscreteModel(domain, partition; isperiodic=(false, false))
labels = get_face_labeling(model)
add_tag_from_tags!(labels, "diri", collect(1:3^d-1))

reffe = ReferenceFE(lagrangian, Float64, 1)
V0 = TestFESpace(model, reffe; conformity=:H1, dirichlet_tags = ["diri"])
Ug = TrialFESpace(V0, 0)
Ω = Triangulation(model)
dΩ = Measure(Ω,2)

Vh = Gridap.FESpaces.interpolate(V, TestFESpace(model, reffe))
writevtk(Ω, "potential", cellfields = ["Vh" => Vh])

b(v) = ∫( 1 * v ) * dΩ  # dummy

a1(u,v) = ∫( ∇(v) ⋅ ∇(u) + V * u * v ) * dΩ
op1 = AffineFEOperator(a1, b, Ug, V0)
stiffnessmat = assemble_matrix(a1, Ug, V0)

a2(u,v) = ∫( v * u ) * dΩ
op2 = AffineFEOperator(a2, b, Ug, V0)
massmat = assemble_matrix_and_vector(a2, b, Ug, V0)[1]

eigendecomp = eigen(Array(stiffnessmat),Array(massmat))
@show eigendecomp.values[1]/eigendecomp.values[2]
@show eigendecomp.values[1]/eigendecomp.values[Lx+1]
@show eigendecomp.values[Lx]/eigendecomp.values[Lx+1]
@show eigendecomp.values[Lx+1]/eigendecomp.values[Lx+2]

writevtk(Ω, "result",
  cellfields=[
    "1" => Gridap.FEFunction(V0, eigendecomp.vectors[:,1]),
    "2" => Gridap.FEFunction(V0, eigendecomp.vectors[:,2]),
    "Lx" => Gridap.FEFunction(V0, eigendecomp.vectors[:,Lx]),
    "Lx+1" => Gridap.FEFunction(V0, eigendecomp.vectors[:,Lx+1]),
    "Lx+2" => Gridap.FEFunction(V0, eigendecomp.vectors[:,Lx+2])
  ]
)
