using Gridap
using LinearAlgebra
using SparseArrays
using LinearMaps

using ddEigenLab

Lx = 5
Ly = 1
ρ = 10
d = 2

Z = 10
# V(x) = 1
# V(x) = Z^2 * sin(x[1]*π)^2 * sin(x[2]*π)^2
# V(x) = Z^2 * sin((x[1]+0.25)*π)^2 * sin(x[2]*π)^2
# V(x) = Z^2 * sin((x[1]+0.2)*π)^2 * sin(x[2]*π)^2
# V(x) = Z^2 * (sin(x[1]*π)^2 * sin(x[2]*π)^2 + 5*(((x[1]% 1)-0.5)^2 )) # doesnt stay constant!!  !! WRONG IS NOT PERIODIC
# V(x) = Z^2 * (sin(x[1]*π)^2 * sin(x[2]*π)^2 + 5*((((10+x[1])%1))^2) ) # doesnt stay constant!!
# V(x) = Z^2 * (sin(x[1]*π)^2 * sin(x[2]*π)^2 + 1*((((10+x[1]-0.25)%1))^2) ) # doesnt stay constant!!
# V(x) = Z^2/Z * (cos(2*x[1]*π) + sin(4*x[1]*π) + 2) * (sin(x[2]*π)^2 + 1)  # Smooth, periodic, but not symmetric?
# V(x) = Z^2 * 1/((abs(abs(x[1]-0.35)%1-0.5) + 1) * ((x[2] + 1))^2 + (abs(abs(x[1]-0.35)%1-0.5) + 1) + 1)
# V(x) = Z^2 * (sin(x[1]*π)^2 * sin(x[2]*π)^2 + 1*((((10+x[1]-0.0)%1))^2) ) # doesnt stay constant!!
# V(x) = Z^2 * (sin(x[1]*π)^2 * sin(x[2]*π)^2 + 50*((((10+x[1]-0.25)%1))^2) ) # doesnt stay constant!!
# V(x) = Z^2 * (sin(x[1]*π)^2 * sin(x[2]*π)^2 + 1*(((x[1]% 1)-0.25)^2 ))  !! WRONG IS NOT PERIODIC
V(x) = Z^2 * (sin(3*x[1]*π)^2+1) * sin(2*x[2]*π)^2
# V(x) = Z^2 * (sin(x[1]*π)^2+1) * sin(x[2]*π)^2

# Periodic

Lxp = 1
Lyp = 1
domain = (0, Lxp, 0, Lyp)
partition = (Lxp * ρ, Lyp * ρ)
model2 = CartesianDiscreteModel(domain, partition; isperiodic=(true, false))
labels2 = get_face_labeling(model2)
add_tag_from_tags!(labels2, "diri", collect(1:6))
# add_tag_from_tags!(labels2, "diri", collect(1:6))
order = 1
reffe2 = ReferenceFE(lagrangian, Float64, order)
V02 = TestFESpace(model2, reffe2; conformity=:H1, dirichlet_tags = ["diri"])
Ug2 = TrialFESpace(V02, 0)
degree = 2
Ω2 = Triangulation(model2)
dΩ2 = Measure(Ω2, degree)

b2(v) = ∫( 1 * v ) * dΩ2  # dummy
a11(u,v) = ∫( ∇(v) ⋅ ∇(u) + V * u * v ) * dΩ2
op11 = AffineFEOperator(a11, b2, Ug2, V02)
stiffnessmat2 = assemble_matrix(a11, Ug2, V02)
a22(u,v) = ∫( v * u ) * dΩ2
op22 = AffineFEOperator(a22, b2, Ug2, V02)
massmat2 = assemble_matrix_and_vector(a22, b2, Ug2, V02)[1]

@info "start eigensolve periodic cell"

eigvec2, eigval2, errors, info = IP(stiffnessmat2, massmat2, 0, 1, 1, 1E-10, 1000)
eigvectmp = eigvec2

uh = FEFunction(op11.trial, eigvec2/findmax(eigvec2)[1]);
writevtk(Ω2, "results", cellfields = ["uh" => uh])
Vhp = Gridap.FESpaces.interpolate(V, TestFESpace(model2, reffe2))
writevtk(Ω2, "Vhp", cellfields = ["Vhp" => Vhp])
@info "end eigensolve periodic cell"


@info "start setup big"
# Spectral Coarse Space
ol = 0.2
function chi(x)
  shift = -1
  return (
    if (-ol - eps(Float64) <= x[1]+shift < ol)
      # 1/(2*ol)*(x[1]+shift + ol)
      0.5
      # 1.0
    elseif (ol <= x[1]+shift <= 1-ol)
      1.0
    elseif 1 + ol >= x[1]+shift > 1 - ol
      # -1/(2*ol)*(x[1]+shift - (1 - ol)) + 1
      0.5
      # 1.0
    else
      0.0
    end
  )
  # return 1
end
# function dchi(x)
#   return (
#     if (x[1] < ol)
#       1/(2*ol)
#     elseif (ol <= x[1] <= 1-ol)
#       0
#     elseif x[1] > 1 - ol
#       -1/(2*ol)
#     else
#       100000
#     end
#   )
#   # return 1
# end
function chis(x)
  return chi(x)^2
end

function chibar(x)
  return (
    if (-ol <= x[1] < ol)
      # 1/(2*ol)*(x[1] + ol)
      # 0.5
      # 1.5
      1.0
    elseif (ol <= x[1] <= 1-ol)
      # 1.5
      2.0
    elseif 1 + ol >= x[1] > 1 - ol
      # -1/(2*ol)*(x[1] - (1 - ol)) + 1
      1.0
      # 1.0
    else
      1.0
    end
  )
  # return 1
end
function chibars(x)
  return chibar(x)^2
end


Lxp3 = Lyp3 = 1
left = 1.0
right = 4.0
domain3 = (-left + 1, 1 + right + 1, 0, Lyp3)
partition3 = ((left + 1 + right) * ρ, Lyp3 * ρ)
# domain3 = (-0.5, Lxp3+0.5, 0, 1)
# domain3 = (-1, Lxp3+1, 0, 1)  # adapt partition3
# partition3 = ((Lxp3+2) * ρ, Lyp3 * ρ)
model3 = CartesianDiscreteModel(domain3, partition3; isperiodic=(false, false))
writevtk(model3, "model3_geneo")
labels3 = get_face_labeling(model3)
# add_tag_from_tags!(labels3, "diri", [1,2,3,4,5,6])

# add_tag_from_tags!(labels3, "diri", collect(1:8))
# add_tag_from_tags!(labels3, "diri", [collect(1:6)...])
add_tag_from_tags!(labels3, "diri", [collect(1:6)...])
# add_tag_from_tags!(labels3, "diri", collect(1:3^d-1))

@info "end setup big"


@info "start setup big 2"
order3 = 1
reffe3 = ReferenceFE(lagrangian, Float64, order3)
V03 = TestFESpace(model3, reffe3; conformity=:H1, dirichlet_tags = ["diri"])
Ug3 = TrialFESpace(V03, 0)
degree3 = 2
Ω3 = Triangulation(model3)
dΩ3 = Measure(Ω3, degree3)

Vh3 = Gridap.FESpaces.interpolate(V, TestFESpace(model3, reffe3))
writevtk(Ω3, "Vh", cellfields = ["Vh" => Vh3])

btrian3 = BoundaryTriangulation(model3)
dΓ3 = Measure(btrian3,degree3)

nb3 = get_normal_vector(btrian3)

@info "end setup big 2"

@info "start stiffnes big"
b3(v) = ∫( 1 * v ) * dΩ3  # dummy
a13(u,v) = ∫(  ∇(v) ⋅ ∇(u) + (x->(V(x)-eigval2)) * u * v ) * dΩ3 + (
  - 0 * (∫((∇(u)⋅nb3)*v)dΓ3 + 1 * ∫((∇(v)⋅nb3)*u)dΓ3)
  + 0 * ( -1 * (∫((u*nb3)⋅(v*VectorValue([1,0])))dΓ3 - 0 * ∫((u*nb3)⋅(v*nb3))dΓ3))
  + 0 * ∫((∇(u)⋅nb3)*(∇(v)⋅nb3))dΓ3 + 0 * ∫(jump(u)*jump(v))dΓ3 + 0 * ∫(u*v)dΓ3
)
op13 = AffineFEOperator(a13, b3, Ug3, V03)
stiffnessmat3 = assemble_matrix(a13, Ug3, V03)
@info "end stiffnes big"

@info "start massmat big"
# a23(u,v) = ∫( v * u ) * dΩ3 # efendiev 2010
# a23(u,v) = ∫( (x->dchi(x)^2) * v * u ) * dΩ3
# a23(u,v) = ∫( chis * v * u ) * dΩ3
# a23(u,v) = ∫( chis * ∇(v) ⋅ ∇(u) + (x->((V(x)-eigval2)*chi(x)^2)) * u * v) * dΩ3
# a23(u,v) = ∫( chis * ∇(v) ⋅ ∇(u) + (x->((V(x))*chi(x)^2)) * u * v) * dΩ3
# a23(u,v) = ∫(
#   ∇(v*(x->chi(x))) ⋅ ∇(u*(x->chi(x))) + (x->((V(x)-0)*chi(x)^2)) * u * v
#   + ∇(v*(x->chi([x[1]-1,x[2]]))) ⋅ ∇(u*(x->chi([x[1]-1,x[2]]))) + (x->((V(x)-0)*chi([x[1]-1,x[2]])^2)) * u * v
#   + ∇(v*(x->chi([x[1]+1,x[2]]))) ⋅ ∇(u*(x->chi([x[1]+1,x[2]]))) + (x->((V(x)-0)*chi([x[1]+1,x[2]])^2)) * u * v
# ) * dΩ3
a23(u,v) = ∫(
  ∇(v*(x->chi(x))) ⋅ ∇(u*(x->chi(x))) + (x->((V(x)-0)*chi(x)^2)) * u * v
) * dΩ3
# a23(u,v) = ∫( ∇(v*chibar) ⋅ ∇(u*chibar) + (x->((V(x)-0)*chibar(x)^2)) * u * v) * dΩ3
# a23(u,v) = ∫( ∇(v*chibar) ⋅ ∇(u*chibar) + (x->((V(x)-eigval2)*chibar(x)^2)) * u * v) * dΩ3
# a23(u,v) = ∫( chibars * ∇(v) ⋅ ∇(u) + (x->((V(x)-eigval2)*chibar(x)^2)) * u * v) * dΩ3
# a23(u,v) = ∫( ∇(v*chi) ⋅ ∇(u*chi) + (x->((V(x)-0)*chi(x)^2)) * u * v) * dΩ3
# a23(u,v) = ∫( ∇(v*chi) ⋅ ∇(u*chi) + (x->((V(x))*chi(x)^2)) * u * v) * dΩ3
# a23(u,v) = ∫( ∇(v) ⋅ ∇(u) + (x->((V(x)-eigval2))) * u * v) * dΩ3
op23 = AffineFEOperator(a23, b3, Ug3, V03)
massmat3 = assemble_matrix(a23, Ug3, V03)
@info "end massmat big"

@info "start eigensolve big"
eigvec3, eigval3, error3, info3 = IP(stiffnessmat3, massmat3, 0, 1, 1, 1E-10, 1000)
@info "end eigensolve big"

@info "start postprocess big"
uh3 = FEFunction(op13.trial, eigvec3/findmax(abs.(eigvec3))[1])
writevtk(Ω3, "results_geneo", cellfields = ["uh" => uh3])
num3 = 40
@info eigen(Array(stiffnessmat3),Array(massmat3)).values |> x->collect(zip(x,1:length(x)))
uhmap = map(i->"uh$(lpad(i,2,"0"))"=>FEFunction(op13.trial, eigen(Array(stiffnessmat3),Array(massmat3)).vectors[:,i]/findmax(abs.(eigen(Array(stiffnessmat3),Array(massmat3)).vectors[:,i]))[1]), 1:num3)
writevtk(Ω3, "results_geneofirst", cellfields = uhmap)
uhmap2 = map(i->"uh$(lpad(i,2,"0"))"=>FEFunction(op13.trial, eigen(Array(stiffnessmat3)).vectors[:,i]/findmax(abs.(eigen(Array(stiffnessmat3)).vectors[:,i]))[1]), 1:num3)
writevtk(Ω3, "results_geneofirststiffness", cellfields = uhmap2)
uhmapmassmat = map(i->"uh$(lpad(i,2,"0"))"=>FEFunction(op13.trial, eigen(Array(massmat3)).vectors[:,i]/findmax(abs.(eigen(Array(massmat3)).vectors[:,i]))[1]), 1:num3)
writevtk(Ω3, "results_geneofirstmassmat", cellfields = uhmapmassmat)
chiuh = Gridap.FESpaces.interpolate(chi * uh3, V03)
writevtk(Ω3, "results_chiuh", cellfields = ["chiuh" => chiuh])
chih = Gridap.FESpaces.interpolate(chi, V03)
writevtk(Ω3, "chih", cellfields = ["chih" => chih])
# chibarh = Gridap.FESpaces.interpolate(chibar, V03)
# writevtk(Ω3, "chibarh", cellfields = ["chibarh" => chibarh])
chish = Gridap.FESpaces.interpolate(chis, V03)
writevtk(Ω3, "chish", cellfields = ["chish" => chish])
@info "end postprocess big"

# model3 = CartesianDiscreteModel(domain3, partition3; isperiodic=(false, false))
# Ω3 = Triangulation(model3)
# order3 = 1
# reffe3 = ReferenceFE(lagrangian, Float64, order3)
# V3 = TestFESpace(model3, reffe3; conformity=:H1)
# chih = Gridap.FESpaces.interpolate(chi, V3)
# writevtk(Ω3, "chih", cellfields = ["chih" => chih])
