using PGFPlotsX
using LinearAlgebra
using LaTeXStrings
using Plots

Z = 1
alpha = 1E-2
beta = 0.2
Lx = 10
function gfunc(x :: Float64, N :: Int64, M :: Int64)
  return Z^2 * (
    -sum([
      # exp(sum(-([x.data...]-[i-0.5,0.5]).^2))
      1/(norm(x-i,2)+beta) * exp(- alpha * norm(x-i,2))
      # log(norm(x-i,2)+beta) * exp(- alpha * norm(x-i,2))
      # (norm(x-i,2)+beta) * exp(- alpha * (norm(x-i,2)+beta))
      # (norm(x-i,2)+beta)
      # 1/(norm(x-i,2)) * exp(- alpha * norm(x-i,2))
    for i=N:M])
  )
end

xx = -1:0.01:1
L = 1
plot((-L:0.01:L)/L,gfunc.(-L:0.01:L,-10,10), labels=L, xlims=(-1, -1+1/20))
# plot((-L:0.01:L)/L,gfunc.(-L:0.01:L,-10,10), labels=L)
L = 5
plot!((-L:0.01:L)/L,gfunc.(-L:0.01:L,-10,10), labels=L)
L = 10
plot!((-L:0.01:L)/L,gfunc.(-L:0.01:L,-10,10), labels=L)
L = 20
plot!((-L:0.01:L)/L,gfunc.(-L:0.01:L,-L,L), labels=L)
L = 100
plot!((-L:0.01:L)/L,gfunc.(-L:0.01:L,-L,L), labels=L)
L = 200
@time plot!((-L:0.01:L)/L,gfunc.(-L:0.01:L,-L,L), labels=L)
L = 500
plot!((-L:0.01:L)/L,gfunc.(-L:0.01:L,-L,L), labels=L)
# L = 1000
# plot!((-L:0.01:L)/L,gfunc.(-L:0.01:L,-L,L), labels=L)




# Lvec = 1:1:1000
# pt = 0.1
# limit = gfunc(pt,-1000000,1000000)
# plot(Lvec,map(L->gfunc(pt,-L,L) - (limit - 1000*eps(Float64)), Lvec), yaxis=:log)
# gfunc(0.,-L,L)
# plot(Lvec,map(L->gfunc(1.0*L,-L,L),Lvec))




# x = -Lx-0.5:0.01:Lx+0.5
# p = @pgf Axis(
#   {
#     xmajorgrids,
#     ymajorgrids,
#     title=L"V_L = - \sum_{i=-L}^L \frac{Z e^{-\alpha |x|}}{|x+0.2|}",
#     # ymin=-8,
#     # ymax=-5
#   },
#   Plot(
#     {
#       no_marks,
#       black,
#       thick
#     },
#     Coordinates(x, gfunc.(x,-Lx/2,Lx/2))
#     ),
#     Plot(
#       {
#         no_marks,
#         blue,
#         thick
#         },
#     Coordinates(x, gfunc.(x,-Lx,Lx))
#   ),
#   Plot(
#     {
#       no_marks,
#       red,
#       thick
#     },
#     Coordinates(x, gfunc.(x,-1000,1000))
#   ),
#   Legend([L"V_{%$(Lx÷2)} = V_\infty + \delta V", L"V_{%$(Lx)} = V_\infty + \delta V", L"V_\infty = \lim_{L \to \infty} V_L"])
# )
# display(VSCodeServer.InlineDisplay(), "image/svg+xml", p)
# # display(VSCodeServer.InlineDisplay(), p)

# pgfsave("plot-perturbation.tex", p; include_preamble=false)
