using Gridap
using SparseArrays
using LinearMaps
using LinearAlgebra
using DataFrames
using PGFPlotsX
using Primes
using CSV
using Colors
using LaTeXStrings

using ddEigenLab

Lx = 2
Ly = 1
ρ = 20
d = 2
Z = 2
alpha = 0.01


# Recheck:
# - -log case has lambda_lim bigger than lambda_L
# - exp case has unsymmetric pot_lim

#! Positive sum also works, although system becomes indefinite!




function solve(
  dims,
  partition,
  pot::Function;
  isperiodic=(false, false),
  name="",
  shift=0.0
)
  model2 = CartesianDiscreteModel(
    dims, partition, isperiodic=isperiodic
  )
  labels2 = get_face_labeling(model2)
  add_tag_from_tags!(labels2, "diri", collect(1:3^2-1))
  reffe2 = ReferenceFE(lagrangian, Float64, 1)
  V02 = TestFESpace(model2, reffe2, dirichlet_tags=["diri"])
  Ug2 = TrialFESpace(V02, 0)
  Ω2 = Triangulation(model2)
  dΩ2 = Measure(Ω2, 2)

  a11(u, v) = ∫(∇(v) ⋅ ∇(u) + pot * u * v) * dΩ2
  a22(u, v) = ∫(v * u) * dΩ2
  stiffnessmat2 = assemble_matrix(a11, Ug2, V02)
  massmat2 = assemble_matrix(a22, Ug2, V02)

  @info "start solve"

  eigvec2, eigval2, errors, info = LOPCG(
    stiffnessmat2, massmat2, Pl=stiffnessmat2 - shift * massmat2,
    tol=1E-10, maxiter=1000
  )

  writevtk(
    Ω2, "out$(name)", cellfields=["out" => FEFunction(Ug2, eigvec2)]
  )
  modeltmp = CartesianDiscreteModel(dims, partition)
  Voutput = TestFESpace(modeltmp, reffe2)
  Vgrid = Gridap.FESpaces.interpolate(pot, Voutput)
  writevtk(Triangulation(modeltmp), "pot$(name)", cellfields=["pot" => Vgrid])

  return (; eval=eigval2, evec=eigvec2, it=info.it)

end


function plot_perturbed_asymptotics(df)
  # @pgf LogLogAxis(
  p = @pgf Axis(
    {
      height = "5cm",
      width = "9cm",
      grid = "major",
    },
    # PlotInc(Expression("exp(-x^5)+11")),
    # LegendEntry("model"),
    [
      PlotInc(
        PGFPlotsX.Table(df[!, 1], df[!, i] .- 10.889914610633163)
      )
      # LegendEntry(i)
      for i = 2:size(df)[2]
    ]
  )
  pgfsave("perturbed-asymptotics.tex", p; include_preamble=false)
  return p
end

function local_defects(x, alpha, beta)
  if abs(x.data[1]) <= beta
    return alpha
  else
    return 1 - (sin(pi * x.data[1])^2 * sin(pi * x.data[2])^2)
  end
end

function prime_defects(x, alpha, beta)
  ps = primes(1, 100)
  if reduce(|, [p <= abs(x.data[1]) <= p + beta for p in ps])
    return alpha
  else
    return 1 - (sin(pi * x.data[1])^2 * sin(pi * x.data[2])^2)
  end
end

function main()

  # params
  rho = 20

  # data
  df = DataFrame(
    "Lx" => Float64[],
    "1.1" => Float64[],
    "1.2" => Float64[],
    "1.3" => Float64[],
    "1.4" => Float64[],
    "2.1" => Float64[],
    "2.2" => Float64[],
    "2.3" => Float64[],
    "2.4" => Float64[],
    "3.1" => Float64[],
    "3.2" => Float64[],
    "3.3" => Float64[],
    "3.4" => Float64[],
  )


  function Vi(r, alpha, beta)
    return (
      1 / (r + beta) * exp(-alpha * r)
    )
  end

  function V(x, Lx, alpha, beta)
    return 150 + 10 * (
      -sum([
        Vi(norm(([x.data...] - [i - 0.5, 0.5]), 2), alpha, beta)
        for i = -Lx+1:Lx])
    )
  end

  alphaarray = [1.0, 1/2, 1/4, 1/8]
  # alphaarray = [1.0]
  beta = 0.1

  limits1 = [solve(
    (-1, 1, 0, 1), floor.(Int, (1 * rho * 2, rho)), x -> V(x, 200, alpha, beta),
    isperiodic=(true, false), name="limit"
  ) for alpha in alphaarray]
  @info [limits1[i].eval for i = 1:length(alphaarray)]
  limits2 = [solve(
    (-1, 1, 0, 1), floor.(Int, (1 * rho * 2, rho)), x -> local_defects(x, -5 * log10(alpha), -1),
    isperiodic=(true, false), name="limit"
  ) for alpha in alphaarray]
  @info [limits2[i].eval for i = 1:length(alphaarray)]
  limits3 = [solve(
    (-1, 1, 0, 1), floor.(Int, (1 * rho * 2, rho)), x -> prime_defects(x, -5 * log10(alpha), -1),
    isperiodic=(true, false), name="limit"
  ) for alpha in alphaarray]
  @info [limits3[i].eval for i = 1:length(alphaarray)]

  MAX_EXP = 2  # 32

  for Lx = 1:1:MAX_EXP
    @info Lx

    # test 1: sum of cell potentials
    r1s = []
    r2s = []
    r3s = []
    for (i, alpha) in enumerate(alphaarray)
      @info alpha
      pot = x -> V(x, Lx, alpha, beta)
      r = solve(
        (-Lx, Lx, 0, 1), floor.(Int, (Lx * rho * 2, rho)), pot,
        shift=limits1[i].eval, name="1" * string(alpha) * "." * lpad(replace(string(Lx), "." => ""), 10, "0")
      )
      push!(r1s, r)
      pot = x -> local_defects(x, -5 * log10(alpha), 1)
      r = solve(
        (-Lx, Lx, 0, 1), floor.(Int, (Lx * rho * 2, rho)), pot,
        shift=0, name="2" * string(alpha) * "." * lpad(replace(string(Lx), "." => ""), 10, "0")
      )
      push!(r2s, r)
      pot = x -> prime_defects(x, -5 * log10(alpha), 1)
      r = solve(
        (-Lx, Lx, 0, 1), floor.(Int, (Lx * rho * 2, rho)), pot,
        shift=0, name="3" * string(alpha) * "." * lpad(replace(string(Lx), "." => ""), 10, "0")
      )
      push!(r3s, r)
    end

    push!(
      df,
      (
        Lx,
        [r.eval for r in r1s]...,
        [r.eval for r in r2s]...,
        [r.eval for r in r3s]...,
      )
    )

  end

  @info [limits1[i].eval for i = 1:length(alphaarray)]
  @info [limits2[i].eval for i = 1:length(alphaarray)]
  @info [limits3[i].eval for i = 1:length(alphaarray)]

  return df

end


function main2()

  # params
  rho = 20

  # data
  df = DataFrame(
    "Lx" => Float64[],
    "1.1" => Float64[],
    "limit" => Float64[],
    "errors" => Float64[],
    "iterations" => Int32[],
    "iterations_noshift" => Int32[],
    # "1.2" => Float64[],
    # "1.3" => Float64[],
    # "1.4" => Float64[],
  )


  function Vi(r, alpha, beta)
    return (
      1 / (r + beta)^1.1 * exp(-alpha * r)
    )
  end

  function V(x, Lx, alpha, beta)
    return 150 + 10 * (
      -sum([
        Vi(norm(([x.data...] - [i - 0.5, 0.5]), 2), alpha, beta)
        for i = -Lx+1:Lx])
    )
  end

  # alphaarray = [1.0, 1/2, 1/4, 1/8]
  alphaarray = [0.0]
  beta = 0.1

  for Lx = 1:1:32
    @info Lx

    # test 1: sum of cell potentials
    r1s = []
    limits = []
    errors = []
    iterations = []
    iterations_noshift = []
    for (i, alpha) in enumerate(alphaarray)
      @info alpha
      pot = x -> V(x, Lx, alpha, beta)
      potlim = x -> V(x, 100, alpha, beta) + (V(VectorValue([0.0,0.5]), Lx, alpha, beta)-V(VectorValue([0.0,0.5]), 100, alpha, beta))

      # limit
      @info "Limit: Start`"
      limit = solve(
        (-1, 1, 0, 1), floor.(Int, (1 * rho * 2, rho)), potlim,
        isperiodic=(true, false), name="limit"
      )
      @info limit.eval
      push!(limits, limit)
      @info "Limit: End"

      # big shift
      r = solve(
        (-Lx, Lx, 0, 1), floor.(Int, (Lx * rho * 2, rho)), pot,
        shift=limit.eval, name="main2" * string(alpha) * "." * lpad(replace(string(Lx), "." => ""), 10, "0")
      )
      push!(r1s, r)

      # big no shift
      r_noshift = solve(
        (-Lx, Lx, 0, 1), floor.(Int, (Lx * rho * 2, rho)), pot,
        shift=0, name="main2" * string(alpha) * "." * lpad(replace(string(Lx), "." => ""), 10, "0")
      )

      # errors
      push!(errors, r.eval - limit.eval)

      # its
      push!(iterations, r.it)
      push!(iterations_noshift, r_noshift.it)

    end

    push!(
      df,
      (
        Lx,
        [r.eval for r in r1s]...,
        [l.eval for l in limits]...,
        [er for er in errors]...,
        [it for it in iterations]...,
        [it for it in iterations_noshift]...,
      )
    )

  end

  return df

end

function plot_cell(df)

  limits = [125.95168263514523, 114.33308800462349, 101.87015525531994, 88.85613919934896]

  colors = [
    RGB(0, 0.4470, 0.7410),
    RGB(0.8500, 0.3250, 0.0980),
    RGB(0.9290, 0.6940, 0.1250),
    RGB(0.4940, 0.1840, 0.5560),
    RGB(0.4660, 0.6740, 0.1880),
    RGB(0.3010, 0.7450, 0.9330),
    RGB(0.6350, 0.0780, 0.1840),
  ]

  # \pgfplotscreateplotcyclelist{matlab}{
  #   color1,every mark/.append style={solid},mark=*\\
  #   color2,every mark/.append style={solid},mark=square*\\
  #   color3,every mark/.append style={solid},mark=triangle*\\
  #   color4,every mark/.append style={solid},mark=halfsquare*\\
  #   color5,every mark/.append style={solid},mark=pentagon*\\
  #   color6,every mark/.append style={solid},mark=halfcircle*\\
  #   color7,every mark/.append style={solid,rotate=180},mark=halfdiamond*\\
  #   color1,every mark/.append style={solid},mark=diamond*\\
  #   color2,every mark/.append style={solid},mark=halfsquare right*\\
  #   color3,every mark/.append style={solid},mark=halfsquare left*\\
  # }

  p = @pgf GroupPlot(
    {
      group_style =
        {
          group_size = "2 by 1",
          xticklabels_at = "edge bottom",
          # yticklabels_at = "edge left"
        },
      # no_markers,
      # mark="*"
    },
    Axis({
      ymin = 80,
      xmajorgrids,
      ymajorgrids,
      xlabel=L"L",
      title=L"\lambda_L",
      legend_pos="south west",
      legend_style="{nodes={scale=0.8, transform shape}}",
    }),
    PlotInc(
      {
        mark_options = "solid, scale = 0.8",
        mark = "*",
        color = colors[1],
      },
      PGFPlotsX.Table(df."Lx", df."1.1"),
    ),
    HLine(
      {
        dashed,
        color = colors[1],
        thick,
      },
      limits[1]
    ),
    PlotInc(
      {
        mark_options = "solid, scale = 0.8",
        mark = "*",
        color = colors[2],},
      PGFPlotsX.Table(df."Lx", df."1.2"),
    ),
    HLine(
      {
        dashed,
        color = colors[2],
        thick,
      },
      limits[2]
    ),
    PlotInc(
      {
        mark_options = "solid, scale = 0.8",
        mark = "*",
        color = colors[3],
      },
      PGFPlotsX.Table(df."Lx", df."1.3"),
    ),
    HLine(
      {
        dashed,
        color = colors[3],
        thick,
      },
      limits[3]
    ),
    PlotInc(
      {
        mark_options = "solid, scale = 0.8",
        mark = "*",
        color = colors[4],
      },
      PGFPlotsX.Table(df."Lx", df."1.4"),
    ),
    HLine(
      {
        dashed,
        color = colors[4],
        thick,
      },
      limits[4]
    ),
    Legend([
      L"\alpha=1/1",
      L"\alpha=1/2",
      L"\alpha=1/4",
      L"\alpha=1/8"
    ]),
    LogLogAxis({
      xmajorgrids,
      ymajorgrids,
      xlabel=L"L",
      title=L"|\lambda_L - \lambda_{\infty}| / \lambda_{\infty}",
      legend_pos="south west",
      legend_style="{nodes={scale=0.8, transform shape}}",
    }),
    PlotInc(
      {
        mark_options = "solid, scale = 0.8",
        mark = "*",
        color = colors[1],
      },
      PGFPlotsX.Table(df."Lx", (df."1.1" .- limits[1])/limits[1])
    ),
    PlotInc(
      {
        mark_options = "solid, scale = 0.8",
        mark = "*",
        color = colors[2],
      },
      PGFPlotsX.Table(df."Lx", (df."1.2" .- limits[2])/limits[2])
    ),
    PlotInc(
      {
        mark_options = "solid, scale = 0.8",
        mark = "*",
        color = colors[3],
      },
      PGFPlotsX.Table(df."Lx", (df."1.3" .- limits[3])/limits[3])
    ),
    PlotInc(
      {
        mark_options = "solid, scale = 0.8",
        mark = "*",
        color = colors[4],
      },
      PGFPlotsX.Table(df."Lx", (df."1.4" .- limits[4])/limits[4])
    ),
    PlotInc(
      {
        dashed,
        color = "black",
        "no marks",
      },
      PGFPlotsX.Table(df."Lx", 1E-2 * df."Lx" .^ (-2))
    ),
    Legend([
      L"\alpha=1/1",
      L"\alpha=1/2",
      L"\alpha=1/4",
      L"\alpha=1/8",
      L"\mathcal{O}(L^{-2})"
    ]),
  )
  pgfsave("perturbed-asymptotics.tex", p; include_preamble=false)

  return p
end

function plot_its(df)
  colors = [
    RGB(0, 0.4470, 0.7410),
    RGB(0.8500, 0.3250, 0.0980),
    RGB(0.9290, 0.6940, 0.1250),
    RGB(0.4940, 0.1840, 0.5560),
    RGB(0.4660, 0.6740, 0.1880),
    RGB(0.3010, 0.7450, 0.9330),
    RGB(0.6350, 0.0780, 0.1840),
  ]

  # \pgfplotscreateplotcyclelist{matlab}{
  #   color1,every mark/.append style={solid},mark=*\\
  #   color2,every mark/.append style={solid},mark=square*\\
  #   color3,every mark/.append style={solid},mark=triangle*\\
  #   color4,every mark/.append style={solid},mark=halfsquare*\\
  #   color5,every mark/.append style={solid},mark=pentagon*\\
  #   color6,every mark/.append style={solid},mark=halfcircle*\\
  #   color7,every mark/.append style={solid,rotate=180},mark=halfdiamond*\\
  #   color1,every mark/.append style={solid},mark=diamond*\\
  #   color2,every mark/.append style={solid},mark=halfsquare right*\\
  #   color3,every mark/.append style={solid},mark=halfsquare left*\\
  # }

  p = @pgf Axis({
    # ymin = 80,
    xmajorgrids,
    ymajorgrids,
    height="6cm",
    xlabel=L"L",
    ylabel=L"\textnormal{LOPCG}_{\sigma} \textnormal{ its.}",
    # title=L"\lambda_L",
    legend_pos="north west",
    legend_style="{nodes={scale=0.8, transform shape}}",
  },
  PlotInc(
    {
      mark_options = "solid, scale = 0.8",
      mark = "*",
      color = colors[1],
    },
    PGFPlotsX.Table(df."Lx", df."iterations_noshift"),
  ),
  PlotInc(
    {
      mark_options = "solid, scale = 0.8",
      mark = "square*",
      color = colors[2],
    },
    PGFPlotsX.Table(df."Lx", df."iterations"),
  ),
    Legend([
      L"\sigma = 0",
      L"\sigma = \sigma_L",
    ]),
  )
  pgfsave("iterations-longrange.tex", p; include_preamble=false)
  return p
end


df = main()
CSV.write("perturbed-potential.csv", df)
plot_perturbed_asymptotics(df)

# plot_cell(df)

# df2 = main2()
# plot_its(df2)










# x = range(-2; stop = 2, length = 4*10)
# y = range(0; stop = 1, length = 10)
# f(x, y) = 1 / (norm([x,y]-[0.5,0.5]) + 1) * exp(-1 * norm([x,y]-[0.5,0.5]))
# @pgf Plot3(
#     {
#         # mesh,
#         # scatter,
#         surf,
#         # samples = 10,
#         # domain = "0:1",
#         "colormap/jet"
#     },
#     # Expression("x * (1-x) * y * (1-y)")
#     Coordinates(x, y, f.(x, y'))
# )
