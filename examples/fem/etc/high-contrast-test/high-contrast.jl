using Gridap
using LinearAlgebra
using SparseArrays
using LinearMaps
using Random
using IterativeSolvers

using ddEigenLab



# try to get some localization and some collapsing fundamental gap
# might be another application of our preconditioner, think about it
# would also be easier to connect to the community
# look at Daniel's GAMM beitrag again

Random.seed!(1236);

Lx = 1
Ly = 1
ρ = 50

domain = (0, Lx, 0, Ly)
partition = (Lx * ρ, Ly * ρ)
model = CartesianDiscreteModel(domain, partition)

labels = get_face_labeling(model)
add_tag_from_tags!(labels, "diri", collect(1:3^2-1))

order = 1
reffe = ReferenceFE(lagrangian, Float64, order)
V0 = TestFESpace(model, reffe; conformity=:H1, dirichlet_tags = ["diri"])

Ug = TrialFESpace(V0, 0)

degree = 2
Ω = Triangulation(model)
dΩ = Measure(Ω,degree)

b(v) = ∫( 1 * v ) * dΩ  # dummy



reffe₁ = ReferenceFE(lagrangian, TensorValue{2, 2, Float64, 4}, 0)
V₁ = FESpace(model, reffe₁)
Ah = interpolate_everywhere(x->TensorValue(1+0.0*rand(1)[1],0,0,1.00+0.00000*rand(1)[1]),V₁)
writevtk(Ω, "high-contrast-Ah",cellfields=["Ah"=>Ah])

reffe_pot = ReferenceFE(lagrangian, Float64, 0)
V_pot = FESpace(model, reffe_pot)
poth = interpolate_everywhere(x->0+10000*rand(1)[1],V_pot)
writevtk(Ω, "high-contrast-poth",cellfields=["poth"=>poth])

a1(u,v) = ∫( Ah ⋅ ∇(v) ⋅ ∇(u) + poth * v * u ) * dΩ
op1 = AffineFEOperator(a1, b, Ug, V0)
stiffnessmat = assemble_matrix(a1, Ug, V0)

a2(u,v) = ∫( v * u ) * dΩ
op2 = AffineFEOperator(a2, b, Ug, V0)
massmat = assemble_matrix(a2, Ug, V0)

# sdecomp = eigen(Array(stiffnessmat),Array(massmat))


struct MatrixPrecond{TD}
  diagonal::TD
end
LinearAlgebra.ldiv!(y, P::MatrixPrecond, x) = y .= P.diagonal \ x
@time res = lobpcg(stiffnessmat, massmat, false, 5, P=MatrixPrecond(factorize(stiffnessmat)), log=true, maxiter=1000, tol=1E-9)
sdecomp = (vectors=res.X, values=res.λ)
@info res.iterations

# eigvec, eigval, _ = LOPCG(stiffnessmat, massmat, (stiffnessmat - 0 * massmat), 1, 1, 1E-10, 1000, 1)
# sdecomp = (vectors=eigvec, values=eigval)

uhs = [FEFunction(op1.trial, sdecomp.vectors[:,i]) for i=1:5];
writevtk(Ω, "high-contrast-sol",cellfields=map(x->"uh$(x[1])"=>x[2], enumerate(uhs)))

@info sdecomp.values
@info "ratio=" sdecomp.values[1]/sdecomp.values[2]
